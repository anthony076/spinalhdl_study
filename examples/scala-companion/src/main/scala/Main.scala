class Foo(xs: Int) {
  private var x: Int = xs

  // 獲取伴生對象中的屬性
  def getY = Foo.y

  // 傳入 class 的實例對象，讓 object 可以存取 class 的屬性
  def accessX = Foo.getX(this)

  // class 調用 object 的方法
  def sayHello = Foo.hello
}

object Foo {
  // object 的屬性和方法，都必須透過 class 存取
  private var y: Int = 20

  // 伴生類中的實例，透過輸入參數傳入
  def getX(cls: Foo) = cls.x

  def hello = {
    println("hello object")
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    var f = new Foo(999)

    println(f.getY) // 20
    println(f.accessX) // 999

    // 錯誤用法，類實例不能直接調用 object 的屬性和方法
    // println(f.hello())

    // 正確用法，object 的屬性和方法，都必須透過 class 存取
    f.sayHello // "hello object"
  }
}
