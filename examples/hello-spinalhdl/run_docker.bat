@echo off

:: =========================
:: Flow

:: check container
::    exist -> start-container
::    not-exist -> check-image

:: check-image
::    exist -> docker-run
::    not-exist -> docker-pull -> docker-run
:: =========================


@REM check container
docker inspect --type=container spinal >null
IF %errorlevel% == 0 (
  GOTO :start-container
) ELSE (
  GOTO :check-image
)

GOTO :eof

:check-image
  echo [run] check-image
  docker inspect --type=image chinghua0731/spinalhdl-dev:icarus >null
  IF %errorlevel% == 0 (
    GOTO :docker-run
  ) ELSE (
    GOTO :docker-pull
  )

:docker-pull
  echo [run] docker-pull
  docker pull chinghua0731/spinalhdl-dev:icarus
  GOTO :docker-run

:docker-run
  echo [run] docker-run
  :: get folder name
  for %%I in (.) do set FolderName=%%~nxI

  cd %~dp0
  set ProjectPath=%~dp0
  
  docker run -v %ProjectPath%:/root/%FolderName% --name spinal -it chinghua0731/spinalhdl-dev:icarus bash
  GOTO :eof

:start-container
  echo [run] start-container
  docker start spinal
  docker attach spinal
  GOTO :eof

:eof