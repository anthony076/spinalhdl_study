## hello-spinalhdl
- Activate VcXsrv
  
- Activate docker
  
  `run_docker.bat`

- build verilog-file
  - `make run`
  - select [1] hello.HelloMain

- run yosys to generate logic-circuit and output blif-file
  
  `make yosys`

- run simulation
  - `make run`，to execute simulation
  - select `[2] hello.HelloSim`
  - `make wave`，to view waveform result
