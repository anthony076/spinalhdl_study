package hello

import spinal.core._
import spinal.sim._
import spinal.core.sim._

import scala.util.Random

object HelloSim {
  def main(args: Array[String]): Unit = {

    SimConfig // 透過 SimConfig 建立模擬環境
      // 產生波形圖
      .withWave
      // 使用 Icarus Verilog 後端進行測試
      .withIVerilog
      // 執行 Simulation，dut 為 MyTopLevel 的實例
      .doSim(new Hello) { dut =>
        dut.clockDomain.forkStimulus(3)

        for (i <- 0 to 20) {
          dut.clockDomain.waitSampling()
          println(dut.counter.toInt)
        }
      }
  }
}
