package hello

import spinal.core._
import spinal.lib._
import spinal.core.sim._

import scala.util.Random

// 定義硬體
class Hello extends Component {
  // 添加 simPublic tag，讓 register 可見
  val counter = Reg(UInt(8 bits)) init (0) simPublic ()
  counter := counter + 1
}

// 入口點，
object HelloMain {
  def main(args: Array[String]): Unit = {
    SpinalVerilog(new Hello)
  }
}
