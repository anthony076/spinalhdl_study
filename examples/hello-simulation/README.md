- 完整代碼，examples/hello-simulation

- 進入 docker 環境
  - docker run -v C:\path\to\projectname:/root/projectname --it chinghua0731/spinalhdl-dev bash
  - cd projectname

- 硬體代碼
  ```scala
  // top.scala
  // 定義硬體
  class MyTopLevel extends Component {
    // 添加 simPublic tag，讓 register 可見
    val counter = Reg(UInt(8 bits)) init (0) simPublic ()
    counter := counter + 1
  }

  // 入口點，
  object Hello {
    def main(args: Array[String]): Unit = {
      SpinalVerilog(new MyTopLevel)
    }
  }
  ```

- 測試代碼
  ```scala
  // test_top.scala
  object HelloSim {
    def main(args: Array[String]): Unit = {

      SimConfig // 透過 SimConfig 建立模擬環境
        // 產生波形圖
        .withWave
        // 使用 Icarus Verilog 後端進行測試
        .withIVerilog
        // 執行 Simulation，dut 為 MyTopLevel 的實例
        .doSim(new MyTopLevel) { dut =>
          dut.clockDomain.forkStimulus(3)

          for (i <- 0 to 20) {
            dut.clockDomain.waitSampling()
            println(dut.counter.toInt)
          }
        }
    }
  }

  ```

- 執行並檢視波形
  - sbt run (或 ./run.sh)
  - 選擇 mylib.HelloSim
  - gtkwave simWorkspace/MyTopLevel/test.vcd (或 ./view.sh)
  - 波形結果

    <img src="spinalhdl/hello-simulation-result.png" width=700 height=auto>
