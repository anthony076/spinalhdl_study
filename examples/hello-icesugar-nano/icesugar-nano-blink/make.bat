@echo off

set oss_activate=C:\oss-cad-suite\oss.bat

@REM ==== 啟動 oss 環境 ====
call %oss_activate%

@REM ==== 產生 blif、json 檔 ====
yosys -p "read_verilog blink.v" ^
-p "opt" ^
-p "synth_ice40" ^
-p "write_blif blink.blif" ^
-p "write_json blink.json"

@REM ==== 產生 asc 檔 ====
:: icesugar-nano 使用 iCE40-LP1K-CM36 的晶片
nextpnr-ice40 --lp1k --package cm36 --freq 48 --json blink.json --pcf io.pcf --asc blink.asc 

@REM ==== 產生 bin 檔 ====
icepack blink.asc blink.bin

@REM ==== 兩種燒錄方式 ====
:: USB 連接後，會有兩個裝置有效，iCELink(D:) 和 DAPLink-CMSIS-DAP

:: 方法1，透過 iCELink，直接複製到 硬碟
:: 注意要透過 icelink 燒綠，必須 iCELink(D:) 裝置是有效的
::copy blink.bin d:\

:: 方法2，透過 icesprog
:: 注意，要透過 icesprog 燒綠，必須 DAPLink-CMSIS-DAP 裝置是有效的
icesprog blink.bin

@REM ==== clean ====
IF /i "%~1"=="/C" (
  del /F /S /Q blink.asc blink.bin blink.blif abc.history blink.json 
)
