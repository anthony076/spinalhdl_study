// ==== 範例 LED Blink ====
module switch(  input CLK,
                output LED
                );
      
   reg [25:0] counter;

   assign LED = ~counter[23];

   initial begin
      counter = 0;
   end

   always @(posedge CLK)
   begin
      counter <= counter + 1;
   end

endmodule

// ==== 範例 LED On ====
// module switch( output LED );
      
//   assign LED = 1;

// endmodule

// ==== 範例 LED chaser ====
