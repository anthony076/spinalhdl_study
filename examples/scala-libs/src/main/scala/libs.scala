package mylibs

class Add() {
  def apply(x: Int, y: Int): Int = {
    x + y
  }
}
