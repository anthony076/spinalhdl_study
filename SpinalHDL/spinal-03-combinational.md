## Combinational 組合邏輯電路
  
- 類似程式語言中的函數，輸出結果只依賴當前的輸入，
  
  輸出`不依賴前一次的結果`，輸出只會跟目前的輸入成一種`函數關係`，
  因此`不會使用到暫存器(REG)或儲存器(ROM/RAM)`來記憶前次執行的結果

  例如，如半加器、全加器、半減器、全減器、數據多工器、數據分配器、編碼器和解碼器

- 組合訊號的幾種方式
  - 透過 Assignment 建立直接連接
  - 透過 Branch(When/Switch/Mux) 建立選擇器的連接
  - 透過 Rule 建立自定義的邏輯方塊

## 利用 assignment 進行訊號組合 (直接連接)
- 直接賦值的三種方式
  - 方法1，`:=` 標準賦值，等效於 VHDL/Verilog 的 <= 
    - 若有多個 := 賦值語句，只有最後一個賦值語句有效
    - 被賦予的值，會經過`一定的延遲時間`後才會被更新
  
  - 方法2，`\=` 立即賦值，等效於 VHDL 的 := 或 Verilog 的 = 
  
  - 方法3，`<>` 自動連接兩個訊號 或兩個具有相同類型的 bundle
  
- 範例，直接賦值
  ```scala
  class Top extends Component{
    val a,b,c = UInt(4 bits)

    a := 0  // path1
    b := a  // path2
    c := a  // path3
  }
  ```
  
  注意，因為 a 是有初始值的，因此，b、c 會得到初始值0 而不是建立關係
  注意，每一個終點會建立一條關係鍊

  <img src="doc/combination-direct-assignment.png" width=250 height=auto>

- 範例，立刻賦值
  - scala
    ```scala
    class Hello extends Component {
      var x = UInt(4 bits)
      val y, z = UInt(4 bits)

      // path1，0 -> x -> y
      x := 0
      y := x

      // path2，1 -> x_1 -> z
      // 因為 x 已經在 path1 使用過，此處會建立另一個終點 x，稱為 x_1，
      // 實際上，:= 會建立新的實例，並不會覆蓋原來的 x
      x \= x + 1
      z := x

    }
    ```
  - verilog
    ```verilog
    reg        [3:0]    x_1;
    wire       [3:0]    x;
    wire       [3:0]    y;
    wire       [3:0]    z;

    always @(*) begin
      x_1 = x;
      x_1 = (x + 4'b0001);
    end

    assign x = 4'b0000;
    assign y = x;
    assign z = x_1;
    ```
  - result
  
    <img src="doc/combination-immedia-assignment.png" width=500 height=auto>

- 範例，賦值的寬度調整
  - { } 是verilog 的拼接語法，見 [HDL語法](HDL-syntax.md)

  - scala
    ```scala
    class Hello extends Component {
      val x = in UInt (10 bits)
      val y = out UInt (8 bits)
      val z, z1 = out UInt ()

      // 將x調整為與y同寬度
      y := x.resized // y=x[7:0]

      // 指定寬度，取 x 的低四位後，再賦值給 z，
      z := x.resize(4) // z = x[3:0]

      // 輸出的寬度比輸入的寬度多時
      //    在 verilog 中就不是透過 index 賦值，而是透過 {} 的語法，拼接出指定長度
      //    output     [11:0]   z1,
      //    assign z1 = {2'd0, x};，代表高位補兩位的0，加上x的10位，長度共12位
      z1 := x.resize(12)

      val a = in SInt (8 bits)
      val b = out SInt (16 bits)

      // 輸出的寬度比輸入的寬度多時，使用 {} 拼接出指定長度
      // {{8{a[7]}}, a} = { {a[7], a[7], a[7], a[7], a[7], a[7], a[7], a[7], a[7:0]} }
      b := a.resized // assign b = {{8{a[7]}}, a};

    }
    ```
  - verilog
    ```verilog
    module Hello (
      input      [9:0]    x,
      output     [7:0]    y,
      output     [3:0]    z,
      output     [11:0]   z1,
      input      [7:0]    a,
      output     [15:0]   b
    );


      assign y = x[7:0];
      assign z = x[3:0];
      assign z1 = {2'd0, x};
      
      assign b = {{8{a[7]}}, a};
    ```
  - result

    <img src="doc/combination-bit-concate.png" width=500 height=auto>

## 利用 Branch(When/Switch/Mux) 進行組合訊號 (透過選擇器連接)
- 範例，When 範例
  ```scala
  val cond1, cond2 = in Bool ()
  val dout = out UInt (8 bits)

  // 在 verilog 中以 if-else 實現
  when(cond1) {
    // cond1 = True 時執行
    dout := 11
  }.elsewhen(cond2) {
    // (not cond1) and cond2 =1 時執行
    dout := 23
  }.otherwise {
    // (not cond1) and cond2 =1 時執行
    dout := 51
  }
  ```

- 範例，Switch 範例
  ```scala
  val x = in UInt (2 bits)
  val dout = out UInt (8 bits)

  // 在 verilog 中以 case 實現
  switch(x) {
    is(0) {
      dout := 11
    }
    is(1) {
      dout := 23
    }
    default {
      dout := 51
    }
  }
  ```

- 範例，Mux 範例
  ```scala
  val cond = in Bool ()
  val muxOutput, muxOutput2 = out UInt (8 bits)

  // 在 verilg 中，以 cond 進行實作

  // cond = True，選擇 U(33, 8 bits)，否則，選擇 U(51, 8 bits)
  muxOutput := Mux(cond, U(33, 8 bits), U(51, 8 bits))

  // cond = True，選擇 U(22, 8 bits)，否則，選擇 U(49, 8 bits)
  muxOutput2 := cond ? U(22, 8 bits) | U(49, 8 bits)
  ```

- 範例，具有手動選擇功能的 Mux
  ```scala
  val io = new Bundle {
    val src0, src1 = in Bool ()
  }

  val sel = UInt(2 bits)

  // 根據 sel 的值進行輸出的選擇
  val result = sel.mux(
    0 -> (io.src0 & io.src1), // sel = 0
    1 -> (io.src0 | io.src1), // sel = 1
    2 -> (io.src0 ^ io.src1), // sel = 2
    default -> (io.src0) // sel not match
  )
  ```

  <img src="doc/combination-sel-mux.png" width=500 height=auto>

- 範例，`muxList` 或 `subdivideIn`，簡化大量的輸入訊號
  - muxList() 接受可返回 List 的表達式，利用表達式簡化 pattern-match
  - subdivideIn() 是分割 List 的語法糖
  
  - 若要建立以下的輸入，每個輸入具有相似性，例如，將多位輸入進行拆分

    <img src="doc/combination-muxLists-subdivideIn-target.png" width=300 height=auto>

  - 代碼
    ```scala
    val sel = in UInt (2 bits)
    val data = in Bits (128 bits)

    // 使用 muxList 簡化輸入
    val output1 = sel.muxList(
      List(
        (0, data(31 downto 0)),
        (1, data(63 downto 32)),
        (2, data(95 downto 64)),
        (3, data(127 downto 96))
      )
    )

    // 使用 muxList 簡化輸入
    val output2 = sel.muxList(
      for (index <- 0 until 4)
        yield (index, data(index * 32 + 32 - 1 downto index * 32))
    )

    // 使用 subdivideIn 簡化輸入
    val output3 = data.subdivideIn(32 bits)(sel)
    ```

## 透過 Rule 進行訊號 (透過自定義的邏輯規則)
- spinalhdl 會根據支援的邏輯規則，建立符合該規則的功能方塊

- 範例，自定義連接的 Rule
  ```scala
  val inc, clear = Bool()
  val counter = Reg(UInt(8 bits))

  when(inc){
    counter := counter + 1
  }

  when(clear){
    counter := 0
  }
  ```

- 範例，可以透過函數自定義連接的邏輯
  ```scala
  val inc, clear = Bool()
  val counter = Reg(UInt(8 bits))

  def setCounter(value: UInt): Unit = {
    counter := value
  }

  when(inc) {
    setCounter(counter + 1) // Set counter with counter + 1
  }
  when(clear) {
    counter := 0
  }
  ```
