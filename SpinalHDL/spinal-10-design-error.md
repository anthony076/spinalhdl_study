## 常見的設計錯誤
- [設計錯誤](https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Design%20errors/index.html)
  
- Combinatorial loop 組合環錯誤
- Assignment overlap 重複賦值
- Clock crossing violation 時脈交叉違規
- Hierarchy violation 層次存取違規
- Io bundle  io-bundle 錯誤
- Latch detected 數據栓鎖，val 變數 重複賦值
- No driver on
- NullPointerException 存取未初始化的變數
- Register defined as component input 寄存器不能作為輸入
- Scope violation 存取範圍違規
- Unassigned register 寄存器未初始化
- Latch detected 偵測到會產生 latch 的語法
- 對誤報的處理方式

## Combinatorial loop 組合環錯誤
- 定義
  - 起始於某個組合邏輯單元經過一串組合邏輯又回到起始組合邏輯單元的邏輯環路，稱為組合邏輯環
  
  - Combinational loop 如果不是用於特別用途，`預設組合環是不應該出現在代碼中的`，所有不預期的Combination loop 都要當做bug 處理，需清除
    - 可以使用組合邏輯環的例外，偽隨機數生成器，由奇數個反相器組成的回環
    - 可以使用組合邏輯環的例外，DFT Bypass 邏輯，在DFT 模式有組合邏輯環

  - 圖解 Combinatorial loop 

  <img src="doc/design-err-combinational-loop-1.png" width=700 height=auto>

- 解決方式，
  
  硬體描述語言是一個流水線的處理，信號中的數據應該順著流水一級一級傳遞，
  因此，在 spinalhdl 利用變數建立的訊號，一經過賦值或功能模塊確定值之後，就不應該再重新賦值

- 範例，組合環錯誤範例
  ```scala
  class TopLevel extends Component {
    val a = UInt(8 bits) 
    val b = UInt(8 bits) 
    val c = UInt(8 bits)
    val d = UInt(8 bits)

    a := b
    b := c | d
    
    // 錯誤，d 和先前出現的 a 建立了聯繫，造成了回環
    // d := a  
    
    // 正確
    d := 42 

    c := 0
  }
  ```

- 範例，組合環錯誤範例
  - 錯誤代碼
    ```scala
    class Top extends Component {
      val d = B(25, 8 bits)

      when(d === 25) {
        d.setAll() // 錯誤行，d.setAll 會修改 d，對 d 重新賦值
      }

      /* 
        根據 log 追蹤錯誤線
        [error]   Partial chain :
        [error]     >>> (toplevel/d :  Bits[8 bits])    // Line59，d.setAll()，會修改 d 的值，造成組合環錯誤
        [error]     >>> (toplevel/when_MyTopLevel_l27   // Line58，when(d === 25) 的 d 會被換成 when_MyTopLevel_l27
        [error]     >>> (toplevel/d :  Bits[8 bits])    // Line56，對 d 賦值
      */
    }
    ```
  - 修正方式
    ```scala
    class Top extends Component {
      val cond = B"00001111"  
      val d = B(25, 8 bits) 

      when(cond === 0){
        d.setAll()
      }

    }
    ```

- 範例，解除誤報，允許組合環
  ```scala
  // 在某些應用中確實要使用組合環，可以手動解除組合環的檢查和限制
  
  class TopLevel extends Component {
    // 解除組合環的限制
    val a = UInt(8 bits).noCombLoopCheck
    
    // 具有組合環的限制
    // val a = UInt(8 bits)

    a := 0
    a(1) := a(0) 

  }

  ```  

## Latch detected  偵測到會產生 Latch 的語法
- 定義
  - 什麼是 Latch，
    - 資料栓鎖器，由電位觸發，Set = 1，輸出為1，Reset = 1，輸出為0，若 Set/Reset不為1，保持前一次的輸出不變

    - 代碼中的 `條件語句`是由 `Latch` 或 `FF` 實現，
      - Latch，若 `輸入被觸發為1`，則 `變更輸出狀態`
      - if-else，若 `條件成立為1`，則`進行某個操作`

    - spinalhdl 默認不允許出現 latch 的結構，實務上也應該盡量避免使用 latch

      不像FF是利用上下緣觸發，Latch 是由電位觸發，電位觸發容易受到`狀態轉換瞬間的不穩定`而造成`震盪現象`，因此不推薦使用

  - 會產生 latch 的語法原因
    - 條件式語句若`沒有完整個條件語句結構`，就會被綜合為 latch 的結構
      - 例如，if-else 只有 if 沒有 else
      - 例如，when-otherwise 只有 when 沒有 otherwise

    - 範例，完整的條件語句，條件的所有可能狀況都被滿足(true/false)，不會有數據保持不變的狀況
      ```scala
      if (false) {
        操作1
      } else {
        操作2
      }
      ```

    - 範例，不完整的條件語句，條件的所有可能狀況`只有一個都被滿足(true)`，條件不符合時，`資料保持不變`，

      簡單的條件語句，有可能會出現`數據保持不變`的狀況，因此，以 latch 的電路實現

      ```scala
      if (true) 操作1
      ```

- 解決方式，
  - 方法1，完整條件語句的結構
  - 方法2，對要操作的變數進行初始化，避免變數出現未初始化的狀況

    初始化的變數會被宣告為寄存器，寄存器具有存儲功能，且其值在時鐘的邊沿下才會改變，這正是觸發器的特性。
  
  - 方法3，透過 noCombLoopCheck 略過 Latch 的限制

- 參考
  - [什麼是 latch](https://www.runoob.com/w3cnote/verilog-latch.html)
  - [強制生成 latch](https://aijishu.com/a/1060000000252400)

- 範例，方法1，完整條件語句的結構
  ```scala
  class Top extends Component {
    val g = UInt(4 bits)

    /* 錯誤，不完整的 when 語句，會產生 Latch 的硬體結構
      when(U(10) > U(7)) {
        g.setAllTo(True)
      }
    */

    // 完整的 when 語句
    when(U(10) > U(7)) {
      g.setAllTo(True)
    } otherwise {
      g.setAllTo(False)
    }

  }
  ```

- 範例，方法2，對要操作的變數進行初始化，避免變數出現未初始化的狀況
  ```scala
  class Top extends Component {
    // val g = UInt(4 bits)  // 錯誤，我初始化 g，不完整的 when 語句會產生 latch
    val g = U(0, 4 bits)     // 正確，初始化 g，使不完整的 when 語句不會出現未賦值的狀況

    // 不完整的 when 語句
    when(U(10) > U(7)) {
      g.setAllTo(True)  
    }
  }
  ```





- 範例，方法3，透過 noCombLoopCheck 略過 Latch 的限制
  - 在某些場景下仍然需要 Latch 的出現，例如，門控時鐘，可以透過以下方式跳過限制
  ```scala
  case class latchDemo() extends Component{
    val a=in UInt(2 bits)
    val b=out UInt(8 bits)

    // 開放 Combinatorial loop 的限制
    b.noCombLoopCheck

    when(a<1){
      b:=3
    }otherwise{
      b:=b
    }
  } 
  ```

## Assignment overlap
- 定義
  - 未初始化的變數，只允許透過 := 賦值一次，多次調用 := 就會出現重複賦值的錯誤

- 解決方式
  - 方法1，透過 when 再次賦值，when y
  - 方法2，透過 allowOverride 解除 重複賦值的限制
  
- 範例，透過 when 再次賦值
  ```scala
  class Top extends Component {
    val a = UInt(8 bits)
    
    a := 42 // 第一次賦值，進行初始化
    // a := 66 // 錯誤，第一次賦值，重複賦值

    when(True) {
      a := 66
    }

    /*
    reg        [7:0]    a;
    wire                when_MyTopLevel_l12;

    assign when_MyTopLevel_l12 = 1'b1;

    always @(*) begin
      a = 8'h2a;
      if(when_MyTopLevel_l12) begin
        a = 8'h42;
      end
    end
    */
  }
  ```

- 範例，透過 allowOverride 解除 重複賦值的限制
  ```scala
  class Top extends Component {
    val a = UInt(8 bits)
    a := 42 // 初始化

    a.allowOverride // 解除重複賦值的限制

    a := 66 // 正確，重複賦值

    /*
      reg        [7:0]    a;
      wire       [7:0] _zz_1;

      # 用來計算 a 最終值的函數
      function [7:0] zz_a(input dummy);
        begin
          zz_a = 8'h2a;
          zz_a = 8'h42;
        end
      endfunction

      # _zz_1 作為中間變數
      # 調用 zz_a() 計算 a 的最終值
      assign _zz_1 = zz_a(1'b0);

      always @(*) a = _zz_1;

    */

  }
  ```

## Clock crossing violation 時脈交叉違規
- 定義
  - spinalhdl 會檢查設計中的每個 reg，必須都使用同一個時脈或同步的時脈

- 解決方式
  - 方法1，透過 crossClockDomain 告知 spinalhdl 允許使用多種時脈
  - 方法2，透過 setSyncronousWith() 進行時脈同步
  - 方法3，只有時脈接腳和葛雷碼接腳(Gray-coded Bits) 可以接受使用不同的時脈

- 範例，透過 crossClockDomain 告知 spinalhdl 允許使用多種時脈
  ```scala
  class Top extends Component {
    val clkA = ClockDomain.external("clkA")
    val clkB = ClockDomain.external("clkB")

    val regA = clkA(Reg(UInt(8 bits)))

    // 錯誤，使用不同的時脈
    // val regB = clkB(Reg(UInt(8 bits)))

    // 正確，開放允許不同的時脈
    val regB = clkB(Reg(UInt(8 bits))).addTag(crossClockDomain)

    val tmp = regA + regA
    regB := tmp
  }
  ```

- 範例，透過 setSyncronousWith() 進行時脈同步
  ```scala
  class Top extends Component {
    val clkA = ClockDomain.external("clkA")
    val clkB = ClockDomain.external("clkB")

    // 將 CLKB 與 CLKA 進行同步，
    // 若 CLKA = 10Mhz，CLKB = CLKA = 10Mhz
    clkB.setSyncronousWith(clkA)

    val regA = clkA(Reg(UInt(8 bits)))
    val regB = clkB(Reg(UInt(8 bits)))

    val tmp = regA + regA
    regB := tmp
  }
  ```

- 範例，只有時脈接腳和葛雷碼接腳(Gray-coded Bits) 可以接受使用不同的時脈
  ```scala
  class Top extends Component {
    val io = new Bundle {
      val pushClock, pushRst = in Bool ()
      val readPtr = in UInt (8 bits)
    }

    // ClockDomain 建立時域，ClockingArea 建立特殊計時區
    val pushCC = new ClockingArea(ClockDomain(io.pushClock, io.pushRst)) {
      // 建立格雷碼位 (Gray-coded Bits)
      val pushPtrGray = RegNext(toGray(io.readPtr)) init (0)
      // 外部的時域和內部的格雷碼位 會使用不同的時脈
    }
  }
  ```

## Hierarchy violation 層級存取違規
- 定義
  - 預設層級的存取規則
    - 可讀取`當前元件`中，所有定義`不具方向性`的訊號
    - 可讀取`當前元件`中，所有定義`具方向性`的訊號，包含輸入訊號、輸出訊號、雙向訊號
    - 可讀取`子元件`中，所有定義具方向性的訊號，包含輸入訊號、輸出訊號、雙向訊號

    - 可賦值`當前元件`中，所有定義`不具方向性`的訊號
    - 可賦值`當前元件`中，所有定義`輸出`或`雙向`的訊號，但不能賦值輸入訊號
    - 可賦值`子元件`中，所有定義`輸入`或`雙向`的訊號，但不能賦值輸出訊號

- 範例
  ```scala
  class Top extends Component {
    val io = new Bundle {
      // 引發錯誤
      // val a = in UInt (8 bits)

      // 正確，將輸入訊號更改為輸出訊號
      val a = out UInt (8 bits)
    }
    val tmp = U"x42"

    // 錯誤，可賦值`當前元件`中，所有定義`輸出`或`雙向`的訊號，但不能賦值輸入訊號
    io.a := tmp
  }

  object MyTopLevelVerilog {
    def main(args: Array[String]): Unit = {
      SpinalSystemVerilog(new Top)
    }
  }
  ```

## Io bundle  io-bundle 錯誤
- 定義
  - 若 bundle 命名為 io，則 io-bundle 內的所有訊號都需要定義方向，可用方向為 in 輸入、out 輸出、 inout 雙向
  - 若 bundle 命名為 io 以外的名稱，則沒有此限制

- 範例，會被判定為 Io bundle 的範例
  ```scala
  class Top extends Component {
    val io = new Bundle {
      // 錯誤，以 io 為名的 bundle 必須標註方向
      // val a = UInt(8 bits)

      val a = in UInt (8 bits) // 正確，io-bundle 必須標註方向
    }
  }
  ```