## Sequential 時序邏輯電路

- 具有 Reg 或 RAM/ROM 的電路為時序邏輯電路
  
  和組合電路不同，時序邏輯電路輸出，依賴輸入訊號和前次的結果，
  需要`暫存器(Reg)或儲存器(RAM/ROM)`來記憶當前結果，例如，計數器、狀態機

- spinalhdl 會根據 reg 的類型，自動添加 clk 或 reset 的訊號
  - 自動添加 clk
    ```verilog
      reg        [3:0]    reg1;
      reg        [3:0]    reg2;
      
      // 沒有具有 reset-pin 的 reg，只會添加 clk
      always @(posedge clk) begin
        reg2 <= (reg1 + 4'b0001);
      end
    ```

  - 自動添加 clk 和 reset
    ```verilog
    always @(posedge clk or posedge reset) begin
      if(reset) begin
        reg3 <= 4'b0000;
      end else begin
        reg3 <= reg2;
      end
    end
    ```

## Reg 的使用
- 4種 Reg
  - Reg()，建立沒有初始值的 reg
  - RegNext()，連接兩個 reg 的語法糖，
    
    會讀取前一級reg值，需要與前一級reg建立關係
    ```scala
      val something = Bool()
      val value = Reg(Bool())
      value := something

      //等校於
      val something1 = Bool()
      val value1 = RegNext(something)
    ```

  - RegInit()，建立具有 `reset-pin` 可進行初始值的 reg，
    > 當 reset-pin=1，reg的值被初始化為指定值

  - RegNextWhen()，具有 `enable-pin (cond)` 的 reg，
    > 當 enable-pine=1，才會讀取輸入

- 建立具有初始值的reg
  - 只要電路中具有初始值(init)的 reg ，spinalhdl 就會自動添加 reset 訊號

    初始值(init)只有當 reset 觸發後，才會將 reg 的內容設置為初始值
  
    注意，只有 RegInit 預設與reset訊號串聯，其他方法只會建立 reset 訊號但不連接

  - 方法1，透過 RegInit()
  - 方法2，透過 bundle 的 init()
  - 方法3，透過 reg 的 init()

- 範例，建立不同類型的 reg

  <img src="doc/combination-create-reg.png" width=400>
  
  ```scala
    val cond = in Bool ()
    val reg1 = Reg(UInt(4 bit))

    // 讀取前一級的 reg
    val reg2 = RegNext(reg1 + 1)

    // 具有初始化功能的 reg
    //    當 reset = 1，reg3 會初始化為 0000
    //    reset-pin 會在 verilog 中自動添加
    val reg3 = RegInit(U"0000")
    reg3 := reg2
    when(reg2 === 5) {
      reg3 := 0xf
    }

    // 具有輸入致能的 reg
    // 當 cond = 1，才會讀取輸入的 reg3
    val reg4 = RegNextWhen(reg3, cond)
  ```

- 範例，建立具有初始值的reg
  ```scala
  // 建立 Bundle
  case class RGB() extends Bundle {
    val data = UInt(4 bits)
    val r, g, b = UInt(8 bits)
  }

  // 定義硬體
  class Hello extends Component {

    // ==== 方法1，透過RegInit，建立具有初始值 + reset 連接的暫存器 ====
    val reg1 = RegInit(U"0011")

    // ==== 方法2，利用 bundle 中的訊號，建立具有初始值的暫存器 ====
    val reg2 = Reg(RGB())

    // 對 bundle中指定的訊號賦予初始值
    // 注意，自動建立的 reset 訊號，並沒有與 reg2 相連接
    reg2.data init (0xf)

    // ==== 方法3，透過 Reg 的 init()，建立具有初始值的暫存器 ====
    // 注意，自動建立的 reset 訊號，並沒有與 reg3 相連接
    val reg3 = Reg(UInt(4 bit)) init (0xf)
  }
  ```
  
## RAM/ROM 的使用
- 透過 Mem class 建立RAM/ROM，
  - RAM的初始內容為空，只需要指定長度
    ```scala
    // 建立 資料寬度為 32bit，總共 256個儲存位置的 RAM，需要 8bit的定址 (2^8=256)
    val mem = Mem(Bits(32 bits), wordCount = 256)
    ```
    
  - ROM需要指定初始值
    ```scala
    // 用來產生 ROM 內容的函數
    def sinTable = for(sampleIndex <- 0 until sampleCount) yield {
      val sinValue = Math.sin(2 * Math.PI * sampleIndex / sampleCount)
      S((sinValue * ((1<<resolutionWidth)/2-1)).toInt,resolutionWidth bits)
    }
    
    // 透過 Mem 的 initialContent 參數，指定 rom 的內容
    val rom =  Mem(SInt(resolutionWidth bits), initialContent = sinTable)
    ```

- 透過 write/read port 進行訊號的映射 (將mem內部訊號與外部訊號連接)
  - 根據不同的操作模式，選擇對應的映射函數，操作模式可分為
    - 模式1，同步寫，Mem.write()，用於設置 write-port
    - 模式2，同步讀取，Mem.readSync()，用於設置 read-port
    - 模式3，異步讀取，Mem.readAsync()，用於設置 read-port
    - 模式4，讀寫自動推斷，同時設置 write/read port
  
  - 模式1，同步寫入，`Mem.write(address, data，enable, mask)`
    - address，寫入地址
    - data，寫入的數據
    - enable，選用，寫入致能，若未提供，會從調用此函數的條件作用域自動推斷
    - mask，選用，寫入位遮罩，
  
  - 模式2，異步讀取，`Mem.readAsync(address, readUnderWrite)`
    - address，讀取地址
    - readUnderWrite，選用，讀寫衝突策略
      - 用於決定同周期內，同時讀寫同一個地址的策略，
      - 可用值1，dontCare，只執行寫入，忽略讀取值
      - 可用值2，readFirst，先讀後寫，讀取會獲得舊值
      - 可用值3，writeFirst，先寫後讀，

  - 模式3，同步讀取，`Mem.readSync(address, enable, readUnderWrite, clockCrossing)`
    - address，讀取地址
    - enable，選用，讀取致能，若未提供，會從調用此函數的條件作用域自動推斷
    - readUnderWrite，選用，讀寫衝突策略，見模式2
    - clockCrossing，選用，是否使用混合時脈，若是，會添加 crossClockDomain-tag
  
  - 模式4，自動推斷讀寫，`mem.readWriteSync(address, data, enable, write, mask, readUnderWrite clockCrossing)`
    - 當 enable & write = 1 時，推斷為寫入
    - 當 enable = 1 時，推斷為讀取
    - 參數見以上模式

  - `注意`，verilog `不支援 writeFirst` 的讀寫衝突策略
    
    VHDL/Verilog 始終處於 readFirst 的讀寫衝突策略，可兼容於 spinalhdl 的 dontCare，
    但不兼容於 spinalhdl 的 writeFirst，若要使用 writeFirst ，需要使用[自動內存黑盒](#自動內存黑盒-automatic-memory-blackboxing)

- 範例，建立 memory，以 `同步寫/同步讀` 為例
  ```scala
  class Hello extends Component {
    val io = new Bundle {
      val writeValid = in Bool ()
      val writeAddress = in UInt (8 bits)
      val writeData = in Bits (32 bits)
      val readValid = in Bool ()
      val readAddress = in UInt (8 bits)
      val readData = out Bits (32 bits)
    }

    // 建立 mem 實例
    // 256個儲存空間，每個儲存空間 32bit，address 需要 8bit 定址
    val mem = Mem(Bits(32 bits), wordCount = 256)

    // 同步寫，映射 write-port 的接口
    mem.write(
      enable = io.writeValid,
      address = io.writeAddress,
      data = io.writeData
    )

    // 同步讀，映射 read-port 的接口
    io.readData := mem.readSync(
      enable = io.readValid,
      address = io.readAddress
    )
  }
  ```

## 自動內存黑盒 Automatic-Memory-Blackboxing
- 使用時機
  
  因為 Verilog 內建的 ram 不支援 writeFirst 的讀寫衝突策略，
  因此，SpinalHDL 集成了一個可選的自動黑箱系統，此系統會查看設計中的所有內存，並用 blockbox 替換，
  然後，生成的代碼將依靠第三方的IP，才能使verilog支援 writeFirst 的讀寫衝突策略

- 啟動自動內存黑盒
  - 透過 SpinalConfig 的 `addStandardMemBlackboxing(黑盒策略)` 啟動

  - 可用的黑盒策略
    - 可用值1，blackboxAll
      > 黑盒所有的 momory，遇到不可黑盒的 memory 會拋出錯誤
    - 可用值2，blackboxAllWhatsYouCan
      > 將所有可使用黑盒的 memory 都替換為黑盒
    - 可用值3，blackboxRequestedAndUninferable
    - 可用值4，blackboxOnlyIfRequested

  - 啟動範例
    ```scala
    def main(args: Array[String]) {
      SpinalConfig()
        // 啟動自動內存黑盒
        .addStandardMemBlackboxing(blackboxAll)
        .generateVhdl(new TopLevel)
    }
    ```
- 將指定的 memory 替換成黑盒
  ```scala
  import spinal.lib.graphic._

  class Top extends Component{
    val io = new Bundle{
        val writeValid = in Bool()
        val writeAddress = in UInt(8 bits)
        val writeData = in(Rgb(RgbConfig(8,8,8)))
        val readValid = in Bool()
        val readAddress = in UInt(8 bits)
        val readData = out(Rgb(RgbConfig(8,8,8)))
    }
      
    val mem = Mem(Rgb(RgbConfig(8,8,8)),1024)
  
    // 顯式地將 mem 標記為黑盒，記得先啟動黑盒的功能
    mem.generateAsBlackBox()
      
    mem.write(
      enable  = io.writeValid,
      address = io.writeAddress,
      data    = io.writeData
    )

    io.readData := mem.readSync(
      enable  = io.readValid,
      address = io.readAddress
    )
  }
  ```

## 啟用資料寬度的混用
- 啟用後，data 的寬度和 ram 的寬度可以不一致

- 啟用方式，
  - step1，開啟自動內存黑盒的功能，參考，[自動內存黑盒](#自動內存黑盒-automatic-memory-blackboxing)
  - step2，使用 xxxMixedWidth() 取代原來的函數
    - mem.writeMixedWidth() 取代 mem.write()
    - mem.readAsyncMixedWidth() 取代 mem.readAsync()
    - mem.readSyncMixedWidth() 取代 mem.readSync()
    - mem.readWriteSyncMixedWidth() 取代 mem.readWriteSync()

- 範例
  ```scala
  class Hello extends Component {
    val io = new Bundle {
      // data-in 的長度為 2bit，mem 定義的資料長度為 4bit，
      // 需要開啟混合寬度，否則會報錯
      val din = in Bits (2 bits)
      val addrWr = in UInt (3 bits)
      val addrRd = in UInt (3 bits)
      val dout = out Bits (4 bits)
    }

    // 建立 資料長度4bit，8個儲存位置，3bit 定址 (2^3 = 8) 的 RAM
    val ram = Mem(Bits(4 bits), 8)

    // 透過 MixedWidth 才能接受 din 和 mem 使用不同的資料長度
    ram.writeMixedWidth(
      address = io.addrWr,
      data = io.din
    )

    io.dout := ram.readSync(
      address = io.addrRd
    )

  }

  // 入口點，
  object HelloMain {
    def main(args: Array[String]): Unit = {
      // SpinalVerilog(new Hello)

      // 混合寬度的功能 verilog 不支援，
      // 因此需要開啟自動內存黑盒的功能，否則 writeMixedWidth 仍然會報錯
      SpinalConfig()
        .addStandardMemBlackboxing(blackboxAll)
        .generateVerilog(new Hello)
    }
  }
  ```

## Ref
- [自定義黑盒](https://spinalhdl.github.io/SpinalDoc-RTD/dev/SpinalHDL/Structuring/blackbox.html?highlight=blackboxing)

- [Instantiate VHDL and Verilog IP](https://spinalhdl.github.io/SpinalDoc-RTD/dev/SpinalHDL/Structuring/blackbox.html?highlight=blackboxing)