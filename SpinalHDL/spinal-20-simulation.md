## Simulation 概論
- spinalhdl 使用第三方的應用程式作為測試的後端，有以下四種選擇
  - 選擇1，Verilator
    - 優，無論是否是可合成的語法，都可以使用
    - 優，使用 compiled C++ simulation model，模擬的速度快
    - 缺，只接受可合成的 Verilog/SystemVerilog 的代碼
    - 缺，因為需要編譯成c代碼，因此啟動慢，啟動後模擬速度快
    - 缺，VHDL 黑盒測試無法模擬

  - 選擇2，GHDL
    - SpinalHDL v1.4.1 後可用
    - 優，可接受 不可合成的代碼
    - 優，啟動速度較 Verilator 快，但模擬速度慢
    - 缺，只能用於 VHDL 的模擬

  - 選擇3，Icarus Verilog
    - SpinalHDL v1.4.1 後可用
    - 優，可接受 不可合成的代碼
    - 優，啟動速度較 Verilator 快，但模擬速度慢
    - 缺，只能用於 Verilog 的模擬

  - 選擇4，Synopsys VCS
    - SpinalHDL v1.6.5 後可用
    - 優，可用於 Verilog/VHDL/SystemVerilog 的代碼
    - 優，支援 FSDB wave format dump
    - 優，編譯和模擬的速度快
    - 缺，商用軟體，
  
## 設置 Simulation 環境，以 Icarus Verilog 為例
- On Docker，
  - 詳見，docker/Dockerfile-spinalhdl-dev
  - `docker pull chinghua0731/spinalhdl-dev:icarus`
  - `docker run -it chinghua0731/spinalhdl-dev bash`

- On Windowss，不推薦使用
  - Issue
    - 在 vscode 中需要起動 Oss-Cad-Suite 和 MSYS MinGW x64 的 shell 環境
    - MSYS MinGW x64 的 shell 環境，會另開新視窗，脫離 vscode 的開發環境
    - 在 MSYS MinGW x64 的 shell 中無法執行 sbt run
    - Icarus 依賴 Boost 庫，
      - 若沒有 Boost 庫，執行 sbt run 後會出現編譯錯誤
      - windows 下設定 Boost 庫繁瑣

  - 安裝 Icarus Verilog
    
    下載，https://bleyer.org/icarus/

    Spinalhdl 使用 `Icarus` 進行模擬時，會調用 `iverilog-vpi.exe`，
    因此，安裝後，需要確定 `iverilog-vpi --help` 是有效的

  - 安裝 MSYS2
    - 透過 MSYS2 安裝 g++
    - 在模擬的過程中，SpinalHDL 會將 *.v 編譯為 lib 後，才能被 iverilog-vpi.exe 調用
    - 下在 MSYS2，https://www.msys2.org/

  - 安裝 Boost Library
    - 在模擬的過程中，會使用到 boost 庫
    - 下載和安裝，https://www.boost.org/doc/libs/1_78_0/more/getting_started/windows.html

## Simulation 庫的使用
- 在測試代碼中，需要指定 `.withIVerilog`
  ```scala
   SimConfig
      .withWave     
      .withIVerilog // 需要添加此行才會調用 iverilog-vpi.exe，
                    // 若不添加此行，默認使用
      .doSim(new MyTopLevel) { dut =>  
        // 模擬代碼
      }
  ```

- 為 reg 設置仿真用的隨機初始值
  ```scala
  val reg1 = Reg(UInt(4 bit)) randBoot()
  ```

## 範例，hello-simulation
- 完整代碼，examples/hello-simulation

- 進入 docker 環境
  - docker run -v C:\path\to\projectname:/root/projectname --it chinghua0731/spinalhdl-dev bash
  - cd projectname

- 硬體代碼
  ```scala
  // top.scala
  // 定義硬體
  class MyTopLevel extends Component {
    // 添加 simPublic tag，讓 register 可見
    val counter = Reg(UInt(8 bits)) init (0) simPublic ()
    counter := counter + 1
  }

  // 入口點，
  object Hello {
    def main(args: Array[String]): Unit = {
      SpinalVerilog(new MyTopLevel)
    }
  }
  ```

- 測試代碼
  ```scala
  // test_top.scala
  object HelloSim {
    def main(args: Array[String]): Unit = {

      SimConfig // 透過 SimConfig 建立模擬環境
        // 產生波形圖
        .withWave
        // 使用 Icarus Verilog 後端進行測試
        .withIVerilog
        // 執行 Simulation，dut 為 MyTopLevel 的實例
        .doSim(new MyTopLevel) { dut =>
          dut.clockDomain.forkStimulus(3)

          for (i <- 0 to 20) {
            dut.clockDomain.waitSampling()
            println(dut.counter.toInt)
          }
        }
    }
  }

  ```

- 執行並檢視波形
  - sbt run (或 ./run.sh)
  - 選擇 mylib.HelloSim
  - gtkwave simWorkspace/MyTopLevel/test.vcd (或 ./view.sh)
  - 波形結果

    <img src="doc/hello-simulation-result.png" width=700 height=auto>

## Ref
- [Simulation](https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Simulation/index.html#)
