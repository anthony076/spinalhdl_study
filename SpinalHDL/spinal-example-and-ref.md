## Ref
- [spinalhdl](https://spinalhdl.github.io/SpinalDoc-RTD/master/index.html)
- [spinalhdl-bootcamp](https://github.com/jijingg/Spinal-bootcamp)
- [spinalhdl-workshop](https://github.com/SpinalHDL/SpinalWorkshop)
- 官方範例代碼，git clone https://github.com/SpinalHDL/SpinalTemplateSbt.git
- spinalhdl-template，`example/hello-spinalhdl`
  