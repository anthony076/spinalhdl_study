## Spinalhdl 的數據類型
- 基礎，變數是宣告輸入/輸出`訊號`，數據類型是對輸入輸出`訊號值的類型限定`，函數是`功能區塊`的封裝，
  - 功能區塊需要與輸入/輸出訊號進行綁定，
  - `建立數據類型的實例`，是對輸入訊號的綁定，同時也是對輸入值的類型限制
  - `函數的返回值`是功能區塊的結果，需要賦值給某個變數作為輸出訊號

  - 例如，
    ```scala
    var x = Bool(True)  // 定義一個初始化值為 1 的 輸入訊號x
    var y = x.edge()    // x.edge() 會建立用於偵測 x 變化的功能區塊，並將結果輸出給 y
                        // spinalhdl 會將 x.edge() 翻譯為 y =  (x ^ x_previous)
    ```

- spinalhdl 的數據類型，都需要定義在 `class 組件名 extends Component { 程式碼 }` 的程式碼區塊中

- `使用 val 建立變數`，var 會出現非預期結果
  ```scala
    val b1 = Bool()
    b1 := True // 被翻譯為 assign b1 = 1'b1;

    val b2 = False // 被翻譯為 assign b3 = 1'b0;

    var b3 = Bool()
    b3 = True // 錯誤 不會被翻譯為 assign 的語句

    var b4 = False // 被翻譯為 assign b4 = 1'b0;
  ```

- 基礎類型
  - Bool，限定訊號的值為 0 或 1
    - 若 Bool(true)，會將訊號翻譯為 1
    - 若 Bool(false)，會將訊號翻譯為 0

  - BitVector 位向量，建立指定寬度的輸入訊號
    - Bits，一組`可邏輯運算，但不可算術運算`的位向量(bit-vector)
    - UInt，一組`可用於無符號整數運算`的位向量
    - SInt，一組`可用於有符號整數運算`的位向量

- 複合類型
  - Bundle，多組訊號的集合
  - Vec

## Bool，單一 Bool 值，以 False/True 簡寫
- [官方文檔](https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Data%20types/bool.html#)
  
- 需要導入 `import spinal.core.Bool`
  
- Bool，限定訊號的值為 0 或 1 的單一訊號
  
  例如，`var x = Bool(True)`，定義一個初始化值為 1 的 輸入訊號x

- 建立 Bool 實例 
  - 方法1，透過 Bool() 建立實例，輸入參數可接受`返回Boolean值的表達式`
  - 方法2，透過 `False/True` 的工廠函數建立 Bool實例
    - False 會建立新的 Bool 實例，並預設賦值為 false
    - True 會建立新的 Bool 實例，並預設賦值為 true

- 對於 `val` 建立的 Bool實例可以透過 `:=` 多次重新賦值

- 和 scala 內建的 Boolean 不同，`Bool 是 spinalhdl 內建的布林值`，用來表示訊號是高電平和低電平
  - Bool(true) 或 True，代表高電平，會被 spinalhdl 翻譯為 b'1'
  - Bool(false) 或 False，代表低電平，會被 spinalhdl 翻譯為 b'0'

- 範例，建立 Bool 實例
  ```scala
  class Top extends Component{
      // ==== 透過 Bool() 建立實例後再賦值 ====
      val b1 = Bool() // 建立 b1 的輸入訊號，並限制b1的值類型為 0 或 1
      b1 := False
      b1 := True      // 透過 := 為 val 變數重新賦值，可多次賦值
                      // := 實際上不是賦值， := 是 Bool 類定義的函數，利用 := 實現重新賦值的效果
      
      // ==== 利用 False/True 直接建立 Bool 實例
      // False 和 True 實際上是 建立 Bool 實例的語法糖，實際上是建立 Bool 實例的工廠函數
      val b2 = False

      // ==== Bool 的輸入參數為 Boolean 值，可以是表達式 ====
      val b3 = Bool(5 < 12)
  }
  ```

- Bool 相關的功能區塊
  - 圖解

    <img src="doc/edge-fall-rise-in-Bool.png" width=700 height=auto>

  - x.edge()，用於產生一組`偵測輸入訊號x`是否發生變化的功能區塊
    - edge() 會被翻譯為，`level_change = (x ^ x_regNext)`

      從翻譯結果可以看出，spinalhdl 會建立 x_regNext 的reg保存前一次x的值，
      ，level_change 是 x 和 x_regNext 互斥的結果，
      代表 x 的值有變化才會使level_change輸出為 1

    - edge() 的翻譯結果
      ```scala
      class Top extends Component {
        val x = Bool()
        val level_change = x.edge()
        /*
          wire                x;              // 輸入訊號
          reg                 x_regNext;      // 功能區塊中的暫存器
          wire                level_change;   // 輸出訊號

          assign level_change = (x ^ x_regNext);  // 定義功能區塊，定義輸入輸出的關係
          
          always @(posedge clk) begin
            x_regNext <= x;
          end
        */
      }
      ```
  - x.edge(initAt: Bool)，用法和 x.edge()同，但 x.edge() 的輸出訊號會具有初始值
    ```scala
    // level_change 用於偵測 x 的變化，並且將 level_change 初始化為 True
    val level_change = x.edge(True)
    ```

  - x.rise()，用於產生一組`偵測輸入訊號x`是否轉變為`上緣`的功能區塊
    - rise() 會被翻譯為，`rised_edge = (x && (! x_regNext))`

      從翻譯結果可以看出，spinalhdl 會建立 x_regNext 的reg保存前一次x的值，
      只有 x=1 且 x_regNext = 0，rised_edge 才會得到1

    - rise() 的翻譯結果
      ```scala
      class Top extends Component {
        val x = Bool()
        val rised_edge = x.rise()
        /*
          wire                x;          // 輸入訊號
          reg                 x_regNext;  // 功能區塊中的暫存器
          wire                rised_edge; // 輸出訊號

          assign rised_edge = (x && (! x_regNext)); // 定義功能區塊，定義輸入輸出的關係

          always @(posedge clk) begin
            x_regNext <= x;
          end
        */
      }
      ```
  - x.rise(initAt: Bool)，用法和x.rise()同，但 x.rise() 的輸出訊號會具有初始值
    ```scala
    // rised_edge 用於判斷是否產生上緣，並且將 rised_edge 初始化為 True
    val rised_edge = x.edge(True)
    ```

  - x.fall()，用於產生一組`偵測輸入訊號x`是否轉變為`下緣`的功能區塊
    - fall() 會被翻譯為，`fall_edge = ((! x) && x_regNext)`

      從翻譯結果可以看出，spinalhdl 會建立 x_regNext 的reg保存前一次x的值，
      只有 x_regNext = 1 且 x = 0， fall_edge 才會得到1

    - rise() 的翻譯結果
      ```scala
      class Top extends Component {
        val x = Bool()
        val fall_edge = x.fall()
        /*
        wire                x;          // 輸入訊號
        reg                 x_regNext;  // 功能區塊中的暫存器
        wire                fall_edge;  // 輸出訊號

        assign fall_edge = ((! x) && x_regNext);  // 定義功能區塊，定義輸入輸出的關係

        always @(posedge clk) begin
          x_regNext <= x;
        end
        */
      }
      ```
  - x.fall(initAt: Bool)，用法和x.fall()同，但 x.fall() 的輸出訊號會具有初始值
    ```scala
    // fall_edge 用於判斷是否產生上緣，並且將 fall_edge 初始化為 True
    val fall_edge = x.edge(True)
    ```

  - x.edges()，產生一組輸出訊號，分別為
    - 偵測`上緣`的功能區塊，rise = ((! x_regNext) && x)
    - 偵測`下緣`的功能區塊，fall = (x_regNext && (! x))
    - `將x的反向`的功能區塊，toggle = (x_regNext != x)

    - edges() 的翻譯結果
      ```scala
      class Top extends Component {
        var x = Bool()
        var edges = x.edges()
        println(edges)

        /*
        wire                x;            // 輸入訊號 
        wire                edges_rise;   // 輸出訊號
        wire                edges_fall;   // 輸出訊號
        wire                edges_toggle; // 輸出訊號
        reg                 x_regNext;    // 功能區塊中的暫存器

        // 定義功能區塊，定義輸入輸出的關係
        assign edges_rise = ((! x_regNext) && x);
        assign edges_fall = (x_regNext && (! x));
        assign edges_toggle = (x_regNext != x);

        always @(posedge clk) begin
          x_regNext <= x;
        end
        */
      }
      ```

  - x.set() / x.clear()
    - x.set()，將 x 設置為 True
    - x.clear()，將 x 設置為 False
  
## Bits，多個 Bool 值，以 B"" 簡寫
- [官方文檔](https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Data%20types/bits.html#bits)

- 需要導入 import spinal.core.Bits

- 和 Bool 不同，Bool 只能建立一個 Bool 值，`Bits 可以指定多個 Bool 值`
- Bits 用於建立指定寬度的輸入訊號，只能透過 Bool 值 設置為 0 或 1 ，
- Bits 只能做邏輯運算，不能做算術運算
  
- 範例，建立指定的線寬與賦值
  ```scala
  // 建立 一個 32-bit 的輸入訊號
  // 被翻譯為 wire [31:0]  myBits1;
  val myBits1 = Bits(32 bits)

  // 建立 一個 8-bit 的輸入訊號，並賦予指定值為 0d25
  // 被翻譯為 wire [7:0] myBits2; assign myBits2 = 8'h19;
  val myBits2 = B(25, 8 bits)

  // 語法，B"位元數'值"
  // 建立 8bit 的線，並賦予指定值為 0xff
  val myBits3 = B"8'xFF"

  // 建立 8bit 的線，並賦予指定值為 0b1001_0011
  val myBits4 = B"1001_0011"

  // 取 bits 的部分值
  val myBits5 = Bits(8 bits)
  // myBits5(6 downto 2) 等效 myBits5(2 to 6)
  val myBits6 = myBits5(6 downto 2) // 翻譯為 myBits6 = myBits5[6 : 2]

  // 透過 element 設置值，
  //    語法，Bits(位元數, element1, element2, ... )
  //    element 的表示方式，位置 -> 值
  //    例如，(7 downto 5) -> B"101"，bit7-bit5 設置為 101
  //    例如，4 -> true，bit4 設置為 1，可使用 Bool
  //    例如，3 -> True，bit3 設置為 1，可使用 Boolean
  //    例如，default -> false，未指定位置設置為 0
  val myBits7 = B(
    8 bits, // 設定寬度
    (5 to 7) -> B"101", // element1
    4 -> true, // element2
    3 -> True, // element3
    default -> false // element4
  )

  val myBits8 = Bits(4 bits)
  myBits8 := (default -> true)

  // 錯誤用法，需要指定寬度
  // val myBits9 = Bits()
  // myBits9 := (4 bits, (3 downto 0) -> B"0110")
  ```

- 範例，Bits 進行邏輯運算
  ```scala
  class Top extends Component {

    val a, b, c = Bits(32 bits)

    c := ~(a & b) // 翻譯為 assign c = (~ (a & b));

    // 將 x 所有位置做 AND 處理
    val all_a = a.andR // 翻譯為 assign all_a = (&a);

    var bits_8bits = Bits(8 bits)

    val left_shift_10bits = bits_8bits << 2 // 左移兩位，低位補0，8位變10位
    val left_shift_8bits = bits_8bits |<< 2 // 左移兩位，低位捨棄

    // 循環左移
    val rotate_3 = bits_8bits.rotateLeft(3) // left bit rotation

    // Set/clear
    val d = B(25, 8 bits)
    val f = B"00001111"

    when(d === 0) {
      f.setAll()
    }

    when(d =/= 0) {
      f.clearAll()
    }

  }
  ```
  
- 範例，Bits 可以用來與`允許任意值的 bits-constant` 進行比較
  ```scala
  class Top extends Component {
    val myBits = Bits(8 bits)
    
    // - 代表任意值
    val itMatch = myBits === M"00--10--"
  }
  ```

## UInt / SInt
- [官方文檔](https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Data%20types/Int.html)

- 需要導入 import spinal.core.UInt

- 和 Bbits 類似，可以建立`指定寬度`的輸入訊號，且不限定為 Bool 值，可以做邏輯運算或算數運算

- 浮點運算
  - [四捨五入 Round 基礎](https://en.wikipedia.org/wiki/Rounding)
  - [浮點運算](https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Data%20types/Int.html#fixpoint-operations)
  - [Spinal HDL数据类型 - 基本数据类型](https://vuko-wxh.blog.csdn.net/article/details/120970940)

- 範例，建立 UInt 和賦值
  ```scala
  class Top extends Component {
    // ==== 建立實例 ====
    // U(值, 寬度)
    val a = U(10, 4 bits) // assign myUint1 = 4'b1010;

    // U(值)
    val b = U(15) // assign myUint2 = 4'b1111;
    val c = U("0101") // assign myUint3 = 4'b0101;
    val d = U("hA") // assign myUint4 = 4'b1010;

    // U(element)
    val e = U(7 -> true, (0 to 6) -> false) // 以函數的方式實現，得到 e = 10000000

    // ==== 賦值 ====
    // val f = U(8 bits)  // 錯誤用法，使用 U() 必須要輸入值
    val f = UInt(8 bits)
    f := 5
  }
  ```

- 範例，使用 UInt 進行邏輯操作
  ```scala
  class Top extends Component {
    val a, b, c = SInt(32 bits)

    c := ~(a & b) // NOT(AND(a, b))

    val all_1 = a.andR // 將 a 所有的 bit 進行 AND 運算

    val uint_8bits = UInt(8 bits)

    val uint_10bits = uint_8bits << 2 // 左移兩位，低位補0，8位變10位
    val shift_8bits = uint_8bits |<< 2 // 左移兩位，低位捨棄

    val myBits = uint_8bits.rotateLeft(3) // 循環左移

  }
  ```


  

  - 

- 範例，使用 UInt/SInt 進行算術運算
  ```scala
  class Top extends Component {
    // ==== 加法操作 ====
    val x, y = UInt(8 bits)
    val z_ov = x + y // 無進位加法，會產生 overflow 的溢位
    val z_carry = x +^ y // 進位加法，不會產生 overflow
    val z_sat = x +| y // 有輸出上限加法，輸出的位數由 Max(a, b) 決定，高位拋棄

    // ==== 浮點操作 ====
    val a = in SInt (16 bits)

    // 原始值x，符號位s，原始寬度w，浮點位數n，執行 floor(x)，輸出寬度為 w - n + 1
    // 例如，原始值x=-3.21 / 符號位s=-1 / 原始寬度 16bit / 浮點位數n=2 / 執行 floor(-3.21) / 輸出寬度為 w - n + 1 = 8-2+1 = 7
    val b = a.ceil(2) // 無條件進位

    // 原始寬度w，浮點位數n，輸出結果為 w - n
    val c = a.floor(2) // 無條件捨去

  }
  ```

## Bundle，複合類型，多組訊號的集合
- 透過 Bundle 建立的訊號，都會以Bundle作為訊號名的前綴，
  - 可用於建立 model-data-structures、bus、interface
  - 利用 `Bundle名.payload.成員名`存取 bundle 中定義的成員

- 範例，利用 Bundle 類建立
  ```scala
  class Top extends Component {

    val myBundle = new Bundle {
      val trigger = False
      val x = U("h10")
      val y = U("h100")
      val z = UInt(4 bits)
    }

    /*
      透過 Bundle 建立的訊號，都會以Bundle作為訊號名的前綴
      wire                myBundle_trigger;
      wire       [7:0]    myBundle_x;
      wire       [11:0]   myBundle_y;

      assign myBundle_trigger = 1'b0;
      assign myBundle_x = 8'h10;
      assign myBundle_y = 12'h100;
    */

  }
  ```

- 範例，利用 case-class 建立 Bundle
  ```scala
  // 建立 Bundle 構造類
  case class RGB(channelWidth: Int) extends Bundle {
    val red = UInt(channelWidth bit)
    val green = UInt(channelWidth bit)
    val blue = UInt(channelWidth bit)

    def isBlack: Bool = red === 0 && green === 0 && blue === 0
  }

  class Top extends Component {
    // 利用構造器
    val source = slave Stream (RGB(8))
    val sink = master Stream (RGB(8))

    sink <-< source.throwWhen(source.payload.isBlack)
  }
  ```

## Vec，複合類型，可以透過 index 存取的多訊號集合
- vector 可以以 index 存取
- vector 可以使用 for-loop 進行遍歷，也可以使用高階函數 (例如，map)
  
- 範例，建立 vec 實例 + 遍歷
  ```scala
  class Top extends Component {
    // === 建立 vec 實例 ====

    // 建立具有兩個 Sint 訊號的 vector
    val a = Vec(SInt(8 bits), 2)
    // 初始化
    a(0) := 2
    a(1) := a(0) + 3

    // 存取指定元素
    a(1).allowOverride := 3

    // 建立具有不同寬度 UInt 的 vector
    val b = Vec(
      UInt(3 bits),
      UInt(5 bits),
      UInt(8 bits)
    )

    // 先建立 vector 的成員
    val x, y, z = UInt(8 bits)
    // 再建立 vector
    val c = Vec(x, y, z)

    // ==== 遍歷 vector ====
    for (el <- b) {
      el := 3 // 透過遍歷進行初始化
    }

    // 使用高階函數
    c.map(_ := 6) // 透過 map 進行初始化
  }
  ```
  
## 字面構造器，必須提供值，不能只宣告寬度
- B(值)，建立指定值的 Bits，例如，B"1001_0011"
- U(值)，建立指定值的 UInt，例如，U"0000_0101"
- S(值)，建立指定值的 SInt，例如，
- M(值)，建立指定值的 MaskedLiteral，例如，M"00--10--"
- L(值)，建立指定值的 Bits，例如，