## Blackbox 黑盒
- 作用，允許用戶通過Blackbox類，將現有的 VHDL/Verilog 組件集成到設計中

## Ref
- [Instantiate VHDL and Verilog IP](https://spinalhdl.github.io/SpinalDoc-RTD/dev/SpinalHDL/Structuring/blackbox.html?highlight=blackbox)

- [chisel黑盒(調用verilog書寫的模塊)](https://blog.csdn.net/qq_39507748/article/details/118090099)