## spinalhdl 基礎  
- 在專案中，需要在 build.sbt 添加 spinalhdl 的庫

  <img src="doc/add-spinalhdl-in-buildsbt.png" width=700 height=auto>

- 在代碼中，需要導入 spinalhdl 相關的庫
  ```scala 
  import spinal.core._
  import spinal.lib._
  ```

- 注意，spinalhdl 只能透過編譯執行

  spinalhdl 是一個硬體描述語言代碼產生器(generator)，使用 spinalhdl，實際上是`編寫 DSL 的語法`，
  需要在經過`轉換器`的轉換才會得到結果，轉換器已經內建在 spinalhdl 庫中，在編譯期會自動執行，
  因此，spinalhdl `不能透過 REPL 直接執行`，因此 spinalhdl 庫隱藏了轉換器的初始化的工作

  若直接透過 REPL 執行，會因為未執行初始化，而會遇到以下的錯誤
  > `Cannot invoke "spinal.core.GlobalData.toplevel()" because the return value of "spinal.core.Component.globalData()" is null`

- 注意，檢視結果的兩種方式
  - spinalhdl 的函數分為兩種，
    - 一種是`功能區塊函數`，不會被執行，但是會被轉譯，只能在轉譯後查看結果
    - 一種是`普通函數`，會立刻執行，但不會被轉譯，只能在 REPL 察看結果

  - 例如，while-loop，是產生硬體描述語言版本的 while-loop 代碼，而不會執行 while-loop 的結果
    ```scala
    class Top extends Component {
      val x = Bool(true)
      val y = Bool()

      when(x) {
        y.set()
      } otherwise {
        y.clear()
      }
    }

    /* 產生的代碼
      assign x = 1'b1;
      
      always @(*) begin
        if(x) begin
          y = 1'b1;
        end else begin
          y = 1'b0;
        end
      end
    */
    ```

- 注意，spinalhdl 的語句，都需要定義在 `class 組件名 extends Component { 程式碼 }` 的程式碼區塊中

- 產生 Verilog / Vhdl / SystemVerilog 代碼的函數
  - 產生硬體描述語言代碼的函數
    - SpinalVerilog()，產生 Verilog 代碼 (*.v)
    - SpinalVhdl()，產生 Vhdl 代碼 (*.vhd)
    - SpinalSystemVerilog()，產生 SystemVerilog 代碼 (*.sv)

  - 產生 Verilog / Vhdl / SystemVerilog 代碼的函數 只接受 `Componet` 的子類
  
  - 手動配置 + 產生 Verilog / Vhdl / SystemVerilog 代碼
    - 透過 SpinalConfig 類 手動進行配置
      
      <img src="doc/SpinalConfig-parameters.png" width=600 height=auto>

    - 配置後，透過 
      - SpinalConfig.generateVerilog()，產生 Verilog 代碼
      - SpinalConfig.generateVhdl()，產生 Vhdl 代碼
      - SpinalConfig.generateSystemVerilog()，產生 Systemlog 代碼
    
    - 範例
      ```scala
      // 手動配置 Verilog / Vhdl / SystemVerilog
      object MySpinalConfig
          extends SpinalConfig(
            defaultConfigForClockDomains = ClockDomainConfig(resetKind = SYNC)
          )

      // 透過配置產生 Verilog / Vhdl / SystemVerilog 代碼
      MySpinalConfig.generateVerilog(new MyTopLevel)
      ```

## spinalhdl 的編程慣例
- 定義 Bundle 或 Component 時，推薦使用 case-class
  - 可以免用 new 關鍵字
  - 默認開放參數
  - case-class 內建 copy()，可以用於深拷貝

- 類名推薦大寫，函數名使用駝峰命名法S

- 利用 Bundle 建立IO 不是必需的
  ```scala
    class Top extends Component{
      val a = in UInts(8 bits)
      
      // 利用 Bundle 建立IO 不是必需的
      val IO = new Bundle{        //IO bundle is not necessary
        val b = in UInts(8 bits) 
      }
    }
  ```

- `未使用的輸入訊號`不會被刪除或拋出錯誤，但`未使用的輸出訊號`是不允許的
  ```scala
  class Top extends Component{
      // 未使用的輸入訊號會被保留，可以做為 debug 用
      val input_1 = in Bits(8 bits)
      val c1 = input_1
      
      // 禁止未使用的輸出訊號，會拋出錯誤
      val output_1 = out Bits(8 bits)
      val c2 = output_1
  }
  ```

- Spinalhdl `未對語法形式做限制`，io 信號允許出現在組件範圍內的任何位置，

  因此，io 也可以如下面的方式使用
  ```scala
  class WhyNot extends Component{
    val ram = Mem(Bits(4 bit),16)
    out(ram.readWriteSync(in UInt(4 bit),in Bits(4 bit),in Bool(),in Bool()))
    out(ram.readWriteSync(in UInt(4 bit),in Bits(4 bit),in Bool(),in Bool()))
  }
  ```

## 常用設計規則
- 使用 val 建立變數，var 會出現非預期結果
  
- 變數是宣告輸入/輸出`訊號`，函數是`功能區塊`的封裝

  函數的返回值是功能區塊的結果，需要賦值給某個變數作為輸出訊號

  功能區塊需要與輸入/輸出訊號進行綁定，例如，
  ```scala
  var x = Bool(True)  // 定義一個初始化值為 1 的 輸入訊號x
  var y = x.edge()    // x.edge() 會建立用於偵測 x 變化的功能區塊，並將結果輸出給 y
                      // spinalhdl 會將 x.edge() 翻譯為 y =  (x ^ x_previous)
  ```

- spinalhdl `建立的變數不像程式語言的變數可以重複利用`
  
  <img src="doc/operation-for-variable.png" width=700 height=auto>

  - 硬體描述語言是建立`輸入訊號`和`輸出訊號`之間的關係，一旦建立了變數(輸入訊號)後，`對變數(輸入訊號)的操作`，
  都相當於`是將輸入訊號進行某個功能區塊`的處理後，將運算的結果重新綁定到原變數中，
  對變數(輸入訊號)重複賦值，容易造成[組合環的問題](spinal-10-design-error.md)
  
  - 硬體描述語言是一個序列的流水線處理，信號中的數據應該順著流水一級一級傳遞，
  因此，預設狀況下，在 spinalhdl 利用變數建立的訊號，一經過賦值或功能模塊確定值之後，就不應該再重新賦值


- 使用完整的條件語句，不完整的條件語句會產生 Latch 的結構
  
  參考，[Latch detected](spinal-1-0-design-error.md)

