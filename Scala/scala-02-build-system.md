# 建構系統概述
- 建構系統有兩種，sbt 和 mill
- [sbt 的問題](https://www.lihaoyi.com/post/SowhatswrongwithSBT.html)
- [mill 的使用](https://github.com/scalacn/hands-on-scala-zh/blob/master/ch02.md)  

## SBT
- 透過 `sbt new 模板名`，來建立專案
  - 可用的模板，https://www.scala-sbt.org/1.x/docs/sbt-new-and-Templates.html
  - 範例， `sbt new scala/hello-world.g8`
  - 會自動建立以下檔案
    - project目錄，用於安裝和管理專案相關的套件和依賴
    - src目錄，用於放置源碼
    - build.sbt，sbt 用於建構專案的配置檔
  
- build.sbt 的範例
  
  <img src="doc/build/sbt-example.png" width=800 height=auto>

- 添加項目依賴項
  - 方法1
    ```scala
    // 添加项目依赖
    libraryDependencies += "ch.qos.logback" % "logback-core" % "1.0.0"
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.0"
    ```
  - 方法2
    ```scala
    libraryDependencies ++= Seq(
                            "ch.qos.logback" % "logback-core" % "1.0.0",
                            "ch.qos.logback" % "logback-classic" % "1.0.0",
                            ...
                            )
    ```

- 直接指定要執行 main 函數
  - 如果專案中有多個 package，sbt 會詢問要執行哪一個 package 的 main 函數
  - 透過 `sbt "runMain 包名.類名"` 可以跳過詢問，直接指定要執行的 main 函數
  
## mill
- why mill
  - 編譯速度快
  - build.sc 的語法簡單
  - 具有 hot-reload 的功能

- 下載 mill 執行檔
  - on Windows
    - 下載 https://github.com/com-lihaoyi/mill/releases/download/0.10.3/0.10.3-assembly
    - 將 0.10.3-assembly 重新命名為 mill.bat
    - 將 mill.bat 放置自定義的目錄，並添加到環境變數中
  - on ArchLinux，`pacman -S mill`
   
- 查詢可用的命令，`mill resolve __`

- 執行，
  - 語法，`mill 包名.要執行的命令`
    
    例如，在目錄 .\src\main\scala\mylib\MyTopLevel.scala 下，源碼指定的包名為 mylib，
    
    執行命令為，`mill mylib.run`

  - 注意，在 windows 執行要加上 [--no-server](https://com-lihaoyi.github.io/mill/mill/Intro_to_Mill.html#_windows) 的參數
    例如，`mill --no-server mylib.run`

- build.sc 的使用
  - 簡單範例
    
    <img src="doc/build/mill-example.png" width=800 height=auto>

  - 指定 scala 的版本 
    > def scalaVersion = "2.11.12"

  - 添加依賴庫的範例
    > ivy"com.github.spinalhdl::spinalhdl-core:$spinalVersion",
    
  - 多個入口函數時，指定要執行的類
    > def mainClass = Some("mylib.MyTopLevelVerilog")
    
- 推薦的專案結構
  - mill 支持多模塊編譯，因此推薦使用 `模塊-src-檔案` 的結構
    ```
    ├── build.sc
    ├── hello     // 模塊名
    │   └── src
    │       └── Main.scala
    └── README.md
    ```