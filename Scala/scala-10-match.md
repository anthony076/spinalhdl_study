
## 模式匹配概述
- 模式匹配有兩種
  - 用於`函數`的 match-case 語法
  - 用於`賦值兩側`的賦值匹配
  
- 要使用匹配模式，需要對象有實現 `unapply()`
  - 可以使用 內建的 `case-class`，已經實作 unapply() 的對象

  - 或自己在object類中，手動實現 unapply()

  - unapply() 的調用時機
  
    unapply() 會在`模式匹配時自動調用`，並將外部值傳遞給 unapply() 的輸入參數，返回的結果再與未綁定的變數進行匹配 (數量需一致，按照位置匹配)

  - 手動實現 unapply()
    - 需要使用 object 不能使用 class

    - 手動實現匹配就`不限定需要同類型`的實例匹配，但是需要自行在 unaaply() 對不同類型進行手動處理

    - unapply() 的返回類型，需要定義為 Option 類型
      - 未匹配成功時，可以返回 null 值
      - 在匹配成功時，將值包裝為 Some 類型

- [unapply() 的用法](scala-class.md)

## 賦值匹配
- tuple 的匹配
  ```scala
  var (x, y) = (3, 10)
  ```

- array 的匹配
  ```scala
  var Array(x, y) = Array(1,2)
  ```

- 函數返回值的匹配
  ```scala
  var name = () => (1,2,3)
  scala> var (x, y, z) = name() // x=1 y=2 z=3
  ```
- case class 的匹配，注意，未實現 unapply() 的 class 會報錯
  ```scala
  case class Foo (x:Int, y:Int){
    def X = x
    def Y = y
  }

  var Foo(x, y) = new Foo(10, 20)
  ```

- 利用 object 手動實現 unapply()
  ```scala
  object Student {
      def unapply(str: String): Option[(String, String, String)] = {
          val parts = str.split(",")
          if (parts.length == 3) Some(parts(0), parts(1), parts(2)) else None
      }
  }

  // 自定義 unapply() 時，賦值匹配的兩側可以不同類型
  val Student(x, y, z) = "aa,bb,cc"

  println(x)   // aa
  println(y)   // bb
  println(z)   // cc

  ```

## 函數的 match-case 語法
- match 透過 case 的先後次序嘗試每個模式來完成計算，
  
  只要發現有一個匹配的 case，剩下的case不會繼續匹配
  
- 對象的匹配，
  - case 的語法中`建立一個新的對象`，用來與傳入的對象進行比較
  - case 的類定義，必須有實作 `unapply/unapplySeq` 的方法，在比較過後才能卸載建立的資源
  - 實作 unapply/unapplySeq 最快的方法，可以直接使用 `case Class` 定義對象

    詳見，[case Class的使用](scala-class.md)

- 範例，值的匹配
  ```scala
   def matchTest(x: Int): String = x match {
      case 1 => "one"
      case 2 => "two"
      case 3 | 4 => "three or four"
      case _ => "many"
   }

   matchTest(3)   // many
  ```

- 範例，對象的匹配
  ```scala
  case class Person(name:String, x: Int, y:Int){
    def Name :String = name
    def X :Int = x
    def Y :Int = y
  }

  def myMatch(p:Person) = p match {
    // 用於 case 的對象，必須有實作 unapply or unapplySeq 的方法
    // 因此用於 case 的對象，可以使用 case class 宣告，使用一般的 class 會報錯
    case Person("Bob", 10, 20) => println("Hi Bob")
    case Person("Boo", 10, 20) => println("Hi Boo")
    case _ => println("Unknown Person")
  }

  var b1 = new Person("Bob", 10, 20)  
  var b2 = new Person("Bob", 20, 30)  
  var b3 = new Person("Boo", 10, 20)

  myMatch(b1) // Hi Bob
  myMatch(b2) // Unknown Person
  myMatch(b3) // Hi Boo
  ```