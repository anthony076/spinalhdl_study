## 操作符概述
- 操作符是透過`類的方法`實現，
  - Scala裡任何類定義的成員方法都是操作符

    任何的類方法調用都能寫成操作符的形式
    - 去掉句點符號
    - 方法參數只有一個時可以省略圓括號
  
  - 例如，運算操作符其實都是定義在 `class Int`、`class Double`等類裡的成員方法，
    - 算術運算的加減乘除，`1 + 1` 等價於 `1.+(1)`
    - 邏輯運算的與或非，
    - 比較運算的大於小於

- 操作符有以下幾種類型
  - 前綴操作符，透過 `def unary_前綴操作符()` 定義，只有四種前綴操作符可用前綴表達式簡化
  - 中綴操作符，實作的方法`必須含有一個輸入參數`
  - 前置操作符，實作的方法`不能有任何輸入參數`

## 內建的 underscore ( _ ) 操作符
- 用於 import
  
  詳見，[package的使用](scala-package.md)

- 用於賦予變量默認值
  ```scala
  scala> var a: Int = _
  var a: Int = 0  // int 的默認值為 0

  scala> var a: Double = _
  var a: Double = 0.0 // Double 的默認值為 0.0
  ```

- 用於臨時變量
  ```scala
  val (a, _) = (1, 2)
  ```

- 用於指代集合中的每個元素，
  ```scala
  scala> var l = List(1, 2, 3) 
  
  scala> l.foreach { _ => println("Hi") }  // Hi Hi Hi
  scala> l.foreach { println(_) }          // 1 2 3

  scala> l.sortWith(_>_) // List(3, 2, 1)
  ```

- 用於 tuple 中的取值
  ```scala
  scala> var t = ("aa", "bb", "cc")._3
  var t: String = cc
  ```

- 用於簡化匿名函數的參數
  > 注意，限定 _ 只能在函數體中出現一次，因此只能用於 lambda 函數，不能用於一般函數

  ```scala
    scala> val sayHello = (name:String) => "Hello " + name
    sayHello: String => String = $$Lambda$1195/2104281815@4d9ad37e

    scala> val sayHello = "Hello " + (_:String)
    sayHello: String => String = $$Lambda$1196/952756535@26401eda

    scala> val sayHello = "Hello " + _
    sayHello: Any => String = $$Lambda$1204/1132118748@3de507af

    scala> sayHello("Daniel")
    res12: String = Hello Daniel
  ```

- 用於指定任意類型
  ```scala
  def printList(arr: Array[_]): Unit = {
    arr.foreach(elem => print(s"$elem "))
  }

  printList(Array(1, 2, 3)) // 1 2 3
  ```

- 用於 case 的默認匹配
  ```scala
  str match{
      case "1" => println("match 1")
      case _   => println("match default")
  }
  ```

- 用於將 def 方法轉換為函數
  ```scala
  scala> def sum(a: Int, b: Int, c: Int) = a + b + c
  
  // 將 sum 方法轉換為函數後，賦值給 s
  scala> val s = sum _

  scala> s(1, 2, 3) // 6 
  ```

- 用於連結字母和特殊符號，使含特殊符號的函數名/變數名有效
  ```scala
  var a@ = 1  // 錯誤
  var a_@ = 1  // 有效

  def a@() = 1 // 錯誤
  def a_@() = 1 // 有效
  ```

- 取得偏函數
  ```scala
  def sum(a: Int, b: Int, c: Int) = a + b + c 

  // 得到 b(_) = 1 + _ + 3 的偏函數 (新函數)
  val b = sum(1, _:Int, 3) 

  b(2) // 6
  ```


## 自定義`前綴`操作符
- 前置表達式，`前置操作符 a`，
  例如，`-1` 是 `1.unary_-` 的簡化
  > 1 為數值實例對象
  
  > 前綴的 - ，實際上是調用自定義的類方法 unary_-，使用前置表達式時，會去掉 unary_ 只會保留 -

- 前綴操作符也是 `一元操作符`，只需要提供一個實例對象，提供前綴操作符功能的實例

- 定義前綴操作符的方法名
  
  在定義前綴操作符的實作時，方法名必須是 `unary_前置操作符`，用於區別前綴操作符和後綴操作符

  例如，`def unary_前置操作符 = 一些操作`

- 限制，可以寫成前綴表達式的只有以下四種
  - unary_+，簡化為 +a
  - unary_-，簡化為 -a
  - unary_!，簡化為 !a
  - unary_~，簡化為 ~a
  - 其餘的只能寫成 `a.unary_前綴操作符` 的方式調用

- 範例
  ```scala
  // ==== 可以使用前綴表達式簡化 ==== 
  class Foo(x:Int){
    def unary_~ = x + 2
  }

  var f = new Foo(10)
  println(~f) // 12

  // ==== 無法使用前綴表達式簡化 ====
  class Foo(x:Int){
    def unary_* = x * 2
  }

  var f = new Foo(10)
  println(*f) // 錯誤，只能簡化 unary_+、unary_-、unary_!、unary_~
  println(f.unary_*) // 正確
  ```

## 自定義`中綴`操作符  
- 用於簡化`單參數`的`實例方法`調用，省略函數調用的 `.` 和 輸入參數的 `()`

  `實例.方法(輸入參數)` 就可以簡化成 `實例 方法 輸入參數` 的中置表達式

  例如，`1.+(2)` 可以簡化為 `1 + 2`
  > 1 是數值的實例
  
  > 1.+()  ，是調用實例對象1的 + 方法

  > 1.+(2) ，調用實例對象1的 + 方法，並將數值2 傳遞給 + 方法

- 中綴操作符也是 `二元操作符`，需要兩個實例
  - 中置表達式，`a 中置操作符 b`，
  - 第一個實例 a 為左操作數，提供中綴操作符功能的類實例
  - 第二個實例 b 為右操作數，中綴操作符的輸入參數實例
 
  - 限制，中綴操作符若以 `:` 結尾，則左右操作數互換，例如，
    ```scala
    class Five() {
      // 一般用法，類實例 中綴操作符 輸入參數實例
      def /(y: Int) : Float = 5 / y

      // 左右操作符互換(中綴操作符以:結尾)，輸入參數實例 中綴操作符: 類實例
      def /:(y: Int) : Float = y / 5
    }

    var five = new Five()

    // 正確
    println(five / 1)   // 5.0
    // 錯誤，中綴操作符以:結尾時，類實例要放在中綴操作符的右側
    println(five /: 1)  // error
    // 正確
    println( 1 /: five) // 0.0
    ```
  
- 範例
  - 若有 class 定義如下
    ```scala
    class Liquor(name: String) {
      def mix(rho: Liquor): String = {
        this.name + " and " + rho.getName
      }

      def getName(): String = this.name
    }
    ```
  - 一般調用方式
    ```scala
    val gin: Liquor = new Liquor("Gin")
    val tonic: Liquor = new Liquor("Tonic")
    var mixed = gin.mix(tonic)
    println(mixed)
    ```

  - 使用中綴表達式進行簡化
    ```scala
    // 省略方法調用的 . 和 輸入參數的 ()
    println(new Liquor("Gin") mix new Liquor("Tonic"))
    ```

- 範例，object 不需要實例化，可以直接使用中綴表達式
  - 若有 class 定義如下
    ```scala
    object Foo{
      def hello(name:String) : String = s"hello $name" 
    }
    ```
  - 使用中綴表達式進行簡化
    ```scala
    // 省略方法調用的 . 和 輸入參數的 ()
    // 相當於 Foo.hello(an)
    println(Foo hello "an")
    ```

- 範例
  
  scala中萬物都是物件，包含數值，

  1 + 2，實際上是 (1).+(2) 的中綴表達式簡化 
  
  (1) 是避免.被解釋為浮點 (1.)，此處的 . 用於調用



## 自定義`後綴`操作符
- 後置表達式，`a 後置表達式`，

  後置表達式簡化`無參數`的`實例方法`調用，省略函數調用的 `.` 和 `()`

  例如，`foo sayhi` 是 `foo.sayhi()` 的簡化
  > foo 為實例對象
  
  > sayhi 為後置操作符

- 後綴操作符也是 `一元操作符`，只需要提供一個實例對象，提供後綴操作符功能的實例

- 必須導入 `import scala.language.postfixOps` 庫

- 範例
  - 若有 class 定義如下
    ```scala
    class Liquor(name: String) {
      def mix(rho: Liquor): String = {
        this.name + " and " + rho.getName
      }

      def getName(): String = this.name
    }
    ```
  - 一般調用方式
    ```scala
    var tonic = new Liquor
    println(tonic.getName())
    ```

  - 使用後綴表達式進行簡化
    ```scala
    // 省略方法調用的 . 和 輸入參數的 ()
    println(new Liquor("Gin") getName)
    ```


## Ref
- [淺談下劃線的用途](https://www.cnblogs.com/tonychai/p/4546264.html)
- [Scala基礎- 下劃線使用指南](https://blog.51cto.com/zhuxianzhong/5022825)
- [SIP-36前綴操作符](https://docs.scala-lang.org/sips/adding-prefix-types.html)