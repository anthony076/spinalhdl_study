## 安裝 scala 
- on Windows
  - 透過 [Coursier](https://docs.scala-lang.org/getting-started/index.html) 安裝
  
  - Coursier 會自動安裝 scala 需要的軟體

  - 預設路徑
    > C:\Users\使用者名\AppData\Local\Coursier

  - Coursier 同時也是 vscode 插件 Metals 的依賴
    
- on Linux/Docker
  - [docker-hub](https://hub.docker.com/repository/docker/chinghua0731/spinalhdl-dev)

  - 完整代碼，[Dockerfile-spinalhdl-dev](../docker/Dockerfile-spinalhdl-dev)

    ```dockerfile
    # dependencies for iverilog
    pacman --noconfirm -S base-devel boost \

    # dependencies for spinalhdl
    pacman --noconfirm -S jdk8-openjdk scala sbt \
    pacman --noconfirm --disable-download-timeout -S iverilog yosys \

    # dependencies for gtkwave
    pacman --noconfirm --disable-download-timeout -S ttf-inconsolata \
    # 產生 machine-id
    dbus-uuidgen > /var/lib/dbus/machine-id \ 
    # 安裝 gtkwave，wave(*.vcd) viewer
    pacman --noconfirm --disable-download-timeout -S gtkwave \
    ```

## IDE，VSCode
- 推薦套件
  - scalameta.metals
  - dragos.scala-lsp
  - scala-lang.scala

## 執行
- 編寫 build.sbt，添加專案需要的依賴
- 執行 sbt run