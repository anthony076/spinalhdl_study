## Class 概述
- class 是 object 的抽象，class 是建立 object 的藍圖，
  - class 不占用內存，透過 class 產生的 object 是具體的實例，會占用內存
  - 透過對 class 使用 new 關鍵字來建立 object

- 定義類，因為沒有靜態成員，改用 object 建立單例對象
  ```scala
  class Color(r:Float, g:Float, b:Float){
    def getGrayLevel() : Float = r * 0.1f + g * 0.2f + b * 0.3f
  }

  scala> var c = new Color(1.1, 1.2, 1.3)
  var c: Color = Color@5e752a2a

  scala> c.getGrayLevel()
  val res12: Float = 0.74
  ```

- `def get = 123` 和 `def get() = 123` 的區別
  - def get = 123 代表`不需要參數`，調用時不需要加上 ()
  - def get() = 123 代表`需要一個 null 參數`，調用時需要加上 ()

## 功能，建構區塊 (作用同建構函數)
- `構造區塊`取代構造函數
  
  scala 沒有特定的構造函數，除了定義函數的區域外，都是用於構造屬性的區塊，
  ```scala
  class Foo {
    var x = 0
    def name = "foo"
    var y = 10
  }

  var f = new Foo()
  f.x // 0
  f.y // 10
  ```

 - 自定義構造函數 (輔助構造函數)
   - 建構區塊是主要的構造函數
   - 內建的 this()，是輔助的構造函數，是工廠函數的一種
   - 輔助建構式不能呼叫父類別建構式的，主要構造函數可以
   - 範例
    ```scala
    class Foo(xs:Int, ys:Int) {
      var x = xs
      var y = ys
      var z = 30

      // 建立 Foo 的建構工廠函數，簽名一致時才會調用
      def this(xs:Int, ys:Int, zs:Int) = {
          this(xs, ys)
          this.z = zs
      }
    }

    var f1 = new Foo(10, 20)
    f1.x // 10
    f1.y // 20
    f1.z // 30

    // 使用自定義的構造函數
    var f2 = new Foo(10, 20, 999)
    f2.z // 999
    ```

## 功能，輸入參數和屬性的可見性
- `輸入參數` 和 `內部屬性` 的可見性
  - 傳入的實參是私有的， 透過 `val` 改為公開
  
  - 構造區塊建立的屬性預設是公開的，透過 `private` 關鍵字改為私有屬性

- 範例
  ```scala
  class Foo (x:Int, y:Int, val z:Int){
    var X = x
  }

  var f = new Foo(10, 20, 30)

  f.x   // 錯誤，輸入參數外部不可見，僅內部可見
  f.y   // 錯誤，輸入參數外部不可見，僅內部可見
  f.X   // 10，輸入參數傳遞給屬性後，外部可見
  f.z   // 30，使用 val 宣告的輸入屬性，外部可見
  ```

- 範例
  ```scala
  // r、g、b 為私有成員，只能內部存取
  class Color(r:Float, g:Float, b:Float){
    def getGrayLevel() : Float = r * 0.1f + g * 0.2f + b * 0.3f
  }

  // r、g、b 為公開成員，可以由外部存取
  class Color(val r:Float, val g:Float, val b:Float){
    def getGrayLevel() : Float = r * 0.1f + g * 0.2f + b * 0.3f
  }

  scala> var c = new Color(1,2,3)
  var c: Color = Color@24a26847

  scala> c.r
  val res16: Float = 1.0
  ```

## 功能，繼承
- 只能繼承一個父類，但可以繼承多個特徵(trait)
  
- 透過 extends 繼承，透過 override 改寫函數的實作
  - 範例，基類無實作的範例 (abstract-class)
    ```scala
    abstract class Base {
      def hello() : String
    }

    class Foo(name:String) extends Base {
      override def hello() : String = "Hello, " + name 
    }

    scala> var f = new Foo("Bob")
    var f: Foo = Foo@3d718248

    scala> f.hello()
    val res0: String = Hello, Bob
    ```

  - 範例，基類有實作的範例
    ```scala
    class Base(name:String){
      def hello():String = "Hello" + name
    }

    class Foo extends Base("Master")

    scala> var f = new Foo()
    var f: Foo = Foo@61fe303d

    scala> f.hello()
    val res19: String = HelloMaster
    ```
- 範例，覆蓋父類的屬性
  ```scala
    // 父類
    class Point(val xc: Int, val yc: Int) {
      var x: Int = xc
      var y: Int = yc
    }

    // 子類，利用父類的屬性
    // 錯誤用法，要利用父類的屬性，需要明確加上 override
    class LocationA(val xc: Int, val yc: Int, val zc :Int) extends Point(xc, yc){
      var x: Int = xc // 錯誤用法，重複建立同名屬性，子類和父類有同名變數
      var y: Int = zc // 錯誤用法，重複建立同名屬性，子類和父類有同名變數
      var z: Int = zc
    }

    // 子類，覆蓋父類的屬性，將輸入參數保留在子類中
    // 正確用法，要利用父類建立屬性，需要顯式加上 override
    class LocationB(override val xc: Int, override val yc: Int, val zc :Int) extends Point(xc, yc){
      var z: Int = zc
    }

    var b = new LocationB(50, 60, 70)
    b.x // 50
    b.y // 60
    b.z // 70
  ```

## 功能，建立 getter/setter
```scala
  case class Person(name:String){
    // 如果在類中定義了一個var類型的字段，
    // 編譯器會隱式地定義一個名為`變量名`的 getter方法，和一個名為`變量名_=`的setter方法。
    private var _name:String = name;

    // getter
    def Name = _name

    // setter，注意，Name_= 是函數名
    def Name_= (newValue:String) = _name = newValue
  }

  var p = new Person("Foo")
  p.Name  // Foo

  p.Name = "Bar" 
  p.Name // Bar

```

## 功能，object 建立單例對象
- scala 沒有 static，改用 object 實現同樣的效果
- object 是單例的，`不能用 new 語法`，new 語法每次會建立一個新的實例
- object 是`不能接受輸入參數的`，因此建立實例時不需要加上()
  
  但可以透過 apply() 接受輸入參數，詳見，[apply() 和 unapply()](#apply-and-unapply)

- object `是單例的`，多次建立同一個 object 時會建立多次引用
- object 可以單獨使用，和 class 一起使用時，object 成為伴生對象
  
- 範例，利用 object 建立單例
  ```scala
  // 定義 object
  object Foo{
    var x = 10
  }

  // 建立 object 實例
  var f1 = Foo  // 建立第1個實例
  f1.x // 10

  var f2 = Foo  // 建立第2個實例 
  f2.x = 50

  f1.x // 50
  ```

## 功能，class + object 伴生對象
- class 用來建立多例的對象，每次調用 new 語法都會產生新的實例
  
- 若需要在 class 中建立單例對象，需要搭配 object 一起使用，
  - 那種既有實例成員又有靜態成員的類的功能
  - 注意，`使用同樣類名`的 class 和 object 才會是伴生類/伴生對象

    class 的類實例，同樣可以`調用不同名的 object 實例，但是不具伴生關係`，object 無法直接存取類實例的屬性，只能透過輸入參數傳入

  - object 稱為伴生對象 (companion-object)
  - class 稱為伴生類 (companion-class)

- 伴生對象和 類實例可以互相存取對方的資訊
  - 原則，所有 object 的屬性和方法，`不能直接透過類實例存取`
    
    所有 object 的屬性和方法對外界都是`私有屬性`，必須`透過 class 存取`，即使不需要存取 class 的屬性

  - 在 class 中，
    - 透過 `類名.屬性名` 或 `類名.函數名()` 存取 object 中的屬性
    - 透過 `this` 將類實例傳遞給 object
  
  - 在 object 中，透過`傳入的 class實例` 存取 class 中的屬性，
    
- 範例，伴生對象存取範例，
  ```scala
  // 以下代碼不能在 REPL 中執行
  // 完整代碼見，examples/companion

  // ==== 建立同名伴生類 ====
  class Foo(xs: Int) {
    private var x: Int = xs

    // 獲取伴生對象中的屬性
    def getY = Foo.y

    // this，傳入 class 的實例對象，讓 object 可以存取 class 的屬性
    def accessX = Foo.getX(this)

    // class 調用 object 的方法
    def sayHello = Foo.hello
  }

  // ==== 建立同名伴生對象 ====
  object Foo {
    // object 的屬性和方法，都必須透過 class 存取
    private var y: Int = 20

    // 伴生類中的實例，透過輸入參數傳入
    def getX(cls: Foo) = cls.x

    def hello = {
      println("hello object")
    }
  }

  object Main {
    def main(args: Array[String]): Unit = {
      var f = new Foo(999)

      println(f.getY) // 20
      println(f.accessX) // 999

      // 錯誤用法，類實例不能直接調用 object 的屬性和方法
      // println(f.hello())

      // 正確用法，object 的屬性和方法，都必須透過 class 存取
      f.sayHello // "hello object"
    }
  }
  ```

## 功能，apply()，類名調用的預設函數 / 構造的工廠函數 (注入輸入參數)
- apply()，類實例寫成函數調用時，預設自動調用的函數
  ```scala
  class Foo(){
    def apply(value:Int) : Int = value + 3 ;
  }

  scala> var f = new Foo()
  var f: Foo = Foo@3294102e

  scala> f(5)
  val res9: Int = 8
  ```

- 伴生對象的 apply()，可以做為工廠函數，省略類實例的 new 關鍵字
  ```scala
  class Foo (val name:String){
    def apply() = {
      println("class.apply() executed")
    }
  }

  object Foo {
    def apply(name:String) : Foo = {
      println("object.apply() executed")
      return new Foo(name)
    }
  }

  var f = Foo("Bar")  // object.apply() executed
  f()                 // class.apply() executed
  ```

- apply() 可以做為構造函數，使不帶輸入參數的 object，改成接受輸入參數
  ```scala
  object Foo {
    def apply(name:String) = {
      println(s"Hi, $name")
    }

    def apply(name:String, age:Int) = {
      println(s"[$name] $age")
    }
  }

  var f1 = Foo("Foo")
  var f2 = Foo("Foo", 20)
  ```

## 功能，unapply()，解構函數 (取出參數)
- apply() 可以做為構造函數，將輸入參數注入到實例中，相反的，
  
  unapply() 可以將輸入類的對象進行解構，取出注入到類的參數

  例如，Foo(1, 2, 3) 執行 unapply() 後，取得 (1, 2, 3)

- 使用場景，用於 `匹配時將類實例的屬性取出` 或 `對象的解構賦值`
  ```scala 
  class Foo (val name:String, val age:Int){}

  object Foo{
    def unapply(f: Foo): Tuple2[String, Int] = (f.name, f.age)
  }

  var f = new Foo("Foo", 20)

  // 解構 f 實例中的屬性
  var result = Foo.unapply(f) // result = (Foo,20)

  var (name, age) = Foo.unapply(f) // result = (Foo,20)
  println(name) // "Foo"
  println(age)  // 20

  ```

- unapply() 會在`賦值`時自動調用
  ```scala
  object Student {
      def unapply(str: String): Option[(String, String, String)] = {
          val parts = str.split(",")
          if (parts.length == 3) Some(parts(0), parts(1), parts(2)) else None
      }
  }

  val Student(number, name, addr) = "B123456,Justin,Kaohsiung"
  ```
- class 和 object 都可以實作 unapply()，但 `推薦使用 object 實現 unapply()`

  - 因為 unapply() 對所有的類實例都應該是同一個，使用 object 不會占用資源

  - 範例，不推薦使用 class 實現 unapply() 的範例
    ```scala
    class Person(var name: String, var age: Int) {
      var x : Int = 123
      def unapply(p: Person): Tuple3[String, Int, Int] = (p.name, p.age, p.x)
    }

    var r = new Person("foo", 10)

    r.unapply(r)  // (foo, 10)

    ```

## 特殊類，case Class 
- case Class 特性
  - 特殊類，會自動生成 apply、unapply、toString、hashCode、equals、copy 等方法，
  - 參數默認為 val 類型，默認使外界可以存取傳入的參數
  - 實例化時，可以使用 new，也可以不使用 new

    不使用 new 時，會自動調用 apply()，case class 的 apply() 會調用 new
    ```scala
    case class Cat(){}

    object Cat{
      def apply() : Cat = new Cat()
    }
    ```
  - 常被用於模式匹配，可快速產生用於匹配的 object
    詳見，[match的使用](scala-condition-loop.md)
  
- 範例，case-class 使用範例
  ```scala
  abstract class Person
  case class Student(name:String, age:Int) extends Person
  case class Teacher(name:String, age:Int) extends Person
  case class Nobody(name:String, age:Int) extends Person

  var p:Person = Student("Anna", 20)

  // case Student( ... ) 會實例化一個 object，並將 p 和 object 進行模式匹配
  p match {
    case Student(name:String, age:Int) => println("Student " + name + ":" + age)
    case Teacher(name:String, age:Int) => println("Teacher " + name + ":" + age)
    case Nobody(name:String, age:Int) => println("Nobody " + name + ":" + age)
  }
  ```

- 範例，case-class 使用範例
  ```scala
  case class Person(name:String, age:Int)

  var ann = new Person("Ann", 20)
  var bob = new Person("Bob", 18)

  var p  = new Person("Bob", 25)

  // case Person 會實例化一個 object，並將 p 和 object 進行模式匹配
  p match {
    case Person("Ann", 20) => println("Hello Ann")
    case Person("Bob", 18) => println("Hello Bob")
    case _ => println("Hello Unknown") 
  }
  ```

- sealed abstrct class，需要確保所有的可能情況都被列出
  - 將基類宣告為 sealed case class
  - 使用 match 時，需要將基類所有的子類列出，否則會報錯
  
  - 範例，正確使用範例
    ```scala
    // 將基類宣告為 sealed abstract class
    sealed abstract class Person

    case class Student(name:String, age:Int) extends Person
    case class Teacher(name:String, age:Int) extends Person
    case class Nobody(name:String, age:Int) extends Person

    object Demo {
      def main(args: Array[String]) : Unit = {
        var p:Person = Student("Anna", 20)

        // 正確 Person 所有的子類都已列出
        p match {
          case Student(name:String, age:Int) => println("Student " + name + ":" + age)
          case Teacher(name:String, age:Int) => println("Teacher " + name + ":" + age)
          case Nobody(name:String, age:Int) => println("Nobody " + name + ":" + age)
        }
      }
    }
    ```

  - 範例，錯誤使用範例
    ```scala
    // 將基類宣告為 sealed abstract class
    sealed abstract class Person

    case class Student(name:String, age:Int) extends Person
    case class Teacher(name:String, age:Int) extends Person
    case class Nobody(name:String, age:Int) extends Person

    object Demo {
      def main(args: Array[String]) : Unit = {
        var p:Person = Student("Anna", 20)

        // 錯誤，沒有把 Person 所有的子類列出
        p match {
          case Student(name:String, age:Int) => println("Student " + name + ":" + age)
        }
      }
    }
    ``` 

## Ref
- [伴生對象詳解](https://juejin.cn/post/6864503527537344525)