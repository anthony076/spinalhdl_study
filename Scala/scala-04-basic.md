
## 語言基礎
- 具有自動類型推斷

- 支援運算符重載，適合 DSL
  
- 萬物皆物件，數值也是物件，以下兩個是等值的
  - 1 + 2 * (3+5)
  - 1.+(2.*(3+5))，詳見，[中綴表達式](scala-expressions.md)
  
- 沒有靜態成員，改用 `object` 宣告為單實例
- 入口 main()，需要定義在對象中
  ```scala
  object TopLevelMain(){
    def main(args: Array[String]){
      ... do something ...
    }
  }
  ```

## 原始碼的檔案位置
  - src 目錄
  
    <img src="doc/build/sbt-src-files.png" width=300 height=auto>
    
    - scala 原始碼，src/main/scala
    - test 原始碼，src/test/scala
    - 資料檔案，src/main/resources 或 src/test/resources
    - 依賴的 jar 檔案，可以放到 lib 目錄下