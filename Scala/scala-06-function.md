
## 函數
- 定義函數的兩種方式
  - 方法1，透過 `val` 定義函數，
    - val 用於宣告常量，詳見 [數據類型](scala-datatype.md)
    - val 可以為匿名函數`賦予函數名`，

  - 方法2，透過 `def` 定義方法
    - 嚴格來說，def 是定義具有函數功能的`方法`成員，不屬於函數
    - 具有函數的功能，常用於定義類的方法成員，`def a(x:Int) : Int = x * 2`
    - 可以存取類實例的其他成員
    - 具有類實例成員的隱式引用 (可以修改類實例的內部成員)
    - 不寫在類中時，實際上是建立`全局物件的方法成員`
      
      scala 所有的東西都是物件，主程式是一個全局的物件

- var/val 定義具名函數 (為匿名函數綁名函數名)
  ```scala
  var add = (x:Int, y:Int) => x + y
  ```

- def 定義函數
  ```scala
  def add(x:Int, y:Int) : Int = {
    return x + y
  }

  // 可以透過順序傳參
  scala> add(1,2)
  val res6: Int = 3
  
  // 可以透過參數名傳參
  scala> add(x=2, y=1)
  val res7: Int = 3
  ```
- 若沒有 return，以最後一個語句的值返回
  ```scala
  // 使用 return 語句
  def add(x:Int, y:Int) : Int = {
    return x + y
  }

  // 不使用 return 語句
  def add(x:Int, y:Int) : Int = {
    x + y
  }

  ```

- 不使用 return，定義函數可以省略函數體
  ```scala
  def add(x:Int, y:Int) : Int = x+y
  ```
  
- 函數的簡化
  - 使用 _ 進行簡化，就不需要 () 定義輸入參數
    ```scala
    def sum = _:Int + _:Int

    ```

### ??? 佔位符，用於函數，代表為函數未實作
```scala
// 返回 Nothing 類型
def testMethod = ???
```

## 偏函數，未取得所有輸入參數的部分函數
```scala
def sum(a: Int, b: Int, c: Int) = a + b + c 

// 得到 b(_) = 1 + _ + 3 的偏函數 (新函數)
val b = sum(1, _:Int, 3) 

b(2) // 6
```

## 尾遞歸標記
- Scala 中的任何遞歸函數都可以使用 @tailrec 註解進行標記
  ```scala
  @tailrec
  def boom(x: Int): Int = {
    ... 遞歸函數 ...
  }
  ```

## 按名稱延遲調用參數
- 通過用=>符號標記參數來推遲對參數的評估
  ```scala
  // x: => Int ，按名稱調用，並且評估被推遲到實際使用參數
  def callByName(x: => Int) = {
    println("x is " + x)
  }
  ```

## 柯里化，將多個參數的函數，轉換為一個函數鏈
- 柯里化使得含數據有隱藏參數的功能，見 [泛型和隱式轉換一節](scala-generic.md)
  ```scala
  def add(x:Int, y:Int) :Int = x + y

  // 柯里化
  def add_curry(x:Int)(y:Int) :Int = x + y
  println(add_curry(1)(2))
  ```