## trait 特徵
- scala 版的強化版 Interface
- trait 中定義的方法，可以選擇是否要實現
- 和 class 的異同
  - 相同，使用 `extends` 和 `with` 將 trait 混入(mixin)子類中，若 extends 已經被占用，就改用 with
    > class Foo extends 特徵A

    > class Foo extends 父類 with 特徵A

    > class Foo extends 父類 with 特徵A with 特徵B with 特徵C 

  - 相同，可以`實現屬性`和`實現方法`， 
  - 相同，trait 也可以有 `構造函數`
  - 相異，class 只能繼承一個，trait 可以多重繼承

- 和 object 的異同
  - 相同，都不能有輸入參數
  - 相同，都不能透過 new 來實例化，object 已經是實例，不需要在實例化
  - 相異，object 是實例的，tarit 介於抽象和實例之間，abstract 是純抽象

- 繼承父類 + 多個 trait 的構造順序
  - 調用父類的構造器；
  - 特徵構造器在父類構造器之後、類構造器之前執行；
  - 特徵`由左到右`，依照宣告的順序依序線性構造；
  - 每個特徵當中，父特徵先被構造；
  - 如果多個特徵共有一個父特徵，父特徵不會被重複構造
  - 所有特徵被構造完畢，子類被構造

- trait 可以混入其他 `trait/單利對象/類`
  - trait Son extend Parent，繼承 `trait/單利對象/類` 的子特徵
  - trait Foo，未繼承任何 `trait/單利對象/類` 的 trait，隱式會自動繼承 AnyRef 類

- trait 混入其他 `trait/單利對象/類` 的限制
  - trait 是 多重繼承的替代，但本質上還是繼承，因此要遵守
   
    繼承對象的層級，必須要大於當前的子類

  - 多重混入時，因為是從左開始混入，左側混入對象的階層最大，越往右側越小，當前的類最小，例如，
    ```scala
    class Foo extend A with B with C with D // A > B > C > D > Foo
    ```


  - 若其中一個混入對象也繼承其他對象，也必須遵守 `比左小比右大`的原則，例如，
    ```scala
    trait B1 extend B2 with B3    // B2 > B3 > B1
    class Foo extend A with B1 with C with D  // A > (B2 > B3 > B1) > C > D

    ```

## Trait 的使用
```scala
// 定義 trait
trait Say {
  // 未實作
  def sayName(name: String): Unit

  // 有實作
  def sayHello(name: String) = println(s"Hello $name")

  // 會被覆寫的實作
  def sayYes() = println("Yes")
}

class Foo extends Say {
  // 實作未實現的方法
  def sayName(name:String) = println(s"$name")
  
  // 覆寫已實現的方法
  override def sayYes() = println("yes")
}

var f = new Foo()

f.sayName("Bob")  // Bob
f.sayHello("Anna")  // Hello Anna
f.sayYes()  // yes
```

## 建立多重混入的便捷方式
- 語法，new 特徵A with 特徵B ... { 定義 }

  ```scala
  // 實際上是以下代碼的簡化
  class AnonymousClass extends Trait1 with Trait2 ... { definition }
  new AnonymousClass
  ```

- 例如
  ```scala
  trait T {
    val tt = "T__T"
  }

  trait X {
    val xx = "X__X"
  }

  val a = new T with X

  a.tt
  a.xx
  ```

## Ref
- [特質的線性化疊加計算](https://blog.csdn.net/qq_34291505/article/details/86773401?spm=1001.2014.3001.5502)