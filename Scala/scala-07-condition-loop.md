## if-else
```scala
if (x < 10) {
  println("x < 10")
} else if (x < 20) {
  println("x < 20")
} else {
  println("x > 20")
}
```

## while-loop

```scala
var index = 0

while (index < times) {
  println(s"index = $index")
  index += 1
}
```

## do-while
```scala
var index = 0

do {
  println(s"index = $index")
  index += 1
} while (index < times)
```

## for-loop
- 範例，透過 `to` 產生 array
  ```scala
  for (index <- 1 to 10) {
    println(s"index = $index")
  }
  ```

- 範例，遍歷 array
  ```scala
  for (index <- Array("Bob", "Anna", "Su")) {
    println(s"index = $index")
  }
  ```

- 範例，array 是引用類型，修改 array 會改變原始值
  ```scala
  def for_loop_array(names:Array[String]) = {
    for (index <- 0 to (names.length -1) ){
      names(index) = names(index) + s"$index"
    }
  }

  var names:Array[String] = Array("Bob", "Anna", "Su")
  for_loop_array(names)

  for(el <- names) println(el)  // Bob0 Anna1 Su2
  ```

## 迭代器 Iterator
- 任何實現迭代器的對象，`都可以用於迴圈語法`
  - 迴圈語法會自動調用迭代器的 `.hasNext()` 判斷是否有下一個元素
  - 迴圈語法會自動調用迭代器的 `.Next()` 返回下一個元素

- 迭代器會內建指針，指向當前的元素
  - 指針之`前`的元素，為`已處理`的元素
  - 指針之`後`的元素，為`未處理`的元素
  - 迭代器提供的操作，`只針對未處理的元素`，
    且迭代器提供的操作，有可能會移動指針的位置，造成執行的結果不一致
  
- 迭代器的使用

  ```scala
  // 建立迭代器
  val it = Iterator(20, 40, 2, 50, 69, 90)

  // 取得長度
  it.length   // 6

  // 找到元素2，並將指針移動到元素2
  // 注意，若找不到目標元素，指針會移動到最後
  scala> it.find( (x)=>x==2 )
  val res17: Option[Int] = Some(2)

  // 取得剩餘元素的長度
  it.length   // 3

  // 是否有下一個元素
  it.hasNext   // true

  // 取回下一個元素，並將指針移動到下一個元素
  it.next()   // 2

  ```
- [Iterator常用的方法](https://www.runoob.com/scala/scala-iterators.html)