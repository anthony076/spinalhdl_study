## 泛型
- 泛型宣告適用於 class、function、trait
  
- 範例，建立泛型類
  ```scala
  // ==== 建立泛型類 ====
  class Stack[A] {
    // Nil 會建立空的 List()
    private var elements: List[A] = Nil

    def peek: A = elements.head

    def push(x: A) = {
      // x :: elements，將 x 插入 elements 的頭部
      elements = x :: elements
    }
    
    def pop(): A = {
      // 取出棧頂元素
      val currentTop = peek
      
      // elements.tail 除了第一個元素外的其他所有元素
      elements = elements.tail

      return currentTop
    }
  }

  val stack = new Stack[Int]
  stack.push(1)
  stack.push(2)
  println(stack.pop())  // 2
  println(stack.pop())  // 1
  ```

## 多型，不同類型之間的取代原則
- 多型的協變和逆變
  - 協變和逆變中的`變`，代表`實際使用的類型`和`宣告的類型`不一致

  - 協變，用`大範圍取代小範圍`，
    - 最安全，不會發生調用問題，
    - 可用成員被限縮

  - 逆變，用`小範圍取代大範圍`，
    - 有可能會有調用問題，只有`基於類型安全`的逆變才是安全的，
    - 逆變可以有效的擴大成員範圍
    - 類型安全
      - 指父類和子類具有同樣的範圍大小
      - 雖然範圍不一樣，但`超出範圍的成員`並`沒有被調用`

- 型變取代的基本原則
  - 原則，不同成員範圍的類型，容易發生調用錯誤的問題
    - 協變，大範圍的類型默認可以取代小範圍的類型
    - 逆變，小範圍的類型必須基於`類型安全`才能取代大範圍
    - 父類的屬性成員必定`小於或等於`子類的數量，父類屬於小範圍類型，子類屬於大範圍類型，

  - 狀況1，協變，子類(大範圍)可以直接取代父類(小範圍)
  - 狀況2，逆變，父類(小範圍)`不能直接`取代子類(大範圍)，必須`基於類型安全`
  - 狀況3，隱式轉換，子類之間可以透過隱式轉換後才能取代

- 型變的發生位置
  - 泛型類的類型宣告
  - 變數的類型宣告
  - 函數輸入參數的類型
  - 返回值的類型
  
- 默認允許的型變類型
  - 泛型類的類型宣告，不變
    - 範例
      ```scala
      // 泛型類
      class Gee[T]

      class P  // 父類
      class S extends P // 子類
      
      // 錯誤，[E007] Type Mismatch Error
      // var gs: Gee[P] = new Gee[S] 

      // 正確，不限
      var gs: Gee[S] = new Gee[S] 
      ```

  - 變數的類型宣告，協變
    ```scala
    class P  // 父類
    class S extends P // 子類
    
    // 正確，協變，子類(大範圍)取代父類(小範圍)
    var p:P = new S
    // 錯誤，逆變，父類(小範圍)取代子類(大範圍)
    var s:S = new P
    ```

  - 頂層函數輸入參數的類型，協變
    ```scala
    class P  // 父類
    class S extends P // 子類
    
    var p = new P
    var s = new S

    // 正確，協變
    def foo(p:P) = p
    foo(s)

    // 錯誤，逆變
    def foo(s:S) = s
    foo(p)
    ```

  - 頂層函數的返回值的類型，協變
    ```scala
    class P  // 父類
    class S extends P // 子類
    
    var p = new P
    var s = new S

    // 正確，協變
    def foo() : P = {
      var s = new S
      return s
    }

    // 錯誤，逆變
    def foo() : S = {
      var p = new P
      return p
    }
    ```

  - 注意，在 class/trait 中，`類型參數`可接受的型變 和` 方法成員`可接受的型變是不一樣
    - 原因

      不像頂層函數，輸入參數傳入的實例是`直接傳遞`給函數的，不會有型變的差異，
      而 `class/trait 定義的型變` 和 `方法成員需要的型變` 是有可能不一樣的

    - 原則
      - 子類可以取代父類，父類不能取代子類
        
        父類擁有更小的成員範圍，若父類沒有子類定義的方法，在調用上就會出問題

      - 非方法調用時，定義為協變可以`接受更多的類型`

      - 在 class/trait 的 `方法成員的輸入參數`使用用`逆變`更安全，

        實例有可能是子類實例，輸入參數有可能是父類實例，有可能造成父類實例調用子類成員的錯誤

    - 狀況1，`類型參數`的類型宣告

      類型參數的類型宣告，是用來宣告泛型類可接受的類型，
      泛型的本質在於使用一個通用的類型符號，可以容納更多種類型，
      在定義 class/trait 的類型參數時，是希望能更容納更多種類型的，

      因此，類型參數可接受的型變`預設是協變`的，用大範圍的子類可以替代小範圍的父類實例，
      在建立實例對象時，就可以用子類實例取代父類

    - 狀況2，`方法成員`的輸入參數類型

      方法成員會調用實例對象的方法，但因為 class/trait 的類型參數是協變的，實際建立的實例有可能是子類實例或父類實例，
      
      當泛型類的實例是子類實例，若`沒有對輸入參數做限制`，則`外部調用`該方法時，傳入的是父類實例，就會造成用父類實例取代子類實例的狀況，容易造成調用錯誤

      ```scala
      class Parent
      class Son extends Parent 

      class Foo[+T] {
        // 錯誤，未對方法的輸入參數做限制，造成傳入的 t 有可能是父類
        def func(t:T) = t
      }

      // 利用泛型類建立了子類實例，f 為子類實例
      var f = Foo[Parent] = new Foo[Son]

      var p = new Parent
      var s = new Son
      f.fun(p)    // 錯誤，子類實例的方法，傳入了父類
      ```

      為了避免`調用了子類實例的方法，但傳入的是父類實例`，造成調用的錯誤，因此，會限制泛型類方法成員的輸入參數`必須是父類的實例`，限制方式是將方法成員的輸入參數定義為`逆變`

    - 狀況3，`返回值`的類型

      在不執行函數調用的實況下，泛型都是希望能夠容納更多類型的，因此，返回值類型也是`協變`的
  
- 範例，泛型的類型取代範例
  ```scala
  class Stack[A] {
    private var elements: List[A] = Nil

    def peek: A = elements.head

    def push(x: A) = {
      elements = x :: elements
    }
    
    def pop(): A = {
      val currentTop = peek
      elements = elements.tail
      return currentTop
    }
  }

  // 父類
  class Fruit
  // 子類1
  class Apple extends Fruit
  // 子類2
  class Banana extends Fruit

  // 允許型變的泛型父類實例
  var stack = new Stack[Fruit]
  // 子類1實例
  var apple = new Apple
  // 子類2實例
  var banana = new Banana

  stack.push(apple)   // 協變，子類取代父類
  stack.push(banana)  // 協變，子類取代父類

  scala> var item1 = stack.pop()
  var item1: Fruit = Banana@1435103b

  scala> var item2 = stack.pop()
  var item2: Fruit = Apple@44126e04
  ```

## 型變註解
- scala 有`預設的型變類型`，若需要更改就需要透過`型變註解`進行更改

- `+ 和 -`，定義`協變/逆變(Variant)`，只能做單一限制
  - 適用於具有`繼承關係`的類型，包含`直接繼承`和`間接繼承`
    ```scala
    class P
    class S1 extends P
    class S2 extends S1

    class Foo[+T]
    
    var p = new P

    // 正確，以直接子類取代父類
    var f: Foo[P] = new Foo[S1]

    // 正確，以間接子類取代父類
    var f: Foo[P] = new Foo[S1]
    ```

  - 不標記型變註解時，編譯器使用默認的型變規則
  - 接受協變，以 +T 表示，代表 T 可以是 T 類型 和 T類型的子類
  - 接受逆變，以 -T 表示，代表 T 可以是 T 類型 和 T類型的父類
  
  - 範例，將默認不允許型變的泛型類，改成可接受協變
    ```scala
      // 泛型類
      // class Gee[T]  ，默認只允許不變 
      // class Gee[+T] ，將不變變更為協變
      class Gee[+T]

      class P  // 父類
      class S1 extends P // 子類
      class S2 extends S1 // 孫類，間接子類
      
      // 正確
      var gs1: Gee[P] = new Gee[S1]
      // 正確
      var gs2: Gee[P] = new Gee[S2] 
    ```




- `>: 和 <:`，定義`上下界(Bounds)`，允許多個限制條件
  - 和協變方向的異同
    - 相同，都是指定接受型變的類型是父類或子類
    - 相異，Bounds 是基於 Variances 之外，額外再添加的型變限制
    - 相異，上界和下界是定義可接受的`類型範圍`，可以添加`多個限制條件`，而 +和- 只能設置一個限制條件

  - 若同時具有上下界，下界在前，上界在後

    ```scala
    def demo[T >: Foo <: Bar]()，接受的類型範圍為 Foo < T < Bar
    ```
  
  - 定義上界，可接受範圍為 `T ~ 上界`，上界 > T，語法為 `T <: 類型名`
    - 上界，表示接受比T更多的成員，子類才能擁有比父類更多的成員，因此

      上界表示接受指定類型的子類，例如， T <: Foo，接受 `Foo` 和 `Foo 的子類` 和 `任何繼承Foo子類的所有子類`

    - 上界範例
      ```scala
      class P 
      class S1 extends P
      class S2 extends S1
      
      // 定義上限
      class Foo[T <: P] (name:T)

      var s1 = new S1
      
      // 錯誤用法，class 是定義上下，但沒有允許協變，
      // 因此，仍然要遵守變型的規則，
      //var f:Foo[P] = new Foo[S]
      
      // 正確用法
      var f = new Foo[P](s1)
      ```

  
  - 定義下界，可接受範圍為 `下界 ~ T`，下界 < T，語法為 `T >: 類型名`
    - 下界，表示接受比T更少成員的類型，父類比子類擁有更少的成員，因此

      T 的範圍 >= 指定類型，表示接受指定類型的父類 (逆變)，

      例如， T >: Foo，接受 `Foo` 和 `Foo 的父類`、`繼承 Foo 父類的所有類`

    - 下界的問題，
      - 下界用於`實例的類型`時，實際上，子類也會通過，因此`無法限制任何實例類型`，
        - 範例
          ```scala
          class Human
          class Person extends Human
          class Son extends Person

          // 模板參數用於實例a的類型
          //    T >: Person，接受 Person 和 Person 的父類
          //    Person 和 Human 可正常編譯，Son 是子類，應該無法通過下界
          def lower[T >: Person](a:T) = println(a)

          lower(new Human)    // PASS
          lower(new Person)   // PASS
          lower(new Son)      // PASS，注意，Son 是 Person 的子類，依然可以通過限制
          ```
        - 原因

          以上為例，下限定義了允許的實例類型，必須是 Person 或 Person 的父類，因此，Human 和 Son 是可以通過編譯的

          但 Son 是 Person 的子類，為何也能通過下限的限制 ?

          T >: Person，代表接受 `Person` 和 `Person的父類` 和 `繼承Person父類的所有類`，
          繼承Person父類的所有類，代表`和Person共用父類的子類`也會通過限制，
          Any是任何類型的父類，任何類型也都是 Any 的子類，因為 Son 和 Person 都共同擁有 Any 類作為父類，因此，Son 也能通過下限的限制

      - 下界用於`泛型類的類型`時，可以過濾子類
        ```scala
        class Parent
        class S1 extends Parent
        class S2 extends S1

        def lower[T >: S1](arr: Array[T]) = println(arr)

        lower(Array(new Parent))  // Pass
        lower(Array(new S1))      // Pass
        lower(Array(new S2))      // Fail
        ```

    - 為什麼需要下界 (下界的使用場景)
      - 應用1，泛型類的類型可以過濾子類
      - 應用2，用於`宣告正確的型變規則`
      - 應用3，若下界的表達式為，B >: A，代表 B 接受 A 或 A 的父類，
        - 在下界的使用場景中，B 常用於方法的類型參數
        - 在下界的使用場景中，A 常用於 class 的類型符號
        - 例如
          ```scala
          // ==== 錯誤用法 ====
          trait List[+A] {
            
            // 錯誤代碼，
            //    trait List[+A] 將 A 設置為協變，
            //    def prepend(elem: A)，class/trait 方法成員的輸入參數預設是逆變的，因此 A 預設是逆變的
            //    trait List[+A] 和 def prepend(elem: A)，兩者定義A的型變方向不同，導致錯誤
            def prepend(elem: A): NonEmptyList[A] = NonEmptyList(elem, this)
          }

          // ==== 正確用法，利用下限將輸入參數定義為逆變 ====
          trait List[+A] {
            // B >: A ，定義下限，B 可以是 A 或 A的父類(逆變)
            def prepend[B >: A](elem: B): NonEmptyList[B] = {
              return NonEmptyList(elem, this)
            }
          }
          ```


  

- 注意，Variances 和 Bounds 不是二選一的選擇，可以同時存在的
  - 範例
    ```scala
    // +A 協變
    trait List[+A] {
      // B >: A ，定義下限 -> 接受父類 -> 逆變
      def prepend[B >: A](elem: B): NonEmptyList[B] = NonEmptyList(elem, this)
    }
    ```
  
  - 範例
    ```scala
    class P 
    class S extends P

    // 定義上限，
    class Foo[T <: P]
    var f:Foo[P] = new Foo[S]  // 錯誤，class 未定義允許協變

    // 定義上下限 + 變型
    class Foo[+T <: P]
    var f:Foo[P] = new Foo[S]  // 正確，class 同時允許協變 + 上下限
    ```

- ~~`>% 和 <%`，定義 `視圖邊界(View-Bounds)`，強化版的上下界~~
  - 注意，scala 3.1.2 中已經取消此語法，可以使用 context-bound 代替

  - `T <% V`，要求必須存在一個 將類型T的實例，轉換到類型V的實例的隱式轉換函數

    因為具有隱式轉換函數的存在，使得兩個不具有繼承關係的類實例可用於同一個泛型類，用於跨越類繼承層次結構

  - 先判斷類型是否符合上下限，若實例不符合上下限時，scala 會判斷是否能夠執行隱式轉換
    - 若是，會自動將輸入的實例進行隱式轉換
    - 若否，報錯
    - 手動建立轉換函數，見 [隱式轉換一節](#隱式轉換)
  
  - 範例，以 implicit 的隱式轉換函數 取代 view-bounds 的語法
    ```scala
    class Parent
    class Son extends Parent
    class Other

    class Foo[T <: Parent] (name:T)

    // other2son 的轉換函數
    implicit def other2son(o:Other) : Son = new Son
    
    // 將沒有繼承關係的 Other實例 轉換為 Son實例
    var s : Son = new Other
    var f = new Foo[Parent](s)
    ```

  - 範例，以 implicit-parameter 取代 view-bounds 的語法
    ```scala
    class Parent
    class Son extends Parent
    class Other

    class Foo[T : Parent] (name:T) {
      
    }

    // other2son 的轉換函數
    implicit def other2son(o:Other) : Son = new Son
    
    // 將沒有繼承關係的 Other實例 轉換為 Son實例
    var s : Son = new Other
    var f = new Foo[Parent](s)
    ```

- `T:M`，定義 `上下文邊界(Context-Bound)`
  - 上下文邊界(Context-Bound) 會使用到`隱式參數`和`隱式變量`的概念，見 [隱式轉換一節](#隱式轉換)

  - 上下文邊界(Context-Bound)中的的概念，
    - [T:M] 實際上是隱式建立 M[T] 泛型類的實例，並透過方法中的隱式參數，將泛型類的實例自動帶入方法的輸入參數中，
      在方法中調用泛型類實例定義的方法

    - 要素1，兩個泛型類，
      - 泛型類1，一個具有實際作用的`功能泛型類M`，功能泛型類M用來保存上下文(context)
      - 泛型類2，一個`限制輸入類型`用的上下文邊界泛型類(Context-Bound-class)
        
        要傳遞給泛型類M的輸入參數，會先傳遞給 Context-Bound-Class 做輸入參數的類型限制後，
        再透過Context-Bound-Class中定義的方法，將輸入參數的實例傳遞給泛型類執行操作

    - 要素2，在 Context-Bound-Class 中的隱式參數

      因為 Context-Bound-Class 只是用來做輸入參數的類型限制用，實際上有作用的是功能泛型類M，
      因此，功能泛型類M會透過`方法的隱式輸入參數傳入`後，在方法中調用功能泛型類M的方法進行實際的操作
    
    - 要素3，提供給隱式參數的隱式變量

      隱式參數需要建立隱式變量，才能將功能泛型類M的實例帶入，
      隱式變量實際上就是建立功能泛型類M的實例，才能與隱式參數定義的符號綁定
      注意，隱式變量需要定義在Context-Bound-Class的外部

  - 語法，`class 類名[T:M]` ，要求 
    - 要求1，M，具有上下文的`泛型類`，
    - 要求2，定義方法的隱式參數
      - def func[T](p:T)(implicit arg:M[T]) = { 函數體 }
      - 代表輸入參數 p 的類型為T，arg 為隱式值，且類型為 M[T]
      - 隱式值代表調用 func(p) 的時候，不需要提供，由 scala 自動帶入
    - 要求3，定義隱式變量

  - 範例
    ```scala
    // 定義輸入參數的類
    class Model(val name:String, val age:Int) {
        println(s"構造對象 {$name,$age}")
    }

    // 定義包含上下文，含有實際操作的泛型類
    class ModelOrdering extends Ordering[Model]{
        override def compare(x: Model, y: Model): Int = {
            if (x.name == y.name) {
                x.age - y.age
            } else if (x.name > y.name) {
                1
            } else {
                -1
            }
        }
    }

    // 定義用來限制 ModelOrdering 輸入實例類型的 Context-Bound-class
    class Fraction[T:Ordering](val a:T, val b:T) {
        // 錯誤，隱式值不能定義在Context-Bound-class的內部
        //implicit val mo:ModelOrdering = new ModelOrdering

        // 將隱式參數綁定外部的隱式變量
        def small(implicit order:Ordering[T]) : T = {
            if (order.compare(a,b) < 0) a else b
        }

    }
    
    // 正確，定義在外部的隱式值，實際上是建立 ModelOrdering 的實例
    implicit val mo:ModelOrdering = new ModelOrdering

    val f = new Fraction[Model](new Model("Shelly",28),new Model("Alice",35))
    println(f.small.name)
    ```

  - 範例，https://blog.csdn.net/BIT_666/article/details/109958187



- `T:ClassTag`，動態類型，可傳入任意類型，不做任何類型限制
  - 需要導入 `scala.reflect.ClassTag`
  - 可傳入任意類型，類型在傳入時才確定
  
  - 範例
    ```scala
    import scala.reflect.ClassTag

    // T* 代表可傳入多個參數
    def makeArr[T:ClassTag](elems: T*) = Array[T](elems: _*)

    makeArr(1, 2, 3)
    makeArr("aa", "bb")
    ```
  
## 多重界定
- 允許 A的父類 或 B的子類
  > T >: A <: B

- 必須有 A[T] 和 B[T] 的隱式值
  > T:A:B

## 隱式轉換
- 隱式轉換函數(implicit-conversion)，用於隱式類型轉換
  ```scala
  // ==== 內建的隱式轉換 ====
  var byte:Byte = 123
  // 正確，scala 內建 Byte 轉 Int 的方法，會自動執行隱式轉換
  var int:Int = byte

  // ==== 錯誤的隱式轉換 ====
  var int:Int = 123

  // 錯誤，scala 未提供 Int 轉 Byte 的方法
  //var byte:Byte = int

  // ==== 手動建立 int -> byte 的轉換函數 ====
  def int2byte(a:Int) = a.toByte

  // 手動調用轉換函數
  var byte:Byte = i2b(int)

  // ==== 建立隱式自動調用的轉換函數 ====
  // 指定輸入類型為 Int，輸出類型 Byte 
  implicit def i2b(x:Int):Byte = x.toByte

  // 建立隱式的轉換函數後，
  // 當輸入類型和輸出類型一致時，就會自動調用隱式的轉換函數
  var byte:Byte = int
  ```

- 隱式類(implicit-class)，隱藏類實例方法
  - 透過隱式類，將指定類的方法隱藏，當指定類的實例調用該方法時，會自動調用隱藏類中定義的方法

  ```scala
  class Person

  // 隱式類，限定用於 Person 類實例
  implicit class PersonAdd(person: Person){
    def hello(name:String):Unit = {
      println(s"hello $name")
    }
  }

  var p = new Person
  p.hello("Peter")

  ```

- 隱藏函數參數和隱藏變量(implicit-parameter)
  - 隱藏函數參數需要搭配 `隱藏變量` 或 使用`隱藏函數參數的預設值` 來獲取隱藏的實例
  - 注意，隱式參數和隱式變量的綁定不是依照函數名，是依照定義的類型

    隱式變量為輸入類型，隱式參數為輸出類型，因此隱式參數名和隱式變量名，兩者可以不一致

  - 範例
    ```scala
    implicit var name:String = "Bob"

    def hello(implicit name:String = "Jenny") = println(s"hello $name")

    hello // hello Bob，使用隱藏變量 name = "Bob"
    hello() // hello Jenny，使用隱藏變量的預設值 name = "Jenny"
    hello("abc") // hello abc，不使用隱藏變量，傳入自定義的值
    ```

  - 範例，
    ```scala
    // 建立隱式變量
    implicit val pwd:String = "12345"

    // 建立隱式參數，
    // 調用方法的差異，會影響調用外部的隱式變量，或是調用隱式參數的預設值
    def login(user:String)(implicit pwd:String = "66" ): Unit = {
      println(s"user=$user, password=$pwd")
    }

    login("Foo")    // 將隱式參數綁定到外部的隱式變量pwd
    login("Foo")()  // 使用隱式參數內建的預設值
    login("Foo")

    ```

## Ref
- [scala泛型和隱式轉換](https://blog.csdn.net/qq_34291505/article/details/87109234?spm=1001.2014.3001.5502)
- [Scala 泛型以及泛型約束](https://www.jianshu.com/p/caca1ba8976e)
- [隱式轉換/隱式類/隱式參數和變量](https://segmentfault.com/a/1190000039306032)
- [context-bound範例](https://www.cnblogs.com/itboys/p/10164234.html)
- [隱式轉換](https://segmentfault.com/a/1190000039306032)