
## 引入第三方庫
- 導入多個指定類
  > import java.util.{Data, Locale}
- 導入指定類
  > import java.text.DataFormat
- 導入所有
  > import java.text.DataFormat._

- 導入所有，但隱藏 Codec 類
  > import scala.io.{Codec => _, _ }

- 導入所有，但將 Codec 類重新命名為 CodecA
  > import scala.io.{Codec => CodecA, _ }

# Package 基礎
- 同一個 package 可以分散在兩個不同的檔案中
  
  <img src="doc/project/p-2-sampe-package-at-diff-file.png" width=250 height=auto>

- 子包中的類，可以訪問父包中的類
  
  <img src="doc/project/p-3-access-parent-package.png" width=500 height=auto>


## 範例集
- 範例，自定義 package
  
  <img src="doc/project/p-1-self-define-package.png" width=800 height=auto>

## Reference
- [package & import](https://www.jianshu.com/p/9d6facd14472)
- [package @ Gossip](https://openhome.cc/Gossip/Scala/Package.html)