## 數據類型
- 類型系統總覽
  - 從 `Any類型` 派生出各種類型，結束於 `Nothing類型`

  - Any 是任何類型的父類，延伸出 AnyVal 和 AnyRef 兩個類型
    - AnyVal 用於原始類型
    - AnyRef 用於複合類型(引用類型)
  
  - Nothing 是任何類型的子類，是各種類型的終點
    - Unit 是 AnyVal的子類，不代表空值，專指`函數無返回內容`，空值和無返回是兩個獨立的類型
    - Null類型 是任何`引用類型`的子類， Null類實例化得到 null，代表空值
    - Nothing 是任何`引用類型`和`值類型`的子類，
      - 没有一个值是Nothing類型的，用於傳遞非正常終止的信號
  
  <img src="doc/datatype/type-system-overview.jpg" width=700 height=auto>

- 宣告變數
  - var 和 val
    - var = variable，宣告變數用，可對變量重複賦值 (可以修改變數名的綁定的數值)
    
    - val = value，宣告常量，只能對常量 `賦值`一次 (不能修改變數名的綁定的數值)

    - 注意，任何`可以取值(取object)的表達式`，都可以使用 val/var 來宣告
      - 方法1，直接賦值，`val a = 123`
      - 方法2，立即取值，`val a = (123 * 123)`
      - 方法3，延後取值，`val a = (x:Int) => x * 2`，不限定純函數，函數本身就是 Object 的一種

        常用於定義匿名函數的函數名，詳見，[函數的使用](scala-function.md)

  - 手動指定類型
    > var 變數名:數據類型 = 值

    > var 變數名 = 值L，指定為 Long
    
    > var 變數名 = 值D，指定為 Double 浮點
  
  - 自動類型推斷，var 變數名 = 值
  
  - 變數名的限制
    - 部分字符無法直接作為變數名，需要利用 `兩個雙引號 (``)` 來略過此限制
    - 例如
      ```scala
      // 錯誤用法
      var @ = 123
      // 正確用法
      var `@` = 123
      ```
  
- 整數
    - Byte，8bit 有符號數值，
    - Short，16bit 有符號數值，
    - Int，32bit 有符號數值
    - Long，64bit 有符號數值

- 浮點數
    - Float，32bit 浮點數
    - Double，64bit 浮點數
  
- 布林，Boolean，`true / false`
- Unit，無值，等同於 `void`，

- 空
  - 空值，`null` 
  - 空集合，`Nil`，是 List[Nothing] 的簡寫
  - 空trait，`Null`
  - 函數無返回值，`Unit`
  - Nothing，`Nothing` 或 `???`


- Array，

  `val scores: Array[Int] = Array(80, 90, 100)`

- 顯示16進制，var h = 0xff
- 顯示2進制，
  - 錯誤用法，var b = 0b1100，不支援 0b 開頭
  - 顯示2進制字串，var b = (3).toBinaryString，

    注意，toBinaryString 是字串，不能進行邏輯運算，只能用於顯示

- 數值進制的轉換
  
  `var dec = BigInt("256", 16)`


## 類型轉換
- 方法1，透過 asInstanceOf
  
  ```scala
  scala> var x = 1
  var x: Int = 1

  scala> x.asInstanceOf[Long]
  val res1: Long = 1
  ```
- 方法1，透過 toXXX()
  ```scala
  scala> var x = 1
  var x: Int = 1

  scala> x.toLong
  val res3: Long = 1

  scala> x.toString
  val res4: String = 1
  ```

## String 的使用
- 定義字串/字元，
  - 單一字元，`'A'`
  - 字串，`"hello"`
  - 多行字串，`""" abc """`
  
- 字串插植
  - 字串插植有三種，
  - 方法1，s 字串插值器，可使用變量
    - 語法，println(s"$變數名")
    - 範例，使用變量
      ```scala
      var name = "an"
      println(s"hello $name")
      ```
    - 範例，使用表達式
      ```scala
      println(s"hello ${123*10}")
      ```
  - 方法2，f 字串插值器，可使用變量 + 格式化變量
    - 語法，println(f"$變數名%格式")
    - 範例，使用變量+格式化
      ```scala
      var name = "an"
      var age = 3.3
      println(s"hello $name%s, age=$age%d")
      ```
  - 方法3，raw 字串插值器，可使用變量 + 對特殊字元不進行處理
    - 語法，println(raw"a\nb")

  - [自定義插值器](https://docs.scala-lang.org/overviews/core/string-interpolation.html#advanced-usage)

## Array / List 的使用
- Array 和 List 的差異
  - List 是以 LinkedList 實現，不會預先分配大小
  
  - Array 是可變的，可以直接透過 index 修改
    ```scala
    scala> var a = Array(1,2,3,4)
    scala> a(0) = 0
    scala> a(0)
    val res0: Int = 0
    ```

    List 是不可變的，需要透過函數進行修改
    ```scala
    scala> var l = List(1,2,3,4)
    var l: List[Int] = List(1, 2, 3, 4)

    scala> l(0) = 0 // 錯誤用法

    scala> l.updated(0, 0) // 正確用法
    val res1: List[Int] = List(0, 2, 3, 4)
    ```
  - 隨機存取值， Array 的速度更快
  - 插入值和刪除值，List 的速度更快
  
- 建立 Array
  - 不指定元素，需要指定類型
    ```scala
    var a: Array[Int] = Array(3) 
    var b: Array[Int] = new Array(3)
    ```
  - 指定元素，可使用自動類型推斷
    ```scala
    var a = Array(1, 2, 3)
    ```

- 存取 Array，需要透過 `()` 存取，不能使用 `[]` 存取
  ```scala
  var a: Array[Int] = Array(3)
  // 錯誤用法
  a[1] = 666
  // 正確用法
  a(1) = 666
  ```

- 遍歷 Array
  ```scala
  var a: Array[Int] = Array(3)
  
  // 透過 for-loop
  for(el <- a) println(el)

  // 透過 foreach
  a.foreach(println(_))
  ```

- Array 常用方法
  - 更多的方法見，`import Array._` 包
  
  - 獲取長度，透過 `arr.length`
    ```scala
    scala> var b = Array(1,2,3)
    scala> b.length   // 3
    ```

  - 插入元素 (append)，透過  `+:` 或 `:+`
    ```scala
    scala> var b = Array(1,2,3)

    // 插入首位
    scala> b +:= 0 // Array(0, 1, 2, 3)

    // 插入尾部
    scala> b :+= 10 // Array(0, 1, 2, 3, 10)
    ```

  - 串接兩個 list，透過 `:::`
    ```scala
    scala> var a = List(1,2,3)
    scala> var b = List(4,5,6)

    scala> a ::: b
    val res12: List[Int] = List(1, 2, 3, 4, 5, 6)
    ```

  - 串接兩個 array，需要導入 `import Array._`
    ```scala
    scala> import Array._

    scala> var a = Array(1,2,3)
    scala> var b = Array(4,5,6)

    scala> concat(a, b)
    val res13: Array[Int] = Array(1, 2, 3, 4, 5, 6)
    ```

## Map (json)
```scala
// 建立 map
var x = Map("one" -> 1, "two" -> 2)

// 讀取 map
x("one")  // 1

// 更新 map
x.updated("one", 666)

// 遍歷 map
for ((k, v) <- x) println(s"[$k] = $v") 
```

## Option 和 Some
```scala
// 建立可為空的類型
var x:Option[Int] = 5       // 錯誤
var x:Option[Int] = null    // 設定空值

// Option 類型的值，需要包裝為 Some 類型
var x:Option[Int] = Some(5) // 設定初始值
```
