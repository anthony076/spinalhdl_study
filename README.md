## HDL 語言
- 比較
  - verilog
    - 優，類似軟體中的C語言，語法成熟可靠，
      有完整的綜合、驗證方面的配套工具
    - 優，語法簡單、特性少
    - 缺，底層語言，就像 C 要寫前端應用，
      對複雜電路，使用起來過於繁瑣和復雜，難以控制，容易出bug
    - 缺，沒有 unit-test-framework (SystemVerilog 和 VHDL 都有)

  - Bluespec
    - 基於 Haskell 的 Domain-Specified-Language 語言
    - 優，2003 開發，2003-2019 為商用的DSL語言，2020 開源編譯器，功能性和實用性得到驗證，

      設計Bluespec這個工具的人是Haskell語言的創始人之一，成功設計過10Gb級別的路由網絡芯片，
      IBM 和 riscv 都使用過 Bluespec 來開發IC

    - 優，不需要像 spinalhdl 一樣學習前還需要學習 scala 的語法
      
      spinalhdl 是 scala 的框架，使用的 scala 的語法，
      使用 spinalhdl 時，容易有軟體語法的干擾，

      Bluespec 是DSL，完全為硬體開發的語言，比 spinalhdl 的干擾性語法更少，
      且基於haskell語言的開發，具有haskell的優勢

      - 具有程式語言常見的類型系統、多態、類型類、對象和實例、繼承、
        多層次的結構化的數據結構（代數數據類型）、高階函數、模式匹配
      - 強大的抽象能力和豐富的表達能力，能夠更直接的將電路描述出來
      - 輸入輸出訊號封裝成方法，自動產生握手訊號
      - 可利用複合數據結構來組織數據，提高代碼可讀性和維護姓
      - 比 verilog 手動維護狀態轉移更加方便
      - 具有大量的官方庫和第三方庫支持各種常見功能
    
    - 優，具有更高的抽象能力，同樣功能比 verilog 的代碼量更少

  - spinalhdl
    - 優，和 verilog 一樣基於時鐘節拍動作來設計電路，切換到 spinalHDL 上手快
    - 優，不像 Chipsel，spinalhdl 可以在 scala 中進行驗證和仿真，
      避免SpinalHDL正確而生成的verilog有錯的問題
    - 缺，spinalhdl 是 scala 語言的框架，不是DSL語言，`會有軟體語法的干擾`
    - 缺，使用前需要學習 scala 的語法，`學習門檻高`

- HDL 基礎
  - [HDL基礎認識](HDL/Verilog-00-basic.md)
  - [HDL模組](HDL/Verilog-01-module.md)
  - [HDL常數](HDL/Verilog-02-const.md)
  - [HDL變數](HDL/Verilog-03-variable.md)
  - [HDL操作符](HDL/Verilog-04-operator.md)
  - [HDL代碼塊](HDL/Verilog-05-code-block.md)
  - [HDL過程塊](HDL/Verilog-06-process-block.md)
  - [HDL賦值](HDL/Verilog-07-assign.md)
  - [HDL條件](HDL/Verilog-08-condition.md)
  - [HDL迴圈](HDL/Verilog-09-loop.md)
  - [HDL任務和函數](HDL/Verilog-10-task-and-function.md)
  - [HDL系統函數與任務](HDL/Verilog-11-system-function-task.md)
  - [HDL預處理器](HDL/Verilog-12-preprocessor.md)
  - [HDL測試和仿真](HDL/Verilog-13-testbench.md)
  - [延遲模型](HDL/Verilog-delay-model.md)
  - [常見錯誤](HDL/Verilog-errors.md)
  - [教學資源+範例集](HDL/Verilog-example-and-ref.md)
  - [面試題](HDL/Verilog-interview.md)

- Bluespec 基礎 
  - [總覽](Bluespec/blue-00-overview.md)
  - [開發環境設置](Bluespec/blue-01-environment.md)

- Scala 基礎
  - [安裝和IDE](Scala/scala-01-environment-ide.md)
  - [建構系統](Scala/scala-02-build-system.md)
  - [包的使用](Scala/scala-03-package.md)
  - [語言基礎](Scala/scala-04-basic.md)
  - [數據類型](Scala/scala-05-datatype.md)
  - [函數的使用](Scala/scala-06-function.md)
  - [迴圈和條件式](Scala/scala-07-condition-loop.md)
  - [Class的使用](Scala/scala-08-class.md)
  - [Trait](Scala/scala-09-trait.md)
  - [匹配](Scala/scala-10-match.md)
  - [操作符和表達式](Scala/scala-11-operators-express.md)
  - [泛型和隱式轉換](Scala/scala-12-generic.md)

- spinalhdl 基礎
  - [Spinalhdl 基礎](SpinalHDL/spinal-01-basic.md)
  - [Spinalhdl 數據類型](SpinalHDL/spinal-02-datatype.md)
  - [Spinalhdl 組合電路](SpinalHDL/spinal-03-combinational.md)
  - [Spinalhdl 序列邏輯電路](SpinalHDL/spinal-04-sequential.md)
  - [Spinalhdl 常見錯誤](SpinalHDL/spinal-10-design-error.md)
  - [Spinalhdl 仿真](SpinalHDL/spinal-20-simulation.md)

## [開發環境] 開發環境概述
對 Lattice-FPGA 晶片進行開發有以下幾種方式

- step1，產生硬體描述語言，
  - `方法1`，手寫硬體描述語言，
    - 手動產生 verilog(*.v) 或 SystemVerilog(*.sv) 或 VHDL(.vhdl) 的檔案
    - 參考，[HDL基礎](#hdl-基礎)
  
  - `方法2`，使用 spinalhdl，
    - 優，可搭配 vscode 使用，具有語法高量、auto-jump 等優點
    - 參考，[Scala 基礎](#sala-基礎) 和 [SpinalHDL 的使用](#spinalhdl-基礎)

- step2，進行 FPGA 的開發 (功能仿真 > 邏輯合成 > 布局 > 佈線 > 時序仿真)
  - `方法1`，使用官方出的 IDE
    - [Intel Altera Quartus II](Tools/quartus2.md)
    - [Xilinx Vivado](https://www.xilinx.com/support/university/vivado.html)
  
    - Lattice Diamond
      - 免費軟體，適用於高密度FPGA，支援 高密度FPGA晶片，
      - [支援晶片詳見](https://www.latticesemi.com/zh-CN/latticediamond#_55E0FC25255E49099D8CE776BB214FB6)

    - Lattice iCEcube2
      - 免費軟體，專門為 iCE40 系列的 IDE
      - [iCEcube2 的使用](Tools/iCEcube2.md)

    - Lattice Radiant: 
      - 免費軟體，iCEcube2 的進階版，支援 iCE40 和等多的晶片，
      - [支援晶片詳見](https://www.latticesemi.com/Products/DesignSoftwareAndIP/FPGAandLDS/Radiant#_98E1AB2EEC5648469B5085A4DF7937CA)

    - Lattice Propel: 專門用於CPU設計的 IDE

  - `方法2`，使用開源/跨平台的開源工具(命令式)
    - 以[開源工具鍊](Tools/00_overview.md)取代官方出的IDE軟體
    - 優，啟動快、使用簡單
    - 缺，需要手動安裝和配置
  
  - `方法3`，使用第三方的 IDE
    - [PlatformIO IDE](https://platformio.org/platformio-ide)
      - 支援多種 MCU、FPGA 晶片和開發版

    - [apio-ide](https://platformio.org/platformio-ide)
      - 專為 ICE40 晶片的IDE
      - Atom + APIO + Platformio-ide
      - 將 iceStorm 整合在 IDE 中
      - APIO: Open source ecosystem for open FPGA boards

    - [iceStudio](https://icestudio.io/)
      - 缺，僅支援 Lattice 的晶片，
      - 缺，僅支援部分開發板
      - 優，開源
      - 優，使用 GUI 簡化開發，最小化代碼
      - 優，有提供 riscv 的範例
      - 優，對支援的 OpenSource-FPGA-board，[以GUI拖拉](https://github.com/FPGAwars/icestudio/)的方式進行編程
      - 參考，
        - [IceStudio - Open source GUI for FPGA design and programming, no code required!](https://www.youtube.com/watch?v=pZWFmupcMTs)

  - 方法4，推薦使用，[在vscode調用官方軟體+開源軟體](HDL/dev-template-quartus/readme.md)


## 開發板資訊
- [IceSugar-nano-board(iCE40LP1k)](hardware/icesuguar-nano-board(iCE40LP1k).md)

## 硬體設計
- [cpu設計指引](examples/cpu-design.md)