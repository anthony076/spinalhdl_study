@echo off

@REM Delete previous result files
FOR %%i in (*.vcd *.vvp) DO (
  IF EXIST "%%i" (
    del /F /S /Q %%i 
  )
)

@REM Compile and simulate
iverilog -o top_tb.vvp top_tb.v
vvp top_tb.vvp

@REM Open vcd file with gtkwave if compile and simulate are sucess
IF EXIST "result.vcd" (
  gtkwave result.vcd
)

