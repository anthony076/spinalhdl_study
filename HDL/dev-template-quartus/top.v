
// 使用 quartus的合成器時，頂層模組名必須是 top
module top(in,en,out);
  // 三態閘
  input en;
  input in;
  output out;

  assign out = en ? in : 'bz;

endmodule