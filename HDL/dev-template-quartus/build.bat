@echo off
set quartus_map="C:\altera\13.0sp1\quartus\bin\quartus_map.exe"
set qnui="C:\altera\13.0sp1\quartus\bin\qnui.exe"
set projectName=dev-template-quartus

@REM 編譯+合成
::格式: quartus_map --read_settings_files=on --write_settings_files=off <專案名> -c <入口模塊>
::    --read_settings_files 參數:  是否從 qsf 檔案讀取專案屬性
::    --write_settings_files 參數:  是否將專案屬性寫回 qsf 檔案，預設為開啟
:: -c 要編譯的主模塊
%quartus_map% --read_settings_files=on --write_settings_files=off %projectName% -c top

@REM 開啟 RTL-Viewer
::格式: quartus_map --read_settings_files=on --write_settings_files=off <專案名> -c <入口模塊>
%qnui% %projectName%
