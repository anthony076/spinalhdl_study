@echo off

set quartus_trash=*.qpf *.qws *.bak *.rpt *.ddb *.summary
set icarus_trash=*.vcd *.vvp

FOR %%i in (%quartus_trash% %icarus_trash%) DO (
  IF EXIST "%%i" (
    del /F /S /Q %%i 
  )
)

FOR %%i in (db incremental_db output_files simulation) DO (
  IF EXIST "%%i" (
    rmdir /S /Q %%i 
  )
)

