## Develop-Environment-Template
- use `QuartusII` as a `synthesis-compiler` (call *.exe from QuartusII folder directly)
- use `IcarusVerilog` as a `simulation-compiler`
- use `GTKWave` to check simulated-waveform

- vscode plugins
  - Plugin: Verilog-HDL/SystemVerilog/Bluespec SystemVerilog support for VS Code
    - `iverilog.exe` (from IcarusVerilog) as a syntax-linter
    - `ctags.exe` for definition-jump
  
  - Plugin: WaveTrace

## For new project
- Flow
  - step1，change the name of project-folder
  - step2 projectName in `build.bat`

- Default
  - default `entry-design-filename` = top.v
  - default `testbench-filename` = top_tb.v

- Limitation:
  - The name of Top-module must same as main-design-filename (top)

## Usage
- For synthesis-code，use `build.bat`
  - compile top.v with QuartusII
  - open RTL-View with QuartusII

- For testbench-code，use `sim.bat`
  - compile top_tv.v with IcarusVerilog
  - show waveform with GTKWave

- Check .vcd only，double-click on result.vcd directly

- clean the project，use `clean.bat`
  
