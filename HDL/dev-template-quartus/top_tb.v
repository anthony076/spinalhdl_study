`timescale 1ns/1ns
`include "top.v"

module test;

  reg in1;
	reg en1;

	wire out1;

	top t1(
    .in(in1),
    .en(en1),
    .out(out1)
  );

	initial begin
    // 設置將波形匯出
    $dumpfile("result.vcd");
    // 匯出接腳設置
    $dumpvars;   // 無參數設計中的所有訊號都會被記錄
    
		// en = 0 ，三態閘為disable，輸出為高阻抗
	  en1 = 1'b0;
		in1 = 1'b1;
		#10
		
	  en1 = 1'b0;
		in1 = 1'b0;
		#10

		// en = 1 ，三態閘為enable，輸出為 in
		en1 = 1'b1;
		in1 = 1'b1; 
		#10;
		
		// en = 1 ，三態閘為enable，輸出為 in
		en1 = 1'b1;
		in1 = 1'b0; 
		#10;

		$finish;

	end

endmodule