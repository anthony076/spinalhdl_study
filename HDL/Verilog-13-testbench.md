## 模擬基本概念
- 模擬分為兩種
  - 功能性模擬
    - 不含邏輯閘的傳輸延遲
    - 只看硬體電路的組合邏輯是否正確
    
  - 綜合模擬
    - 包含邏輯閘的延遲
    - 根據模擬的結果，適時添加約束

  - 時序模擬
    - 包含邏輯閘+傳輸線的延遲，較接近真實狀況
    - 根據模擬的結果，適時添加約束
    
- 模擬需要定義輸入輸出的接腳
  - 輸入用 reg，可供測試過程中多次取用
  - 輸出用 wire，輸出結果不會再次被取用，因此只需要定義為 wire
    
- 測試不可合成(模擬)的語句
  
  因為模擬使用的是`不可合成的語句`，因此`模擬的代碼不會使用合成器編譯執行`，
  
  模擬代碼需要再 simulator 的軟體中，進行編譯和測試，
  參考，[測試手寫的 testbench](../Tools/modelsim.md#modelsim-的使用測試手寫的-testbech)

- 撰寫 testbench 流程
  - step1，定義訊號接腳
    - 定義`輸入訊號接腳`，輸入訊號接腳用於產生訊號，並傳遞給 DUT 的輸入端
      > 輸入接腳宣告為 reg 類型，在模擬的過程中，輸入接腳會不斷地重新賦值，因此需要記憶功能

    - 定義`輸出訊號接腳`，輸出訊號接腳用於接收 DUT 的結果
      > 輸出接腳宣告為 wire 類型，在模擬的過程中，輸出接腳只會在取得DUT結果時賦值一次，因此不需要記憶功能
  
  - step2，實例化待測模組，並將輸入/輸出訊號接腳與 DUT 的接口進行連接
    > 若沒有DUT，則此步驟可以省略

  - step3，對`輸入訊號接腳`設置訊號
    - 注意，只需要對輸出接腳設置，模擬器會自動從待測模組的輸出接腳，捕獲 DUT 的輸出訊號
    - 若是`一次性`的訊號，使用 initial代碼塊
    - 若是`連續性`的訊號，
      - `方法1`，可使用 always代碼塊產生連續訊號，例如，`always begin ... end`
      - `方法2`，可使用 initial代碼塊 + forever 代碼塊
      - 注意，無論是上述哪一種實現方式，連續性的訊號都需要`添加結束條件`
      - 範例，見 [為無窮迴圈的波形添加停止條件](#範例集)
  
  - 範例
    ```verilog
    `timescale 1ns/1ns

    module test;
      // ==== step1，建立輸入訊號的輸出接腳 ====
      reg x, y;
      wire z;
      
      // ==== step2，實例化 DUT ====
      // 用於將輸入訊號的接腳與DUT的接口進行連接
      and a1(z, x, y);

      // ==== step3，設置輸入訊號 ====
      initial begin
        x=0; y=0;
        #10;
        x=0; y=1;
        #10;
        x=1; y=0;
        #10;
        x=1; y=1;
        #10;
        $finish;
      end

    endmodule
    ```
 
## testbench 的撰寫 (和 top module 的差異)
- 在文件的起始，透過 `timescale 設置圖表的時間單位
  - 格式，`` `timescale 延遲時間單位 / 圖表時間單位 ``
    - 延遲時間單位，使用 `#延遲時間` 的單位
    - 圖表時間單位，圖表顯示的最小單位
    - 圖表時間單位 <= 延遲時間單位
  
  - 注意
    - 數字只能是1、10、100，不能為其它的數字
    - 圖表時間單位 不能大於 延遲時間單位
    - 錯誤範例，`timescale 1ps/1ns，圖表時間單位 > 延遲時間單位

- 模組不需要定義輸入輸出接腳

  testbench 中定義的模組的目的，是用來模擬不是用來描述硬體，
  因此不需要定義輸入輸出的接腳

- 在模組中需要定義變數，用來儲存要給主模組的輸入和輸出訊號，

- 需要調用 system 的模組，需要將變數與系統連接，讓 simulation-tool 可以讀取變數值

- 需要 initial 語句，用來設置輸入接腳的初始訊號

- 透過系統任務匯出波形
  - `$dumpfile`，設置匯出檔案
  - `$dumpvars`，設置匯出的相關參數
  - `$dumpvars(層數, 起始模組)`，設置要獲取訊號的模組和獲取的深度，若層數為0，代表記錄所有子模組
    - 例如，$dumpvars (0, top);， 層數=0，紀錄 top 模組 和其下所有子模組所有的訊號
    - 例如，$dumpvars (1, top);， 層數=1，只紀錄 top 模組的所有訊號
    - 例如，$dumpvars (0, top.訊號1, top.訊號2)，只記錄 top 模組中的 訊號1和訊號2，層數只對模塊有效，對指定訊號無效
    - 例如，$dumpvars (3, top.訊號1, top.aa模塊)，記錄 top 模組中的 訊號1，和 top.aa模塊以下三層的訊號
  - `$dumplimit(size)`，限制VCD文件的大小 (以byte為單位)
  - `$dumpall`，記錄所有
  - `$dumpon`，開始紀錄，使 dump 指令生效
  - `$dumpoff`，停止紀錄，使 dump 指令失效
  
  - 注意，$dumpfile 和 $dumpvars 都是系統任務，任務需要寫在 initial 塊中
      
  - 範例，輸出波形範例

    <img src="doc/testbench/diff-between-design-and-testbench.png" width=80% height=auto>

- [其他可用的系統任務與系統函數](Verilog-11-system-function-task.md)

## 使用時間延遲
- 時間延遲的種類
  - 種類1，`語句延遲`，執行語句前先延遲再執行語句，常用於仿真，
    - 語法，`#延遲時間 語句;` 或是 `#延遲時間; 語句;`，兩種方式產生的延遲效果相同
  
  - 種類2，`賦值延遲`，先保存賦值語句右側表達式的結果，再進行延遲，延遲後才進行賦值
    - 語法，`變數 = #時間延遲 表達式`
    - 若賦值右側的表達式是`常數`: 效果同`語句延遲`
    - 若賦值右側的表達式是`變數`: 與`語句延遲`產生不同的延遲效果
  
  - 種類3，[wait語句](#等待語句wait-暫停代碼塊的執行)，代碼塊暫停執行，直到條件成立，

- 時間延遲的使用場景
  - (賦值延遲)用於`assign 語句`，適合模擬模組`處理訊號`的延遲
  - (賦值延遲)用於`變數宣告語句`，適合模擬`訊號傳遞`的延遲
  - (語句延遲)用於`initial塊`內，用時間延遲來模擬`輸入訊號`的`持續時間`

- 延遲時間的單位，由 [`timescale](#testbench-的撰寫-和-top-module-的差異) 指定

- 注意，時間延遲可出現在硬體描述中，但是會被`合成器`忽略，不會被翻譯為硬體，
  時間延遲對`模擬器`才會有實際作用

- 注意，避免慣性延遲 (輸入訊號變化頻率 < 延遲時間)
  
  慣性延遲，指延遲時間內遇到輸入訊號多次變更，而造成`時間延遲沒有作用`，
  
  例如，當輸入訊號發生變更A，進入時間延遲，等待延遲時間內，又發生了輸入訊號變更B，
  當延遲時間到達後，輸出值會以當前的輸入狀態 (輸入訊號變更B) 決定，
  本來預期輸入訊號發生變更A延遲後，會輸出輸入訊號發生變更A的結果，但最後產生輸入訊號變更B的結果，
  使得延遲沒有發揮作用

  慣性延遲發生在輸入信號變更的頻率小於延遲時間，
  例如，輸入訊號的變化率為 2ns，延遲時間為 5ns，就會產生慣性延遲

- 範例: 模組延遲賦值 + 時間延遲在模擬的應用
  ```verilog
  `timescale 1ns/1ns

  module myand(out, x, y);
    parameter delay = 5;
    input x, y;
    output out;
    
    // 合成編譯器會忽略時間延遲(#)的語句，模擬編譯器才會執行時間延遲(#)的語句
    // 每次 x、y 有變化時，都會產生 #delay 的延遲後，out 才會產生改變
    assign #delay out = x || y;
  endmodule

  module test;
    reg x1;
    reg y1;
    wire out1;

    // 修改模組的時間延遲參數
    myand #1 m1(out1, x1, y1);

    initial begin
      $dumpfile("result.vcd");
      $dumpvars(0, myand);
      
      x1 = 1'b0;
      y1 = 1'b1;
      // 執行下一個語句前的時間延遲
      #10

      x1 = 1'b0;
      y1 = 1'b0;
      #10

      x1 = 1'b1;
      y1 = 1'b0;
      #10

      x1 = 1'b0;
      y1 = 1'b0;
      #10

      x1 = 1'b1;
      y1 = 1'b1;
      #10

      x1 = 1'b0;
      y1 = 1'b0;
      #10
      $finish;

    end

  endmodule
  ```

  waveform
  
  <img src="doc/time/delay-example.png" width=100% height=auto>


## 用於覆寫模組內變數的過程連續賦值語句
參考，[過程連續賦值語句](Verilog-06-assign.md)

## 等待語句(wait): 暫停代碼塊的執行
- 使用場景:
  常用於時序電路中，由於暫存器需要時脈的觸發才會變更值，
  當某個操作必須依賴其他變數符合條件後才執行時，
  可以利用 wait 等待依賴條件滿足後，才繼續往下執行其他語句

- 語法
  ```verilog
  wait (條件表達式);
  ```

- 範例
  ```verilog
  `timescale 1ns/1ns
  
  module test ;
      reg          rstn ;
      reg          clk ;
      reg [3:0]    cnt ;
      wire         cout ;
  
      counter10     u_counter (
          .rstn    (rstn),
          .clk     (clk),
          .cnt     (cnt),
          .cout    (cout));
  
      initial begin
          clk       = 0 ;
          rstn      = 0 ;
          #10 ;
          rstn      = 1'b1 ;
          wait (test.u_counter.cnt_temp == 4'd4) ;
          
          @(negedge clk) ;
          force     test.u_counter.cnt_temp = 4'd6 ;
          force     test.u_counter.cout     = 1'b1 ;
          #40 ;
          
          @(negedge clk) ;
          release   test.u_counter.cnt_temp ;
          release   test.u_counter.cout ;
      end
  
      initial begin
          clk = 0 ;
          forever #10 clk = ~ clk ;
      end
  
      // finish the simulation
      always begin
          #1000;
          if ($time >= 1000) $finish ;
      end
  
  endmodule
  ```

## 範例集
- 範例，輸出波形範例
  ```verilog
  module top;

    parameter period = 5'd10;
    reg clk;

    initial begin
      // 設置將波形匯出
      $dumpfile("result.vcd");
      // 匯出接腳設置
      $dumpvars;   // 無參數設計中的所有訊號都會被記錄
      
      clk = 0;
      repeat(6) #(period/2) clk = ~clk;
    end

  endmodule
  ```

- 範例，產生 clock 的多種方式
  - 方法1，透過 initial
    ```verilog
    parameter clk_period = 10;
    reg clk;

    initial begin
      clk = 0;
      forever #(clk_period/2) clk = ~clk;
    end
    ```

  - 方法2，透過 always
    ```verilog
    parameter clk_period = 10;
    reg clk;

    initial begin
      clk = 0;
    end

    always #(clk_period/2) clk = ~clk;
    ```

  - 方法3，自定義空佔比
    ```verilog
    parameter HIGH = 5; LOW = 20
    reg clk;

    always begin
      clk = 1;
      #HIGH;
      
      clk=0
      #LOW;
    end
    ```

- 範例，為無窮迴圈的波形添加停止條件
  ```verilog
  `timescale 1ns/1ns
  
  module test ;
  
    reg          clk ;

    initial begin
      $dumpfile("result.vcd");
      $dumpvars;

      clk = 0 ;
      
      forever begin
          clk = ~clk ;
          #5 ;
      end
    end

    always begin
      #10
      if ( $time >=100) $finish;
    end

  endmodule
  ```

- 範例，利用 task 自動檢測波形結果是否正確 (簡易單元測試)
  ```verilog
  `timescale 1ns/1ns

  module test;
    // 建立訊號的輸出接腳
    reg x, y;
    wire z;
    reg valid;
    
    // 實例化 DUT
    and a1(z, x, y);

    // 設置匯出波形
    initial begin
      $dumpfile("result.vcd");
      $dumpvars;
    end

    // 設置輸入訊號
    initial begin
      x=0; y=0; valid=0;
      check(0);
      #9;

      x=0; y=1;
      //check(0);    // 正確結果
      check(1);      // 測試是否能檢查出錯誤，若檢測出錯誤，valid=1
      #9;
      
      x=1; y=0;
      check(0);
      #9;

      x=1; y=1;
      check(1);
      #9;
      $finish;
    end

    // 檢測任務
    task check;
      input expect_z;

      begin
        #1  // 等待x、y設置完成，若不等待，z的狀態可能是錯誤的
      
        if (z != expect_z)
          valid = 1;
        else
          valid = 0;
      
        $display("x=%d, y=%d, z=%d, valid=%d", x, y, z, valid);
      end
    endtask

  endmodule
  ```

- 範例，檢測模組內部的訊號，並轉換為易讀格式
  
  完整範例，見 [檢測1101序列](fsm-sequence-1101/)
  ```verilog
  // 顯示當前狀態
  reg [15:0] currentState ;
  always@(*)begin
    case(uut.currentState)
      S0: currentState = "S0";
      S1: currentState = "S1";
      S2: currentState = "S2";
      S3: currentState = "S3";
      S4: currentState = "S4";
      default:currentState = "S0";
    endcase
  end
  ```

- 同步清零 vs 異步清零
  同步清零
  ```verilog
  module dff(d,clk,clr,q);
    input d, clk, clr;
    output q;
    reg q;
    
    always@(posedge clk) begin
      if(!clr) q <= 0;
      else q <= d;
    end

  endmodule
  ```

  異步清零
  ```verilog
  module dff(d, clk, clr, q);
    input d, clk, clr;
    output q;
    
    reg q;
    always@(posedge clk or negedge clr) begin
      if(!clr) q <= 0;
      else q <= d;
    end
    
  endmodule
  ```

- [範例](https://www.youtube.com/watch?v=f-MyEpbChGw)

  <img src="doc/testbench/example-1.png" width=80% height=auto>
  
## Ref
- [dumpvars 的使用](https://blog.csdn.net/marvellousbinary/article/details/79842347)
- [TestBench结构+多種測試訊號的設計](https://blog.csdn.net/qq_26652069/article/details/96422293)
- [inout 接口的模擬](https://blog.csdn.net/Times_poem/article/details/52037380)