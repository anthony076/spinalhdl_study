## 數位邏輯基礎

- 暫存器(register) vs 栓鎖器(latch) vs 觸發器(flip-flop)
  - Reg: 
    - 指能暫時儲存數據的資料保存器，是依照功能描述的元器件，但不是實際的物理元器件
    - 暫存器實際對應的物理元器件，可以由 latch 或 ff 實現暫存器的功能

  - Latch:
    - 由電位觸發的資料保存器，可以由RS電路實現
      > 輸出端對輸入端是透明的，容易產生抑制雜訊
    - 當latch未致能時，輸出=前一狀態
    - 當latch致能時，輸出=輸入

  - FF:
    - 由clock的上升沿觸發的資料保存器
    - 由兩個 latch 構成的主從式觸發器，
      > 輸入對輸入端是不透明的，可抑制雜訊
    - 只有在clock的上升沿時，才會改變輸出的結果，
      否則FF的輸出都保持在最後儲存的狀態

## 語言的比較
- Verilog
  - 基於C語言

- SystemVerilog
  - Verilog 的擴展，支援更多的C語法，能以更少的語法描述硬體

- VHDL
  - 基於 Ada 語言
  
## 可合成和不可合成
- 什麼是可合成，用於 組合邏輯或時序邏輯

  合成就是將Verilog描述的RTL級的電路模型構造出門級網表的過程。
  
  合成只是個中間步驟，`合成後生成的網表文件`，
  就是由導線相互連接的寄存器傳輸級功能塊（像是觸發器、算術邏輯單元和多路選擇器等）組成的。

  產生網表文件後，邏輯優化器會讀入網表，並且會按照用戶提供的`面積（即資源要求）`和`定時（時序要求）約束`為目標來優化網表。
  並且，這些面積和定時約束也能夠指導模塊構造器恰當地選取或生成寄存器傳輸級功能塊

- 什麼是不可綜合: `用於仿真`，模組內具有沒有相應的硬件元件對應的語句

  Verilog中存在一些用於仿真驗證的子集，屬於仿真驗證語言，
  `只在仿真時候使用`，不能被綜合成電路，因為沒有相應的硬件元件與其對應。
  
  例如，系統任務 $display()、initial等語句

- 可用於綜合的語句
  - 模組宣告，module、macromodule
  - 數據類型宣告，wire、reg、integer 
  - parameter
  - 端口類型宣告，input、output、inout
  - 大部分運算符都支持
    - 除 === 或 !== 外
    - 對`除法(/)`和`求模(%)`有使用限制，只有 `除數是常數`且`除數是2的指數`的除法才支持
  - 基本邏輯閘: AND、OR、BUF、NOT、bufif0、bufif1、pullup、pulldown、 ... 等
  - 持續賦值 assign
  - 過程賦值: 阻塞賦值( = ) 或 非阻塞賦值( <= )
  - 條件語句: 大部分合成器都支持，部分合成器不支援 casex，casez 的條件語句
  - for 循環語句
  - always 過程語句
  - begin-end 塊語句
  - function、endfunction
  - task、endtask: 大部分合成器都支持，部分合成器不支援
  - 編譯指示相關語句: include 或 define 或 ifdef 或 else 或 endif

- 不可用於綜合的語句
  - UDP
  - 可出現，但會被忽略的語句
    - 延時控制 #xxx
    - scalared、vectored
    - specify
    - small、large、medium、
    - weak0、highz0、pull0、
    - time
    - wait
  
  - 絕對不能出現的語句 (可用於 Simulator 中)
    - 在 assign 的語句中，等式左邊出現含有變量的位選擇
    - 全等運算符 === 或 !==
    - cmos、nmos、rcmos、rnmos、pmos、rpmos
    - deassign、defparam、event、
    - force、release
    - fork、join
    - 沒有明確的循環次數: forever、while、repeat
    - tran、rtran、tranif0、rtranif0
    - initial
    - table、endtable
    - primitive、endprimitive
    - $finish

## FPGA 設計流程
- step1，編寫 HDL

- step2，功能仿真
  > 對模型電路進行功能上的仿真驗證，查找設計的錯誤並修正，
  > 此時的仿真驗證並沒有考慮到信號的延遲等一些timing 因素，只是驗證邏輯上的正確性

- step3，邏輯合成 (Logic-Synthesis)
  - 將HDL描述的硬體設計，轉換和優化為邏輯閘級別的電路連線網表的過程
  
  - 透過合成將 HDL 轉換為 RTL 
    
    RTL = Register-Transfer-Level，暫存器轉換語言，指利用暫存器描述電路的語言

    硬體描述語言(HDL)是高階的語言，但描述出的電路不一定是可合成出硬體電路的，
    若編譯器將 HDL 轉換為 RTL 級的語言，代表該電路一定是可合成的，才能被轉換為 RTL，
    
    HDL 的硬體描述中，邏輯閘是不需要轉換的，除邏輯閘的部分會被轉換為 Register，
    而 RTL 代表可將 HDL 轉換為 Register 和 電路的連接狀況(傳輸狀況)

- step4，布局 (Placement)
  > 將邏輯閘映射到物體的 Look-Up-Table(LUTs) 和 Flip-Flop(FFs)

- step6，布線 (Routing)
  > 選擇/尋找設計中所有元件之間的最優路徑

- step7，時序仿真
  > 佈局佈線後，電路模型中已經包含了時延信息。利用在佈局佈線中獲得的精確參數，用仿真軟件驗證電路的時序。

- 合成代碼和仿真代碼需要不同的執行環境
  - 合成代碼使用 quartusII 進行測試
    - 推薦使用，[yosys](../Tools/yosys.md)，速度快，有RTLView
    - 可使用 [quartusII](../Tools/quartus2.md)，quartusII 具有 RTLView，可以檢查合成後的邏輯圖，但檔案大
    - 不推薦使用 iverilog，iverilog 沒有 RTLView
  
  - 仿真代碼
    - 推薦使用 [iverilog](../Tools/IcarusVerilog.md)，
    - 不推薦使用 modelsim，可用，但速度慢，使用較繁瑣

## 硬體電路區分: 組合邏輯和時序邏輯

- 組合邏輯: 與時序無關，單純表達邏輯元件之間關係的邏輯組合，例如，O = X + Y
  - 不需要 clock 觸發元件工作
  - 由高電位/低電位 觸發每一個元件

- 時序邏輯: 利用時序，控制邏輯元件之間關係的邏輯組合
  - 需要 clock 觸發元件工作，由 clock 的上緣或下緣觸發每一個元件工作
  - 無法立刻執行，需要前一個元件產生結果，才能推後下一個元件

## 同步電路和異步電路
- 同步電路，指使用同一個 clock 源的時序電路，
  
  時序電路的每一個正反器都使用同一個 clock 源，
  使得時序電路的每一個正反器，都會統一在 clock 上緣/下緣 才會進行輸出

- 異步電路，指使用不同 clock 源的時序電路，

## 描述硬體的方式
- 注意，以下三種建模方式，差別在描述硬體的方式不同，對一個同一個電路都可以用以下三種方式建立，
  
  並不是建模方法只對應某一種邏輯電路，不存在行為建模只能用於時序邏輯電路，且在同一個模組中，三種方法是可以混用的
  
- `建模方法1`，邏輯閘建模 (Logic-Gate-Level)
  - 可合成出`組合邏輯`電路
  - 利用內置的12個`基本門級元件`或`模組調用的方式`，去描述邏輯電路圖中元件與元件之間的連接關係。
  - 與時序無關，程式碼調換位置不影響功能

- `建模方法2`，數據流建模 (DataFlow-Level)
  - 可合成出`組合邏輯`電路
  - 利用`表達式賦值`，去描述電路，例如，`assign out = x and y`
  - 不限定使用 logic-gate，可以描述電路的功能或元件之間的連接關係

- `建模方法3`，行為級建模 (Behavior-Level)
  - 可合成出`時序邏輯`電路，
  - 常用於建立`需要觸發的電路`，利用行為描述的方式建立
  - 利用多個並行、動態的[過程模塊](#特殊模塊-過程模塊)，
    描述系統的工作，描述電路所具有的行為，或者說當電路在哪些輸入信號來臨時會有怎樣的輸出。
  - 抽象程度高，只考慮功能的算法情況，而不關心底層到底是如何實現的，
    利用算數表達式表達功能，綜合器會根據情況自動來選擇如何完成電路

- 邏輯閘建模 VS 數據流建模 的差異
  <img src="doc/module/logicgate-vs-dataflow-vs-behavior.png" width=80% height=auto>

- 範例，以下三種描述硬體的方式是等效的
  - 以 AB+BC+AC 的邏輯為例

    | A | B | C | Out |
    | 0 | 0 | 0 |  0  |
    | 0 | 0 | 1 |  0  |
    | 0 | 1 | 0 |  0  |
    | 0 | 1 | 1 |  1  |
    | 1 | 0 | 0 |  0  |
    | 1 | 0 | 1 |  1  |
    | 1 | 1 | 0 |  1  |
    | 1 | 1 | 1 |  1  |
  
  - 方法1，以邏輯閘建模
    ```verilog
    module top (
      input a, 
      input b,
      input c,
      output out
    )
      // 邏輯閘建模
      wire AB, BC, AC;
      and u1(AB, A, B);
      and u1(BC, B, C);
      and u1(AC, A, C);

      or u4(out, AB, BC, AC);

    endmodule
    ```
  
  - 方法2，以數據流建模
    ```verilog
    module top (
      input a, 
      input b,
      input c,
      output out
    )

    // 數據流建模
    assign out = (A&&B) || (B&&C) || (A&&C);

    endmodule
    ```
  
  - 方法3，以行為建模
    ```verilog
    module top (
      input a, 
      input b,
      input c,
      output out
    )

      // 行為建模(利用真值表建模)
      case ({A, B, C})
        3'b000: out = 1'b0;
        3'b001: out = 1'b0;
        3'b010: out = 1'b0;
        3'b011: out = 1'b1;
        3'b100: out = 1'b0;
        3'b101: out = 1'b1;
        3'b110: out = 1'b1;
        3'b111: out = 1'b1;
      endcase
    endmodule
    ```

## 推薦的 verilog 代碼風格
- 對於低電位觸發的變數，在變數名後方加上 _n，例如，rst_n
- 經過鎖存器後的變數，在變數名後方加上 _r，例如，clr_r
- 經過多個鎖存器後的變數，在變數名後方加上多個r，例如，clr_rr，代表經過兩級的鎖存器
- 端口變數名，應包含 1_數據方向(發送方在前，接收方在後) 和 2_數據名稱
  > CPUMMU_WrReq，代表該端口變數為 CPU 發向 MMU 的寫入訊號請求

## Ref
- [三種建模方式的比較](https://blog.csdn.net/weixin_43804836/article/details/113888959)
- [三種建模方式的比較](https://blog.csdn.net/qq_39507748/article/details/108717195)