## 函數與任務概述
- task 和 function 用於進行`重複性操作`，
  可以多次調用，避免同樣代碼重複編寫，可以使代碼更為簡潔

- function 和 task 的區別
  - function 只能在模塊中定義，作用範圍也局限於此模塊，外部無法調用
  - function 用於對`組合邏輯`的變數，進行數據處理 (處理賦值表達式的複雜邏輯)
  - task 可在時序邏輯或組合邏輯被調用，但 task 內部無法出現過程代碼塊，因此無法利用 task 描述出時序邏輯
    不僅能完成函數的功能，還可以包含時序控制邏輯
  
  <img src="doc/task-function/task-function-compare.png" width=100% height=auto>

  參考，[disable 的使用](Verilog-05-code-block.md)

- [系統任務和系統函數](Verilog-11-system-function-task.md)
  
## Function 函數
- 函數的種類
  - 種類1，一般函數，
  - 種類2，常數函數，在編譯期就`可以推算出返回值為常數`的函數
  - 種類3，automatic函數，`使用動態內存`的函數

- `種類1`，一般函數的使用
  - 語法，一般函數語法
    ```Verilog
    function 特殊標誌符 返回值類型 [返回值寬度] 函數名;
      input 輸入參數;
      
      reg aa; 
      //wire bb;  // 錯誤，function 內部所有的變數都是 reg

      begin
        ... 內容省略 ...
      end
    endfunction
    ```

  - 函數名同時也是返回值，在函數中可以對返回值進行賦值
    ```verilog
    function value;   // 函數名 value，同時也是返回值 value
      input aa;       
      value = aa;   // 對返回值 value 進行賦值
    endfunction
    ```
  - 返回值類型，預設為 reg 類型的變數，可省略
  - 返回值寬度，預設為 1bit，可省略
  - 特殊標誌符，視實際情況添加，例如 automatic

- `種類2`，常數函數
  - 指在編譯期就`可以推算出返回值為常數`的函數
  - 限制，??常數函數`不能訪問全局變量或調用系統函數`，但可以調用另一個常數函數
  - 可以用來取代常量

- `種類3`，automatic 函數
  - 使用場景

    function 預設是是`靜態的`，每次函數調用都`存取同一個內存空間`，
    因此有數據競爭的風險，
    
    可以透過 automatic 關鍵字改為`動態內存`空間，以避免資源競爭的問題，
    因此，只要有兩處以上的調用，就需要宣告為 automatic函數
  
  - 限制

    每次調用 automatic 函數，該函數的 context 每次都在不同的記憶體位置，
    因此，`函數內部的局部變量是無法透過層次命名存取的`，
    但函數指針是固定位置的，可以透過層次命名來調用函數，但不是每個編譯器都有支援，部分有支援

  - 語法
    ```verilog
    // function automatic 返回值類型 函數名
    function automatic  integer factorial ;
    ```

- 範例，利用 function 實現 16to1 範例
  ```verilog
  module mux16to1(W,S16,f);
    input [15:0] W;
    input [3:0] S16;
    output reg f;
    reg [3:0] M;

    function mux4to1;
      input [3:0] W;
      input [1:0] S;

      if(S==2'b00) mux4to1 = W[0];
      else if(S==2'b01) mux4to1 = W[1];
      else if(S==2'b10) mux4to1 = W[2];
      else if(S==2'b11) mux4to1 = W[3];
    endfunction

    always @(W,S16)
    begin
      M[0] = mux4to1(W[3:0],S16[1:0]);
      M[1] = mux4to1(W[7:4],S16[1:0]);
      M[2] = mux4to1(W[11:8],S16[1:0]);
      M[3] = mux4to1(W[15:12],S16[1:0]);
      f = mux4to1(M[3:0],S16[3:2]);
    end

  endmodule
  ```

- 範例，一般函數範例，利用 function 簡化 七段顯示器的代碼
  ```verilog
  module digital_tube
      (
        input             clk ,
        input             rstn ,
        input             en ,
  
        input [3:0]       single_digit ,
        input [3:0]       ten_digit ,
        input [3:0]       hundred_digit ,
        input [3:0]       kilo_digit ,
  
        output reg [3:0]  csn , //chip select, low-available
        output reg [6:0]  abcdefg        //light control
        );
  
      reg [1:0]            scan_r ;  //scan_ctrl
      always @ (posedge clk or negedge rstn) begin
          if(!rstn)begin
              csn            <= 4'b1111;
              abcdefg        <= 'd0;
              scan_r         <= 3'd0;
          end
          else if (en) begin
              case(scan_r)
              2'd0:begin
                  scan_r    <= 3'd1;
                  csn       <= 4'b0111;     //select single digit
                  abcdefg   <= dt_translate(single_digit);
              end
              2'd1:begin
                  scan_r    <= 3'd2;
                  csn       <= 4'b1011;     //select ten digit
                  abcdefg   <= dt_translate(ten_digit);
              end
              2'd2:begin
                  scan_r    <= 3'd3;
                  csn       <= 4'b1101;     //select hundred digit
                  abcdefg   <= dt_translate(hundred_digit);
              end
              2'd3:begin
                  scan_r    <= 3'd0;
                  csn       <= 4'b1110;     //select kilo digit
                  abcdefg   <= dt_translate(kilo_digit);
              end
              endcase
          end
      end
  
      /*------------ translate function -------*/
      function [6:0] dt_translate;
          input [3:0]   data;
          begin
          case(data)
              4'd0: dt_translate = 7'b1111110;     //number 0 -> 0x7e
              4'd1: dt_translate = 7'b0110000;     //number 1 -> 0x30
              4'd2: dt_translate = 7'b1101101;     //number 2 -> 0x6d
              4'd3: dt_translate = 7'b1111001;     //number 3 -> 0x79
              4'd4: dt_translate = 7'b0110011;     //number 4 -> 0x33
              4'd5: dt_translate = 7'b1011011;     //number 5 -> 0x5b
              4'd6: dt_translate = 7'b1011111;     //number 6 -> 0x5f
              4'd7: dt_translate = 7'b1110000;     //number 7 -> 0x70
              4'd8: dt_translate = 7'b1111111;     //number 8 -> 0x7f
              4'd9: dt_translate = 7'b1111011;     //number 9 -> 0x7b
          endcase
          end
      endfunction
  
  endmodule
  ```
  
- 範例，常數函數範例
  ```verilog
  module top;
    parameter MEM_DEPTH = 256 ;
    reg  [logb2(MEM_DEPTH)-1: 0] addr ; // 編譯器可推算出 addr 為 8bit 
    
    function integer     logb2;
      input integer     depth ;

      // 在 depth = 2 的時候停止，
      // depth = 0d256 -> 128 -> 64 -> 32 -> 16 -> 8 -> 4 -> 2，共 8 次
      // 返回值 logb2 最後值為 8
      for(logb2=0; depth>1; logb2=logb2+1) begin
          depth = depth >> 1 ;
      end
    endfunction
  endmodule 
  ```

- 範例，automatic 範例
  ```verilog
  wire [31:0]          results3 = factorial(4);

  function automatic integer factorial ;
    input integer data ;
    integer i ;
    begin
        factorial = (data>=2)? data * factorial(data-1) : 1 ;
    end
  endfunction // factorial
  ```

## Task 任務
- 注意，某些編譯器有可能不支援 task 的合成

- 語法，
  ```verilog
  task 特殊標誌符 任務名;
    // 注意，可以沒有輸入參數，且可以定義為 inout 類型的端口
    input 輸入參數1; // 函數引數1，調用時需要傳入的輸入參數
    input 輸入參數2; // 函數引數2，調用時需要傳入的輸入參數

    // 注意，可以沒有輸出參數
    output 輸出參數1;  
    output 輸出參數2;

    begin // 必要，多語句沒有加 begin ... end 會報錯
      ... 內容省略 ...
    end
  endtask
  ```

  或和宣告module一樣，寫成函數調用的形式
  ```verilog
  task 特殊標誌符 任務名(
    // 注意，可以沒有輸入參數，且可以定義為 inout 類型的端口
    input 輸入參數1; // 函數引數1，調用時需要傳入的輸入參數
    input 輸入參數2; // 函數引數2，調用時需要傳入的輸入參數

    // 注意，可以沒有輸出參數
    output 輸出參數1;  
    output 輸出參數2;  
  );
    begin
      ... 內容省略 ...
    end
  endtask
  ```

  錯誤的寫法，不能向模組一樣，寫成函數型但輸入參數沒有指名方向 (不能只在代碼塊寫明端口方向)
  ```verilog
  task 特殊標誌符 任務名(輸入參數1,  輸入參數2);  // 錯誤，寫成函數引數一定要指名方向
    input 輸入參數1;
    input 輸入參數2;

    output 輸出參數1;  
    output 輸出參數2;  
  );
    begin
      ... 內容省略 ...
    end
  endtask
  ```


  - 對於`輸入變數`可以不需要再次宣告類型，預設為 wire 類型
  - 對於`輸出變數`可以不需要再次宣告類型，預設為 reg 類型
  - task 常用於時序邏輯和仿真之中，為避免時序錯亂(避免考慮task的執行時間)，輸出變數盡量使用`阻塞賦值`

- 範例，簡易 task 範例
  ```verilog
  task xor_operation;
      input [N-1:0]   x;
      input [N-1:0]   y;
      output [N-1:0]  z ;
      
      output reg [N-1:0]  z ; // 可以省略

      #3  z = x ^ y ;   // 因為 z 預設是 reg，因此不需要使用 assign
  endtask
  ```

- 範例，task 用於時序邏輯中
  ```verilog
  module top
    #(parameter N = 2)
    (
      input clk ,
      input rstn ,
      input [N-1:0] a ,
      input [N-1:0] b ,
      output [N-1:0] out  );

    // ==== step1，建立組合邏輯 ====
    reg [N-1:0]          xor_out ;
    always @(*) begin
        // 調用 task 進行組合邏輯
        xor_operator(a, b, xor_out);
    end

    // ==== step2，緩存組合邏輯的結果 ====
    reg [N-1:0] out_buf ;
    always @(posedge clk or negedge rstn) begin
        // reset pin
        if (!rstn) begin
            out_buf   <= 'b0 ;
        end

        // 緩衝數據
        else begin
            out_buf <= xor_out ; 
        end
    end
    assign       out = out_buf ;

    // ==== 建立 task ====
    task xor_operator;
        input [N-1:0]   x;
        input [N-1:0]   y;
        output [N-1:0]  z ;
        #3  z = x ^ y ;   //阻塞赋值，易于控制时序
    endtask
    
    endmodule
  ```

- 範例，task 用於模擬
  ```verilog
  `timescale 1ns/1ns

  module xor_op
      #(parameter         N = 4)
      (
          input             clk ,
          input             rstn ,
          input [N-1:0]     a ,
          input [N-1:0]     b ,
          output [N-1:0]    out  );

      reg [N-1:0]          xor_out ;

      always @(*) begin
          // 調用 task
          xor_operator(a, b, xor_out);
      end

      reg [N-1:0]          out_buf ;
      always @(posedge clk or negedge rstn) begin
          if (!rstn) begin
              out_buf   <= 'b0 ;
          end

          else begin
              out_buf   <= xor_out ; // 緩衝數據
          end
      end
      assign       out = out_buf ;

      // 建立 task
      task xor_operator;
          input [N-1:0]   x;
          input [N-1:0]   y;
          output [N-1:0]  z ;
          #3  z       = x ^ y ;   //阻塞赋值，易于控制时序
      endtask
    
  endmodule

  module test;
      reg          clk, rstn ;
      reg  [3:0]   a, b;
      wire [3:0]   co ;

      // 硬體連接
      xor_op u_xor_op
      (
        .clk (clk),
        .rstn (rstn),
        .a (a),
        .b (b),
        .out (co)
      );
      
      // 設置clk訊號
      initial begin
        $dumpfile("result.vcd");
        $dumpvars;

        rstn = 0 ;
        #8 rstn = 1 ;
        forever begin
          clk = 0; 
          # 5; clk = 1; 
          # 5;
        end
      end
      
      // 設置 ao、bo 的訊號
      initial begin
        a = 0 ;
        b = 0 ;
        sig_input(4'b1111, 4'b1001, a, b); // a=F, b=9, xor(a,b)=6
        sig_input(4'b0110, 4'b1011, a, b); // a=6, b=B, xor(a,b)=D 
        sig_input(4'b1000, 4'b1111, a, b); // a-8, b=F, xor(a,b)=7
      end
  
      task sig_input ;
        input [3:0] a;
        input [3:0] b ;
        output [3:0] ao ;
        output [3:0] bo ;
        
        begin
          @(posedge clk) ;
          ao = a ;
          bo = b ;
        end
      endtask ; // sig_input
      
      // 停止條件
      initial begin
          forever begin
              #100;
              if ($time >= 50)  $finish ;
          end
      end

  endmodule // test
  ```

  電路圖 + 仿真結果

  <img src="doc/task-function/example-task-in-simulation.png" width=60% height=auto>

- 對 task 內部的訊號進行觀察
  - `寫法1`，無法觀察到波形的寫法: 透過輸入參數傳遞變數
    
    透過輸入參數傳遞變數時，是透過`值傳遞`，將變數值傳遞到 task 內部的另一個獨立的變數，
    由於 task 在執行的過程中，`內部的變數外界無法存取`，因此無法觀察 task 內部訊號的變化

    ```verilog
    `timescale  1ns/1ns

    module test;

      reg [3:0] out;
      initial begin
          $dumpfile("result.vcd");
          $dumpvars;

          run(out); // 透過輸入變數傳遞值
          #10
          $finish;

      end
      
      task run( output [3:0] o_t);
        begin
          // o_t 是內部變數，外界無法存取
          #0 o_t = 4'b0001;   // 1
          #10 o_t = 4'b0011;  // 3
          #15 o_t = 4'b0111;  // 7
        end
      endtask

    endmodule
    ```

    透過檢查 task 的輸出引腳的波形，只會得到 `task 執行後的終值`

    <img src="doc/task-function/example-observe-signal-inside-task-1.png" width=80% height=auto>

  - `寫法2`，可以觀察到波形的寫法: 在 task 內部直接存取變數
    ```verilog
    `timescale  1ns/1ns

    module test;

      reg [3:0] out;
      initial begin
          $dumpfile("result.vcd");
          $dumpvars;

          run();

          #10
          $finish;

      end
        
      task run; // 不使用輸入引數
        begin
          // 直接在 task 內存取外部變量 out
          #0 out = 4'b0001;   // 1
          #10 out = 4'b0011;  // 3
          #15 out = 4'b0111;  // 7
        end
      endtask

    endmodule
    ```

    <img src="doc/task-function/example-observe-signal-inside-task-2.png" width=80% height=auto>

- automatic任務
  
  使用方式和`automatic函數`相同，只要有兩處以上的調用，就需要宣告為 automatic任務，
  避免數據競爭的問題
