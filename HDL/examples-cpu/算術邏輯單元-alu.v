`define    ADD 2'b00
`define    SUB 2'b01
`define    AND 2'b10
`define    OR  2'b11

module ALU( Data_A, Data_B, OP_Code, Data_Out );

    parameter   Data_Size = 8;
    parameter   OP_Code_Size = 2;

    input   [Data_Size-1:0] Data_A, Data_B;
    input   [OP_Code_Size-1:0] OP_Code;
    output  [Data_Size-1:0] Data_Out;

    reg     [Data_Size-1:0] Data_Out;

    always @( Data_A, Data_B, OP_Code ) begin
      case( OP_Code )
        `ADD:       Data_Out <= Data_A + Data_B;
        `SUB:       Data_Out <= Data_A - Data_B;
        `AND:       Data_Out <= Data_A & Data_B;
        `OR:        Data_Out <= Data_A | Data_B;
        default:    Data_Out <= 0;
      endcase
    end

endmodule