// 2對1多工器
module Mux2_1( In1, In2, Sel, Out );

    input   In1, In2, Sel;
    output  Out;

    wire    In1, In2, Sel;
    reg     Out;

    always @( In1, In2, Sel ) begin
        if( !Sel )
            Out <= In1;
        else
            Out <= In2;
    end

endmodule

// 1對2解多工器
module DeMux2_1( In, Sel, Out1, Out2 );

    input   In, Sel;
    output  Out1, Out2;

    wire    In, Sel;
    reg     Out1, Out2;

    always @( In, Sel ) begin
        case( Sel )
            1'b0: Out1 <= In;
            1'b1: Out2 <= In;
        endcase
    end

endmodule
