// ==== 1bit 全加器電路 (編譯器自動實現加法器電路) ====
module full_adder_1bit(cin, x, y, sum, cout);
  input cin, x, y;
  output sum,cout;

  assign {cout, sum} = x+y+cin;
endmodule


// ==== 4bit 全加器電路 (編譯器自動實現加法器電路) ====
module full_adder_4bit(cin,x,y,s,cout);
  input [3:0] x,y;
  input cin;
  output [3:0] s;
  output cout;

  assign {cout,s} = x + y + cin;
endmodule


// ==== 手寫1bit全加器 ====
module full_adder_1bit_manual(cin, x, y, s, cout);
  input cin;//carry in bit
  input x;
  input y;

  output s;
  output cout;//carryout bit

  assign s = x^y^cin;
  assign cout = (x&y)|(x&cin)|(y&cin);
endmodule


// ==== 手寫4bit全加器 ====
module full_adder_4bit_manual(cin, x, y,s,cout);
  input cin;
  input [3:0] x;
  input [3:0] y;

  output [3:0] s;
  output cout;
  wire [3:1] c;

  fulladd stage0(.cin(cin),.x(x[0]),.y(y[0]),.s(s[0]),.cout(c[1]));
  fulladd stage1(.cin(c[1]),.x(x[1]),.y(y[1]),.s(s[1]),.cout(c[2]));
  fulladd stage2(.cin(c[2]),.x(x[2]),.y(y[2]),.s(s[2]),.cout(c[3]));
  fulladd stage3(.cin(c[3]),.x(x[3]),.y(y[3]),.s(s[3]),.cout(cout));
endmodule
