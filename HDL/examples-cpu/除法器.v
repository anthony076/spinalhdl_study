// ==== 除法器設計 - 單步運算(未流水化) ====
module divider_cell
#( parameter N = 5, parameter M = 3)

(
  input clk ,
  input rstn ,
  input en ,

  input [M:0] dividend,
  input [M-1:0] divisor,
  input [N-M:0] merchant_ci, //上一級輸出的商
  input [N-M-1:0] dividend_ci, //原始除數

  output reg [N-M-1:0] dividend_kp,  //原始被除數信息
  output reg [M-1:0] divisor_kp,   //原始除數信息
  
  output reg [N-M:0] merchant,  //運算單元輸出商
  output reg [M-1:0] remainder   //運算單元輸出餘數

  output reg rdy,
) ;

  always @ ( posedge clk or negedge rstn ) begin
    if ( ! rstn ) begin
      // 變數初始化
      rdy <= 'b0 ;
      merchant <= 'b0 ;
      remainder <= 'b0 ;
      divisor_kp <= 'b0 ;
      dividend_kp <= 'b0 ;
      end
    else if ( en ) begin
      // 除法器致能
      rdy             <= 1'b1 ;
      divisor_kp     <= divisor ;  //原始除數保持不變
      dividend_kp     <= dividend_ci ;  //原始被除數傳遞

      // 藉由比大小決定當前的商值
      if ( dividend >= { 1'b0 ,divisor } ) begin
        merchant     <= ( merchant_ci << 1 ) + 1'b1 ; //商為1
        remainder   <= dividend - { 1'b0 , divisor } ; //求餘
        end
      else begin
        merchant     <= merchant_ci << 1 ;  //商為0
        remainder   <= dividend ;        //餘數不變
        end

      end
    else begin
      // 除法器未智能
      rdy             <= 'b0 ;
      merchant       <= 'b0 ;
      remainder       <= 'b0 ;
      divisor_kp     <= 'b0 ;
      dividend_kp     <= 'b0 ;
      end
  end

endmodule

// ==== 流水化除法器，以 29/5=5...4 為例 ====
module    divider_man
    #(parameter N=5,
      parameter M=3,
      parameter N_ACT = M+N-1)
    (
      input                     clk,
      input                     rstn,

      input                     data_rdy ,  //數據致能
      input [N-1:0]             dividend,   //被除數
      input [M-1:0]             divisor,    //除數

      output                    res_rdy ,
      output [N_ACT-M:0]        merchant ,  //商寬度:N
      output [M-1:0]            remainder ); //最終餘數

    wire [N_ACT-M-1:0]   dividend_t [N_ACT-M:0] ;
    wire [M-1:0]         divisor_t [N_ACT-M:0] ;
    wire [M-1:0]         remainder_t [N_ACT-M:0];
    wire [N_ACT-M:0]     rdy_t ;
    wire [N_ACT-M:0]     merchant_t [N_ACT-M:0] ;

    // 初始化第一個運算單元
    divider_cell #(.N(N_ACT), .M(M)) u_divider_step0
    ( .clk              (clk),
      .rstn             (rstn),
      .en               (data_rdy),
      
      // 用被除數最高位 1bit 數據，做第一次單步運算的被除數，高位補0
      .dividend         ({{(M){1'b0}}, dividend[N-1]}),
      .divisor          (divisor),                  
      .merchant_ci      ({(N_ACT-M+1){1'b0}}),   //商初始化為0
      .dividend_ci      (dividend[N_ACT-M-1:0]), //原始被除數
      
      //output
      .dividend_kp      (dividend_t[N_ACT-M]),   //原始被除數訊息傳遞
      .divisor_kp       (divisor_t[N_ACT-M]),    //原始除數訊息傳遞
      .rdy              (rdy_t[N_ACT-M]),
      .merchant         (merchant_t[N_ACT-M]),   //第一次商結果
      .remainder        (remainder_t[N_ACT-M])   //第一次餘數
      );

    genvar i ;
    generate
      for(i=1; i<=N_ACT-M; i=i+1) begin: sqrt_stepx
        divider_cell #(.N(N_ACT), .M(M)) u_divider_step
          (.clk              (clk),
            .rstn             (rstn),
            .en               (rdy_t[N_ACT-M-i+1]),
            .dividend         ({remainder_t[N_ACT-M-i+1], dividend_t[N_ACT-M-i+1][N_ACT-M-i]}),   // 餘數與原始被除數單bit數據拼接
            .divisor          (divisor_t[N_ACT-M-i+1]),
            .merchant_ci      (merchant_t[N_ACT-M-i+1]),
            .dividend_ci      (dividend_t[N_ACT-M-i+1]),
            //output
            .divisor_kp       (divisor_t[N_ACT-M-i]),
            .dividend_kp      (dividend_t[N_ACT-M-i]),
            .rdy              (rdy_t[N_ACT-M-i]),
            .merchant         (merchant_t[N_ACT-M-i]),
            .remainder        (remainder_t[N_ACT-M-i])
          );
    endgenerate

    assign res_rdy       = rdy_t[0];
    assign merchant      = merchant_t[0];  //最後一次商結果作為最終的商
    assign remainder     = remainder_t[0]; //最後一次餘數作為最終餘數

endmodule

// ==== 流水化除法器的 testbench ====
`timescale 1ns/1ns
module test ;
    parameter    N = 5 ;
    parameter    M = 3 ;
    reg          clk;
    reg          rstn ;
    reg          data_rdy ;
    reg [N-1:0]  dividend ;
    reg [M-1:0]  divisor ;

    wire         res_rdy ;
    wire [N-1:0] merchant ;
    wire [M-1:0] remainder ;

    //clock
    always begin
        clk = 0 ; #5 ;
        clk = 1 ; #5 ;
    end

    //driver
    initial begin
        rstn      = 1'b0 ;
        #8 ;
        rstn      = 1'b1 ;

        #55 ;
        @(negedge clk ) ;
        data_rdy  = 1'b1 ;
                dividend  = 25;      divisor      = 5;
        #10 ;   dividend  = 16;      divisor      = 3;
        #10 ;   dividend  = 10;      divisor      = 4;
        #10 ;   dividend  = 15;      divisor      = 1;
        repeat(32)    #10   dividend   = dividend + 1 ;
        divisor      = 7;
        repeat(32)    #10   dividend   = dividend + 1 ;
        divisor      = 5;
        repeat(32)    #10   dividend   = dividend + 1 ;
        divisor      = 4;
        repeat(32)    #10   dividend   = dividend + 1 ;
        divisor      = 6;
        repeat(32)    #10   dividend   = dividend + 1 ;
    end

    //对输入延迟，便于数据结果同周期对比，完成自校验
    reg  [N-1:0]   dividend_ref [N-1:0];
    reg  [M-1:0]   divisor_ref [N-1:0];
    always @(posedge clk) begin
        dividend_ref[0] <= dividend ;
        divisor_ref[0]  <= divisor ;
    end

    genvar         i ;
    generate
        for(i=1; i<=N-1; i=i+1) begin
            always @(posedge clk) begin
                dividend_ref[i] <= dividend_ref[i-1];
                divisor_ref[i]  <= divisor_ref[i-1];
            end
        end
    endgenerate

    //自校验
    reg  error_flag ;
    always @(posedge clk) begin
      # 1 ;
      if (merchant * divisor_ref[N-1] + remainder != dividend_ref[N-1] && res_rdy) begin      //testbench 中可直接用乘号而不考虑运算周期
        error_flag <= 1'b1 ;
        end
      else begin
        error_flag <= 1'b0 ;
        end
    end

    //module instantiation
    divider_man  #(.N(N), .M(M)) u_divider
     (
      .clk              (clk),
      .rstn             (rstn),
      .data_rdy         (data_rdy),
      .dividend         (dividend),
      .divisor          (divisor),
      .res_rdy          (res_rdy),
      .merchant         (merchant),
      .remainder        (remainder));

   //simulation finish
   initial begin
      forever begin
         #100;
         if ($time >= 10000)  $finish ;
      end
   end

endmodule // test