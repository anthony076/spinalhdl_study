// 8對3編碼器
module EnCoder( In, Out );

    input   [7:0] In;
    output  [2:0] Out;

    wire    [7:0] In;
    reg     [2:0] Out;

    always @( In ) begin
        case( In )
            8'b0000_0001:   Out <= 3'b000;
            8'b0000_0010:   Out <= 3'b001;
            8'b0000_0100:   Out <= 3'b010;
            8'b0000_1000:   Out <= 3'b011;
            8'b0001_0000:   Out <= 3'b100;
            8'b0010_0000:   Out <= 3'b101;
            8'b0100_0000:   Out <= 3'b110;
            8'b1000_0000:   Out <= 3'b111;
            default:        Out <= 3'bxxx;
        endcase
    end

endmodule

// 3對8解碼器
module DeCoder( In, Out );

    input   [2:0] In;
    output  [7:0] Out;

    wire    [2:0] In;
    reg     [7:0] Out;

    always @( In ) begin
        case( In )
            3'b000:     Out <= 8'b0000_0001;
            3'b001:     Out <= 8'b0000_0010;
            3'b010:     Out <= 8'b0000_0100;
            3'b011:     Out <= 8'b0000_1000;
            3'b100:     Out <= 8'b0001_0000;
            3'b101:     Out <= 8'b0010_0000;
            3'b110:     Out <= 8'b0100_0000;
            3'b111:     Out <= 8'b1000_0000;
            default:    Out <= 8'bxxxx_xxxx;
        endcase
    end
endmodule
