module Counter1( CLK, RST, Cnt_Num, Cnt_Data );

  parameter Cnt_Num_Size = 2;
  parameter Cnt_Data_Size = 16;

  input   CLK, RST;
  input   [Cnt_Num_Size-1:0] Cnt_Num;
  output  [Cnt_Data_Size-1:0] Cnt_Data;

  wire    CLK, RST;
  wire    [Cnt_Num_Size-1:0] Cnt_Num;
  reg     [Cnt_Data_Size-1:0] Cnt_Data;

  always @( posedge CLK, negedge RST ) begin
      if( !RST )
          Cnt_Data <= 0;
      else
          Cnt_Data <= Cnt_Data + Cnt_Num;
  end

endmodule

module Counter_Up( CLK, RST, Cnt );

    /* 計數資訊 */
    parameter Cnt_SB  = 4;      // 計數寬度
    parameter Cnt_UP  = 1'b1;   // 計數值
    parameter Cnt_Min = 4'd0;   // 計數最小值
    parameter Cnt_Max = 4'd8;   // 計數最大值

    input   CLK, RST;
    output  [Cnt_SB-1:0] Cnt;

    reg     [Cnt_SB-1:0] Cnt = Cnt_Min;

    always @( posedge CLK or negedge RST ) begin
        if( !RST )
            Cnt <= Cnt_Min;
        else if( Cnt == Cnt_Max )
            Cnt <= Cnt_Min;
        else
            Cnt <= Cnt + Cnt_UP;
    end

endmodule

