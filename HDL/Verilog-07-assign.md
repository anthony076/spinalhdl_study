## 賦值概論
- 賦值有三種類型
  - `類型1`，連續賦值語句(Continuous-Assignments): 用於`組合邏輯`，為`wire變數`賦值
    > 連續，代表輸入端的任何變化(等式右側，任何表達式的改變)，會立刻反映到輸出端(等式左側)
    
    > 所有等號右側的值，會立刻且連續執行，不需要等待

  - `類型2`，過程賦值語句(Procedural-Assignments): 用於`組合邏輯或時序邏輯`，在過程代碼塊中為`reg變數`賦值
    > 當過程賦值語句被執行時，才執行賦值操作，又分為非阻塞賦值或阻塞賦值
  
  - `類型3`，過程連續賦值語句(Procedural-Continuous-Assignments): 
    - 不可合成的語句
    - 用於`仿真`，在過程代碼塊中為 wire變數/reg變數賦值

- 比較表: 連續賦值 vs 過程賦值 vs 過程連續賦值

  | 比較項目   | 連續賦值             | 過程賦值                                      | 過程連續賦值                               |
  | ---------- | -------------------- | --------------------------------------------- | ------------------------------------------ |
  | assign語句 | 需要                 | 不需要                                        | 選用，依類型決定                           |
  | 過程代碼塊 | 過程代碼塊外部       | 過程代碼塊內部                                | 過程代碼塊內部                             |
  | 適用類型   | wire類型             | reg類型                                       | reg類型 (assign/deassign)                  |
  | 執行順序   | 並行                 | 阻塞式賦值(=)是串行<br>非阻塞式賦值(<=)是並行 | 串行                                       |
  |            |                      |                                               | wire+reg類型 (force/release)               |
  | 限制       | 不能用於過程代碼塊內 | 不能用於過程代碼塊外                          | 不能用於過程代碼塊外                       |
  | 限制       |                      |                                               | 會啟用占用狀態，並使其他類型的賦值語句失效 |
  | 特殊要求   |                      |                                               | 成對使用                                   |
  | 使用場景   | 組合邏輯             | 組合邏輯/時序邏輯                             | 模擬，常用於直接覆寫子模組內部變數的值     |
  
## 連續賦值語句: wire類型變數 / 不能用於過程代碼塊 / 需要 assign
- 使用 `assign` 語句，為`wire變數`進行賦值，用於描述`組合邏輯`

- 用於 wire 變數賦值的 assign 語句，`不能`用於過程代碼塊
  
  用於 reg 變數賦值的 assign 語句，`可以`用於過程代碼塊

- 連續性賦值總是處於激活狀態，任何操作數的改變，
  都會立刻改變連續性賦值右側表達式的結果，並立刻將結果賦值給左側的變數

- 調整`連續賦值語句`和`實例化語句`的順序，不會影響最後映射出的硬體電路

  在組合邏輯中，調整這些語句的順序，並不會影響建立出來的硬體電路的結果

- 用於組合邏輯的 assign 是`並行的`，改變語句的順序，不影響硬體連接的方式
  ```verilog
  // 以下兩種方式合成出來的硬體相同
  
  // 方法1，
  assign b = c;
  assign d = b + 1;
  
  // 方法2，
  assign d = b + 1;
  assign b = c;

  ```

- 參考，[變數的使用](Verilog-03-variable.md)

## 過程賦值語句: reg類型變數 / 必須在過程區塊 / 不需 assign  
- 重要概念: 
  - 組合邏輯電路
  
    就組合邏輯而言，當輸入訊號傳入後，訊號就會從輸入端依序一路傳遞到輸出端，
    執行過程是順序的，且不需要等待的

    在實作上，讓阻塞式賦值`順序執行`，且每一個阻塞式賦值(=)左側的變數都會立即獲取值，
    實現組合邏輯順序且不需要等待的性質，
    當 always代碼塊執行完畢，所有阻塞式賦值的變數都被執行完畢且更新，並產生出最後的結果

  - 時序邏輯電路

    就時序邏輯而言，當輸入訊號傳入後，`暫存器的輸出不會立刻變更`，
    當輸入到達後會進行等待，等到有 clock 的觸發後，才會將當前暫存器的輸出值進行更新，

    在時序電路中，所有暫存器都會同時被觸發，
    但只有自己的輸入值有變化，輸出才會在時脈結束後被更新，
    在若輸入值沒有變化，暫存器的輸出會保持原來的值，

    因此，時序邏輯是`需要等待和觸發的`，
    且前一級暫存器輸出的值有更新，後一級暫存器的值，才會在下一此觸發時接續更新，
    而不像組合邏輯，一旦輸入變更就會觸發連鎖(連續)反應

    在實作上，為了模擬暫存器需要觸發才會傳遞值，因此，`非阻塞式賦值的語句在實作上不會直接將等號右側的值，直接傳遞給等號左側的變數`，
    而是透過`中間變量`，避免非阻塞式賦值被執行後，位於 <= 左側的變數值被立刻更新，

    b <= a; 的非阻塞式賦值語句，實際上被拆分為
      temp_b = a;
      b = temp_b;

    透過上述的拆分，`讓非阻塞式賦值語句無法在第一次觸發就完成賦值`，
    只有在 always區塊完成後，才完成一句賦值，

    因為非阻塞式賦值語句無法馬上完成，當其中一個賦值語句正處與在`更新的過程`中，下一句賦值語句因為上一句賦值語句未完成，因此`只能讀取舊值`

    所有的非阻塞式賦值的語句都是並行執行的，
    但只有前一句`完成`賦值(消耗1次觸發)，在下一次觸發時，才會更新當前語句，
    其他未進入更新的語句，就會被`賦予舊值`

    因此，時序電路中，
    串接電路上的每個暫存器的值，是依照時間序一個一個更新的，串接電路需要 clk 一級一級的觸發更新
    並接電路上的每個暫存器的值，則是並發更新的

  - 範例
    
    阻塞式賦值(描述組合邏輯)
    
    ```verilog
    // 取樣a賦值b > 取樣b賦值c
    // 若 a = 2，always 完成後，c = b = a = 2
    always @ (a, b)
    begin
      b = a;
      c = b;
    end
    ```

    非阻塞式賦值(描述時序邏輯)

    ```verilog
    always @ (posedge clk)
    begin
      // 每一個非阻塞式賦值語句就是一個暫存器。
      // 且形成一個串接的時序電路1: a -> b -> c -> d -> e
      b <= a;
      c <= b;
      d <= c;
      e <= d;

      //以下形成一個串接的時序電路2: aa -> bb -> cc
      bb <= aa;
      cc <= bb; 

      // 注意 時序電路1 和 時序電路2 是並行的，所以 a 和 aa 會同一個時間更新，
      // b 和 bb 會同一個時間更新，c 和 cc 會同一個時間更新
    end
    ```

    假設初始值都是 0，當 a 變更為 2 時，觸發上述的 always代碼塊
    |         | 第1次觸發  | 第2次觸發     | 第3次觸發     | 第4次觸發     |
    | ------- | ---------- | ------------- | ------------- | ------------- |
    | b <= a; | 將b更新為2 | b=2(完成b<=a) |               |               |
    | c <= b; | c=舊b=0    | 將c更新為2    | c=2(完成c<=b) |               |
    | d <= c; | d=舊c=0    | d=舊c=0       | 將d更新為2    | d=2(完成d<=c) |
    | e <= d; | e=舊d=0    | e=舊d=0       | e=舊d=0       | 將e更新為2    |

- 非阻塞賦值(<=)只能在[過程代碼塊](Verilog-09-process-block.md)中使用，
  但過程代碼塊(initial、always)可使用`阻塞式賦值`或`非阻塞式賦值`
  - 當 always代碼塊描述`組合邏輯`時，使用阻塞式賦值
  - 當 always代碼塊描述`時序邏輯`時，使用非阻塞式賦值

- 阻塞式賦值(=)

    <img src="doc/assign/combination-logic-circuit-in-always-block.png" width=80% height=auto>

  - 只能用於`組合邏輯`

  - 屬於`順序`執行，前一個阻塞式賦值語句執行完畢後，才會繼續執行下一個賦值語句
  
    執行完畢指的是，會`讀取等號右側變數的值`，並`完成賦值給等號左側的變數`
  
  - always代碼塊內的阻塞式賦值，會順序執行且立刻執行完畢，直到全部賦值語句完成後，always 代碼塊才會結束

- 非阻塞式賦值(<=)
  
    <img src="doc/assign/sequential-logic-circuit-in-always-block.png" width=80% height=auto>

  - 只能用於`時序邏輯`
    
    用於描述暫存器的賦值，每一個非阻塞式賦值的語句，就代表其中一個暫存器的賦值，

    賦值的`右側`代表輸入端，賦值的`左側`代表暫存器的輸出

  - 屬於`並行`執行，
    
    所有的非阻塞式賦值的語句都會同時執行，但不是每一句都會完成賦值的語句，

    賦值語句是否完成，依照等式`右側的輸入變數`是否有更新決定，
      - 若右側的輸入變數值`有`更新，賦值語句(<=)會在always代碼塊`執行期間進行更新`，在always代碼塊`結束後完成賦值`
      - 若右側的輸入變數值`無`更新，賦值語句(<=)會採用`舊的輸入變數值`，並且處於未完成賦值的狀態

    由於完成賦值會在always代碼塊結束後才會完成，
    因此，每次觸發只會完成一個非阻塞式賦值語句的賦值 (只會完成一個暫存器的更新)

    例如，`b <= a; c <= b;`，若 a 的值有變化，
    - 在第1次觸發時，`b <= a` 的語句會進入更新的狀態，
      - `c <= b` 會因為上一句未完成賦值，因此 c=b的舊值，
      - 觸發結束後，`b <= a`才完成賦值，
  
    - 在第2次觸發時，
      - `c <= b` 因為`b <= a`已完成賦值，因此 c=b的新值，
      - 觸發結束後，`c <= b`才完成賦值，
  


- 注意事項
  - 不要在一個過程結構中混合使用阻塞賦值與非阻塞賦值，時序不容易控制
  - 在仿真電路時，initial 塊中，訊號都是透過 wire 傳遞，大都是使用阻塞賦值(=)

## 過程連續賦值: reg + wire類型變數 / 必須在過程區塊 / 需要 assign或force / 成對使用 
- 不可合成的語句

- 在過程區塊中使用assign語句，就是過程連續賦值

- 常用於模擬而不是用於合成硬體電路，可以`直接覆寫`模組內部變數的值，優先權比連續賦值和過程賦值高

- 過程連續賦值的語句有兩種
  - 語法1，`assign / deassign` : 優先權低，只能用於 reg類型變數
    - assign / deassign 必須成對使用

    - assign: 使reg變數進入過程連續賦值的`占用狀態`，
      
      在過程連續賦值的占用狀態下，
      `只有過程連續賦值語句是有效的，普通的過程賦值語句是失效的`

      直到執行 deassign 語句以取消 assign 語句的占用狀態後，
      普通的過程賦值語句才會重新恢復有效，因為 reg 可以保存值，該變數會保持`執行 assign 語句之後`所保存的值??

      未取消占用狀態前，後面的過程連續賦值語句，會覆蓋前一句過程連續賦值的結果

    - deassign: 使reg變數`取消`過程連續賦值的`占用狀態`，恢復過程連續賦值的有效性

  - 語法2，`force / release`: 優先權高，用於 wire變數賦值 + reg類型變數
    
    用法與 assign/deassign 相同，但可用於wire變數和reg變數，

    優先權較 assign 高，使用 force 語句後，會進入 force 語句的占用狀態，
    並使得`assign的過程連續賦值語句失效`，和`普通的過程賦值語句失效`，
    
    直到執行 release 語句以取消 force 語句的占用狀態後，普通的過程賦值語句才會重新恢復有效，
    - 若被覆寫的變數是 wire 類型，因為 wire 不能保存值，該變數會回到`執行 force 語句之前`所保存的值
    - 若被覆寫的變數是 reg 類型，因為 reg 可以保存值，該變數會保持`執行 force 語句之後`所保存的值

- 範例，過程連續賦值 assign/deassign 範例
  ```verilog
  reg [3:0] out;
  initial begin
    out = 4'b0000;        // 過程賦值語句
    #10;
    assign out = a & b;   // 過程連續賦值語句1，reg 進入過程賦值的占用狀態，並使過程賦值語句失效
    #10;
    assign out = c & d;   // 過程連續賦值語句2
    assign out = e & f;   // 過程連續賦值語句3
    deassign out;			    // 取消 reg 過程賦值的占用狀態，重新使過程賦值語句生效
  end
  ```

- 範例，使用 force 直接覆寫模組內部變數的值
  
  可以透過外部的引腳改變模組內部變數的值，但透過過程連續賦值更直覺更方便
  ```verilog
  `timescale 1ns/1ns

  module add(
    input [1:0] a,
    input [1:0] b,
    output [2:0] c
  );
    assign c = a + b;
  endmodule

  module test;
    reg  [1:0] a1,b1;
    wire [2:0] c1;

    // 實例化模組
    add u_add(
      .a(a1),
      .b(b1),
      .c(c1)
    );

    // 輸入訊號初始化
    initial begin
      $dumpfile("result.vcd");
      $dumpvars;

      // === 寫法1，使用 force 直接覆寫模組內 wire 變數的值 ===
      // a1 = 2'b01;
      // b1 = 2'b01;
      // #20  force u_add.a = 2'b10;  
      // #10  release u_add.a ;

      // === 寫法2，透過外部的引線，間接覆寫模組內 wire 變數的值 ===
      a1 = 2'b01;
      b1 = 2'b01;
      #20 
      a1 = 2'b10;
      #10
      $finish;
    end
  endmodule
  ```

- 範例，過程連續賦值用於模擬的硬體建模中
  ```verilog
  module test_dff_asyn_clear(clk,clr,d,q);
    input clk,clr;
    input d;
    output q;
    reg q;
    
    initial begin
      $dumpfile("result.vcd");
      $dumpvars;
    end

    always@(clr) begin
      if(!clr)
        assign q =0 ;
      else
        deassign q;
    end

    always@(posedge clk) begin
      q = d; 
    end

  endmodule
  ```

- 範例，利用`非阻塞賦值`取代`過程連續賦值`

  部分合成器不支持過程連續賦值的語句建模時(例如，quartus2 v13.1.0)，
  可利用非阻塞賦值取代

  - 過程連續賦值的建模
    ```verilog
    module dff_assign(
        input       rstn,
        input       clk,
        input       D,
        output reg  Q
    );
    
        always @(posedge clk) begin
            Q <= D ;       //Q = D at posedge of clock
        end
    
        always @(negedge rstn) begin
            if(!rstn) begin
                assign Q = 1'b0 ; //change Q value when reset effective
            end
            else begin        //cancel the Q value overlay,
                deassign Q ;  //and Q remains 0-value until the coming of clock posedge
            end
        end
    
    endmodule
    ```

  - 非阻塞賦值的版本
    ```verilog
    module dff_normal(
        input       rstn,
        input       clk,
        input       D,
        output reg  Q
    );

        always @(posedge clk or negedge rstn) begin
            if(!rstn) begin   //Q = 0 after reset effective
                Q <= 1'b0;
            end
            else begin
                Q <= D ;       //Q = D at posedge of clock
            end
        end

    endmodule
    ```



## always 的設計規範
- 為了避免在 always代碼塊中使用不正確的語句，造成輸出結果不如預期，
  建議使用 always 時使用以下規則

- 規則1，建立時序邏輯(暫存器或栓鎖器)時，使用非阻塞賦值
- 規則2，建立組合邏輯時，使用阻塞式賦值
- 規則3，在同一個 always代碼塊中，建立時序邏輯+組合邏輯時，使用非阻塞賦值
- 規則4，在同一個 always代碼塊中，不要同時使用阻塞賦值(=)和非阻塞賦值(<=)的語句
- 規則5，不要在一個以上的 always代碼塊中，對同一個變量進行賦值，會造成回授
- 規則6，用$strobe系統任務來顯示用非阻塞賦值的變量值
  > 在所有$display命令之後，更新非阻塞賦值
- 規則7，在賦值時不要使用#0延遲
  > ＃0強制分配給“非活動事件隊列

- 錯誤範例，利用阻塞賦值描述組合邏輯，順序錯誤
  ```verilog
  module pipeb1 (q3, d, clk);
  output [7:0] q3;
  input [7:0] d;
  input clk;
  reg [7:0] q3, q2, q1;

  always @(posedge clk)
  begin
  q1 = d;
  q2 = q1;
  q3 = q2;
  end

  endmodule
  ```

- 不推薦範例，利用阻塞賦值描述組合邏輯，可行但不推薦

  若要使用阻塞賦值描述組合邏輯，應該使用`倒敘`的方式，
  若使用正敘的方式描述電路，中間電路會被簡化，造成電路錯誤，詳見上一個範例
  
  ```verilog
  module pipeb2 (q3, d, clk);
  output [7:0] q3;
  input [7:0] d;
  input clk;
  reg [7:0] q3, q2, q1;

  always @(posedge clk)
  begin
  q3 = q2;  // output，q3
  q2 = q1;
  q1 = d;   // input，d
  end

  endmodule
  ```

- 不推薦範例，將暫存器語句區分太多alway塊

  同樣條件下，多個 always 都是並行處理的，無法確定哪一個 always 會先執行
  ```verilog
  module pipeb3 (q3, d, clk);
  output [7:0] q3;
  input [7:0] d;
  input clk;
  reg [7:0] q3, q2, q1;

  always @(posedge clk) q1 = d;
  always @(posedge clk) q2 = q1;
  always @(posedge clk) q3 = q2;

  endmodule
  ```

- 正確範例，使用非阻塞賦值描述時序電路
  ```verilog
  module pipen1 (q3, d, clk);
  output [7:0] q3;
  input [7:0] d;
  input clk;
  reg [7:0] q3, q2, q1;

  always @(posedge clk) 
  begin
  q1 <= d;
  q2 <= q1;
  q3 <= q2;
  end

  endmodule
  ```

## Ref 
- [阻塞赋值与非阻塞赋值](https://blog.csdn.net/Times_poem/article/details/52032890)
- [設計規範](https://blog.51cto.com/u_15057841/4037601)
- [設計規範](https://zhuanlan.zhihu.com/p/72034401)