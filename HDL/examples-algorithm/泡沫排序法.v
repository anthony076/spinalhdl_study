// function bubble_sort (array, length) {
//     var i, j;
//     for(i from 0 to length-1){
//         for(j from 0 to length-1-i){
//             if (array[j] > array[j+1])       // 比較大小
//                 swap(array[j], array[j+1])   // 交換
//         }
//     }
// }


assign out_valid = (sort_cnt == 4);

 always@(posedge clk)begin
         if(rst)
            sort_idx <= 0;
          else if(in_valid)
            sort_idx <= ~sort_idx;
       end
       

always@(posedge clk)begin
  if(rst)
  sort_cnt <= 0;
else if(in_valid)
  sort_cnt <= (sort_cnt == 4) ? sort_cnt : sort_cnt+1;
end

always@(posedge clk)begin
  if(in_valid & ~sort_idx)begin
    for(i=0;i<4;i=i+2)begin
      array[i  ] <= (array[i] > array[i+1]) ? array[i+1] : array[i  ];
      array[i+1] <= (array[i] > array[i+1]) ? array[i  ] : array[i+1];
    end
  end
  else if(in_valid & sort_idx)begin
    for(i=1;i<3;i=i+2)begin
      array[i  ] <= (array[i] > array[i+1]) ? array[i+1] : array[i  ];
      array[i+1] <= (array[i] > array[i+1]) ? array[i  ] : array[i+1];
    end
  end