// i = 1
// while i < length(A)
//     j = i
//     while j > 0 and A[j-1] > A[j]
//         swap A[j] and A[j-1]
//         j = j - 1
//     end while
//     i = i + 1
// end while

reg [6:0]bit_array;
reg [2:0]insert_array[5:0];

always@(*)begin
  if(rst)begin
  for(i=0;i<6;i=i+1)
    bit_array[i] = 0;
  bit_array[6] = 1;
end
else if(in_valid)begin
  for(i=0;i<6;i=i+1)
    bit_array[i] = (data_in > insert_array[i]) ? 1 : 0;		  
end
else begin
  for(i=0;i<6;i=i+1)
    bit_array[i] = 0;
  bit_array[6] = 1;
end
end

always@(posedge clk)begin
  if(in_valid)begin
  for(i=6;i>1;i=i-1)begin
    insert_array[i-1] <= (bit_array[i-:2]==2'b10) ? data_in : 
								 (bit_array[i-:2]==2'b11) ? insert_array[i-1] : insert_array[i];		
  end
end
end 

always@(posedge clk)begin
  if(in_valid)
  out_valid <= 1;
else
  out_valid <= 0;
end