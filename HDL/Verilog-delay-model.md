## 延遲模型 Delay Model
- 延遲模型，輸入變化到輸出變化之間的延遲，主要用在對行為仿真延時建模並進行測試
  
## VHDL 中的延遲模式
- 在 VHDL 中，delay-model 有以下幾種
  - `傳輸延遲`模型（Transport delay model）

    信號會根據定義的傳輸延遲，`推遲至指定時間後`被賦值或更改，
    經延遲後的新信號波形將會把從延遲時間開始後的源信號波形刷新，原信號的活動列表（event list）會被取代

  - `慣性延遲`模型（Inertial delay model），常用於過濾訊號

    信號會根據定義的延遲，延後賦值或更改，同時具備`覆蓋抹去維持時間低於延遲時間的電平信號`，
    持續時間過小的信號會被吃掉
  
  - `拒絕-慣性延遲`模型（Reject-Inertial delay model)，常用於手動指定過濾訊號

    改變慣性延遲模型中`過濾信號的判定條件`，在慣性延遲模型中，延遲條件同時也是過濾條件，
    在拒絕-慣性延遲模型，透過 `Reject <過濾時間> ns INERTIAL` 可以自定義過濾條件

  - 增加延遲 (Delta Delay)，預設的延遲時間
    
    訊號在傳遞的過程中，每一個組件都會發生一些無限小的延遲，稱為 delta delay，訊號轉變過程中延遲，
    若不指名延遲時間時，delta-delay 為預設的傳輸延遲時間，

    增量延遲沒有可測量的單位，但從數字電子硬件設計的角度來看，應該將增量延遲視為可以測量的最小時間單位


- 範例，比較三種延遲模型的差異
  - 圖解

    <img src="doc/delay-model/hdl-delay-model-compare.png" width=600 height=auto>
    
  - 定義傳輸延遲模型範例
    ```scala
    sig_x <= TRANSPORT '1' AFTER 5ns, '0' AFTER 10ns

    // sig_x 會在 5ns 後被賦值為 1，10ns 後被賦值為 0，
    // 注意，10ns 是相對於起始信號，不是變成 1 之後的 10ns
    ```

  - 定義傳輸延遲範例
    ```scala
    // 定義傳輸延遲
    sig_x <= TRANSPORT '1' AFTER 1ns, '0' AFTER 4ns,
                      '1' AFTER 10ns, '0' AFTER 12ns;

    // 定義慣性延遲
    sig_i <= sig_x AFTER 3ns;
    // sig_i 在延遲3ns後，獲得 sig_x信號的賦值
    // sig_x 信號中，小於持續時間 3ns 的電平信號會被過濾，不會出現在 sig_i 中
    ```

  - 定義拒絕-慣性延遲範例
    ```scala
    // 定義傳輸延遲
    sig_x <= TRANSPORT '1' AFTER 1ns, '0' AFTER 4ns,
                       '1' AFTER 10ns, '0' AFTER 12ns;

    // 定義拒絕-慣性延遲
    sig_r <= REJECT 2ns INERTIAL sig_x AFTER 3ns;
    // sig_r 在延遲3ns後，獲得 sig_x信號的賦值
    // sig_x 信號中，小於持續時間 2ns 的電平信號會被過濾，不會出現在 sig_r 中
    ```
  
## Verilog 中的延遲模型
- 一般用於連續賦值，用於控制任意操作數發生變化到賦值左端獲得新數值的時間延遲，區分為
  - 普通賦值延時，
  - 隱式延時
  - 聲明延時
 
- 三種類型時延都歸屬於`慣性延時`，與VHDL中的慣性延遲模型處理原則一致

- 範例1，普通延時
  ```scala
  wire Z, A, B;
  assign #10 Z = A|B  // A|B計算結果，延遲 10個時間單位後，才賦值給 Z
  ```

## Ref
- [VHDL中的delta cycle](https://blog.csdn.net/weixin_58946213/article/details/122321280)
- [simulation cycle](http://www.people.vcu.edu/~rhklenke/tutorials/vhdl/modules/m10_23/sld037.htm)
- [關於延遲模型（Delay Model）](https://blog.csdn.net/weixin_58946213/article/details/119358162)