## Module 基本概念
- 模塊是具有一定功能的電路結構組合，可以視為是組成大型電路的基礎積木

  根據`功能`將系統劃將系統分割成更小的子模塊，並規劃各模塊的接口關係，
  一個好的建模設計要層次清晰，各子模塊功能明確，這樣不僅使自己寫代碼時思路清晰，而且也利於別人閱讀你的代碼

- 可以只有一個主模塊，也可以使用模塊嵌套
  
  各模塊連接完成整個系統需要一個頂層模塊（top-module）

- 模塊之間是並行運行的

- 模塊是分層的，高層模塊通過調用、連接低層模塊的實例來實現複雜的功能

- [硬體邏輯區分組合邏輯和時序邏輯](Verilog-00-basic.md#硬體電路區分-組合邏輯和時序邏輯)

- [描述硬體的三種方式](Verilog-00-basic.md#描述硬體的方式)

## 自定義模塊
- 注意，[宣告模組或實際化模組時，對端口的類型有要求](Verilog-03-variable.md#宣告模組或實際化模組時對端口類型的要求)

- 注意，將`端口變數宣告為多個類型`時，要注意使用 ANSI-style 或 non-ANSI-style
  - non-ANSI-style: 將同一個變數，但不同的類型宣告分開
  - ANSI-style: 將同一個變數，但不同的類型宣告寫在同一行
  - 參考，[Error-10759](Verilog-errors.md#error-10759-object-out-declared-in-a-list-of-port-declarations-cannot-be-redeclared-within-the-module-body)

- `語法1`，只定義輸入變數
  ```verilog
  module 模組名(輸出引腳1, 輸出引腳2, ..., 輸入引腳1, 輸入引腳2, ...);
    
    // ==== 定義硬體電路需要的材料 ====
    
    // 定義端口引腳
    output 輸出引腳1
    output 輸出引腳2
    input 輸入引腳1
    input 輸入引腳2
    
    // 定義訊號傳遞元件 (定義變數或常數)
    wire or_wire;
    reg sum, count;

    // ==== 以下為描述硬體電路 ====
    // 參考，描述硬體的三種方式

    // 透過邏輯閘建立硬體電路
    or o1(or_wire, x)

    // 透過數據流建立硬體電路
    assign sum=a^b

    // 透過功能描述建立硬體電路

  endmodule
  ```

- `語法2`，簡化輸入變數的宣告，可以直接在參數列表中，定義引腳的類型
  ```verilog
  module 模組名(output 輸出引腳1, output 輸出引腳2, ..., input 輸入引腳1, input 輸入引腳2, ...);
  ```

- `語法3`，定義輸入變數 + 內部常數

  注意，常數也可以不定義在 module 的宣告中，可以定義在 module 的內部，

  參考，[常數的使用](Verilog-02-const-and-datatype.md)

  ```verilog
  module 模組名 #(parameter N = 4) (input x, input [N-1:0] y , output [N-1:0] z);
               ^^^^^^^^^^^^^^^^^^  定義常數區
                                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  定義引數區
  ```

  可寫成以下形式
  ```verilog
  module 模組名 #(parameter N = 4) (
    input x, 
    input [N-1:0] y, 
    output [N-1:0] z
  );
  
  ```

## 導入模塊
- 語法，`include 模塊名.v
- 和 C 語言的 include 一致
- 範例

  <img src="doc/module/include-module.png" width=80% height=auto>  

## 調用模組(透過實例化模組建立元件)
- 依照順序傳入引腳
  ```verilog
  模組名 自定義實例名(引腳1, 引腳2, 引腳3, ...);
  ```

- 具名傳入引腳
  ```verilog
  模組名 自定義實例名(
    .out(引腳1), 
    .x(引腳2), 
    .y(引腳3)
  );
  ```

## 修改模組的內部參數
- 參考，[建立參數](Verilog-02-const-and-datatype.md)

- 修改內部參數有兩種方法
  - 方法1，透過 defparam語句，適合修改嵌套模組的內部參數
  - 方法2，透過 #()語句，適合修改單層模組的內部參數

- `方法1`，透過 defparam語句
  - 語法1，在`實例化`時修改模組內部的參數
    ```verilog
    // 注意，是實例名不是模組名
    defparam 自定義實例名.參數名 = 新值;

    // 實例化模組
    模組名 自定義實例名( ... )
    ```

  - 語法2，`在模組中`，修改嵌套模組的內部參數
    ```verilog
    defparam 
      模組名.實例名1.實例名2.常數1 = 新常數值1, 
      模組名.實例名1.實例名2.常數2 = 新常數值2; 
    ```

- `方法2`，透過 #()語句
  - 語法1，具名修改參數
    ```verilog
    模組名 #(.參數名1(新參數值1), .參數名2(新參數值2),) 自定義實例名(
      .引腳名1(傳遞參數1), 
      .引腳名2(傳遞參數2),
      .引腳名3(傳遞參數3),
    );
    ```

  - 語法2，按參數宣告的順序
    ```verilog
    模組名 #(.參數名1(新參數值1), .參數名2(新參數值2),) 自定義實例名(
      .引腳名1(傳遞參數1), 
      .引腳名2(傳遞參數2),
      .引腳名3(傳遞參數3),
    );
    ```

- 範例，defparam 和 #() 的等效寫法

  透過 #() 修改模組內部參數
  ```verilog
  mymodule #(.aa(123), .bb(456)) m1(x, y, z);
  ```

  透過 defparam 修改模組內部參數
  ```verilog
  defparam m1.aa = 123;
  defparam m1.bb = 456;

  mymodule m1(x, y, z);
  ```

- 範例，在模組中，修改嵌套模組的內部參數
  ```verilog
  // 定義嵌套模組，A --> B --> C
  module A;
    wire W;
    B b1( );
  endmodule
  
  module B;
    wire W
    C c1( );
    C c2( );
  endmodule
  
  module C;
    parameter P = 0;
  endmodule
  
  // 定義主模組
  module Top;
    // 修改嵌套模組的參數
    defparam 
      A.b1.c1.P = 2,
      A.b1.c2.P = 3;
  endmodule       
  ```

## [基本模塊] 內建邏輯閘模塊
- 內建的模組
  - 基本邏輯閘: and、or、xor、nand、nor、buf、not
  - 三態門: bufif0、bufif1、notif0、notif1
  - mos開關: nmos、pmos、cmos、rnmos、rpmos、rcmos、
  - 雙向開關: tran、tranif0、tranif1、rtran、rtranif0、rtranif1、
  - 上拉/下拉電阻: pullup、pulldown

- buffer元件，可組合成以下三個功能
  <img src="doc/basic/usage-of-buffer.png" width=80% height=auto>

  - 作為 緩衝器
  - 作為 not-gate = 緩衝器 + 反向器
  - 作為 三態門(Tri-state-buffer) = 緩衝器 + 控制器
  
- 由 buffer 延伸出的元件 
  - 範例，`緩衝器`，例如，buf b1(輸出接腳, 輸入接腳);

    <img src="doc/basic/buffer-true-table.png" width=40% height=auto>
    
  - 範例，`多輸出緩衝器`，例如，buf b1(輸出接腳1, 輸出接腳2, 輸入接腳);

  - 範例，`not-gate`，例如，not n1(輸出接腳, 輸入接腳);

    <img src="doc/basic/not-true-table.png" width=40% height=auto>

  - 範例，`多輸出Not元件`，例如，not n1(輸出接腳1, 輸出接腳2, 輸入接腳);

  - 範例，`bufif1元件`，例如，bufif1 b1(輸出接腳, 輸入接腳, 控制接腳);

    <img src="doc/basic/bufif1-true-table.png" width=80% height=auto>

  - 範例，`bufif0元件`，例如，bufif0 b1(輸出接腳, 輸入接腳, 控制接腳);

    <img src="doc/basic/bufif0-true-table.png" width=80% height=auto>

  - 範例，`notif1元件`，例如，notif1 n1(輸出接腳, 輸入接腳, 控制接腳);

    <img src="doc/basic/notif1-true-table.png" width=80% height=auto>

  - 範例，`notif0元件`，例如，notif0 n1(輸出接腳, 輸入接腳, 控制接腳);

    <img src="doc/basic/notif0-true-table.png" width=80% height=auto>
  

## 範例集

- 範例，建立模組範例

  <img src="doc/module/define-module.png" width=50% height=auto>

  ```verilog
  module mymodule(o, x, y);
    // ==== 定義端口 ====
    output o;
    input x,y;

    // ==== 定義信號類型 ====

    // 定義接線
    wire or_wire, not_wire;

    // ==== 定義邏輯功能 ====

    // 透過實例化模組建立元件，
    // 透過內部的接線可以建立元件之間的關係
    or o1(or_wire, x, y);   // 建立 or 元件
    not n1(not_wire, y);    // 建立 not 元件
    and a1(o, or_wire, not_wire)  // 建立 and 元件
    
  endmodule // mymodule
  ```
