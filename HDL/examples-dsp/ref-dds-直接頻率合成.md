## DDS: Direct-Digital-Synthesizer 直接頻率合成
- 使用場景
  
  從一個單一（或混合）的頻率源中產生任意波形和頻率

## Ref
- 系列文
  - [1_正弦波發生器的verilog實現](https://blog.csdn.net/qq_34769608/article/details/109984388)
  - [2_DDS發生器的頻率控製字原理和基本結構](https://blog.csdn.net/qq_34769608/article/details/109998554)
  - [3_DDS發生器的verilog實現](https://blog.csdn.net/qq_34769608/article/details/110058982)

- [Verilog DDS 設計](https://www.runoob.com/w3cnote/verilog-dds.html)


