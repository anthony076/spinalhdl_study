
## [Error] illegal ... assignment.

  用過程語句給一個net類型的或忘記聲明類型的信號賦值。
  
## [Error] XXX has illegal output port specification.
  
  將實例的輸出連接到聲明為register類型的信號上。

## [Error] incompatible declaration, 訊號名 ...

  將模塊的輸入信號聲明為register類型。

## [Error-10759] object out declared in a list of port declarations cannot be redeclared within the module body
  
  若編譯器限制使用 ANSI-style 的方式宣告變量，但代碼卻使用 non-ANSI-style 的方式編碼，
  就會出現此錯誤

  - IEEE-1995標準使用`傳統樣式的端口列表(non-ANSI style)`
    
    一個變量如果既是端口又是內部變量，需要分開宣告

    ```verilog
    // non-ANSI style
    module top(
      input in,
      output out  // out 宣告為端口變量
    )

    reg out;  // out 宣告為內部變量

    endmodule
    ```

  - Verilog-2001 以後允許將既是端口又是內部變量寫成單句(ANSI-style)
    
    一個變量如果既是端口又是內部變量可以寫在端口宣告的列表中，
    並寫成同一行
    
    ```verilog
    // ANSI style
    module top(
      input in,
      output reg out  // out 宣告為端口變量+內部變量
    )

    endmodule
    ```