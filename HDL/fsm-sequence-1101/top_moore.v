module top(
	input clk,
	input rst_n,
	input in,
	output reg out
);
  reg [4:0] currentState;
  reg [4:0] nextState;

  parameter s0=5'b00001;  // 1
  parameter s1=5'b00010;  // 2
  parameter s2=5'b00100;  // 4
  parameter s3=5'b01000;  // 8
  parameter s4=5'b10000;  // 16
  
  // ==== 第一段: 設置當前狀態 ====
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)
      currentState <= s0;
    else
      currentState <= nextState;
  end
  
  // ==== 第二段: 條件跳轉/下一個狀態 ====
  always@(*)begin
    case(currentState) 
      s0:if(in)
          nextState=s1;
        else
          nextState=s0;
      s1:if(in)
          nextState=s2;
        else
          nextState=s0;
      s2:if(in)
          nextState=s2;
        else
          nextState=s3;
      s3:if(in)
          nextState=s4;
        else
          nextState=s0;
      s4:if(in)
          nextState=s1;
        else
          nextState=s0;
      default:nextState=s0;
    endcase
  end

  // ==== 第三段: 設置輸出 ====
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)
      out <= 0;
    else if(currentState==s4)
      out <= 1;
    else
      out <= 0;
end
 
endmodule