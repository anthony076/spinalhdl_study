
`timescale 1ns / 1ns
//`include "top_moore.v"
`include "top_mealy.v"

module tb;
	
  reg clk;
	reg rst_n;
	reg in;
	wire out;

  localparam S0 = 5'b00001;
  localparam S1 = 5'b00010;
  localparam S2 = 5'b00100;
  localparam S3 = 5'b01000;
  localparam S4 = 5'b10000;

  // 實例化
	top uut (
		.clk(clk), 
		.rst_n(rst_n), 
		.in(in), 
		.out(out)
	);

  // ==== 設置輸出 ====
  initial begin
    $dumpfile("result.vcd");
    $dumpvars;
  end

  // ==== 設置 clock ====
	initial clk=0;
	always #10 clk=~clk;

  // ==== 設置 rst ====
	initial begin
		in=0;
		rst_n = 0;
		#20;
		rst_n = 1;
	end

// ==== 顯示狀態 ====
// 顯示當前狀態
reg [15:0] currentState ;
always@(*)begin
	case(uut.currentState)
		S0: currentState = "S0";
		S1: currentState = "S1";
		S2: currentState = "S2";
		S3: currentState = "S3";
    S4: currentState = "S4";
		default:currentState = "S0";
	endcase
end

// 顯示下一個狀態
reg [15:0] nextState ;
always@(*)begin
	case(uut.nextState)
		S0: nextState = "S0";
		S1: nextState = "S1";
		S2: nextState = "S2";
		S3: nextState = "S3";
    S4: nextState = "S4";
		default:nextState = "S0";
	endcase
end

// ==== 設置輸入訊號 ====
reg [7:0] data;
always@(posedge clk or negedge rst_n)begin
  if(!rst_n)
    // 以下序列參考狀態表，為狀態前進的序列
    //  S0(1) -> S1(0) -> S0(1) -> S1(1) -> S2(0) -> S3(1) -> S4(1) -> S1(0) -> S0 
    data <= 8'b1011_0110; 
  else
    // 左移: 將最高位移到最低位，其餘往前一位
    // 例如: 1111_0000 -> 1110_0001 -> 1100_0011 -> ...
    data <= {data[6:0],data[7]};
end

always@(posedge clk or negedge rst_n)begin
  if(!rst_n)
    in <= 0;
  else
    in <= data[7];
end

// ==== 設置結束條件 ====
always begin
  #10
  if ( $time >=450) $finish;
end
      
endmodule
