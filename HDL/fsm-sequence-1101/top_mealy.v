module top(
	input clk,
	input rst_n,
	input in,
	output reg out
);
  reg [3:0] currentState;
  reg [3:0] nextState;

  parameter s0=4'b0001;
  parameter s1=4'b0010;
  parameter s2=4'b0100;
  parameter s3=4'b1000;

  // ==== 第一段: 配置當前狀態 ====
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)
      currentState <= s0;
    else
      currentState <= nextState;
  end
	 
  // ==== 第二段: 條件跳轉/下一個狀態 ====
  always@(*)begin
    case(currentState) 
      s0:
        if(in) nextState = s1;
        else nextState = s0;

      s1:
        if(in) nextState = s2;
        else nextState = s0;

      s2:
        if(in) nextState = s2;
        else nextState = s3;

      s3:
        if(in) nextState = s0;
        else nextState = s0;

      default:nextState=s0;
      
    endcase
  end

  // ==== 第三段: 設置輸出 ====
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)
      out<=0;
    else if(currentState == s3 && in == 1'b1)
      out<=1;
    else
      out<=0;
  end
  
endmodule