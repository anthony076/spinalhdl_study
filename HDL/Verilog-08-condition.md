## 條件語句的概述
- if-else 和 case 的語句，對應硬體電路的選擇器，
  
  if-else 和 case 都可以實現相同的功能，
  但 if-else 是利用`二路選擇器`串聯而成，而不是多工選擇器，電路實現較複雜，
  且 `if-else 是有優先級`的，只有if或者當前一級else if的條件不滿足才會進行後面的判斷，
  case 的硬體電路就是多路選擇器，電路實現較簡單

  且 if-else 的語法容易查升歧異，需要添加額外的 begin ... end 語句來避免歧異，
  對於多層嵌套的 if 語法，若要確保不會產生歧異，在使用上就會較複雜，

  因此，若不需要有優先級的選擇器，推薦使用 case 來取代 if-else 的語句

- if-else 和 case 要盡量列出所有可能的狀況，否則對於省略的條件，會添加栓鎖器來保持原值

- 對於條件值的判斷，盡量寫出位元數，避免產生位寬不匹配的情況

## if-else 語句
- 注意，if-else 必須在 [過程區塊](Verilog-09-process-block.md) 中使用

- 注意，若 if-else 的語句`沒有使用 else`，會添加`栓鎖器(Latch)`的元件

  <img src="doc/condition/dff-vs-dlatch.png" width=80% height=auto>

  若沒有使用 else，代表條件不符合時，輸出維持原值，此為栓鎖器(latch)的特性
  
- if-else 的硬體會被翻譯為`多個暫存器和比較器`

  <img src="doc/condition/hardware-of-ifelse.png" width=80% height=auto>

- 語法，標準用法，單層 if-else 用
  ```verilog
  if (條件1)       
    true_statement1 ;
  else if (條件2)
    true_statement2 ;
  else if (條件3)
    true_statement3 ;
  else
    default_statement ;
  ```

- 語法，避免歧異用，用於嵌套的 if-else
  ```verilog
  if (條件1) begin
    true_statement1 ;
  end 
  else if (條件2) begin
    true_statement2 ;
  end 
  else if (條件3) begin
    true_statement3 ;
  end 
  else begin
    default_statement ;
  end
  ```

- 支持簡化語法
  > if(expression) 等效於 if( expression == 1 )

- 範例，四路選擇器
  
  根據 sel 的值，選擇要輸出的 port
  ```verilog
  module onebit_mux4to1(
    input [1:0] sel,
    input  p0,
    input  p1,
    input  p2,
    input  p3,
    output  out);

    reg result ;

    always @(*) 
    begin
        if (sel == 2'b00)
            result = p0 ;
        else if (sel == 2'b01)
            result = p1 ;
        else if (sel == 2'b10)
            result = p2 ;
        else
          result = p3 ;
    end

    assign out = result ;

  endmodule
  ```

  testbench
  ```verilog
  `timescale 1ns/1ns
  `include "top.v"

  module test;
    reg [1:0] sel;
    wire sout;

    // 實例化測試模組
    onebit_mux4to1 m1(
      .sel(sel),
      .p0(1'b1),
      .p1(1'b1),
      .p2(1'b0),
      .p3(1'b0),
      .out(sout)
    );

    // 設置輸入訊號
    initial begin
      $dumpfile("result.vcd");
      $dumpvars;

      sel = 0;
      #10 sel = 1;
      #10	sel = 2;
      #10	sel = 3;
      #10;
      
      $finish;

    end

  endmodule
  ```

- 範例，容易引起歧異的範例
  ```verilog
  if(en)
      if(sel == 2'b1)
          sout = p1s ;
  // 發生歧異處，
  // 根據就近原則，else 仍然屬於內層if的，減少縮排不會改變else的作用域
  else
    sout = p0 ;
  ```

  避免歧異的寫法
  ```verilog
  if(en)
    begin
      if(sel == 2'b1)
          sout = p1s ;
    end
  else
    sout = p0 ;
  ```

## case 語句
- 注意，case 必須在 [過程區塊](Verilog-09-process-block.md) 中使用

- 注意，變數值建議要完整寫出值的寬度，避免寬度不匹配
  > 例如，以 4'd3 取代 d3，省略寬度時，編譯器會使用系統的寬度，d3 == 32'd3

- 注意，`若有遺漏條件` + `沒有設置 default`，會添加`栓鎖器(Latch)`的元件

- 注意，x 和 z 在硬體電路中是不可合成的

  case 語句中的 x 或 z 的比較邏輯是不可綜合的，在硬體描述中不需要考慮 x 或 z 的條件，在模擬中可用

- case 的硬體會被翻譯為`多工選擇器`
  
  <img src="doc/condition/hardware-of-case.png" width=80% height=auto>

- 條件匹配時，會從上到下開始匹配，輸出`第一個匹配成功`的值

- 語法，
  ```verilog
  case(變數)
      變數值1: true_statement1 ;
      變數值2: true_statement2 ;
      ...
      default: default_statement ;
  endcase
  ```

- case / casex / casez 的差異
  - 以下出現 P 代表匹配成功，F 代表匹配失敗

  - case 真值表，`輸入變數`和`匹配條件`需要完全一致，才會匹配成功
    |     | 0   | 1   | x   | z   |
    | --- | --- | --- | --- | --- |
    | 0   | P   | F   | F   | F   |
    | 1   | F   | P   | F   | F   |
    | x   | F   | F   | P   | F   |
    | z   | F   | F   | F   | P   |

  - casex 真值表，只要`輸入變數`或`匹配條件`出現 x 或 z，就會匹配成功，
    不考慮高阻抗和不定值的時候用
    |     | 0   | 1   | x   | z   |
    | --- | --- | --- | --- | --- |
    | 0   | P   | F   | P   | P   |
    | 1   | F   | P   | P   | P   |
    | x   | P   | P   | P   | P   |
    | z   | P   | P   | P   | P   |

  - casez 真值表，只要`輸入變數`或`匹配條件`出現 z，就會匹配成功，
    不考慮高阻抗的時候用
    |     | 0   | 1   | x   | z   |
    | --- | --- | --- | --- | --- |
    | 0   | P   | F   | F   | P   |
    | 1   | F   | P   | F   | P   |
    | x   | F   | F   | P   | P   |
    | z   | P   | P   | P   | P   |

- 範例，四路選擇器
  ```verilog
  module onebit_mux4to1(
    input [1:0] sel,
    input  p0,
    input  p1,
    input  p2,
    input  p3,
    output  out);

    reg result ;

    always @(*) 
    begin
      case (sel)
        2'b00: result = p0;
        2'b01: result = p1;
        2'b10: result = p2;
        2'b11: result = p3;
        default: result = p0;
      endcase
    end

    assign out = result ;

  endmodule
  ```

  testbench
  ```verilog
  `timescale 1ns/1ns
  `include "top.v"

  module test;
    reg [1:0] sel;
    wire sout;

    // 實例化模組
    onebit_mux4to1 m1(
    .sel(sel),
    .p0(1'b1),
    .p1(1'b1),
    .p2(1'b0),
    .p3(1'b0),
    .out(sout)
    );

    // 設置輸入訊號
    initial begin
      $dumpfile("result.vcd");
      $dumpvars;

      sel = 0;
      #10 sel = 1;
      #10	sel = 2;
      #10	sel = 3;
      #10;
      
      $finish;

    end

  endmodule
  ```

- 範例，對 x 或 z 的處理範例
  ```verilog
  case ( select[1:2] )
  2 'b00: result = 0;
  2 'b01: result = flaga;
  2 'b0x,
  2 'b0z: result = flaga? 'bx : 0;
  2 'b10: result = flagb;
  2 'bx0,
  2 'bz0: result = flagb? 'bx : 0;
  default: result = 'bx;
  endcase
  ```

- 範例，模糊匹配範例
  - 透過 casez 才能使用?，表示可為任意值
    ```verilog
    casez(var)
      8'b1???????: 代碼1;
      8'b01??????: 代碼2;
      8'b00010???: 代碼3;
      default: 代碼4;
    endcase
    ```
  
  - 透過 casex 才能使用x，表示可為任意值
    ```verilog
    casez(var)
      8'b1xxxxxxx: 代碼1;
      8'b01xxxxxx: 代碼2;
      8'b00010xxx: 代碼3;
      default: 代碼4;
    endcase
    ```


## Ref
- [Verilog RTL優化策略（一）：推薦使用assign語法替代if-else和case語法](https://iter01.com/615779.html)