// 七段顯示器解碼(共陽)

module SegSevenA( SEL_In, DIG_In, SEL_Out, DIG_Out );

    input  [2:0] SEL_In;    // Use PNP
    input  [3:0] DIG_In;    // MSB D, C, B, A LSB
    output [5:0] SEL_Out;   // 七段顯示器選擇
    output [7:0] DIG_Out;   // MSB dp, g, f, e, d, c, b, a LSB
    
    reg [5:0] SEL_Out;
    reg [7:0] DIG_Out;

    always @( SEL_In ) begin
        case( SEL_In )
            3'b000:    SEL_Out <= 6'b000000;    // all on
            3'b001:    SEL_Out <= 6'b111110;    // 1
            3'b010:    SEL_Out <= 6'b111101;    // 2
            3'b011:    SEL_Out <= 6'b111011;    // 3
            3'b100:    SEL_Out <= 6'b110111;    // 4
            3'b101:    SEL_Out <= 6'b101111;    // 5
            3'b110:    SEL_Out <= 6'b011111;    // 6
            3'b111:    SEL_Out <= 6'b111111;    // all off
        endcase
    end
    always @( DIG_In ) begin
        case( DIG_In )
            4'b0000:    DIG_Out <= 8'b11000000;    // 0
            4'b0001:    DIG_Out <= 8'b11111001;    // 1
            4'b0010:    DIG_Out <= 8'b10100100;    // 2
            4'b0011:    DIG_Out <= 8'b10110000;    // 3
            4'b0100:    DIG_Out <= 8'b10011001;    // 4
            4'b0101:    DIG_Out <= 8'b10010010;    // 5
            4'b0110:    DIG_Out <= 8'b10000011;    // 6
            4'b0111:    DIG_Out <= 8'b11111000;    // 7
            4'b1000:    DIG_Out <= 8'b10000000;    // 8
            4'b1001:    DIG_Out <= 8'b10010000;    // 9
            4'b1010:    DIG_Out <= 8'b01111111;    // dp
            default:    DIG_Out <= 8'b11111111;    // off
        endcase
    end

endmodule

module SegSevenB( input clk, output segA, segB, segC, segD, segE, segF, segG);
   // cnt is used as a prescaler
   reg [23:0] cnt;
   always @(posedge clk) 
	   cnt <= cnt+24'h1;
   wire cntovf = &cnt;

  // BCD is a counter that counts from 0 to 9
  reg [4:0] BCD;
  
  always @(posedge clk) 
     if(cntovf) 
	    BCD <= (BCD==5'h15 ? 5'h0 : BCD+5'h1);

  reg [7:0] sseg;
  always @*
	 
	 begin
	   case(BCD)
		  4'h0: sseg = 7'b1111110; // 0
        4'h1: sseg = 7'b0110000; // 1
		  4'h2: sseg = 7'b1101101; // 2
		  4'h3: sseg = 7'b1111001; // 3
		  4'h4: sseg = 7'b0110011; // 4
		  4'h5: sseg = 7'b1011011; // 5
		  4'h6: sseg = 7'b1011111; // 6
		  4'h7: sseg = 7'b1110000; // 7
		  4'h8: sseg = 7'b1111111; // 8
		  4'h9: sseg = 7'b1111011; // 9
		  4'ha: sseg = 7'b1110111; // A
		  4'hb: sseg = 7'b0011111; // B
 		  4'hc: sseg = 7'b1001110; // C
		  4'hd: sseg = 7'b0111101; // D
		  4'he: sseg = 7'b1001111; // E
		 default: sseg = 7'b1000111; // F
	   endcase 
	 end

	 assign {segA, segB, segC, segD, segE, segF, segG} = sseg;

endmodule
