## 範例集
- [可綜合範例 @ OpenCores](https://opencores.org/)
- [設計範例 @ 陳鍾誠](http://ccckmit.wikidot.com/ve:main)
- [設計範例 @ runoob](https://www.runoob.com/w3cnote/verilog-pipeline-design.html)
- [設計範例 @ y2b-天璇](https://www.youtube.com/playlist?list=PLkH9pBMaZuHQ0_P26d8ctZSd9trPajCmI)
- [設計範例 @ chipverify](https://www.chipverify.com/verilog/verilog-jk-flip-flop)
- [verilog相關project @ github-alexforencich](https://github.com/alexforencich)
- [常用代碼 @ github-pConst](https://github.com/pConst/basic_verilog)
- [Nyuzi Processor 開源代碼，通用圖形處理器(GPGPU) 學習 @ github-jbush001](https://github.com/jbush001/NyuziProcessor)
- [Github 上有哪些優秀的Verilog/FPGA 項目，持續更新中](https://www.zhihu.com/question/348990787)
  
- RISCV
  - [香山開源高性能處理器 by Chisel](https://github.com/OpenXiangShan/XiangShan)
  - [Hummingbirdv2 E203](https://github.com/riscv-mcu/e203_hbirdv2)
  - [平頭哥無劍100](https://github.com/T-head-Semi/wujian100_open)
  - [RISC-V-starship SoC Generator](https://github.com/riscv-zju/riscv-starship)
  - [VexRiscv by SpinHDL](https//github.com/SpinalHDL/VexRiscv)
  - [ZipCPU 輕量級 RISCV CPU (含教學)](https://github.com/ZipCPU/zipcpu)
  - [推薦，tinycpu (含中文教學)](https://gitee.com/liangkangnan/tinyriscv)

## Ref
- 線上模擬器
  - [edaplayground，支援多個編譯器和模擬器，支援 yosys ](https://www.edaplayground.com/home)
  - [makerchip，線上FGPA板模擬器 @ github](https://github.com/os-fpga/Virtual-FPGA-Lab)
  - [makerchip，線上FGPA板模擬器](https://makerchip.com/)
    - 需要學習額外的語法做視覺化

- 教學網站
  - [教學資源統整 @ github-LeiWang1999](https://github.com/LeiWang1999/FPGA)
  - [Verilog 從放棄到有趣](https://ithelp.ithome.com.tw/users/20107543/ironman/1492)
  - [Verilog 初級教程 @ runoob](https://www.runoob.com/w3cnote/verilog-tutorial.html)
  - [Verilog 高級教程 @ runoob](https://www.runoob.com/w3cnote/verilog2-tutorial.html)
  - [Learn FPGA 系列影片 * 21](https://www.youtube.com/playlist?list=PL2935W76vRNGRtB09yXBytO6F3zSZFZGr)
  - [推薦，verilog语言入门 @ bili-Sky_SiliconThink](https://www.bilibili.com/video/BV11Q4y1Y7Hz/?vd_source=3c0cf410b123e705f0c291052893ff16)
  - [FPGA-101](https://nandland.com/fpga-101/)
  - [6小時掌握Verilog語法 @ bilibili-VinWei2014](https://www.bilibili.com/video/BV1cZ4y157XS/)
  - [推薦，从电路设计的角度入门VerilogHDL @ 之乎-Forever snow](https://www.zhihu.com/zvideo/1541037205484564480?playTime=0.0)
  - [Verilog 電路設計 @ 陳鍾誠](http://ccckmit.wikidot.com/ve:main)
  - [Verilog入門教學](https://www.youtube.com/playlist?list=PLkH9pBMaZuHQ0_P26d8ctZSd9trPajCmI)
  - [數位邏輯設計 by 吳順德](https://www.youtube.com/watch?v=yxyjzg9-8kw&list=PLXxs-fSMcpYdwkCsnTXAkpvI2afmUkQgI)
  - [數位邏輯實驗 by 吳順德](https://www.youtube.com/playlist?list=PLXxs-fSMcpYdYd6pjgs27nEJg3sf7srCY)
  - [Verilog 教學](https://www.javatpoint.com/verilog-control-blocks)
  - [Verilog 教學 @ chipverify](https://www.chipverify.com/verilog/verilog-parameters)

- HDLBits
  - [HDLBits官方網站](https://hdlbits.01xz.net/wiki/Main_Page)
  - [HDLBits介紹](https://zhuanlan.zhihu.com/p/56646479)
  - [在線學習Verilog系列 @ HDLBits 中文導學](https://zhuanlan.zhihu.com/c_1131528588117385216)
  - [HDLBits 解答 @ github-xiaop1](https://github.com/xiaop1/Verilog-Practice)
  - [HDLBits 解答 @ github-viduraakalanka](https://github.com/viduraakalanka/HDL-Bits-Solutions)

- 相關文章
  - [Verilog基础](https://blog.csdn.net/qq_39507748/category_10869096.html)
  - [Verilog語法系列 @ 羅成](https://www.zhihu.com/column/c_1117750063287488512)
  - [Verilog學習筆記-基本語法篇 @ SYoong](https://www.cnblogs.com/SYoong/p/5849168.html)
  - [verilog 相關文章 @ 邁克老狼2012](https://www.cnblogs.com/mikewolf2002/category/1329314.html)
  - [verilog 語法學習系列 @ 邁克老狼2012](https://www.cnblogs.com/mikewolf2002/p/10183032.html)
  - [Verilog HDL 話題 @ 之乎](https://www.zhihu.com/topic/19609393/hot)
  - [verilog-hdl 話題 @ 之乎](https://www.zhihu.com/topic/19923584/hot)
  - [FPGA 論文 @ sunbust](http://www.sunburst-design.com/papers/)
  - [FPGA 相關文章 @ 之乎-siliconthink](https://www.zhihu.com/org/siliconthink/posts)
  - [Verilog 十大基本功](https://blog.csdn.net/times_poem/category_6330818.html)

- 進階
  - [什麼是Multicycle Path](https://aijishu.com/a/1060000000206169)
  - [Multicycle Paths](https://blog.csdn.net/Times_poem/article/details/85989972)

