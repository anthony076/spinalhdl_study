module KEY_Debounce( CLK, RST, KEY_In, KEY_Out );

    parameter    DeB_Num = 4;        // 取樣次數
    parameter    DeB_SET = 4'b0000;  // 設置
    parameter    DeB_RST = 4'b1111;  // 重置

    input   CLK, RST;
    input   KEY_In;
    output  KEY_Out;
    reg     KEY_Out = 1'b1;
    reg     [DeB_Num-1:0] Bounce = 4'b1111; // 初始化

    always @( posedge CLK, negedge RST ) begin  // 一次約200Hz 5ms
        if( !RST )
            Bounce <= DeB_RST;    // Bounce重置
        else begin    // 取樣4次
            integer    i;
            Bounce[0] <= KEY_In;
            for( i=0; i<DeB_Num-1; i=i+1 )
                Bounce[i+1] <= Bounce[i];
        end
        case( Bounce )
            DeB_SET:    KEY_Out <= 1'b0;
            default:    KEY_Out <= 1'b1;
        endcase
    end

endmodule


// https://www.fpga4fun.com/Debouncer2.html
module PushButton_Debouncer(
    input clk,
    input PB,  // "PB" is the glitchy, asynchronous to clk, active low push-button signal

    // from which we make three outputs, all synchronous to the clock
    output reg PB_state,  // 1 as long as the push-button is active (down)
    output PB_down,  // 1 for one clock cycle when the push-button goes down (i.e. just pushed)
    output PB_up   // 1 for one clock cycle when the push-button goes up (i.e. just released)
);

    // First use two flip-flops to synchronize the PB signal the "clk" clock domain
    reg PB_sync_0;  always @(posedge clk) PB_sync_0 <= ~PB;  // invert PB to make PB_sync_0 active high
    reg PB_sync_1;  always @(posedge clk) PB_sync_1 <= PB_sync_0;

    // Next declare a 16-bits counter
    reg [15:0] PB_cnt;

    // When the push-button is pushed or released, we increment the counter
    // The counter has to be maxed out before we decide that the push-button state has changed

    wire PB_idle = (PB_state==PB_sync_1);
    wire PB_cnt_max = &PB_cnt;	// true when all bits of PB_cnt are 1's

    always @(posedge clk)
      if(PB_idle)
          PB_cnt <= 0;  // nothing's going on
      else
        begin
            PB_cnt <= PB_cnt + 16'd1;  // something's going on, increment the counter
            if(PB_cnt_max) PB_state <= ~PB_state;  // if the counter is maxed out, PB changed!
        end

    assign PB_down = ~PB_idle & PB_cnt_max & ~PB_state;

    assign PB_up   = ~PB_idle & PB_cnt_max &  PB_state;
    
endmodule