/* 掃描鍵盤 KEY_Row讀取, KEY_Col掃描 */

module KEY_Scan( CLK, RST, KEY_Row, KEY_Col, KEY );

    /* STATE */
    parameter Col_1 = 4'b1110;
    parameter Col_2 = 4'b1101;
    parameter Col_3 = 4'b1011;
    parameter Col_4 = 4'b0111;

    input   CLK, RST;
    input   [3:0] KEY_Row;
    output  [3:0] KEY_Col;
    output  [15:0] KEY;

    reg    [15:0] KEY = 16'h0000;
    reg    [3:0] KEY_Col = Col_1;

    always @( posedge CLK, negedge RST ) begin
        if( !RST ) begin
            KEY <= 16'b0;
            KEY_Col <= Col_1;
        End

        else begin
            case( KEY_Col )
                Col_1: begin
                    KEY[3:0] <= KEY_Row;
                    KEY_Col <= Col_2;
                end
                Col_2: begin
                    KEY[7:4] <= KEY_Row;
                    KEY_Col <= Col_3;
                end
                Col_3: begin
                    KEY[11:8] <= KEY_Row;
                    KEY_Col <= Col_4;
                end
                Col_4: begin
                    KEY[15:12] <= KEY_Row;
                    KEY_Col <= Col_1;
                end
                default:    KEY_Col <= Col_1;
            endcase
        end
    end

endmodule