module PWM(clk, LED);
   
	input clk;
	output LED;

	reg [23:0] cnt;
	
	always @(posedge clk) cnt<=cnt+1;
	
	wire [3:0] PWM_input = cnt[23] ? cnt[22:19] : ~cnt[22:19];    // ramp the PWM input up and down

	reg [4:0] PWM;
	
	always @(posedge clk) PWM <= PWM[3:0]+PWM_input;

	assign LED = PWM[4];
endmodule