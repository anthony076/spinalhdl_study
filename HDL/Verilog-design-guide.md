## 建立可綜合模型的原則
- 語句
  - 不使用 `initial`
  - 可使用延遲語句(可使用 `#延遲時間` 的語句)，但會被合成器忽略
  - 不使用`循環次數不確定`的循環語句，如forever、while等。
  - 如果不打算把變量推導成鎖存器，那麼必須在if語句或case語句的所有條件分支中都對變量明確地賦值。
  - 避免在case語句的分支項中使用x值或z值。
  - 用always過程塊描述組合邏輯，應在敏感信號列表中列出所有的輸入信號。

- 設計原則
  - 除非是關鍵路徑的設計，一般不採用調用門級元件來描述設計的方法，建議採用`行為語句`來完成設計。
  - 所有的內部寄存器都應該能夠被復位，在使用FPGA實現設計時，應盡量使用器件的全局復位端作為系統總的複位。
  - 盡量`使用同步方式`設計電路: 輸入訊號與與 clock 有關
  - 不使用用戶自定義的UDP元件，UDP元件無法被綜合
  - 建議不要在異步時對變量讀取，即異步復位時，對信號賦以常數值。
  - 使用 always 描述組合邏輯時，應該在敏感列表中，將使用到的訊號都列出，
    若沒有列出，有可能會出現 [latch](#不要使用會出現-latch-的設計)
  - 在 case 的語句中，盡量避免使用x值或z值

- 設計時序電路時
  - 盡量不使用局部變量。
  - 不能在一個以上的always過程塊中對同一個變量賦值(重複賦值)
  - 對同一個賦值對像不能既使用阻塞式賦值，又使用非阻塞式賦值(使用不同的賦值方式)
  - 避免混合使用上升沿和下降沿觸發的觸發器。
  - 時序邏輯應盡量使用`非阻塞賦值`，組合邏輯應盡量使用`阻塞賦值`
  - 在同一個過程代碼塊中，最好`不要同時用阻塞賦值和非阻塞賦值`。
  - 同一個變數的賦值，不能受多個時鐘控制 (不能使用不同的時鐘沿觸發)

## 不要使用會出現 latch 的設計
- latch 的危害
  - 容易產生毛刺 (glitch)

    latch 是電位觸發，latch 的輸出端對輸入端是透明的，輸入端的訊號直接到輸出端，容易造成輸入端有雜訊時，也會反映到輸出端

    FF 是由兩個 latch 構成的主從式觸發器，輸出訊號對輸入端是不透明的，
    可以消除毛刺訊號

  - 需要的硬體資源更多，需要一個邏輯門和FF來組成 latch
  - 使得靜態時序分析更複雜

- 避免出現 latch
  - if 語句一定要有 else，且所有的訊號都要在 if 的分支中被賦值

  - 組合邏輯中，不能出現自己給自己賦值，或間接給自己賦值的狀況
    
    時序邏輯中，可以出現自己給自己賦值
    ```verilog
    always @* begin
      if (rst == 1'b1) counter = 32'h00000000;
      else counter = counter + 1  // 出現 latch，自己給自己賦值 
    ```
  - case 語句，必須加上 default

  - always 中使用到的訊號，必須都添加到敏感列表中 (避免不完整的敏感訊號列表)
    
    在 always代碼塊中，如果賦值表達式的右側，
    引用了沒有出現在敏感列表的訊號，
    因為該訊號需要被讀取，因此編譯器會為該訊號添加一個 latch

## 產生毛刺的原因: 訊號的競爭和冒險
- 什麼是競爭和冒險
  - 競爭(Competition):
    
    在組合邏輯中，對於同一個邏輯閘的輸入端，
    由於`不同路徑的輸入訊號`，存在的`訊號傳輸延遲`，稱為競爭

  - 冒險(Hazard):

    因為輸入訊號的競爭，造成`不如預期的輸出`，稱為冒險，

    有競爭不一定會產生非預期的輸出，不一定會有冒險，
    但是有冒險，一定是競爭造成的

  - 只要邏輯閘的各個輸入端延遲時間不同，就有可能產生競爭和冒險

- `輸入端訊號延遲`的競爭和冒險，造成的毛刺現象

  在組合邏輯中，對於同一個邏輯閘的輸入端，
  由於不同路徑的輸入訊號存在的訊號傳輸延遲(競爭)，造成輸出有機會不如預期(冒險)

  以下圖為例，兩個相反的輸入端，$A$ 和 $\overline{A}$，

  - 在理想狀況下，因為輸入端互為反向，不會出現同時為1 的狀況，
  因此，輸出皆為 0
  
  - 在實際狀況下，因為 $\overline{A}$ 的訊號會多經過一個反向器，
    因此，$\overline{A}$ 的訊號傳遞會比 $A$的訊號傳遞稍慢，
    使得`兩個輸入端有機會同時為1`，進而造成輸出為1的狀況，
    此狀況不是在預期的結果內，因此稱為毛刺 

  - 注意，不是所有的延遲都會產生毛刺，以下為例，毛刺只會發生在上升沿

  <img src="doc/design-guide/glitch-from-sign-delay.png" width=500 height=auto>

- `輸入端的雜訊`造成輸出的毛刺

  <img src="doc/design-guide/glitch-from-input-noise.png" width=200 height=auto>

- 如何判斷是否有競爭和冒險的狀況
  - 方法1，代數法

    在組合邏輯的輸出方程式中，固定同一個變數，將其他變數設置為 0 或 1，
    - 例如，$Y = \overline{A} + A$，
    - 例如，$Y = \overline{A} \cdot A$
    - 若固定變數可以表達成以上形式，代表該電路可能存在競爭和冒險的狀況

  - 方法2，卡諾圖法
    當不同組的圈選，重複出現在同一行或同一列時，就有機會出現競爭和冒險

    <img src="doc/design-guide/check-glitch-by-KarnaughMap.png" width=700 height=auto>

- 消除競爭與冒險
  - 在某些情況下，輸入訊號的競爭和冒險是無法避免的，
    但是可以透過以下方法降低發生的機會

  - `方法1`，在輸出端並聯濾波電容
    
    利用輸出端的濾波電容，避免突波傳遞到下一級的輸入訊號，
    利用濾波電容，將突波的強度削弱到閥值以下，從而避免產生毛刺的機會

    缺點，會增加訊號翻轉的時間，會破壞波形

  - `方法2`，增加冗餘項 (增加重疊項)
    
    在卡諾圖中，重疊欄或重疊屬於冗餘項，將該冗餘項添加到輸出方程式中

    <img src="doc/design-guide/remove-glitch-by-KarnaughMap.png" width=700 height=auto>

  - `方法3`，改用需要觸發的正反器

    時序電路的輸出變化，都發生在 clock 的上下緣，
    只要毛刺不出現在 clock 的上下緣，且不滿足數據的建立和保持時間，就`有機會`不會產生毛刺，
    因此，時序電路的正反器具有消除毛刺的效果

    注意，使用正反器是`有機會消除`，但是不是一定會消除毛刺，

    使用 FF 仍然產生毛刺的範例，
    完整範例，參考，[改善毛刺範例](design-glitch-improve/)
    ```verilog
    module top
    (
      input clk ,
      input rstn ,
      input en ,
      input din_rvs ,
      output reg flag
    );

      // 容易產生毛刺的邏輯
      wire condition = din_rvs & en;

      always @(posedge clk or negedge rstn) begin
        if (!rstn) begin
            flag <= 1'b0 ;
        end
        else begin
            flag <= condition ;
        end
      end

    endmodule
    ```
    <img src="doc/design-guide/glitch-on-ff-output.png" width=800 height=auto>

    改善方式: 將輸入也改用 FF，適時增加輸入與輸出之間的拍數(增加輸入到輸出之間的處理時間)，可有效消除毛刺，
    此方法利用時間延遲或輸入雜訊都是短時的特性，增加時間可有效的避免毛刺

    ```verilog
    module top
    (
      input clk ,
      input rstn ,
      input en ,
      input din_rvs ,
      output reg flag
    );

      reg din_rvs_r ;
      reg en_r ;

      always @(posedge clk or negedge rstn) begin
          if (!rstn) begin
              din_rvs_r <= 1'b0 ;
              en_r <= 1'b0 ;
          end
          else begin
              din_rvs_r <= din_rvs ;
              en_r <= en ;
          end
      end

      wire condition = din_rvs_r & en_r ;
      
      always @(posedge clk or negedge rstn) begin
          if (!rstn) begin
              flag <= 1'b0 ;
          end
          else begin

              flag <= condition ;
          end
      end

    endmodule
    ```

    <img src="doc/design-guide/glitch-on-ff-output-improve.png" width=800 height=auto>

  - `方法4`，對於`狀態變更`或`計數器`使用格雷碼

    - 若不使用格雷碼，狀態的變更會遇到多位的狀態轉變
      例如，`狀態5(101) -> 狀態6(110)`，其中，bit1 bit0 會由 01 轉變為 10，共有兩位值的變更

      理想狀況下，沒有任何延遲下，狀態會直接變更，狀態5(101) -> 狀態6(110)
      在有延遲的情況下，會產生中間狀態 (本來應該有兩個 bit 一起變更，卻只有一個 bit 進行變更)

      例如，狀態5(101) -> `中間狀態(111)，只變更bit1` -> 狀態6(110)，只變更bit0
      例如，狀態5(101) -> `中間狀態(100)，只變更bit0` -> 狀態6(110)，只變更bit1

      因為有中間狀態的存在，就會造成毛刺的產生

    - 使用格雷碼消除毛刺
      
      格雷碼: 000 -> 001 (bit0變更) -> 011 (bit1變更) -> 111 (bit2變更)

      使用格雷碼進行狀態的變更，每次只會有一個 bit 變更，因此不會有毛刺產生

## 流水線設計
- https://www.zhihu.com/zvideo/1541037205484564480?playTime=0.0
- 流水線設計可以提高處理器的工作頻率和處理器的效率。
- 流水線並不是越長越好，流水線越長要使用的資源就越多、面積就越大。
- 設計CPU前，要確定設計的處理器要達到什麼樣的性能(或者說主頻最高是多少)，使用的資源的上限是多少，功耗範圍是多少
- 流水線是用時序電路實現

## 跨時鐘域 (多時鐘域)
- 跨時鐘域一定要做同步處理，
  - 多時鐘域容易有頻差問題
  - 對控制訊號: 可以採用雙採樣
  - 對數據訊號: 可以採用異步FIFO
    > 注意，異步FIFO只能解決小部分的頻差問題
  - 對特殊的IO接口，只使用Tsu、Tco、Th 的約束往往不夠

    盡可能使用FPGA內部的PLL、DLL、DDIO、管腳可設置延遲等工具來實現

## Ref
- [flipflop和latch以及register的區別](https://blog.csdn.net/Times_poem/article/details/85986055)

- [推薦設計流程](https://www.zhihu.com/question/484101037)