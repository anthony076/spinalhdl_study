## 過程區塊 process-block
- 屬於過程區塊的語法
  - [initial-block](#initial-只會執行一次僅用於仿真)
  - [always-block](#always-執行多次用於功能敘述的方式描述硬體)
  - [task-block](Verilog-10-task-and-function.md#task)
  - [function-block](Verilog-10-task-and-function.md#function)

- 過程模塊的性質
  - 可以有多個 initial、always、task、function 語句，但2個語句不能嵌套使用
  
  - 每個過程區塊`依照條件觸發執行`，並在過程區塊之間`並行執行`，和過程區塊的前後順序沒有關係

    例如以下代碼，所有的 initial代碼塊都是從 t=0 的時候開始執行
    ```verilog
    module test;
      reg a, b, c, d;

      initial begin
        $dumpfile("result.vcd");
        $dumpvars;
        a = 0 ;
        b = 1 ;
        c = 1 ;
        d = 0 ;
      end

      initial begin // t=0
        #10 a=1'b1; // t=10
        #25 b=1'b0; // t=35
      end

      initial begin // t=0
        #15 c=1'b0; // t=15
        #25 d=1'b1; // t=40
      end

      initial begin   // t=0
        #60 $finish;  // t=60
      end
    endmodule
    ```
    
    時序圖，並行處理，三個 initial 代碼塊都是從 t=0 的時刻開始執行
    <img src="doc/process/initial-pallallel-example.png" width=80% height=auto>

  - 每個 initial 或 always 語句都會產生一個獨立的控制流
    - 若使用阻塞賦值，則語句的內部是順序執行的
    - 若使非用阻塞賦值，則語句的內部是平行執行的
    - 參考，[賦值的使用](Verilog-06-assign.md)

- 控制過程代碼塊的執行
  - 若使用 always代碼塊，有多種方式可以控制觸發執行的執行
  - 其他的過程代碼塊，可透過 [wait語句](Verilog-13-testbench.md#等待語句wait-暫停代碼塊的執行) 來實現條件式執行

## initial: 只會執行一次，僅用於仿真
- 語法: 
  - 從 t=0 開始執行，只會執行一次
  - 若 initial 內只有一句，可省略 begin ... end
  - 常用於初始化、信號檢測
  - 範例
    ```verilog
    initial begin
      // (選用) 賦值
      // (選用) 不可合成的控制語句
      // (選用) 延時
    end
    ```

- 範例
  ```verilog
  `timescale 1ns/1ns
  
  module test ;
      reg  ai, bi ;
  
      initial begin
          ai         = 0 ;
          #25 ;      ai        = 1 ;
          #35 ;      ai        = 0 ;        //absolute 60ns
          #40 ;      ai        = 1 ;        //absolute 100ns
          #10 ;      ai        = 0 ;        //absolute 110ns
      end
  
      initial begin
          bi         = 1 ;
          #70 ;      bi        = 0 ;        //absolute 70ns
          #20 ;      bi        = 1 ;        //absolute 90ns
      end
  
      //at proper time stop the simulation
      initial begin
          forever begin
              #100;
              //$display("---gyc---%d", $time);
              if ($time >= 1000) begin
                  $finish ;
              end
          end
    end
  
  endmodule
  ```

## always: 執行多次，用於功能敘述的方式描述硬體
- always 代碼塊是流水線設計的基礎，
  - 3級流水線代表使用3個D型觸發的暫存器，
  - 5級流水線代表使用5個D型觸發的暫存器，

- always 的使用場景
  - 用於描述`時序邏輯`，`具有觸發功能`的寄存器(ff)或栓鎖器(latch)
  - 用於描述`組合邏輯`，
  - 用於仿真，用於產生連續訊號，或用於檢測電路

- always 如何決定硬體電路
  - 如果敏感列表中的訊號是`時鐘上升沿或下降沿`，always 會被翻譯為`具有觸發功能的暫存器`，
    > 例如 always@（posedge or negedge）會被翻譯為暫存器

  - 如果敏感列表中的訊號是`某一信號的高低電平`，always 會被翻譯為`組合邏輯`。

- 觸發 always代碼塊執行的幾種方式
  - 方法1，添加 `@ 敏感列表`，條件式觸發
    
    敏感列表中，若只使用分號(,) 分隔訊號，默認代表 OR 的關係
    > 例如，always @ (a, b) 等效於 always @ (a OR b)

  - 方法2，always 不添加 @ 時，`總是觸發`
    > 從 t=0 開始執行，執行到最後一個語句後，會再次回到第一個語句，重複循環執行

  - 方法3，[透過 event 語句手動觸發](#透過event語句觸發always代碼塊的執行手動觸發)

- 語法1，用於時序邏輯，`always @ (posedge 訊號1 or negedge 訊號2)`，
  - always代碼塊由訊號1或訊號2的邊緣觸發
  - posedge 代表正緣觸發，negedge 代表負緣觸發
  ```verilog
  module top(out, in, clk);

    input in, clk;
    output out;

    reg a, b, c;
    
    // 當clk上升緣時，觸發以下 always 代碼塊的執行
    always @(posedge clk) begin
      a <= in;
      b <= a;
      c <= b;
    end

    assign out = c;
  endmodule
  ```

- 語法2，用於組合邏輯，`always @ (訊號1 or 訊號2)`，
  
  當訊號1或訊號2的`值變更`時，執行 always代碼塊

  ```verilog
  module top (y, a, b, c, d);
    output y;
    input a, b, c, d;

    reg y, tmp1, tmp2;
    
    always @(a or b or c or d) begin
      // 輸出值 <= 輸入1 邏輯關係 輸入2
      tmp1 <= a & b;
      tmp2 <= c & d;
      y <= tmp1 | tmp2;
    end
  endmodule
  ```

- 語法3，自動添加敏感訊號，`always @ (*)` 或 `always @*`，常用於組合邏輯
  - 編譯器根據 always 內的輸入變量自動添加敏感列表

- 語法4，用於模擬，`always begin`
  
  沒有 @ ，代表不需要滿足特定條件才執行 == 執行完畢後，會立刻執行下一次，相當於無窮迴圈

  ```verilog
  always begin
    // (選用) 賦值
    // (選用) 不可合成的控制語句
    // (選用) 延時
  end
  ```   

  - 範例
    ```verilog
    `timescale 1ns/1ns
    
    module test ;
    
        parameter CLK_FREQ   = 100 ; //100MHz
        parameter CLK_CYCLE  = 1e9 / (CLK_FREQ * 1e6) ;   //switch to ns
    
        reg  clk ;
        initial      clk = 1'b0 ;      //clk is initialized to "0"
        always     # (CLK_CYCLE/2) clk = ~clk ;       //generating a real clock by reversing
    
        always begin
            #10;
            if ($time >= 1000) begin
                $finish ;
            end
        end
    
    endmodule
    ```

## 透過event語句觸發always代碼塊的執行(手動觸發)
- 使用方法

  - step1，建立觸發源(建立事件)
    
    1-1，在 always 上方添加 `event 事件名`，建立要監聽的事件
    1-2，在敏感列表中，定義`觸發時機`
    1-3，在 always 代碼塊中，透過 `-> 事件名`，手動觸發指定的事件

  - step2，建立觸發後要執行的always代碼塊

- 範例，
  ```verilog
  
  // step1，建立事件
  event     start_receiving ;         // 1-1，建立監聽事件
  always @( posedge clk_samp) begin   // 1-2，定義觸發時機
          -> start_receiving ;        // 1-3，手動觸發指定的事件 
  end
  
  // step2，當start_receiving被觸發後，執行以下的 always代碼塊 
  always @(start_receiving) begin
      data_buf = {data_if[0], data_if[1]} ; 
  end
  
  ```

## Ref
- [always 使用範例](https://www.twblogs.net/a/5b80a3902b71772165a86dca)
