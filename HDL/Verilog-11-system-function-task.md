## 系統任務和系統函數

- [任務和函數的使用](Verilog-10-task-and-function.md)

- 不常用的系統任務

  $bitstoreal、$rtoi、$setup、$skew、$hold、
  $setuphold、$itor、$period、
  $timefoemat、$width、$real、$tobits、$recovery、

## 顯示，用於追蹤變數值
- $display，在`編譯 testbench 檔案`時，在控制台輸出訊息，`被動顯示`
  - 不需要像$display，執行到 display 的語句時，才會讀取變數值並輸出

- $monitor，在`編譯 testbench 檔案`時，在控制台輸出訊息，`主動顯示`
  - 不需要像$display，執行到 display 的語句時，才會讀取變數值並輸出，

    只需要寫一句 $monitor 的語句，就可以時時追蹤指定變數的狀態，
    每當追蹤的變數有變化時，monitor 就會自動輸出

  - $monitor 語句定義要追蹤的變數和輸出的格式
    - $monitoron，使 $monitor 語句生效，預設值
    - $monitoroff，使 $monitor 語句失效
    - 對於多模組的 $monitor，由於全局只能有一個 $monitor 語句生效，
  
      因次需要透過 $monitoron、$monitoroff 搭配使用開啟要監測的模塊，
      並關閉不監測的模塊

- $write，不會自動換行版的 $display (不會在尾部添加 `\n`)

- 顯示不定值或高阻抗
  - 若`所有`的位置均為不定值，顯示`小寫`的 x，
  - 若`部分`的位置均為不定值，顯示`大寫`的 X，

  - 若`所有`的位置均為高阻抗，顯示`小寫`的 z，
  - 若`部分`的位置均為高阻抗，顯示`大寫`的 Z，

- 語法糖
  - 輸出2進制格式的訊息，`$monitorb`、`$displayb`、`$writeb`
  - 輸出8進制格式的訊息，`$monitoro`、`$displayo`、`$writeo`
  - 輸出16進制格式的訊息，`$monitorh`、`$displayh`、`$writeh`

- format 
  - %b: 將數值格式化為 2進制
  - %nb: 以2進制顯示，並占用n位字符，值不夠指定位數時，以空白補齊到n位 (例如，%3b)
  - %0nb: 以2進制顯示，並占用n位字符，值不夠指定位數時，以0補齊到n位 (例如，%03b)

  - %d: 將數值格式化為 10進制
  - %o: 將數值格式化為 8進制
  - %h: 將數值格式化為 16進制
  - %c: 將數值格式化為 ASCII 格式
  - %s: 將數值格式化為 String 格式
  - %m: 輸出當前模組的層次
  

## 探測，
- $stobe，常用於顯示非阻塞賦值(<=) 的結果
  - 在某時刻所有的事件都執行完畢後，在時間步的尾部將 $stobe 的語句輸出(延遲輸出)
  - $display 是立刻輸出，$stobe 要等到時間步的末尾
    
    注意，時間步的末尾不是代碼塊結束，而是`以 $stobe 語句為一個區間`，
    $stobe 語句一定是該區間的最後一個執行單位

- 範例 $stobe 範例

  <img src="doc/system/stobe-example.png" width=80% height=auto>

## 模擬相關
- $finish，結束模擬
- $stop，暫停模擬，並進入交互模式，將控制權交給使用者

## 時間相關
- $time，獲取仿真時間，返回一個64bit`整數`的仿真時間，非整數時間時`會自動進位`

  $time 輸出的時間總`是時間尺度的整倍數`，若 $time 的結果是小數，系統會`自動進位`後再輸出

- $realtime，獲取仿真時間，返回一個實數(帶小數點的數)的仿真時間，非整數時間時`不會自動進位`，

  但是`需要時間尺度`和`顯示格式`的配合，才能正確輸出小數，見以下範例

- 範例，$time 輸出整數時間範例
  
  例如，若時間尺度為 10ns，16ns/10=1.6倍， 若在 16ns 執行 $time 會得到 $time=2 的結果

  ```verilog
  `timescale 10ns/10ns
  `define clock_period 20

  module vtrobe_tb;
    reg [4:0] a;
    
    initial begin
      #1.6 $display("time=%d", $time);  // time=2
    end
  endmodule
  ```
  
- 範例，$realtime 輸出小數時間範例
  
  ```verilog
  `timescale 1ns/100ps  // 需要將時間尺度調整至合適的尺度
  `define clock_period 20

  module vtrobe_tb;
    reg [4:0] a;
    
    initial begin
      #1.6 $display("realtime=%f", $realtime);  // 要顯示小數必須使用正確的 format
    end

  endmodule
  ```


## 隨機數
- $random，返回一個 32bit 帶符號的隨機整數，
  
  注意，$random 不是真隨機數，無論執行幾次，產生的隨機數都是相同的

- 範例，$random 使用範例
  ```verilog
  `timescale 1ns/1ns
  `define clock_period 20

  module vtrobe_tb;
    reg [4:0] a;
    
    initial begin : loop
      integer i;
      $monitor("a=%d", a);

      for(i=0; i<10; i=i+1) 
        #20 a = $random % 60; 
    end

  endmodule
  ```

## 文件相關
- $fopen("檔案名", modoe)，開啟文件，返回 file-handler
  - mode 值的選擇
    - w:  寫入模式，從文件起始位置開始寫入 (會覆蓋原始檔案內容)
    - w+: 讀取+寫入模式，可讀取檔案內容 + 從文件起始位址開始寫入
    - a:  續寫模式，從文件結束位置接續寫入 (會覆蓋原始檔案內容)
    - a+: 讀取+續寫模式，可讀取檔案內容 + 從文件結束位址接續寫入
  
- 將 $monitor、$display、$write、$stobe 的結果寫入檔案中
  - fdisplay(file-handler, 訊號1、訊號2、...)，用於將 $display 的結果寫入檔案中
  - $fmonitor(file-handler, 訊號1、訊號2、...)，用於將 $monitor 的結果寫入檔案中
  - $fwrite(file-handler, 訊號1、訊號2、...)，用於將 $write 的結果寫入檔案中
  - $fstobe(file-handler, 訊號1、訊號2、...)，用於將 $stobe 的結果寫入檔案中

- $fclose(file-handler)，將以開啟的文件關閉

- 讀取數據，並保存在指定的memory
  - 注意，此處的memory為`使用者手動建立的reg陣列`，而不是執行環境的內存

  - 從文件讀取2進制數據: `$readmemb("檔案名"，memory名，保存的起始位置，保存的結束位置)`
  - 從文件讀取16進制數據: `$readmemh("檔案名"，memory名，保存的起始位置，保存的結束位置)`

  - 保存位置/結束位置，可省略，也可以透過指令/文件指定
    - 若指令沒有指定起始/結束位置，則從頭開始存放，直到 `memory滿` 或 `數據寫入結束`
    - 若指令只有指定起始位置，從指定位置開始存放，直到 memory滿
    - 若指令有起始/結束位置，依照指示存放
    - 若指令和文件同時都有指示保存位置，
      
      文件指示的保存位置不能超過實際 memory 的範圍，否則會報錯

  - 注意，文件保存的數據，必須符合下列規範
    - 2進制數值、16進制數值
    - 以空白區隔數值
    - 若指定位址，位址的格式為 @位址

## 匯出模擬波形
- 詳細使用見，[testbech的使用](Verilog-13-testbench.md)
- $dumpfile，設置匯出檔案
- $dumpvars，設置匯出的相關參數


## 其他系統任務
- $printtimescale，顯示當前的 timescale 設置 (顯示 `` `timescale `` 的內容)