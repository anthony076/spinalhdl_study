## 循環語句概述
- 循環語句有以下幾種
  - for: 有明確次數，可合成
  - repeat: 可合成
  - while: 可合成
  - forever: 無有明確次數，不可合成，常用於模擬

- 利用 generate `打開循環語句或條件語句的限制`和`功能增加`
  - 解除always代碼塊的限制
    
    循環語句常常需要建立臨時變數，才能在迴圈中重複的賦值，因此需要 reg 類型的變數才會有記憶的效果，
    且 reg 類型變數只能在過程區塊中使用，因此`不使用generate的循環語句需要寫在過程區塊`中  

    透過添加 generate 語句就沒有此限制

  - 解除 if/case `無法使用Macro參數` 作為條件的限制

    使用 generate 後，if/case 內的條件，都是使用`靜態參數值`，而不是暫存器值，

    不使用 generate，if/case 內的條件，都是使用`暫存器的變數值`，
    在合成時，編譯器會考慮該變數所有的可能值，並將所有可能性的電路都生成

  - 功能增加

    獨立的循環語句(不使用generate) 只能在單一的always代碼塊中使用，
    因此只能複製的範圍限制在always代碼塊之中，generate 則可以複製多個 always代碼塊，複製的範圍更大

- generate 可用於循環語句(generate-for)或條件語句(generate-if/generate-case)
  
- [透過 disable 語句，跳出循環語句](Verilog-05-code-block.md)

## for 語句
- 必須寫在 always區塊中
- for 的起點和終點必須是常數才可以合成
- 迴圈變數i，可以宣告為 integer 或 reg 類型，integer 較方便使用

- 語法，
  ```verilog
  for(迴圈變數=初始值; 停止條件; step條件) begin
    ... 內容省略 ...
  end
  ```

- 注意，要`迴圈變數i`的宣告位置，
  - `狀況1`，若迴圈變數i定義在`alway-block 之外`，
    
    迴圈變數i為`全局變數`，則 always 中的 begin區塊`不需要具名`，
    因為是全局變數，不適合給多個 always 同時使用，會有多個 always 區塊競爭迴圈變數i的問題

  - `狀況2`，若迴圈變數i定義在`alway-block 之內`，
    
    迴圈變數i為`局部變數`，則 always 中的 begin區塊`需要具名`，
    因為是局部變數，只能給單一個 always 使用，必須透過具名的 begin區塊，
    編譯器才能得知`迴圈變數i歸屬於哪一個 alway-block`

  <img src="doc/loop/i-and-named-block.png" width=80% height=auto>

- 注意，遞增條件不支援 `i++` 的語法，要寫成 `i=i+1`

- 範例，利用 for 簡化 數據線的連接
  ```verilog
  module top( 
      input [7:0] in,
      output reg [7:0] out
  );
      always@(*) begin: bit_reverse
          integer i;
          for(i = 0; i<8; i=i+1) begin
              out[i] = in[7-i];
          end
      end

  endmodule
  ```
  <img src="doc/loop/use-loop-to-create-wires.png" width=40% height=auto>

- 範例，利用 for 建立時序電路
  ```verilog
  module top(
    input sysclk,
    input [3:0] a,
    output reg [3:0] temp = 0
  );

  genvar i;
  generate
    for (i = 0; i < 4 ; i = i + 1) begin: timing_logic
        always @(posedge sysclk) begin
            temp[i] <= a[i];
        end
    end
  endgenerate

  endmodule
  ```
  <img src="doc/loop/use-loop-in-sequencial-circuit.png" width=40% height=auto>

## while 語句
- 使用到迴圈變數時，必須寫在 always區塊中，

- 範例，while 用於合成範例
  ```verilog
  module top( 
      input [7:0] in,
      output reg [7:0] out
  );
      
      integer i;

      always@(*) begin
        i = 0;

        while(i<8) begin
          out[i] = in[7-i];
          i = i + 1;
        end
        
      end

  endmodule
  ```

- 範例，用於模擬的計數範例
  ```verilog
  `timescale 1ns/1ns
  
  module test ;
  
      reg [3:0]    counter ;

      initial begin
        $dumpfile("result.vcd");
        $dumpvars;

        counter = 'b0 ;
        while (counter<=10) begin
            #10 ;
            counter = counter + 1'b1 ;
        end
      end
  
      always begin
          #10;  
          if ($time >= 130) $finish ;
      end
  
  endmodule
  ```

## repeat 語句
- 語法
  ```verilog
  
  repeat(次數或變數) begin
    ... 內容省略 ...
  end

  ```
- 使用到迴圈變數時，必須寫在 always區塊中，
  
  repeat區塊中，若使用到變數，則該變數就必須具有記憶的特性，才能在每次迴圈中賦予變數新值，
  因此，repeat區塊就必須使用到 reg 類型的變數(reg 或 integer)，也因此需要寫在 always 區塊中

- 範例，repeat 用於合成範例
  ```verilog
  module top( 
      input [7:0] in,
      output reg [7:0] out
  );
      
      integer i;
      always@(*) begin
        i = 0;
        repeat(8) begin
          
          out[i] = in[7-i];
          i = i + 1;

        end
      end

  endmodule
  ```

- 範例，repeat 用於模擬範例
  ```verilog
  `timescale 1ns/1ns

  module test;

    integer var1, i;
    
    initial begin
      var1  = 8 ;
      i = 0 ;
      
      repeat(var1) begin
        i = i + 1 ;
        $display("i =  %0d", i) ; 
      end

      $finish; 
    end

  endmodule
  ```

## forever 語句
- 不可用於合成，常用於生成 clock 訊號

- forever 可寫在 initial代碼塊 或 always代碼塊之中

  雖然 forever 語句沒有循環變數，待在 forever 一般會存取其他的reg變數，
  因此也寫在過程區塊中

- 語法
  ```verilog
  forever begin
    ... 內容省略 ...
  end
  ```

- 範例，產生連續的時鐘訊號
  ```verilog
  `timescale 1ns/1ns
  
  module test ;
  
    reg          clk ;

    initial begin
      $dumpfile("result.vcd");
      $dumpvars;

      clk = 0 ;
      
      forever begin
          clk = ~clk ;
          #5 ;
      end
    end

    always begin
      #10
      if ( $time >=100) $finish;
    end

  endmodule
  ```

- 範例，forever 寫在 always代碼塊之中
  ```verilog
  `timescale 1ns/1ns
  
  module test ;
  
    reg          clk ;

    initial begin
      $dumpfile("result.vcd");
      $dumpvars;
    end

    // 設置 clock
    always begin
      clk = 0 ;
      
      forever begin
          clk = ~clk ;
          #5 ;
      end

    end
      
    // 設置結束訊號
    always begin
      #10
      if ( $time >=100) $finish;
    end

  endmodule
  ```

## generate 語句
- 使用場景
  - 場景1，解決循環語句必須寫在 always 區塊的問題
  - 場景2，生成的實例用於模組、用戶自定義模塊語句、Gate-level語句、連續賦值語句、過程塊(initial、always)
  - 場景3，允許例化語句包含在for循環和if-else語句內部
  - 場景4，允許不同循環語句的嵌套

- generate 的使用
  - 語法
    ```verilog
    genvar 變數名;
    generate
      ... 內容省略
    endgenerate
    ```
  - 必須透過`genvar`來建立變數，genvar 可寫在 generate 區塊內部或外部
  - 使用genvar建議的變數必須直接調用，
    - 推薦，`for(i=0; i<DEPTH; i=i+1)`
    - 不推薦，`for(i=0; i<DEPTH*5; i=i+1)`
  - 不同的 generate區塊不能使用同一個 genvar 建立的變數，
    每一個 generate區塊必須擁有`獨立的 genvar 變數`
  - generate 內部使用的 for 語句，`強制需要添加區塊名`

- 範例，generate-for 範例，用於 assign 語句
  ```verilog
  module top(
    input [7:0] in,
    output [7:0] out
  )
    genvar i;

    generate
      // 需要加上 block-name
      for(i=0; i<8; i=i+1) begin:loopa
        assign out[i] = in[7-i];
      end
    endgenerate

  endmodule
  ```

- 範例，generate-for 範例，用於Gate-level語句
  ```verilog
  module full_adder4(
      input [3:0]   a ,   //adder1
      input [3:0]   b ,   //adder2
      input         c ,   //input carry bit
  
      output [3:0]  so ,  //adding result
      output        co    //output carry bit
      );
  
      wire [3:0]    co_temp ;

      //第一個模塊格式有差異，單獨實例化
      full_adder1  u_adder0(
          .Ai     (a[0]),
          .Bi     (b[0]),
          .Ci     (c==1'b1 ? 1'b1 : 1'b0),
          .So     (so[0]),
          .Co     (co_temp[0]));

      // 定義臨時變數
      genvar i ;
      
      // 自動重複大量實例化
      generate
          for(i=1; i<=3; i=i+1) begin: adder_gen
            full_adder1  u_adder(
                .Ai     (a[i]),
                .Bi     (b[i]),
                .Ci     (co_temp[i-1]), //上一个全加器的溢位是下一个的进位
                .So     (so[i]),
                .Co     (co_temp[i]));
          end
      endgenerate
  
      assign co = co_temp[3] ;
  
  endmodule
  ```

- 範例，generate-if 範例
  ```verilog
  module top( 
    input t0,
    input t1,
    input t2,
    output d
  );
    
    // 定義模組內的私有常數，外界不可修改
    localparam S = 6;

    generate
        if(S<7)
          assign d = t0 | t1 | t2;
        else
          assign d = t2 & t1 & t2;
    endgenerate

  endmodule
  ```

- 範例，generate-case 範例
  ```verilog
    module top( 
    input t0,
    input t1,
    input t2,
    output d
  );
    
    // 定義模組內的私有常數，外界不可修改
    localparam S = 6;

    generate
        case(S)
          0: assign d = t0 | t1 | t2;
          1: assign d = t2 & t1 & t2;
          default: assign d = t2 & t1 | t2;
        endcase
    endgenerate

  endmodule
  ```

## [注意] 循環語句有可能帶來的副作用
- 利用循環語句簡化 wire 的連接副作用最小

- 注意，在合成硬體電路的代碼中，`謹慎使用循環語句`，for語法簡單卻可能造成副作用

  在合成器中，編譯器會將循環語句展開，會將循環語句內的`相同電路進行多次複製`，
  因此，要小心循環語句內產生的硬體是否符合預期，若循環的次數越多，占用面積越大，綜合就越慢

  例如，以下的 for 迴圈會複製付出四個相同的電路，`運算效率高，一個周期就可以產生結果，但消耗的邏輯資源大`，
  對於速度要求不是很高的應用，且邏輯資源有限的情況下，可以使用多個週期完成
  
  <img src="doc/loop/comple-for-circuit.png" width=80% height=auto>

- 時序邏輯中盡量避免for循環，如果要使用，要確認輸出的電路是否符合預期

  可以透過 [function/task](Verilog-10-task-and-function.md) 取代 for 循環的設置

- 範例，for 造成`處理時間加長`，以清除多個暫存器為例
  - 方法1，利用 `for` 迴圈清除暫存器
    ```verilog
    reg[31:0]matrix[8:0];

    always@(posedge clk)begin
      if(reset)
        for(idx=0; idx <9; idx = idx +1)begin
          matrix[idx] <= 0;
        end
    end
    ```
    編譯器展開為
    ```verilog
    always@(posedge clk)begin
      matrix[0] <= 0;
      matrix[1] <= 0;
      matrix[2] <= 0;
      matrix[3] <= 0;
      matrix[4] <= 0;
      matrix[5] <= 0;
      matrix[6] <= 0;
      matrix[7] <= 0; 
      matrix[8] <= 0;
      matrix[9] <= 0; 
    end
    ```
    實際上，利用 for 進行清除暫存器時，因為是時序電路，因此總共需要花費`9個clock`才能清除完畢

  - 方法2，利用 `reset接腳` 清除暫存器
    在時序電路中，reset 是並行接到每一個暫存器的，
    因此`只需要1個clock`就能完成
  
- 範例，for 迴圈造成`硬體資源浪費`範例
  ```verilog
  module top(clk,Rst_n,data, num);

    input clk; 
    input Rst_n;
    input [3:0] data;

    output reg [2:0] num;
    integer i;

    always @(posedge clk) begin
      if(Rst_n==0)
        num = 0;
      else      
        begin
          for(i=0;i < 4; i=i+1)
            if(data[i])  num = num + 1;
        end
    end

  endmodule
  ```

## Ref
- [如何無誤使用 generate-for](https://blog.csdn.net/Reborn_Lee/article/details/99567453)
- [for 和 generate-for 的區別](https://www.twblogs.net/a/5d5f0260bd9eee541c3279b1)
- [generate的使用總結](https://blog.csdn.net/moon9999/article/details/106969615)
- [for的使用總結](https://blog.csdn.net/moon9999/article/details/106971710)
- [使用generate的好處](https://www.zhihu.com/question/492833265)

Verilog里的generate到底有什么好处