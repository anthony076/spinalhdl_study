## 常用預編譯指令
- 以下預編譯指令的使用方法與 C 相同

- `` `include ``: 從其他文件中導入模組
  ```verilog
  `include         "../../param.v"
  `include         "header.v"
  ```

- `` `define ``: 用於定義常數，在編譯期，編譯器會將替換目標替換為替換內容
  > 語法: `define 替換目標 替換內容
  
- `` `undef ``: 取消透過 define 定義的常數

- 條件式編譯
  ```verilog
  // `ifdef ... `elsif ... `else ... endif 

  // 範例
  `ifdef       MCU51
      parameter DATA_DW = 8   ;
  `elsif       WINDOW
      parameter DATA_DW = 64  ;
  `else
      parameter DATA_DW = 32  ;
  `endif
  ```

- `` `timescale ``: 用於設置模擬文件的時間單位
  使用方式詳見，[testbench的使用](Verilog-13-testbench.md)

- `` `default_nettype ``，
  - 用法1，
    > `default_nettype wand
  
  - 用法2，`` `default_nettype none ``，停止自動為未宣告的變數，添加 wire 類型
    
    沒有定義的變數，編譯器會自動設置為 wire 類型
    ```verilog
    module test_and(
            input      A,
            input      B,
            output     Z);
        assign Z1 = A & B ;  // warning，Z1 被自動設置為 wire 類型
    endmodule
    ```

    使用 default_nettype none 後，不允許出現未宣告類型的變數
    ```verilog
    `default_nettype none
    module test_and(
            input      A,
            input      B,
            output     Z);
        assign Z1 = A & B ;  // error
    endmodule
    ```

- `` `resetall ``，
  
- `` `calldefine `` 和 `` `endcalldefine ``，將模塊標記為單元模塊
  ```verilog
  `celldefine
  module (
      input      clk,
      input      rst,
      output     clk_pll,
      output     flag);
          ……
  endmodule
  `endcelldefine
  ```

- `` `unconnected_drive `` 和 `` `nounconnected_drive ``，用於設置未接線接腳的狀態
  ```verilog
  `unconnected_drive pull1
    //在unconnected_drive和nounconnected_drive之間未連接的接腳，設置為高電位
  `nounconnected_drive
  ```

    ```verilog
  `unconnected_drive pull0
    //在unconnected_drive和nounconnected_drive之間未連接的接腳，設置為低電位
  `nounconnected_drive
  ```

## 其他預編指令
- `` `default_nettype `` 
- `` `reset ``
- `` `autoexpand_vectornets ``

- `` `accelerate `` 和 `` `noaccelerate ``

- `` `celldefine `` 和 `` `endcelldefine ``

- `` `expand_vectornets `` 和 `` `noexpand_vectornets ``

- `` `remove_gatenames `` 和 `` `noremove_gatenames ``

- `` `remove_netnames `` 和 `` `noremove_netnames ``

- `` `unconnected_drive `` 和 `` `nounconnected_drive ``

- `` `protect `` 和 `` `endprotect ``

- `` `protected `` 和 `` `endprotected ``
