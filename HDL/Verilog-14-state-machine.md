## 狀態機 Finate-State-Machine(FSM) 基礎
- 狀態機的類型，依照輸出與輸入是否有關係，可分為
  - `類型1`，moore型: 輸出只和當前狀態有關，與輸入無關

    <img src="doc/state-machine/moore-state-machine.png" width=80% height=auto>

  - `類型2`，mealy型: 輸出和輸入+當前狀態有關

    <img src="doc/state-machine/mealy-state-machine.png" width=80% height=auto>

- 注意，狀態`轉移條件`與`輸出`無關
  
  狀態圖中，輸出是`寫在狀態泡泡中`或是`與寫成輸入/輸出的形式`，主要用於區分是 moore 或 mealy 的狀態機，
  
  無論是何種狀態機，`狀態轉移的條件`都是根據輸入值決定，狀態轉移條件`與輸出無關`

- 注意，`輸出變數可以有多個`，但`所有狀態都共用同一組輸出`，

  狀態機的輸出，指的是正反器的結果輸出後(產生狀態後)，會`再經過一組組合邏輯電路`後得到的結果，(也可以不經過組合邏輯電路，直接將狀態輸出)

  無論當前狀態為何，使用的都是同一組的輸出，並且在任何狀態下都會產生輸出，而不是每個狀態都有獨立的輸出

  參考，[LED 移位範例，Moore + 多個輸出](#範例led-移位範例多個輸出範例-moore--mealy)
  
- 注意，`中間狀態`也不一定總是0，到達`最後狀態`的輸出不一定總是1，依需求決定狀態和輸出的關係
  
  參考，[LED 移位範例，Moore + 多個輸出](#範例led-移位範例多個輸出範例-moore--mealy)

- 注意，輸入/輸出的組合邏輯不是固定的，是跟隨狀態轉移表的，
  - 狀態的編碼不同，
  - 跳轉的狀態不同，
  
  以上都會都會影響輸入輸出的組合邏輯，雖然`組合邏輯不是固定的`，但`一定都是符合狀態轉移表的`，

  參考，[以不同狀態編碼實現 Moore 狀態機](https://www.cnblogs.com/mikewolf2002/p/10197777.html)

- 有限狀態機設計流程
  - step1，畫出狀態圖 -> 將狀態圖轉換為狀態表
  - step2，對照[激勵表](#基礎-真值表與激勵表)，將狀態表轉換為轉態表(transition-table)
  - step3，利用 轉態表+[卡諾圖](#基礎-卡諾圖karnaugh-map)，求出化簡後的正反器輸入/輸出方程式
  - step4，根據輸入方程式繪製電路圖
  - 範例
    - [以 Moore 實現 101 序列的檢測電路](#手寫moore型狀態機)
    - [以 Mealy 實現 101 序列的檢測電路](#手寫mealy型狀態機)

## [基礎] 真值表與激勵表
- 真值表(truth-table): 根據輸入值，推導出輸出的下一個狀態
- 激勵表(excitation-table): 根據當前狀態和下一個狀態的值，推測出輸入值

- SR-NOR-latch 的真值表與激勵表
  - SR 真值表

    | S   | R   | Qn+1 | comment     |
    | --- | --- | ---- | ----------- |
    | 0   | 0   | Qn   | 未設置0或1  |
    | 0   | 1   | 0    | 輸出清除為0 |
    | 1   | 0   | 1    | 輸出設置為1 |
    | 1   | 1   | X    | 0或1都可能  |
    
  - SR 激勵表 (只關注 Qn+1)

    | Qn  | Qn+1 | S   | R   | comment<br>(以下值，請參考真值表)                                    |
    | --- | ---- | --- | --- | -------------------------------------------------------------------- |
    | 0   | 0    | 0   | X   | SR=00，Qn+1=Qn=0或1<br>SR=01，Qn+1=0<br>由以上可知，S=0，R可以是0或1 |
    | 0   | 1    | 1   | 0   | S=1，將Qn+1 設置為1                                                  |
    | 1   | 0    | 0   | 1   | R=1，將Qn+1 清除為0                                                  |
    | 1   | 1    | X   | 0   | SR=00，Qn+1=Qn=0或1<br>SR=10，Qn+1=1<br>由以上可知，R=0，S可以是0或1 |

- JK正反器 的真值表與激勵表
  - 改善 SR-latch 在 SR=11時，輸出為X(不確定)的狀態

    JK正反器在 JK=11時，輸出為 $\overline{Q}_n$

  - JK 真值表
     
    | J   | K   | Qn+1             | comment                      |
    | --- | --- | ---------------- | ---------------------------- |
    | 0   | 0   | Qn               | 輸出反向                     |
    | 0   | 1   | 0                | 相當於SR中的R，將輸出清除為0 |
    | 1   | 0   | 1                | 相當於SR中的S，將輸出設置為1 |
    | 1   | 1   | $\overline{Q}_n$ | 輸出反向                     |
    
  - JK 激勵表 (只關注 Qn+1)

    | Qn  | Qn+1 | J   | K   | comment<br>(以下值，請參考真值表)                                                  |
    | --- | ---- | --- | --- | ---------------------------------------------------------------------------------- |
    | 0   | 0    | 0   | X   | JK=00，Qn+1=Qn=0或1<br>JK=01，Qn+1=0<br>由以上可知，J=0，K可以是0或1               |
    | 0   | 1    | 1   | X   | JK=10，Qn+1=1<br>JK=11，Qn+1=$\overline{Q}_n$=0或1<br>由以上可知，J=1，K可以是0或1 |
    | 1   | 0    | X   | 1   | JK=01，Qn+1=0<br>JK=11，Qn+1=$\overline{Q}_n$=0或1<br>由以上可知，J可以是0或1，K=1 |
    | 1   | 1    | X   | 0   | JK=00，Qn+1=Qn=0或1<br>JK=10，Qn+1=1<br>由以上可知，J可以是0或1，K=0               |

- D正反器 的真值表與激勵表
  - 數據的緩衝器，輸出與輸入同向

  - D 真值表
     
    | D   | Qn+1 |
    | --- | ---- |
    | 0   | 0    |
    | 1   | 1    |
    
  - D 激勵表 (只關注 Qn+1)
    | Qn  | Qn+1 | D   |
    | --- | ---- | --- |
    | 0   | 0    | 0   |
    | 0   | 1    | 1   |
    | 1   | 0    | 0   |
    | 1   | 1    | 1   |

- T正反器 的真值表與激勵表
  - 數據的反向器，輸入為1時輸出反向，反則，輸出為同向

  - T 真值表
     
    | T   | Qn+1             |
    | --- | ---------------- |
    | 0   | Qn               |
    | 1   | $\overline{Q}_n$ |
    
  - T 激勵表 (關注 Qn 和 Qn+1 是否反向)
    | Qn  | Qn+1 | D   |
    | --- | ---- | --- |
    | 0   | 0    | 0   |
    | 0   | 1    | 1   |
    | 1   | 0    | 1   |
    | 1   | 1    | 0   |

## [基礎] 卡諾圖(Karnaugh-map)
- 使用場景: 用於簡化布林函數

- 建構卡諾圖的要求
  - 布林函數中有 n 個變數，卡諾圖就必須有 $2^n$ 個方格
  - 任意相鄰的兩格，亦即相鄰的兩項，其對應的變數字母只有一個是不同的

    <img src="doc/state-machine/karnaugh-map.png" width=600 height=auto>

- 化簡步驟
  - 規則1: 將在真值表中可產生1的每個基礎乘積項，對應的填入卡諾圖的空格中，並標記為1，其他的空格則填入0

    > 注意，如果有缺項，需要把缺項可能的值補上，

    > 例如，X = $\overline{ABC}+\overline{B}C+\overline{A}B$，
    其中，$\overline{A}B$ 缺 C，應該填充為在 $\overline{A}BC$ 和 $\overline{A}B\overline{C}$

  - 規則2: 依序圈出相鄰的8個1、相鄰的4個1、相鄰的2個1，
    > 注意，空格中的1可被重複圈選，以便消除最多的變數，若只有一個1，也要個別圈選

    > 注意，要讓所有1的空格都被圈到，而圈選的組數要愈少愈好

    > 注意，位於邊界的上下(或左右)兩格1可以圈為同一組，

    > 注意，如果是 X，X 可為1或0，依照周圍是否出現1決定，原則是讓周圍能夠出現成對的1，成對的1越多，化簡的項目越多

    > 注意，每一個圈選的結果是一個乘積項，將所有的乘積項OR起來即是化簡後的布林代數式。

    <img src="doc/state-machine/karnaugh-map-simplify-example.png" width=600 height=auto>

- 卡諾圖化簡完整範例

  <img src="doc/state-machine/karnaugh-map-full-example.png" width=600 height=auto>

## 手寫Moore型狀態機
- 性質
  - `狀態改變`和`輸出結果`只和當前狀態有關，與輸入無關
  - 同步輸出狀態機 (串行)
  - 同一個應用下，使用 moore 比 mealy需要更多的狀態

- 範例，以 Moore 偵測 101 序列為例
  - 要求:

    依序檢測輸入值，每次檢查輸入的其中一個bit，

    只有輸入的順序依序為 101 時(進入S3狀態)，輸出才會1，否則輸出都是0，

  - `step1`，繪製狀態圖和狀態表
    
    <img src="doc/state-machine/moore-example-101-state-map.png" width=700 height=auto>

    - 1-1，第1狀態(S0=00)，`檢查 input 中的第1位`，與預期值中的第1位(1) 是否一致，
      - 若檢查值 == 1，與預期一致，進入下一個狀態(S1=01)
      - 若檢查值 == 0，與預期不一致，維持當前狀態(S0=00)
      - 輸出，未完成所有位檢測，因此輸出為0

    - 1-2，第2狀態(S1=01)，`檢查 input 中的第2位`，與預期值中的第2位(0) 是否一致，
      - 若檢查值 == 0，與預期一致，進入下一個狀態(S2=10)
      - 若檢查值 == 1，與預期不一致，維持當前狀態(S1=01)
      - 輸出，未完成所有位檢測，因此輸出為0

    - 1-3，第3狀態狀態(S2=10)，`檢查 input 中的第3位`，與預期值中的第3位(1) 是否一致，
      - 若檢查值 == 1，與預期一致，進入下一個狀態(S3=11)
      - 若檢查值 == 0，與預期不一致，回到初始狀態(S0=00)
      - 輸出，未進入結束狀態(S3)，因此輸出為0

    - 1-4，結束狀態(S3=11)，所有的檢查位與預期值皆相同，
      - 若檢查值 == 0，進入S2狀態，
        > S3 已收到 101，若再收到 0 表示已收到 1010，後兩位元是10，因此回到S2狀態
      
      - 若檢查值 == 1，進入S1狀態，
        > S3 已收到 101，若再收到 1 表示已收到 1011，後兩位元是11，因此回到S1狀態
  
      - 輸出，已完成所有位檢測，因此輸出為1

    - 1-5，最後，將上述描述轉換為狀態表
      
      | Qn (當前狀態) | Qn+1 (下一個狀態) | Qn+1 (下一個狀態) | Output |
      | ------------- | ----------------- | ----------------- | ------ |
      | 狀態名(Q0 Q1) | 若輸入X=0         | 若輸入X=1         | Y      |
      | S0(00)        | Qn+1=S0(00)，FAIL | Qn+1=S1(01)，PASS | 0      |
      | S1(01)        | Qn+1=S2(11)，PASS | Qn+1=S1(01)，FAIL | 0      |
      | S2(10)        | Qn+1=S0(00)，FAIL | Qn+1=S3(11)，PASS | 0      |
      | S3(11)        | Qn+1=S2(11)       | Qn+1=S1(01)       | 1      |

---

  - `step2`，根據狀態表和激勵表，繪製轉態表(transition-table)
    - 根據狀態數決定需要的`正反器數`和`選擇要實現的正反器`
      
      在此範例中共有4個狀態(S0-S3)，共需要 `2個正反器`，每個正反器可保存1bit的狀態值，
      兩個正反器才能表達一個完整的狀態

      - 靠近輸出端的正反器為 `FF0`，輸入端為 `J0、K0`，輸出端(輸出狀態)為 `Q0`，Q0 對應真值表中的 Qn(current-state)，
      - 靠近輸入端的正反器為 `FF1`，輸入端為 `J1、K1`，輸出端(輸出狀態)為 `Q1`，Q1 對應真值表中的 Qn(current-state)，

      依據選用何種正反器決定了使用何種激勵表，
      
      選擇正反器沒有特別的限制，`但是越簡單的正反器(例如，D-FF)會比狀態多的正反器(例如，JK-FF)，需要更多的邏輯才能實現`

    - <font color=blue> (選項1) 狀態表 + JK激勵表 </font>
      - Q0 == 輸出端JK正反器的輸出接腳，J0、K0 == 輸出端JK正反器的輸入接腳
      - Q1 == 輸入端JK正反器的輸出接腳，J1、K1 == 輸入端JK正反器的輸入接腳
      - 得到 Qn 和 Qn+1 的狀態表後，根據 Qn 和 Qn+1 的值，`填入對應的JK激勵值`

      <img src="doc/state-machine/moore-example-101-excitation-table-JK.png" width=900 height=auto>
    
    - <font color=blue> (選項2) 狀態表 + D激勵表 </font>
      - Q0 == 輸出端D-FF的輸出接腳，D0 == 輸出端D-FF的輸入接腳
      - Q1 == 輸入端D-FF的輸出接腳，D1 == 輸入端D-FF的輸入接腳
      - 得到 Qn 和 Qn+1 的狀態表後，根據 Qn 和 Qn+1 的值，`填入對應的D激勵值`

      <img src="doc/state-machine/moore-example-101-excitation-table-D.png" width=700 height=auto>

---

  - `step3`，利用卡諾圖化簡方程式
    - (選項1) 化簡JK轉態表 
      
      從 step2 的 <font color=blue>JK轉態表</font> 可以得到`輸入端(J0、K0)`和`當前狀態(Q0、Q1)`和`輸入(X)`，三者未化簡之前的關係

      <img src="doc/state-machine/moore-example-101-complex-eq-JK.png" width=700 height=auto>
      
      利用卡諾圖化簡後
      
      <img src="doc/state-machine/moore-example-101-simplify-eq-JK.png" width=700 height=auto>

    - (選項2) 化簡D轉態表 
      
      從 step2 的 <font color=blue>D轉態表</font> 可以得到`輸入端(J0、K0)`和`當前狀態(Q0、Q1)`和`輸入(X)`，三者未化簡之前的關係

      <img src="doc/state-machine/moore-example-101-complex-eq-D.png" width=600 height=auto>

      利用卡諾圖化簡後
      
      <img src="doc/state-machine/moore-example-101-simplify-eq-D.png" width=00 height=auto>

---

  - `step4`，根據化簡後的方程式，繪製出電路

    <img src="doc/state-machine/moore-example-101-final-circuit.png" width=600 height=auto>

## 手寫mealy型狀態機
- 性質
  - 輸出結果和當前狀態有關，且和輸入有關
  - 異步輸出狀態機 (並行)

    不需要等待 clock 就可以輸出，有可能會有毛刺現象，
    因此輸出端通常會加上一個需要觸發的暫存器，來實現輸出同步的效果
  - 需要的狀態更少

- 範例，以 Mealy 偵測 101 序列為例
  - 要求:

    依序檢測輸入值，每次檢查輸入的其中一個bit，

    只有輸入的順序依序為 101 時(進入S3狀態)，輸出才會1，否則輸出都是0，

  - `step1`，繪製狀態圖和狀態表
    
    <img src="doc/state-machine/mealy-example-101-state-map.png" width=700 height=auto>
    
    - 1-1，第1狀態(S0=00)，`檢查 input 中的第1位`，與預期值中的第1位(1) 是否一致，
      - 若檢查值 == 1，與預期一致，進入下一個狀態(S1=01)
      - 若檢查值 == 0，與預期不一致，維持當前狀態(S0=00)
      - 輸出，未完成所有位檢測，因此輸出為0

    - 1-2，第2狀態(S1=01)，`檢查 input 中的第2位`，與預期值中的第2位(0) 是否一致，
      - 若檢查值 == 0，與預期一致，進入下一個狀態(S2=10)
      - 若檢查值 == 1，與預期不一致，維持當前狀態(S1=01)
      - 輸出，未完成所有位檢測，因此輸出為0

    - 1-3，第3狀態狀態(S2=10)，`檢查 input 中的第3位`，與預期值中的第3位(1) 是否一致，
      - 若檢查值 == 1，與預期一致，進入上一個狀態(S1=01)，
        > 注意，因為沒有 S3 狀態，`只有在 S2 狀態時，且下一個輸入為 1`，才會輸出為 1
      
      - 若檢查值 == 0，與預期不一致，回到初始狀態(S0=00)，輸出為 0
        > 注意，若輸入符合預期，已經回到上一個狀態(S01=01)，因此，若輸入不符合預期，只能回到初始狀態(S0=00)

    - 注意，在 mealy 中，只有`最後一個狀態可能會有兩種輸出值`
      - 符合預期時，輸出為 1，並回到上一個狀態
      - 不符合預期時，輸出為 0，回到初始狀態
      - 和 moore 相比，
        - mooore 在最後的完成狀態(S3)時，輸出必定為 1，在 S3 不會有兩種輸出
        - mealy 在最後的狀態時(S2)，`必須輸入符合預期`，輸出才會為 1，會有兩種輸出

    - 1-4，最後，將上述描述轉換為狀態表
      
      | Qn (當前狀態) | Qn+1 (下一個狀態) | Qn+1 (下一個狀態) | Output                   |
      | ------------- | ----------------- | ----------------- | ------------------------ |
      | 狀態名(Q0 Q1) | 若輸入X=0         | 若輸入X=1         | Y                        |
      | S0(00)        | Qn+1=S0(00)，FAIL | Qn+1=S1(01)，PASS | 0                        |
      | S1(01)        | Qn+1=S2(11)，PASS | Qn+1=S1(01)，FAIL | 0                        |
      | S2(10)        | Qn+1=S0(00)，FAIL | Qn+1=S3(11)，PASS | X=0 => Y=0<br>X=1 => Y=1 |

---

  - `step2`，根據狀態表和激勵表，繪製轉態表(transition-table)
    - 根據狀態數決定需要的`正反器數`和`選擇要實現的正反器`
      
      在此範例中共有3個狀態(S0-S2)，共需要 `2個正反器`，每個正反器可保存1bit的狀態值，
      兩個正反器才能表達一個完整的狀態

      - 靠近輸出端的正反器為 `FF0`，輸入端為 `J0、K0`，輸出端(輸出狀態)為 `Q0`，Q0 對應真值表中的 Qn(current-state)，
      - 靠近輸入端的正反器為 `FF1`，輸入端為 `J1、K1`，輸出端(輸出狀態)為 `Q1`，Q1 對應真值表中的 Qn(current-state)，

      依據選用何種正反器決定了使用何種激勵表，
      
      選擇正反器沒有特別的限制，`但是越簡單的正反器(例如，D-FF)會比狀態多的正反器(例如，JK-FF)，需要更多的邏輯才能實現`

    - <font color=blue> (選項1) 狀態表 + JK激勵表 </font>
      - Q0 == 輸出端JK正反器的輸出接腳，J0、K0 == 輸出端JK正反器的輸入接腳
      - Q1 == 輸入端JK正反器的輸出接腳，J1、K1 == 輸入端JK正反器的輸入接腳
      - 得到 Qn 和 Qn+1 的狀態表後，根據 Qn 和 Qn+1 的值，`填入對應的JK激勵值`

      <img src="doc/state-machine/mealy-example-101-excitation-table-JK.png" width=900 height=auto>
    
    - <font color=blue> (選項2) 狀態表 + D激勵表 </font>
      - Q0 == 輸出端D-FF的輸出接腳，D0 == 輸出端D-FF的輸入接腳
      - Q1 == 輸入端D-FF的輸出接腳，D1 == 輸入端D-FF的輸入接腳
      - 得到 Qn 和 Qn+1 的狀態表後，根據 Qn 和 Qn+1 的值，`填入對應的D激勵值`

      <img src="doc/state-machine/mealy-example-101-excitation-table-D.png" width=700 height=auto>

---

  - `step3`，利用卡諾圖化簡方程式
    - (選項1) 化簡JK轉態表 
      
      從 step2 的 <font color=blue>JK轉態表</font> 可以得到`輸入端(J0、K0)`和`當前狀態(Q0、Q1)`和`輸入(X)`，三者未化簡之前的關係

      <img src="doc/state-machine/mealy-example-101-complex-eq-JK.png" width=600 height=auto>

      利用卡諾圖化簡後
      
      <img src="doc/state-machine/mealy-example-101-simplify-eq-JK.png" width=600 height=auto>

    - (選項2) 化簡D轉態表 
      
      從 step2 的 <font color=blue>D轉態表</font> 可以得到`輸入端(D0、D1)`和`當前狀態(Q0、Q1)`和`輸入(X)`，三者未化簡之前的關係

      <img src="doc/state-machine/mealy-example-101-complex-eq-D.png" width=600 height=auto>

      利用卡諾圖化簡後
      
      <img src="doc/state-machine/mealy-example-101-simplify-eq-D.png" width=600 height=auto>

---

  - `step4`，根據化簡後的方程式，繪製出電路

    <img src="doc/state-machine/mealy-example-101-final-circuit.png" width=400 height=auto>

## moore 和 mealy 的比較
- 比較表 

  | 比較項                  | moore                            | mealy                        |
  | ----------------------- | -------------------------------- | ---------------------------- |
  | 狀態數量(若需求狀態為n) | n+1                              | n                            |
  | 結構                    | 結構簡單                         | 結構複雜                     |
  | 輸出與輸入的關係        | 無關係                           | 有關係                       |
  | 輸出值                  | 固定值                           | 選擇值，依輸入值決定         |
  | 同步處理                | 輸出只和狀態有關，不需要同步處理 | 輸出和輸入有關，需要同步處理 |

- 比較1，`mealy 是對 moore 的簡化`
  - moore 比 mealy 多一個狀態
    
    狀態越多，正反器的輸入方程的`組成項`就越多，若沒有辦法化簡，電路就越複雜，
    因此，簡化電路的方式，是狀態越少越好

  - moore 的輸出與輸入無關

    mealy 的輸出與輸入和狀態有關，使 mealy 輸出方程的組成項中，比 moore 多了一個輸入變數的變因，
    此變因在輸出組合邏輯的卡諾圖中，就會提供更多1的機會，能夠`有機會`使輸出的電路更簡潔

- 比較2，`狀態數量的差異`
  - moore: 
    
    若根據需求，需要有 n 個狀態，
    則 moore 需要有 n+1 個狀態，最後一個狀態為完成狀態，
    
  - mealy: 

    若根據需求，需要有 n 個狀態，則 mealy 需要有 n 個狀態
    
- 比較3，`輸出的差異`
  - moore: 輸出只和狀態有關，和輸入無關
    - 在使用卡諾圖簡化輸出Y的組合邏輯時，moore 的輸出不需要考慮輸入變數
    - 在最後一個狀態時，無論輸入為何，輸出都是固定值

  - moore: 輸出和狀態、輸入有關
    - 在使用卡諾圖簡化輸出Y的組合邏輯時，mealy 的輸出需要多考慮輸入變數
    - 在最後一個狀態時，`需要根據輸入值`，才能決定輸出值，輸出為選擇值

## 在verilog中的有限狀態機
- 使用場景
  
  對於複雜的電路，需要設置很多 always代碼塊的敏感列表來控制電路的行為，對於閱讀代碼不夠直覺，或是電路有 bug 時，不容易發現有 bug 的代碼行，因此會選擇使用 FSM 的方式編寫代碼

  因此，透過使用FSM，可以`降低 verilog 代碼的複雜度`和`增加代碼的可讀性`

- 注意，在 verilog 中，`不需要寫正反器輸入的組合邏輯，只需要指定狀態`，合成器會自動實現輸入/輸出的組合邏輯
  
  若要驗證合成的組合邏輯是否正確，可以手寫狀態機加以驗證

- 狀態編碼: 狀態使用正反器儲存，因此需要`為狀態進行編碼`，決定了正反器輸出才能透過激勵表決定輸入

  使用不同的狀態編碼只會影響組合邏輯的組成方式，不會影響結果，
  例如，XOR = $\overline{X} \overline{Y} + XY$，可以用AND和OR的組合來表達 XOR

  三種常見的編碼方式
  - 注意，狀態的編碼沒有硬性規定，但是會影響`狀態的解碼速度`和`保存狀態的正反器數量(硬體資源)`

  - `編碼1`，讀熱編碼(one-shot-code)，
    - N個狀態，就需要N位編碼，每個狀態都只有一位為1，
      ```
      s0 = 0001
      s1 = 0010
      s2 = 0100
      s3 = 1000
      ```
    - 適合大型設計
    - 優，狀態比較時只需要比較一位，解碼速度快，`減少產生毛刺的概率`
    - 缺，需要更多個正反器，占用資源較多
  
  - `編碼2`，二進制編碼(binary-code)，
    - 以二進制碼對狀態進行編碼，需要更多的資源進行解碼
      ```
      s0 = 00
      s1 = 01
      s2 = 10
      s3 = 11
      ```
    - 適合小型設計
    - 用於保存狀態的正反器較讀熱編碼少，與格雷編碼相同
   
  - `編碼3`，格雷編碼(gray-code)
    - 任意兩個相鄰的狀態只有一位二進制數不同
      ```
      s0 = 00   // s0和s1高位相同為0
      s1 = 01   // s1和s2低位相同為1
      s2 = 11   // s2和s3高位相同為1
      s3 = 10
      ```
    - 適合小型設計
    - 優，狀態轉移時，只需要一位的訊號翻轉就能完成狀態的轉移，產生亞穩態的機率較小
    - 缺，需要較多的資源進行解碼，解碼速度較慢

- 狀態機依照`代碼分段`的寫法，可分為 1段式、2段式、3段式
  - 原則
    - `當前狀態設置`和`輸出`的設置，使用`時序`邏輯+`非阻塞`賦值
    - `條件判斷/下一個狀態`的設置，使用`組合`邏輯+`阻塞`賦值，

  - 一段式寫法
    - 將`當前狀態的設置`、`條件判斷設置`、`輸出的設置`、都寫在同一個`時序邏輯`的 always代碼塊中
    - 在此方法中不需要 nextState 變數
    - 寫起來較簡潔，但不方便調試，適合狀態較簡單的狀態機中

  - 二段式寫法，少用，有毛刺
    - 將代碼區分為當前狀態的設置、條件判斷/下一個狀態的設置、輸出的設置，分成兩個always代碼塊中
      - 第一段`時序邏輯`代碼: 負責`當前狀態`的設置，使用非阻塞賦值
      - 第二段`組合邏輯`代碼: 負責`條件判斷`、`輸出`的設置，使用阻塞賦值
  
    - 缺，組合邏輯的輸出較容易出現毛次等問題

      由於有組合邏輯的存在，狀態機`輸出`信號會因為`競爭冒險`現象而出現毛刺
    
      另外，如果狀態信號是多位值的，則在電路中對應了多條信號線。
      由於存在`傳輸延遲`，各信號線上的值發生改變的時間則存在先後，從而使得狀態遷移時在初始狀態和目的狀態之間出現`臨時狀態`，
      這些臨時狀態是無效的，而輸出也因為組合邏輯原因產生這些臨時狀態，即毛刺。

  - 三段式寫法
    - 將代碼區分為當前狀態的設置、條件判斷/下一個狀態的設置、輸出的設置、三者分別寫在獨立的 always 代碼塊
      - 第一段`時序邏`輯代碼: 負責`當前狀態`的設置，使用非阻塞賦值
      - 第二段`組合邏輯`代碼: 負責`條件判斷和下一個狀態`的設置，使用阻塞賦值
      - 第三段`時序邏輯`代碼: 負責`輸出`的設置，使用非阻塞賦值
    - 最容易維護，適合大規模的電路，但消耗的資源較多

  - 範例，[1段式|2段式|3段式 的寫法範例，以販賣機為例](#範例1段式--2段式--3段式-寫法範例以販賣機為例)

- 有限狀態機的內容
  - step1，定義狀態碼
  - step2，設置`當前狀態`
  - step3，在 case 中，根據`跳轉條件`設置`下一個狀態`
  - step4，設置`輸出`

  - 注意
    - 賦值方式的選擇
      - 若使用組合邏輯，使用阻塞賦值
      - 若使用時序邏輯，使用非阻塞賦值
    
    - 輸出要有默認值

    - 對輸入進行同步處理

      在具體的物理電路中信號的傳播延遲一般都不會為-0，
      那麼輸入信號經過不同的邏輯進入到狀態機中的觸發器時序會有差異，
      因此需要對輸入進行同步處理，避免狀態跳轉異常

## 範例，LED 移位範例，多個輸出範例 (moore + mealy)
  
  <img src="doc/state-machine/example-led-flash.png" width=700 height=auto>

## 範例，1段式 | 2段式 | 3段式 寫法範例，以販賣機為例
- 完整範例，[販賣機範例(含testbench)](fsm-drink-seller/)

- 問題
  - 輸入In，投入的硬幣，In=1，投入1元硬幣，In=2，投入2元硬幣
  - 輸出Out，瓶數，Out=0，未輸出飲料，Out=1，輸出飲料一瓶
  - 輸出Change，找零，Change=1，找零1元，Change=2，找零2元
  - 狀態轉移條件:
    - 當投入的金額 > 飲料價格4元 時，輸出飲料，並輸出找零

- 狀態表

  <img src="doc/state-machine/drink-seller-example-state-map.png" width=600 height=auto>

- 範例，[1段式寫法](drink-seller-example/top_one.v)
  
  使用`時序`邏輯，`狀態/條件判斷/輸出`寫在一起，都使用`非阻塞`賦值

  verilog 代碼
  ```verilog
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        currentState <= S0;
        change <= 0 ;
        out <= 0 ;
        end
    else begin
      case(currentState)
        S0: begin
          if(in==1)begin
              currentState <= S1;
          end
          else if(in==2)begin
              currentState <= S2;
          end
          else begin
              change <= 0 ;
              out <= 0 ;
          end
        end
        // ================
        S1: begin
          if(in==1)begin
              currentState <= S2;
          end
          else if(in==2)begin
              currentState <= S3;
          end
        end
        // ================
        S2: begin
          if(in==1)begin
              currentState <= S3;
          end
          else if(in==2)begin
              currentState <= S0;
              out <= 1 ;	// 輸出一瓶飲料，不找零
          end
        end
        // ================
        S3: begin
          if(in==1)begin
              currentState <= S0;
              out <= 1 ;	// 輸出一瓶飲料，不找零
          end
          else if(in==2)begin
              currentState <= S0;
              change <= 1 ;	// 找零 1元
              out <= 1 ;	// 輸出一瓶飲料，
          end
        end
        
        // ================
        default:currentState   <= S0;

      endcase
    end
  end
  ```
  
  時序圖
  <img src="doc/state-machine/drink-seller-example-tb-one.png" width=100% height=auto>

- 範例，[2段式寫法](drink-seller-example/top_two.v)

  - 第一段: 使用`時序邏輯`，當前狀態的設置獨立一段，並使用`非阻塞`賦值
  - 第二段: 使用`組合邏輯`，條件判斷/下一個狀態的設置、輸出的配置寫在一起，並使用`阻塞`賦值
  - 缺點: 有機會容易產生毛刺

  ```verilog
	// ==== 第一段: 設置狀態 ====
	always@(posedge clk or negedge rst_n)begin
		if(!rst_n)
				currentState <= S0;
		else
				currentState <= nextState;
	end
  // ==== 第一段: 設置下一個狀態和輸出 ====
  always@(*)begin
    case(currentState)
      S0: begin
        if(in==1)begin
          nextState = S1;
          end
        else if(in==2)begin
          nextState = S2;
          end
        else begin
          nextState = currentState;
          change = 0 ;
          out = 0 ;
          end
      end
      
      // ==============
      S1: begin
        if(in==1)begin
          nextState = S2;
          end
        else if(in==2)begin
          nextState = S3;
          end
        else begin
          nextState = currentState;
          end
      end

      // ==============
      S2: begin
        if(in==1)begin
          nextState = S3;
          end
        else if(in==2)begin
          nextState = S0;
          out = 1 ;
          end
        else begin
          nextState = currentState;
          out = 0;
          end
      end

      // ==============
      S3: begin
        if(in==1)begin
          nextState = S0;
          out = 1 ;
          end
        else if(in==2)begin
          nextState = S0;
          change     = 1 ;
          out = 1 ;
          end
        else begin
          nextState = currentState;
          change     = 0;
          out = 0;
          end
      end

      // ==============
      default:nextState = S0;
    endcase

  end
  ```

  時序圖
  <img src="doc/state-machine/drink-seller-example-tb-two.png" width=100% height=auto>

- 範例，[3段式寫法](drink-seller-example/top_three.v)
  - 第一段: 使用`時序邏輯`，當前狀態(currentState)的設置獨立一段，並使用`非阻塞`賦值
  - 第二段: 使用`組合邏輯`，條件判斷/下一個狀態(nextState)的設置，並使用`阻塞`賦值
  - 第三段: 使用`時序邏輯`，輸出的設置寫成獨立一段，並使用`非阻塞`賦值

  - 優點: 可以消除2段式寫法的毛刺

  ```verilog
  // ==== 第一段: 配置當前狀態 ====
  always @(posedge clk or negedge rst_n)begin
    if(!rst_n)
      currentState <= S0;
    else
      currentState <= nextState;
  end

  // ==== 第二段: 跳轉條件和設置下一個狀態 ====
  always @(*)begin
    case(currentState)
      S0: begin
        if(in==1)
          nextState = S1;
        else if(in==2)
          nextState = S2;
        else
          nextState = currentState;
      end
      
      // ============
      S1: begin
        if(in==1)
          nextState = S2;
        else if(in==2)
          nextState = S3;
        else
          nextState = currentState;
      end
      
      // ============
      S2: begin
        if(in==1)
          nextState = S3;
        else if(in==2)
          nextState = S0;
        else
          nextState = currentState;
      end
      
      // ============
      S3: begin
        if(in==1 || in==2)      // in != 0也行
          nextState = S0;
        else
          nextState = currentState;
      end
      
      // ============
      default:nextState = S0;

    endcase
  end

  // ==== 第三段: 配置輸出 ====
  // 設置輸出，配置找零
  always @(posedge clk or negedge rst_n)begin
    if(!rst_n)
        change <= 0;
    else if(currentState==S3 && in==2)
        change <= 1;
    else
        change <= 0;
  end

  // 設置輸出，配置輸出
  always @(posedge clk or negedge rst_n)begin
      if(rst_n==1'b0)
          out <= 0;
      else if((currentState==S2 && in==2) || (currentState==S3 && in!=0))
          out <= 1;
      else
          out <= 0;
  end
  ```

  時序圖
  <img src="doc/state-machine/drink-seller-example-tb-three.png" width=800 height=auto>

## 範例，車輛超速檢測
- 完整範例，[車輛超速範例(含testbench)](fsm-car-speed-detect/)

- 問題
  
  - 輸入值W，用於判斷是否超速，W=0=速度正常，W=1=超速
  - 輸出值Z，設置減速訊號，Z=1=設置減速訊號，Z=0=不須設置減速訊號
  - 狀態轉移條件: 
    - 條件1，若超速次數(W=1)達到`兩次`或`兩次以上`，則需要設置減速訊號(Z=1)，
    - 條件2，設置 Z=1 後，直到車輛恢復正常速度(W=0)後，才關閉減速訊號(Z=0)

- Moore 版本

  step1，狀態轉移圖

  <img src="doc/state-machine/car-speed-detect-moore-example-state-map.png" width=400 height=auto>

  step2，狀態轉移表

  | Current-State(Qn) | Next-State(Qn+1) | Next-State(Qn+1) | Output       |
  | ----------------- | ---------------- | ---------------- | ------------ |
  |                   | W = 0 (未超速)   | W = 1  (超速)    | Z (減速訊號) |
  | Q0 Q1             | Q0 Q1            | Q0 Q1            |              |
  | A(00)，初始狀態   | A(00)            | B(01)            | 0            |
  | B(01)，超速一次   | A(00)            | C(10)            | 0            |
  | C(10)，超速兩次   | A(00)            | C(10)            | 1            |
  | D(11)，完成狀態   | X                | X                | X            |

  step3，轉態表 + 卡諾圖化簡

  <img src="doc/state-machine/car-speed-detect-moore-example-transition.png" width=800 height=auto>

  step5，verilog 代碼

  ```verilog

  // 狀態編碼
  parameter A=2'b00, B=2'b01, C=2'b10;
  
  // ==== 第一段: 利用時序邏輯設置 current_state ====
  always @(posedge clk, negedge rst_n) begin
    if(rst_n == 0)
      current_state <= A;
    else
      current_state <= next_state;
  end

  // ==== 第二段: 利用組合邏輯設置 next_state ====
  always @(w, current_state) begin
    case(next_state)
      A:
        if(w==0) next_state=A;  //  未超速，保持當前狀態
        else next_state=B;      //  超速，進入超速1次的狀態(B狀態)

      B:
        if(w==0) next_state=A;  //  已超速 -> 未超速，回到初始狀態
        else next_state=C;      //  超速，進入超速2次的狀態(C狀態)

      C:
        if(w==0) next_state=A;  //  已超速 -> 未超速，回到初始狀態
        else next_state=C;      //  持續超速，保持在當前狀態

      default: next_state=2'bxx;

    endcase
  end

  // ==== 第三段: 利用組合邏輯設置 output ====
  assign z = (current_state == C);
  ```

  時序圖 (testbench產生的時序有誤，以下時序圖來自[原始網站](https://www.cnblogs.com/mikewolf2002/p/10197777.html))

  <img src="doc/state-machine/car-speed-detect-moore-testbench.png" width=100% height=auto>

- Mealy 版本
  
  狀態轉移圖
  
  <img src="doc/state-machine/car-speed-detect-mealy-example-state-map.png" width=800 height=auto>

  轉態表 + 卡諾圖化簡

  <img src="doc/state-machine/car-speed-detect-mealy-example-transition.png" width=800 height=auto>

  verilog 代碼
  ```verilog
  // ==== 第一段: 設置 current_state ====
  always @(posedge clk, negedge rst_n) begin
    if(rst_n==0)
      current_state <= A;
    else
      current_state <= Y;
  end

  // ==== 第二段: 設置 next_state 和 輸出 ====
  always @(w, current_state)
  begin
    case(current_state)
      A:
        if(w==0) begin
          next_state=A;
          z=0;
          end
        else begin
          next_state=B;
          z=0;
      end

    B:
      if(w==0) begin
        next_state=A;
        z=0;
        end
      else begin
        next_state=B;
        z=1;
        end
    endcase
  end
  ```

## 範例，檢測 1101 序列
- 完整範例，[檢測1101序列範例](fsm-sequence-1101/)

- 問題
  - 輸入 datain，當前序列的n位值，例如，1100 的第1位，datain=1
  - 輸出 flag，是否檢測到 1101 的序列，例如，flag=0=未檢測到1101序列
  - 轉移條件
    - 當 datain 和 序列值相同時，進入下一個狀態，只有序列符合 1101 時，才會進入 S4 狀態，
    - 只有在 S4 狀態時，輸出flag=1，否則輸出flag=0

- Moore 版本
  - 狀態表

    <img src="doc/state-machine/sequence-1101-moore-example-state-map.png" width=700 height=auto>
  
  - verilog 代碼
    ```verilog
    parameter s0=5'b00001;  // 0x1
    parameter s1=5'b00010;  // 0x2
    parameter s2=5'b00100;  // 0x4
    parameter s3=5'b01000;  // 0x8
    parameter s4=5'b10000;  // 0x10
    
    // ==== 第一段: 設置當前狀態 ====
    always@(posedge clk or negedge rst_n)begin
      if(!rst_n)
        currentState <= s0;
      else
        currentState <= nextState;
    end
    
    // ==== 第二段: 條件跳轉/下一個狀態 ====
    always@(*)begin
      case(currentState) 
        s0:if(in)
            nextState=s1;
          else
            nextState=s0;
        s1:if(in)
            nextState=s2;
          else
            nextState=s0;
        s2:if(in)
            nextState=s2;
          else
            nextState=s3;
        s3:if(in)
            nextState=s4;
          else
            nextState=s0;
        s4:if(in)
            nextState=s1;
          else
            nextState=s0;
        default:nextState=s0;
      endcase
    end

    // ==== 第三段: 設置輸出 ====
    always@(posedge clk or negedge rst_n)begin
      if(!rst_n)
        out <= 0;
      else if(currentState==s4)
        out <= 1;
      else
        out <= 0;
    ```   

  - 時序圖
    
    <img src="doc/state-machine/sequence-1101-moore-example-testbench.png" width=100% height=auto>

- Mealy 版本

  狀態圖
  
  <img src="doc/state-machine/sequence-1101-mealy-example-state-map.png" width=700 height=auto>
  
  verilog 代碼
  ```verilog
  parameter s0=4'b0001;
  parameter s1=4'b0010;
  parameter s2=4'b0100;
  parameter s3=4'b1000;

  // ==== 第一段: 配置當前狀態 ====
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)
      currentState <= s0;
    else
      currentState <= nextState;
  end
	 
  // ==== 第二段: 條件跳轉/下一個狀態 ====
  always@(*)begin
    case(currentState) 
      s0:
        if(in) nextState = s1;
        else nextState = s0;

      s1:
        if(in) nextState = s2;
        else nextState = s0;

      s2:
        if(in) nextState = s2;
        else nextState = s3;

      s3:
        if(in) nextState = s0;
        else nextState = s0;

      default:nextState=s0;
      
    endcase
  end

  // ==== 第三段: 設置輸出 ====
  always@(posedge clk or negedge rst_n)begin
    if(!rst_n)
      out<=0;
    else if(currentState == s3 && in == 1'b1)
      out<=1;
    else
      out<=0;
  end
  ```

  時序圖

  <img src="doc/state-machine/sequence-1101-mealy-example-testbench.png" width=100% height=auto>

## Ref
- [以 FSM 實現費伯納西數列(Fibonacci number)](https://ithelp.ithome.com.tw/articles/10194217)

- [FSM @ 小狐狸事務所](https://yhhuang1966.blogspot.com/2019/06/finite-state-machine.htmsl)

- [上下數計數器範例](https://yhhuang1966.blogspot.com/2019/06/finite-state-machine.html)

- [車輛超速範例](https://www.cnblogs.com/mikewolf2002/p/10197777.html)

- [verilog 有限狀態機的基本概念和三種寫法介紹](https://zhuanlan.zhihu.com/p/458310675)

- [狀態機: Moore and Mealy](https://blog.csdn.net/qq_42107624/article/details/120475045)