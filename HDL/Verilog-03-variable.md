## 變量 variable
- 所有的變數都是`全域變數`，只要加上`模組名稱`就可直接存取
  ```verilog
  module top;
    integer myglobalvar;
  endmodule

  module any;
    initial $display(top.myglobalvar);
  endmodule
  ```

- 變量的數值類型與`訊號傳遞的方式`有關

  - 變量位於賦值`左側`，代表`接收`上游數據，變量位於賦值`右側`，代表將變量的數據，`傳遞`給下游組件
    - 以 wire 類型變量為例，

      賦值右側代表輸入訊號，賦值左側代表輸出訊號，
      wire a = b，代表b訊號輸入給a訊號，讀取 a 的值，代表a訊號輸出

    - 以 reg 類型變量為例

      賦值右側代表輸入訊號，賦值左側代表輸出訊號，
      reg a = b，代表b訊號輸入給a正反器的輸入端，讀取 a正反器的值，代表a正反器的訊號輸出

  - 在`組合邏輯`中，輸入端有任何的變化，邏輯閘的結果會`立刻反映`到輸出端，
    透過賦值語句(=)表示了訊號傳遞的過程，代表訊號的傳遞會立刻完成，不會有任何延遲，
    訊號會`立刻傳遞`時，使用 wire，對應的硬體為`硬體連線`，不能保存值，不能用於描述邏輯電路，不能用於迴圈中，
    wire 無法保存值，若需要輸出多有變化，就需要連續輸入驅動，輸出端才有可能有變化
  
  - 在`時序邏輯`中，輸入端有任何變化，觸發器的結果`不會立刻反映`到輸出端，需要等待時脈的觸發，
    透過賦值語句(<=)表示了訊號傳遞的過程，代表訊號的傳遞不會立刻完成，會有延遲，
    訊號`不會立刻傳遞`，在觸發前`訊號會暫時保留在元件中`時，使用 reg，對應的硬體為`暫存器`，可用於描述邏輯電路，可用於迴圈，常用於過程代碼塊中

  - 對於`模擬`來說，

    對`輸入訊號`需要不斷地改變訊號值(重新賦值)並保持一段時間，以判斷每一種訊號對應的輸出是否正確，
    因此對輸入訊號需要宣告為 reg 變數，

    對於`輸出訊號`，只需要將DUT輸出的結果傳遞到外面，不需要重新賦值，因此對輸出訊號只需要宣告為 wire 變數

- `網線類型變量`
  - 用於描述兩個元件的連接，連接一個邏輯模塊的輸出(賦值給網線類型變量) 和 另一個邏輯模塊的輸入(讀取網線類型變量的值)

  - 可用於合成的網線類型變量
    - wire 類
    - tri 類
  
  - 不可用於合成的網線類型變量，無對應硬體，只能用於模擬
    - wor 類
    - trior 類
    - wand 類
    - triand 類
    - trireg 類
    - tri0 類
    - tri1 類
    - supply0 類
    - supply1 類
  
  - 網線類型變量的使用限制
    - 不能儲存值，可用於迴圈語句
    - 未賦值前，默認初始值為z
    - 只能在 assign 的語句中，透過 = 進行賦值
    - 不能在 always 的語句中進行賦值

- `暫存器類型變量`
  - 具有記憶效果儲存元件，

  - 可用於合成的暫存器類型
    - reg 類: 可儲存任意值類型的儲存元件
  
  - 不可用於合成的暫存器類型，無對應硬體，只能用於模擬
    - integer 類: reg 的限定版，限定儲存值類型必須是整數的儲存元件
    - time 類:  reg 的限定版，用於模擬時的時間設定
  
  - 暫存器類型變量的使用限制
    - 未賦值前，默認初始值為x
  
    - 暫存器`對應的實際硬體有多種可能`，
  
      有可能是正反器、栓鎖器、ROM、RAM、wire，
      依照 reg 使用的位置決定

    - 可儲存值，保持原有的數據，直到被改寫
  
    - 可用於迴圈語句

## 宣告模組或實際化模組時，對端口類型的要求
  <img src="doc/variable/port-type-limitation.png" width=80% height=auto>

- `宣告模組`，
  - input 接口的類型: wire類型
  - output 接口的類型: wire類型 或 reg類型
  - inout 接口的類型: wire類型

- `實例化模組(調用模組)`，
  - 傳入 input 接口的值類型: wire類型 或 reg類型
  - 傳入 output 接口的值類型: wire類型
  - 傳入 inout 接口的值類型: wire類型

- 注意，原則上接口的類型是`wire類型`，
  若模組中使用到 always 區塊時，改成`reg類型`才能在 always 區塊中進行賦值

## [網線類型] wire/tri (對應硬體為物理接線)

- wire 和 tri 的區別
  - `wire` : 
    - 若不指定輸出輸入接腳的類型時，默認使用 wire 類型
      ```verilog
      module Mymodule(out, in);
        ...
      endmodule
      
      等效於

      module Mymodule(wire out, wire in);
        ...
      endmodule

      等效於

      module Mymodule(out, in);
        ...
        output out;
        input in;
        ...
      endmodule
      ```

    - 可作為任何元件的輸入或輸出，且連接的元件沒有限制
  
  - `tri` : 
    - 可用於標記元件有使用三態門(tri-state-buffer)，但非必要
    - 建立三態門不一定需要使用到 tri變數
    - 只有標記作用，功能與 wire 相同，可與`控制訊號`或`輸出訊號連接`，見 [tri變數範例]()
    - [三態門元件基礎](Verilog-01-module.md#基本模塊-內建邏輯閘模塊)

- 注意事項
  - wire 變數不能在 always 區塊內賦值（=或<=）左側使用
  - wire 是 assign語句左側唯一的合法類型
  - wire 只能用於組合邏輯
  - [宣告模組或實際化模組時，對端口的類型有要求](#宣告模組或實際化模組時對端口類型的要求)

- 定義 wire
  - 語法，`wire [MSB位索引值:LSB位索引值] 數據線1, 數據線2, 數據線3, ...;`
    - 其中，`[MSB位索引值:LSB位索引值]`，用來指示wire變數值的存取方式，並可推算出值的位元數

    - 利用索引值可以推算出數據的寬度，`數據的寬度 = |最大值 - 最小值 + 1|`
      - 例如，wire [1:4]，代表 wire 的長度為 4-1+1 = 4bit，分別為 MSB:wire[1]、wire[2]、wire[3]、LSB:wire[4]
      - 例如，wire [0:3]，代表 wire 的長度為 3-0+1 = 4bit，分別為 MSB:wire[0]、wire[1]、wire[2]、LSB:wire[3]

    - 索引值用來存取對應位置的值，只是一個名稱，所以可以接受負值，可以拿來運算
      - 例如，wire [-1:2]，代表 wire 的長度為 2+1+1 = 4bit，分別為 MSB:wire[-1]、wire[0]、wire[1]、LSB:wire[2]

  - 語法，`wire [n-1:0] 總線名1, 總線名2, 總線名i, ...;`，可利用邏輯運算表示索引值
  - 語法，`wire [n:1] 總線名1, 總線名2, 總線名i, ...;`，索引值不需要從0開始

  - 範例，`wire a;`，定義1個1bit的wire類型變數a，不指定值的位元數時，由系統決定，有可能是 32bit 起跳
  - 範例，`wire [7:0] b;`，定義1個8bit的wire變數b
  - 範例，`wire [4:1] b, c;`，定義2個4bit的wire變數b和wire變數c
  - 範例，`wire gnd = 1'b0;`，定義+初始化

- wire變數的賦值

  - 將數值，傳遞給網線類型變量的來源，稱為驅動源

  - wire變數必須透過`連續賦值語句(assign)`來獲取值
    - 賦值符號(=)的左側，必須為網線變數類型
    - 賦值符號(=)的右側，必須為`描述電路的組合邏輯(可以得到值)`或`常數`，不可以是另一個網線類型的變量
      ```verilog
        wire a, b;

        // 正確用法，
        assign b = 10;
        
        // 錯誤用法，assign 語句中，= 的右側必須為值，或可以得到值的組合邏輯，不可以是另一個 wire變數
        assign a = b;     
      ```

- 範例，wire變數範例
  ```verilog
  module top(output o, input x, input y);

    wire _or, _and;

    assign _or = x | y;
    assign _and = y & _or;
    assign o = _and;

  endmodule
  ```

- 範例，tri變數範例
  ```verilog
  module top(out, in, en);

    input in, en;
    output out;
    
    // 以下兩種語句都可以建立三態閘，且不需要tri變數
    //bufif1  b1(out, in, en);		// 透過邏輯閘建模
    //assign out = en ? in : 'bz;	// 透過數據流建模
    
    // ==== 將 tri 變數與 en 訊號連接 ====
    // 非必要，bufif1  b1(out, in, en); 就能產生三態閘
      tri oe;
      assign oe = en;
      bufif1  b1(out, in, oe);

    // ==== 將 tri 變數與 out 訊號連接 ====
    // 非必要，bufif1  b1(out, in, en); 就能產生三態閘
    // 使 out 既是 output 類型，也是 tri 類型
    // 可以凸顯 out 是三態閘的輸出
    // tri out;
    // bufif1  b1(out, in, en);
    
  endmodule
  ```
  
- 多個驅動源的狀況 (多個賦值)

  <img src="doc/variable/multiple-drive-source.png" width=80% height=auto>

  - 多個驅動源時，網線變數的值(r)，由兩個驅動源共同決定
  - 狀況1，兩個驅動源輸出相同時，網線變數的值 = 驅動源輸出結果
  - 狀況2，兩個驅動源輸出不同時，網線變數的值 = x，不確定值
  - 狀況3，任一驅動源輸出x時，網線變數的值 = x，不確定值
  - 狀況4，任一驅動源輸出z時，網線變數的值 = 由不輸出Z的驅動源輸出決定

## [暫存器類型] reg (對應硬體為多種硬體類型)

- 注意事項
  - reg 可用於組合邏輯、時序邏輯、仿真 

  - reg 不一定被翻譯為暫存器
    - reg 是數據儲存單元的抽象，根據使用的位置不同，reg 會被翻譯成不同的硬體對象

    - `狀況1`，在帶有時鐘訊號的 always 區塊中，reg 被合成為`觸發器`
      > 例如， always @（posedge clk）

    - `狀況2`，在沒有時鐘訊號的 always 區塊中，reg 被合成為`物理接線(wire)`
      > 例如，always @（a or b or c）

    - `狀況3`，在不完全組合邏輯中，reg 被合成為`鎖存器`
  
  - reg 不一定需要有驅動源，
    > 在仿真模塊中，reg 單純作為變數，不需要驅動源

  - reg 不一定需要有時鐘信號驅動，
    > 在仿真模塊中，reg 單純作為變數，不需要時鐘訊號
  
  - reg 不能用於 assign 語句
  
  - [宣告模組或實際化模組時，對端口的類型有要求](#宣告模組或實際化模組時對端口類型的要求)

- 定義 reg
  - 和 wire 使用方式相同
  - 語法: `reg 變數名;`，用於指定1個reg具有1bit的值
  - 語法: `reg [MSB位的索引值:LSB位的索引值] 變數名;`，用於指定1個reg變數，該變數擁有nbit長度的值
    - 注意，[MSB位的索引值:LSB位的索引值] 是值的長度，reg變數仍然只有一個
    - bit 可以由索引值推算出來，`n = |MSB位的索引值 - LSB位的索引值+1|`
    - 索引值用來存取對應位置的值，只是一個名稱，所以可以接受負值，可以拿來運算
      - 例如，reg [-1:2]，代表 reg 的長度為 2+1+1 = 4bit，分別為 MSB:reg[-1]、reg[0]、reg[1]、LSB:reg[2]

  - 範例，reg counter;            //  定義1個具有1bit值的reg變數
  - 範例，reg [3:0] counter;      //  定義1個具有4bit值的reg變數，值的MSB為counter[3]，LSB為count[0]
  - 範例，reg [32-1:0] counter;   //  定義1個具有32bit值的reg變數，值的MSB為counter[31]，LSB為count[0]
  - 範例，reg [8:2] counter;      //  定義1個具有7bit值的reg變數，值的MSB為counter[8]，LSB為count[2]
  - 範例，reg [0:31] counter;     //  定義1個具有32bit值的reg變數，值的MSB為counter[0]，LSB為count[31]

- reg變數的賦值與傳遞
  - 建立 reg變數但不進行初始化時，reg變數的值為高阻抗(z)
  
  - reg 可以儲存負值，但是會被轉換為補數來儲存，例如，-1 會被轉換為 +15

  - reg變數的賦值，可以代表一組觸發器被觸發後，觸發器內部存儲單元的值
    因為 always 常用於描述觸發器，因此使用 reg 變數代表對觸發器內部狀態的改變
    
  - reg變數必須在使用`過程賦值`，例如，always 或 initial 區塊

    在[過程區塊](Verilog-09-process-block.md)內 (例如 initial 或 always)，
    透過 = 對`暫存器類型變量`進行賦值，不需要加 assign

    ```verilog
    reg rstn ;

    initial begin
        // 對 reg 變數進行賦值
        rstn = 1'b0 ;
        #100 ;
        rstn = 1'b1 ;
    end
    ```

  - 傳遞方式2，(傳出)
    ```verilog
    module top;
      reg [8:2] addr;
      reg [8:2] temp;
      initial begin
        addr = 7'b0000000;

        temp[3:2]= addr[8:7] + 1'b1;  // temp[3:2]=2'b01，因為加數 1'b1 只有1bit，因此 temp[3]=0

        addr[3:2] = 2'b11;  // addr[3]=1, addr[2]=1
        temp[3:2] = addr[3:2];  // temp[3]=1, temp[2]=1
      end
    endmodule
    ```
  
## [暫存器類型] integer (可合成，有符號數版的reg)
- 和 reg 的差異
  - 常用於迴圈中的變數
  - reg 變數為無符號數，integer變數為有符號數 

- 定義 integer 
  - 與 reg 相同
  - 不需要宣告數據總線的寬度，寬度由編譯器決定

- 範例
  ```verilog
  reg [31:0]      data1 ;
  reg [3:0]       byte1 [7:0]; //数组变量，后续介绍
  integer j ;  //整型变量，用来辅助生成数字电路
  always@* begin
      for (j=0; j<=3;j=j+1) begin
          byte1[j] = data1[(j+1)*8-1 : j*8];
          //把data1[7:0]…data1[31:24]依次赋值给byte1[0][7:0]…byte[3][7:0]
          end
  end
  ```

## [暫存器類型] time (不可合成，無對應硬體，用於仿真)
- 範例
  ```verilog
  time       current_time ;
  initial begin
        #100 ;
        current_time = $time ; //current_time 的大小为 100
  end
  ```

## [數組] 數組的使用
- 數組`不限制元素類型`，可以是 wire、reg、integer、time、 ... 等

- 定義數組
  
  在`單一變數宣告`的後方，加上數組符號 []
    - 例如，單一變數，reg [3:0] aa， 定義變數aa，變數值為 4bit
    - 例如，數組，reg [3:0] aa [1:0]，定義數組aa，aa[1] 和 aa[0] 都是值長度為4bit的reg

  其他範例

  ```verilog
  integer          flag [7:0] ; //8个整数组成的数组
  reg  [3:0]       counter [3:0] ; //由4个4bit计数器组成的数组
  wire [7:0]       addr_bus [3:0] ; //由4个8bit wire型变量组成的数组
  wire             data_bit[7:0][5:0] ; //声明1bit wire型变量的二维数组
  reg [31:0]       data_4d[11:0][3:0][3:0][255:0] ; //声明4维的32bit数据变量数组
  ```

- 對數組進行賦值
  ```verilog
  flag [1]   = 32'd0 ; //将flag数组中第二个元素赋值为32bit的0值
  counter[3] = 4'hF ;  //将数组counter中第4个元素的值赋值为4bit 十六进制数F，等效于counter[3][3:0] = 4'hF，即可省略宽度;
  assign addr_bus[0]        = 8'b0 ; //将数组addr_bus中第一个元素的值赋值为0
  assign data_bit[0][1]     = 1'b1;  //将数组data_bit的第1行第2列的元素赋值为1，这里不能省略第二个访问标号，即 assign data_bit[0] = 1'b1; 是非法的。
  data_4d[0][0][0][0][15:0] = 15'd3 ;  //将数组data_4d中标号为[0][0][0][0]的寄存器单元的15~0bit赋值为3
  ```

## [數組] 儲存器變數
- 儲存器是`專指元素為reg的數組`，用來描述RAM或ROM的操作
  
- 定義儲存器
  ```verilog
  reg               membit[0:255] ;  //256 bit的1bit存储器
  reg  [7:0]        mem[0:1023] ;    //1K byte存储器，位宽8bit
  mem[511] = 8'b0 ;                  //令第512个8bit的存储单元值为0
  ```

## [技巧] 利用寬度表示LSB位的索引值
- 利用減法表示
  ```
  // 語法
  [bit- : width]，從 bit 位開始遞減，遞減的寬度為 width

  // 範例，以下兩種表達方式是等效的
  A = data1[31-: 8];    // LSB位的值 = 從 31 開始遞減8位 = 24
  A = data1[31:24];     
  ```

- 利用加法表示
  ```
  // 語法
  [bit+ : width]，從 bit 位開始遞增，增加的寬度為 width

  // 範例，以下兩種表達方式是等效的
  B = data1[0+ : 8] ;   // LSB位的值 = 從 0 開始遞增8位 = 7
  B = data1[0:7] ;  
  ```

## 注意事項
- 注意，每一個定義的變量，都是有實際意義的接腳或信號，
  `重複調用變量，有可能會造成回授`

  ```verilog
  // 調用變量 x 兩次，使得輸出和輸入都連接到 x 接腳
  例如，or o1(x, x)
  ```

## Ref 
- [線網類型](https://www.cnblogs.com/mikewolf2002/p/10183255.html)
- [wor/trior、wand/triand、trireg、tri0/tri1、supply0/supply1，不可合成線網類型的真值表和仿真範例](https://www.cnblogs.com/mikewolf2002/p/10183255.html)
- [integer/time，不可合成暫存器類型的真值表和仿真範例](https://www.cnblogs.com/mikewolf2002/p/10183255.html)
- [integer 的使用](https://www.cnblogs.com/oomusou/archive/2008/05/27/verilog_integer.html)
- [數據類型 @ runoob](https://www.runoob.com/w3cnote/verilog-data-type.html)