// ref，https://www.fpga4fun.com/SerialInterface4.html
// ref，rs232編寫思路 @ p524-征途Mini《FPGA Verilog开发实战指南——基于Altera EP4CE10》2020.10.17.pdf
// ref，https://blog.csdn.net/qq_39814612/article/details/105605371

/*
  - 由於要將發和收的頻率統一起來，所以設計了一個脈衝發生器BaudGen，脈衝的頻率就是通信的波特率。
  - 在發送階段，每到一個脈衝，TxD_data[7:0]中的一位傳送到TxD端口。
  - 在接收階段，在每個脈衝處採樣一個RxD數據，將其送入RxD_data[7:0]

  因此，代碼分為三個module設計
  - 波特率發生器，用於設定發出信號的頻率和接受信號的頻率
  - 發射器，在TxD_start有效時，發射數據，數據由一個起始位（低電平），8位有效數據，2個停止位組成。
  - 接收器，在RxD端口信號採集到低電平後，開始對8個有效數據進行採樣，將每次採到的樣放入寄存器RxD_state中。
*/


/*
  波特率發生模塊。
  OverSampling參數: 可以在調用這個模塊時設置這個參數，當OverSampling=8，就會輸出一個頻率為8倍的脈衝波形，這可以用於接收模塊的排除偶然的異常跳轉。
*/
module BaudGen(clk,Clk_Sampling);
  input clk;
  output Clk_Sampling;
  reg Clk_Sampling = 0;
  reg [7:0] Baud_Count = 0;

  parameter OverSampling = 1;
  parameter N1 = 176/OverSampling;

  //50000000HZ clk/ x bps = 176 
  //actually 176 is a number you can set according to the baud
  parameter N2 = N1/2-1;//half of N1

  always @(posedge clk) begin
  if(Baud_Count == N1-1) 
    Baud_Count <= 0;
  else
    Baud_Count <= Baud_Count + 1;
  end
  always @(posedge clk) begin
  if(Baud_Count == N2) 
    Clk_Sampling <= 1;
  else
    Clk_Sampling <= 0;
  end

endmodule

/*
  數據發送模塊
  a.開始信號來臨，將TxD_data轉入TxD_shift。
  b.TxD_state開始跳轉，當開始信號來臨，TxD_state=4當正式開始發送數據時，TxD_state[3]為1，用這兩個信號判TxD_shift[0]和TxD是否相連。
  c.發送完成，TxD_state[3]變為0010，TxD值不再變化。
*/
module transmitter(clk,TxD_start,TxD_data,TxD);
  input clk,TxD_start;//TxD_start signal = 1, TxD_data -> TxD bit by bit
  input [7:0] TxD_data;
  output TxD;
  wire clk,TxD_start;
  wire [7:0] TxD_data;
  wire TxD;
  wire Clk_Sampling;
  reg [3:0]TxD_state=0;
  reg [7:0]TxD_shift=0;
  reg [7:0]TxD_count=0;

  parameter OverSampling =1;
  BaudGen #(OverSampling) u1(.clk(clk),.Clk_Sampling(Clk_Sampling));
  //generate a baud signal 

  always@(posedge clk)begin
  if(TxD_start)
    TxD_shift <= TxD_data;
  else if(TxD_state[3]&&Clk_Sampling)
    TxD_shift <= TxD_shift>>1;
  case(TxD_state)
    4'b0000:if(TxD_start) TxD_state <= 4'b0100;//wait until clk_sampling
    4'b0100:if(Clk_Sampling) TxD_state <= 4'b1000;//bit start
    4'b1000:if(Clk_Sampling) TxD_state <= 4'b1001;//bit0
    4'b1001:if(Clk_Sampling) TxD_state <= 4'b1010;//bit1
    4'b1010:if(Clk_Sampling) TxD_state <= 4'b1011;//bit2
    4'b1011:if(Clk_Sampling) TxD_state <= 4'b1100;//bit3
    4'b1100:if(Clk_Sampling) TxD_state <= 4'b1101;//bit4
    4'b1101:if(Clk_Sampling) TxD_state <= 4'b1110;//bit5
    4'b1110:if(Clk_Sampling) TxD_state <= 4'b1111;//bit6
    4'b1111:if(Clk_Sampling) TxD_state <= 4'b0010;//bit7
    4'b0010:if(Clk_Sampling) TxD_state <= 4'b0011;//bit stop1
    4'b0011:if(Clk_Sampling) TxD_state <= 4'b0000;//bit stop2
    default: if(Clk_Sampling) TxD_state <= 4'b0000;
  endcase 
  end
  assign TxD = (TxD_state<4) | (TxD_state[3] & TxD_shift[0]);
  //use the TxD_state[3] to determine if TxD_shift[0] be transferred into TxD or not. 
  //use the TxD_state<4 to determine the start

endmodule

/*
  數據接收模塊
  a.生成一個8倍頻率的脈衝波形
  b.對脈衝進行篩選，如果連續3次採樣都是同一個值，則將RxD信號存入RxD_bit
  c.再將8倍頻率的脈衝波形分頻1/8，這樣就和發射端的波特率一樣，用這個頻率去採樣RxD_bit信號。
  d.利用RxD_state[3]和RxD_state<4兩個條件判斷是否將採樣到的RxD_bit存入RxD_data中。
*/
module reciever(input clk, input RxD, output reg [7:0] RxD_data = 0);
  reg [3:0] RxD_state = 0;
  parameter OverSampling = 8;
  BaudGen #(OverSampling) u2(clk,Clk_Sampling);
  
  //generate a sampling signal whose frequency is 8 times faster than tne transmitter sending rate
  reg [2:0]SamplingCnt = 0;

  always@(posedge clk) if(Clk_Sampling) SamplingCnt <= (RxD_state==0) ? 1'd0 : SamplingCnt + 1'd1;
  wire SamplingNow = (Clk_Sampling)&&(SamplingCnt == OverSampling/2-1);
  
  //generate a signal that, its frequency is same as transmitter.
  reg RxD_bit;//0 is valid
  reg [1:0] Filter_Cnt = 2'b11;

  always@(posedge clk)begin
  if(Clk_Sampling)begin
    if(RxD == 1&&Filter_Cnt != 2'b11) Filter_Cnt <= Filter_Cnt + 1;
    else if(RxD == 0&&Filter_Cnt != 2'b00) Filter_Cnt <= Filter_Cnt - 1;
    if(Filter_Cnt == 2'b11) RxD_bit <= 1;
    else if(Filter_Cnt == 2'b00) RxD_bit <= 0;
  end
  end
  
  //use the 8times faster signal to sample RxD, if it gets 1 or 0 for 3 times in a row, the 1 or 0 siganl is considered valid;
  always@(posedge clk)begin
    case(RxD_state)
      4'b0000:if(~RxD_bit) RxD_state <= 4'b0100;//start to sampling
      4'b0100:if(SamplingNow) RxD_state <= 4'b1000;//bit start
      4'b1000:if(SamplingNow) RxD_state <= 4'b1001;//bit 0
      4'b1001:if(SamplingNow) RxD_state <= 4'b1010;//bit 1
      4'b1010:if(SamplingNow) RxD_state <= 4'b1011;//bit 2
      4'b1011:if(SamplingNow) RxD_state <= 4'b1100;//bit 3
      4'b1100:if(SamplingNow) RxD_state <= 4'b1101;//bit 4
      4'b1101:if(SamplingNow) RxD_state <= 4'b1110;//bit 5
      4'b1110:if(SamplingNow) RxD_state <= 4'b1111;//bit 6
      4'b1111:if(SamplingNow) RxD_state <= 4'b0001;//bit 7
      4'b0001:if(SamplingNow) RxD_state <= 4'b0000;//bit stop
      default :RxD_state <= 4'b0000;
    endcase
  end
  
  always @(posedge clk)
    if(SamplingNow && RxD_state[3]) RxD_data <= {RxD_bit, RxD_data[7:1]};
  //use the RxD_state[3] to determine if RxD be transferred into RxD_data or not, 
endmodule

/*
  testbench
  The RS-232 settings are fixed
  TX: 8-bit data, 2 stop, no-parity
  RX: 8-bit data, 1 stop, no-parity (the receiver can accept more stop bits of course)
*/
`timescale 1ns/1ns
module test_topme;
  reg clk=0;
  reg TxD_start=0;
  reg [7:0]TxD_data;
  wire TxD;
  wire [7:0] RxD_data;
  always#10 clk = ~clk;
  initial begin
  TxD_data <= 8'b01100001;
  #50  TxD_start <= 1'b1;
  #50  TxD_start <= 0;
  end
  transmitter u1(.clk(clk),.TxD_start(TxD_start),.TxD_data(TxD_data),.TxD(TxD));
  reciever u2(.clk(clk),.RxD(TxD),.RxD_data(RxD_data));
  endmodule 