/*
  Ethernet
  - Ethernet/IP/UDP packets，https://www.fpga4fun.com/10BASE-T2.html
*/

`include "ram512.v"

module TENBASET_TxD(clk20, RxD, Ethernet_TDp, Ethernet_TDm);
  input clk20; // 20MHz clock
  input RxD;   // RS-232 input
  output Ethernet_TDp, Ethernet_TDm;  // Ethernet 10BASE-T outputs

  //////////////////////////////////////////////////////////////////////
  wire [7:0] RxD_data;
  wire RxD_endofpacket, RxD_data_ready;
  async_receiver async_rxd(.clk(clk20), .RxD(RxD), .RxD_data_ready(RxD_data_ready), .RxD_data(RxD_data), .RxD_endofpacket(RxD_endofpacket));

  reg [8:0] wraddress, wrtotal, rdaddress;
  always @(posedge clk20) if(RxD_endofpacket) wraddress<=0; else if(RxD_data_ready) wraddress<=wraddress+1;
  always @(posedge clk20) if(RxD_endofpacket) wrtotal<=wraddress;

  wire [7:0] ram_output;
  ram512 ram_flash(
    .data(RxD_data), .wraddress(wraddress), .wren(RxD_data_ready), .clock(clk20),
    .q(ram_output), .rdaddress(rdaddress)
  );

  //////////////////////////////////////////////////////////////////////
  wire StartSending = RxD_endofpacket;
  reg SendingPacket;
  reg [3:0] ShiftCount;
  always @(posedge clk20) if(StartSending) SendingPacket<=1; else if(ShiftCount==14 && rdaddress==wrtotal) SendingPacket<=0;
  always @(posedge clk20) ShiftCount <= SendingPacket ? ShiftCount+1 : 15;
  wire readram = (ShiftCount==15);
  always @(posedge clk20) if(readram) rdaddress <= SendingPacket ? rdaddress+1 : 0;
  reg [7:0] ShiftData; always @(posedge clk20) if(ShiftCount[0]) ShiftData <= readram ? ram_output : {1'b0, ShiftData[7:1]};

  // generate an NLP every 13ms (the spec calls for anything between 8ms and 24ms).
  reg [17:0] LinkPulseCount; always @(posedge clk20) LinkPulseCount <= SendingPacket ? 0 : LinkPulseCount+1;
  reg LinkPulse; always @(posedge clk20) LinkPulse <= &LinkPulseCount[17:1];

  // TP_IDL, shift-register and manchester encoder
  reg SendingPacketData; always @(posedge clk20) SendingPacketData <= SendingPacket;
  reg [2:0] idlecount; always @(posedge clk20) if(SendingPacketData) idlecount<=0; else if(~&idlecount) idlecount<=idlecount+1;
  reg qo; always @(posedge clk20) qo <= SendingPacketData ? ~ShiftData[0]^ShiftCount[0] : 1;
  reg qoe; always @(posedge clk20) qoe <= SendingPacketData | LinkPulse | (idlecount<6);
  reg Ethernet_TDp; always @(posedge clk20) Ethernet_TDp <= (qoe ?  qo : 1'b0);
  reg Ethernet_TDm; always @(posedge clk20) Ethernet_TDm <= (qoe ? ~qo : 1'b0);

endmodule
