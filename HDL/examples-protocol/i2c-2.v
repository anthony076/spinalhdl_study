
`define Xilinx
//`define Altera

module I2CslaveWith8bitsIO(SDA, SCL, IOout);
inout SDA;
input SCL;
output [7:0] IOout;

// The 7-bits address that we want for our I2C slave
parameter I2C_ADR = 7'h27;

//////////////////////////
// I2C start and stop conditions detection logic
// That's the "black magic" part of this design...
// We use two wires with a combinatorial loop to detect the start and stop conditions
//  ... making sure these two wires don't get optimized away
`ifdef Xilinx
    BUF mybuf(
		.O(SDA_shadow),
		.I((~SCL | start_or_stop) ? SDA : SDA_shadow)
		);
    BUF SOS_BUF(
		.O(start_or_stop), 
		.I(~SCL ? 1'b0 : (SDA ^ SDA_shadow))
		); 
`else
    wire SDA_shadow = (~SCL | start_or_stop) ? SDA : SDA_shadow /* synthesis keep = 1 */;
    wire start_or_stop = ~SCL ? 1'b0 : (SDA ^ SDA_shadow) /* synthesis keep = 1 */;
`endif

//reg incycle;
reg incycle = 1'b0;  //for simulation MBL
always @(negedge SCL or posedge start_or_stop) 
	if(start_or_stop) incycle <= 1'b0; 
	else if(~SDA) incycle <= 1'b1;

//////////////////////////
// Now we are ready to count the I2C bits coming in
//reg [3:0] bitcnt;  // counts the I2C bits from 7 downto 0, plus an ACK bit
reg [3:0] bitcnt = 4'h0; //for simulation MBL
wire bit_DATA = ~bitcnt[3];  // the DATA bits are the first 8 bits sent
wire bit_ACK = bitcnt[3];  // the ACK bit is the 9th bit sent
//reg data_phase;
reg data_phase = 1'b0; //for simulation MBL

always @(negedge SCL or negedge incycle)
if(~incycle)
begin
    bitcnt <= 4'h7;  // the bit 7 is received first
    data_phase <= 1'b0;
end
else
begin
    if(bit_ACK)
    begin
    	bitcnt <= 4'h7;
    	data_phase <= 1'b1;
    end
    else
    	bitcnt <= bitcnt - 4'h1;
end

// and detect if the I2C address matches our own
wire adr_phase = ~data_phase;
//reg adr_match, op_read, got_ACK;
reg adr_match = 1'b0, op_read = 1'b0 , got_ACK = 1'b0; //for simulation MBL
//reg SDAr;
reg SDAr = 1'b0; //for simulation MBL
always @(posedge SCL) SDAr<=SDA;  // sample SDA on posedge since the I2C spec specifies as low as 0s hold-time on negedge
//reg [7:0] mem; 
reg [7:0] mem = 8'hFF; //for initial read access to slave and simulation MBL
wire op_write = ~op_read;

always @(negedge SCL or negedge incycle)
if(~incycle)
begin
    got_ACK <= 1'b0;
    adr_match <= 1'b1;
    op_read <= 1'b0;
end
else
begin
    if(adr_phase & bitcnt==7 & SDAr!=I2C_ADR[6]) adr_match<=1'b0;
    if(adr_phase & bitcnt==6 & SDAr!=I2C_ADR[5]) adr_match<=1'b0;
    if(adr_phase & bitcnt==5 & SDAr!=I2C_ADR[4]) adr_match<=1'b0;
    if(adr_phase & bitcnt==4 & SDAr!=I2C_ADR[3]) adr_match<=1'b0;
    if(adr_phase & bitcnt==3 & SDAr!=I2C_ADR[2]) adr_match<=1'b0;
    if(adr_phase & bitcnt==2 & SDAr!=I2C_ADR[1]) adr_match<=1'b0;
    if(adr_phase & bitcnt==1 & SDAr!=I2C_ADR[0]) adr_match<=1'b0;
    if(adr_phase & bitcnt==0) op_read <= SDAr;
    if(bit_ACK) got_ACK <= ~SDAr;  // we monitor the ACK to be able to free the bus when the master doesn't ACK during a read operation

    if(adr_match & bit_DATA & data_phase & op_write) mem[bitcnt] <= SDAr;  // memory write
end

// and drive the SDA line when necessary.
wire mem_bit_low = ~mem[bitcnt[2:0]];
wire SDA_assert_low = adr_match & bit_DATA & data_phase & op_read & mem_bit_low & got_ACK;
wire SDA_assert_ACK = adr_match & bit_ACK & (adr_phase | op_write);
wire SDA_low = SDA_assert_low | SDA_assert_ACK;
assign SDA = SDA_low ? 1'b0 : 1'bz;

assign IOout = mem;
endmodule

// ===================
// Testbench
// ===================

// Target Device: XC2C384 (Coolrunner 2) 
`timescale 1ns / 1ps
module sim_main;

	// Inputs
	reg reset;	// this signal serves as marker in the waveform diagram - no further meaning
	reg scl;

	// Bidirs
	tri1 sda;	// sda is pulled up in real world
	
	// Outputs
	wire [7:0] parallel_data_output_by_slave;	 

	integer serial_data_red_from_slave; // 8 bit number received via I2C bus from slave

	// Instantiate the Unit Under Test (UUT) -> the I2C slave in this case
	I2CslaveWith8bitsIO is1(
		.SDA(sda),
		.SCL(scl),
		.IOout(parallel_data_output_by_slave)
		);

	initial begin
		// Initialize Inputs
		scl = 1;
		release sda;		
		reset = 1;

		// Wait 100 ns for global reset to finish
		#100;
		// Add stimulus here
		
		reset = 0; // indicate start of write access in waveform diagram
		#10;
		reset = 1;

		// WRITE ACCESS TO SLAVE
		start;
		#50;

    tx_slave_address_wr(7'h27); // slave address is hard coded in line 19 of UUT
    tx_slave_data(8'h8D); // tx 8Dh to slave

		// slave parallel output should be 8Dh now
		$display("parallel hex data output by slave: %h" , parallel_data_output_by_slave);
		stop;
				
		reset = 0; // indicate start of read access in waveform diagram
		#10;
		reset = 1;
		
		// READ ACCESS TO SLAVE
		start;
		#50;
		tx_slave_address_rd(7'h27); // slave address is hard coded in line 19 of UUT
    rx_slave_data(serial_data_red_from_slave); // NOTE: data red is equal to data written previously.
																 // data is red from UUT internal register "mem" , not from parallel IO port !
																 // modify line 80 (reg [7:0] mem = 8'h7E;) to change initial data	
    
		$display("serial hex data red from slave memory register: %h" , serial_data_red_from_slave);
		stop;
		#50;

		reset = 0; // indicate end of simulation
		#10;
		reset = 1;

	end

	// tasks
	task start;
		begin
			//scl and sda are assumed already high;
			#50 force sda = 0;
			#50 scl = 0;
		end
	endtask

	task stop;
		begin
			//scl assumed already low;			
			#50 force sda = 0;
			#50 scl = 1;
			#50 release sda;
			#50;
		end
	endtask

	task tx_slave_address_wr;
		//scl and sda are assumed already low;			
		input integer slave_address;
		integer clock_ct;
		integer bit_ptr=7; // first bit to send is MSB of a total of 7 address bits !
		begin // do 7 clock cycles
			for (clock_ct = 0 ; clock_ct < 7 ; clock_ct = clock_ct + 1)
				begin
					#50 force sda = slave_address[bit_ptr]; //NOTE: forcing sda H is no elegant way since sda is of type "tri1"
					bit_ptr = bit_ptr - 1; // be ready for next address bit
					#50 scl = 1;
					#50 scl = 0;
				end
		#50 force sda = 0; //WRITE access requested
		#50 scl = 1;	//do 8th clock cycle 
		#50 scl = 0;

		ackn_cycle;
		end
	endtask

	task tx_slave_address_rd;
		//scl and sda are assumed already low;			
		input integer slave_address;
		integer clock_ct;
		integer bit_ptr=7; // first bit to send is MSB of a total of 7 address bits !
		begin // do 7 clock cycles
			for (clock_ct = 0 ; clock_ct < 7 ; clock_ct = clock_ct + 1)
				begin
					#50 force sda = slave_address[bit_ptr]; //NOTE: forcing sda H is no elegant way since sda is of type "tri1"
					bit_ptr = bit_ptr - 1; // be ready for next address bit
					#50 scl = 1;
					#50 scl = 0;
				end
		#50 force sda = 1; //READ access requested
		#50 scl = 1;	//do 8th clock cycle 
		#50 scl = 0;

		ackn_cycle;
		end
	endtask

	task tx_slave_data;
		//scl and sda are assumed already low;			
		input integer slave_data;
		integer clock_ct;
		integer bit_ptr=8; // first bit to send is MSB of a total of 8 data bits !
		begin // do 8 clock cycles
			for (clock_ct = 0 ; clock_ct < 8 ; clock_ct = clock_ct + 1)
				begin
					#50 force sda = slave_data[bit_ptr]; //NOTE: forcing sda H is no elegant way since sda is of type "tri1"
					bit_ptr = bit_ptr - 1; // be ready for next data bit
					#50 scl = 1;
					#50 scl = 0;
				end

		ackn_cycle;
		end
	endtask
	
	task rx_slave_data;
		//scl assumed already low;			
		//sda assumed already released (H) by previous ackn_cycle
		output [7:0] slave_data;
		integer clock_ct;
		integer bit_ptr=7; // first bit to receive is MSB of a total of 8 data bits !
		begin // do 8 clock cycles
			for (clock_ct = 0 ; clock_ct < 8 ; clock_ct = clock_ct + 1)
				begin
					#50 scl = 1;
					slave_data[bit_ptr] = sda;
					bit_ptr = bit_ptr - 1; // be ready for next data bit
					#50 scl = 0;
				end

		no_ackn_cycle;
		end
	endtask

	task ackn_cycle;
		begin
			#50 release sda;	//ackn from slave expected -> slave drives L on ACK
			#50 scl = 1;	//do 9th clock cycle 
			#50 scl = 0;
			//slave releases sda line now
		end
	endtask

	task no_ackn_cycle;
		begin
			//tx a notackn to slave -> sda remains released (H)
			#50 scl = 1;	//do 9th clock cycle 
			#50 scl = 0;
		end
	endtask
endmodule

