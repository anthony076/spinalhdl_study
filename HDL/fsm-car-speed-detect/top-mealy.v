module top(clk, w, rst_n, z);

  input clk;   
  input w;
  input rst_n; 
  output  reg z;
  reg  current_state, next_state; 

  parameter A=1'b0, B=1'b1;

  // 設置 current_state
  always @(posedge clk, negedge rst_n) begin
    if(rst_n==0)
    current_state <= A;
    else
    current_state <= Y;
  end

  // 設置 next_state
  always @(w, current_state)
  begin
    case(current_state)
      A:
          if(w==0) begin
          next_state=A;
          z=0;
          end
          else begin
          next_state=B;
          z=0;
      end

      B:
      if(w==0) begin
          next_state=A;
          z=0;
          end
      else begin
          next_state=B;
          z=1;
          end
      endcase
  end

endmodule