  module top(clk, w, rst_n, z);

    input clk;
    input w;      // 輸入訊號，是否超速
    input rst_n; 
    output z;     // 輸出訊號，是否設置減速訊號

    reg [1:0] current_state, next_state; 

    parameter A=2'b00, B=2'b01, C=2'b10;

    // 利用組合邏輯設置 next_state
    always @(w, current_state) begin
      case(next_state)
        A:
          if(w==0) next_state=A;  //  未超速，保持當前狀態
          else next_state=B;      //  超速，進入超速1次的狀態(B狀態)

        B:
          if(w==0) next_state=A;  //  已超速 -> 未超速，回到初始狀態
          else next_state=C;      //  超速，進入超速2次的狀態(C狀態)

        C:
          if(w==0) next_state=A;  //  已超速 -> 未超速，回到初始狀態
          else next_state=C;      //  持續超速，保持在當前狀態

        default: next_state=2'bxx;

      endcase
    end

    // 利用時序邏輯設置 current_state
    always @(posedge clk, negedge rst_n) begin
      if(rst_n == 0)
        current_state <= A;
      else
        current_state <= next_state;
    end

    // 利用組合邏輯設置 output
    assign z = (current_state == C);

  endmodule