`timescale 1ns/1ns
`define clock_period 20
//`include "top-moore.v"
`include "top-mealy.v"

module moore_tb;
  reg w;
  reg clk=1;
  reg Rst_n;
  wire z;

  top moore0(.clk(clk),.w(w),.Rst_n(Rst_n),.z(z));
  always # (`clock_period/2) clk = ~clk;

  initial begin
    w = 1'b0;
    Rst_n = 1'b1;
    #(`clock_period)
    Rst_n = 1'b0;
    #(`clock_period)
    Rst_n = 1'b1;
    w = 1'b0;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b0;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b0;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b0;
    #(`clock_period)
    w = 1'b1;
    #(`clock_period)
    w = 1'b1;

    #(`clock_period*20)
    $finish;
  end

endmodule