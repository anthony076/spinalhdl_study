`timescale 1ns/1ps
`define Clock 20

//`include "top_one.v"
//`include "top_two.v"
//`include "top_three.v"

module tb;

reg clk;
reg rst_n;
reg  [1:0] in;
wire [1:0] change;
wire out;

// 實例化模塊
top uut
(
	.clk (clk ),
	.rst_n (rst_n ),
	.in (in ),
	.change (change ),
	.out (out )
);


localparam S0 = 4'b0001;
localparam S1 = 4'b0010;
localparam S2 = 4'b0100;
localparam S3 = 4'b1000;

// 顯示當前狀態
reg [15:0] state_name ;
always@(*)begin
	case(u_FSM_3.currentState)
		S0: state_name = "S0";
		S1: state_name = "S1";
		S2: state_name = "S2";
		S3: state_name = "S3";
		default:state_name = "S0";
	endcase
end

initial begin
	$dumpfile("result.vcd");
	$dumpvars;	
end

initial begin
	clk = 1;
	forever
	#(`Clock/2) clk = ~clk;
end

initial begin
	rst_n = 0; #(`Clock*5+1);
	rst_n = 1;
end

// 設置輸入訊號
initial begin
	#1;
	in = 0;
	#(`Clock*5+1); //初始化完成

	//情况1--------------------------
	in = 1;         //投入1元，進入S1
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 1;         //投入1元，進入S2
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 1;         //投入1元，進入S3
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 1;         //投入1元，進入S0，輸出飲料，不找零
	#(`Clock*1);
	in = 0;
	#(`Clock*10);
		
	//情况2--------------------------
	in = 1;         //投入1元，進入S1
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 1;         //投入1元，進入S2
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 1;         //投入1元，進入S3
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 2;         //投入2元，進入S0，輸出飲料，找零1元
	#(`Clock*1);
	in = 0;
	#(`Clock*10);

	//情况3--------------------------
	in = 1;         //投入1元，進入S1
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 1;         //投入1元，進入S2
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 2;         //投入2元，進入S0，輸出飲料，不找零
	#(`Clock*1);
	in = 0;
	#(`Clock*10);
	
	//情况4--------------------------
	in = 1;         //投入1元，進入S1
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 2;         //投入2元，進入S3
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 2;         //投入2元，進入S0，輸出飲料，找零1元
	#(`Clock*1);
	in = 0;
	#(`Clock*10);

	//情况5--------------------------
	in = 2;         //投入2元，進入S2
	#(`Clock*1);
	in = 0;
	#(`Clock*1);
	in = 2;         //投入2元進入S0，輸出飲料，不找零
	#(`Clock*1);
	in = 0;
	#(`Clock*10);

	$finish;
end


endmodule