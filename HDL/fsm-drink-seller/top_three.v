module top
(
	input clk,
	input rst_n,

	input [1:0] in,
	
	output reg [1:0] change,
	output reg out
);

	reg [3:0] currentState;
	reg [3:0] nextState;

	localparam S0 = 4'b0001;
	localparam S1 = 4'b0010;
	localparam S2 = 4'b0100;
	localparam S3 = 4'b1000;

	// ==== 第一段: 配置當前狀態 ====
	always @(posedge clk or negedge rst_n)begin
		if(!rst_n)
			currentState <= S0;
		else
			currentState <= nextState;
	end

	// ==== 第二段: 跳轉條件和設置下一個狀態 ====
	always @(*)begin
		case(currentState)
			S0: begin
				if(in==1)
					nextState = S1;
				else if(in==2)
					nextState = S2;
				else
					nextState = currentState;
			end
			
			// ============
			S1: begin
				if(in==1)
					nextState = S2;
				else if(in==2)
					nextState = S3;
				else
					nextState = currentState;
			end
			
			// ============
			S2: begin
				if(in==1)
					nextState = S3;
				else if(in==2)
					nextState = S0;
				else
					nextState = currentState;
			end
			
			// ============
			S3: begin
				if(in==1 || in==2)      // in != 0也行
					nextState = S0;
				else
					nextState = currentState;
			end
			
			// ============
			default:nextState = S0;

		endcase
	end

	// ==== 第三段: 配置輸出 ====
	// 設置輸出，配置找零
	always @(posedge clk or negedge rst_n)begin
		if(!rst_n)
				change <= 0;
		else if(currentState==S3 && in==2)
				change <= 1;
		else
				change <= 0;
	end

	// 設置輸出，配置輸出
	always @(posedge clk or negedge rst_n)begin
			if(rst_n==1'b0)
					out <= 0;
			else if((currentState==S2 && in==2) || (currentState==S3 && in!=0))
					out <= 1;
			else
					out <= 0;
	end


endmodule