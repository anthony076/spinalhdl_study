module top
(
	input clk,
	input rst_n,
	input [1:0] in,
	output reg [1:0] change,
	output reg out
);

	reg  [3:0] currentState;
	reg  [3:0] nextState;
  
  // one-shot-coding
	localparam S0 = 4'b0001;
	localparam S1 = 4'b0010;
	localparam S2 = 4'b0100;
	localparam S3 = 4'b1000;

	// ==== 第一段: 設置當前狀態 ====
	always@(posedge clk or negedge rst_n)begin
		if(!rst_n)
				currentState <= S0;
		else
				currentState <= nextState;
	end

  // ==== 第二段: 設置下一個狀態和輸出 ====
	always@(*)begin
		case(currentState)
			S0: begin
				if(in==1)begin
					nextState = S1;
					end
				else if(in==2)begin
					nextState = S2;
					end
				else begin
					nextState = currentState;
					change     = 0 ;
					out = 0 ;
					end
			end
			
			// ==============
			S1: begin
				if(in==1)begin
					nextState = S2;
					end
				else if(in==2)begin
					nextState = S3;
					end
				else begin
					nextState = currentState;
					end
			end

			// ==============
			S2: begin
				if(in==1)begin
					nextState = S3;
					end
				else if(in==2)begin
					nextState = S0;
					out = 1 ;
					end
				else begin
					nextState = currentState;
					out = 0;
					end
			end

			// ==============
			S3: begin
				if(in==1)begin
					nextState = S0;
					out = 1 ;
					end
				else if(in==2)begin
					nextState = S0;
					change     = 1 ;
					out = 1 ;
					end
				else begin
					nextState = currentState;
					change     = 0;
					out = 0;
					end
			end

			// ==============
			default:nextState = S0;
		endcase

	end

endmodule