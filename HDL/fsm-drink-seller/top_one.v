module top (
	input clk,
	input rst_n,
	input [1:0] in,
	output reg [1:0] change,	// 找零
	output reg out		// 是否輸出飲料
);

reg  [3:0] currentState;

// 狀態編碼
localparam S0 = 4'b0001;
localparam S1 = 4'b0010;
localparam S2 = 4'b0100;
localparam S3 = 4'b1000;

always@(posedge clk or negedge rst_n)begin
	if(!rst_n)begin
			currentState   <= S0;
			change     <= 0 ;
			out <= 0 ;
			end
	else begin
		case(currentState)
			S0: begin
				if(in==1)begin
						currentState   <= S1;
				end
				else if(in==2)begin
						currentState   <= S2;
				end
				else begin
						change     <= 0 ;
						out <= 0 ;
				end
			end
			// ================
			S1: begin
				if(in==1)begin
						currentState   <= S2;
				end
				else if(in==2)begin
						currentState   <= S3;
				end
			end
			// ================
			S2: begin
				if(in==1)begin
						currentState   <= S3;
				end
				else if(in==2)begin
						currentState   <= S0;
						out <= 1 ;	// 輸出一瓶飲料，不找零
				end
			end
			// ================
			S3: begin
				if(in==1)begin
						currentState   <= S0;
						out <= 1 ;	// 輸出一瓶飲料，不找零
				end
				else if(in==2)begin
						currentState   <= S0;
						change     <= 1 ;	// 找零 1元
						out <= 1 ;	// 輸出一瓶飲料，
				end
			end
			
			// ================
			default:currentState   <= S0;

		endcase
	end
end

endmodule