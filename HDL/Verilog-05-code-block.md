## 串行代碼塊(begin...end) 和 並行代碼塊(fork...join)
- 使用場景
  - 場景1，用來界定代碼的範圍，見[if語句的範例](Verilog-08-condition.md)

  - 場景2，類似作用域的功能，`代碼塊可添加名稱`，
    - 為局部變量添加作用域，區分不同代碼塊的`同名變數`
    - 透過 disable 可以停止指定的代碼塊，類似C語言的 break 功能
    - 對於多層次內嵌的代碼塊，代碼塊名可以提供存取的路徑

- 代碼塊主要用`組織代碼`，常用於`判斷語句`或`迴圈語句`或`過程代碼塊`中

- 代碼塊可區分為以下幾種
  - 串行塊(begin...end)
  - 並行塊(fork...join)

- 串行塊(begin...end)
  - 所有的語句都是`自上而下依序進行`
    
  - 語法
    ```verilog
    // 代碼塊名，示情況添加，參考，迴圈語句
    begin: 代碼塊名
      執行語句1;
      執行語句2;
      ...
    end
    ```

- 並行塊(fork...join)
  - 所有的語句都是`並行運行`，即使是使用阻塞式賦值(=)也是並行的，

  - 語法
    ```verilog
    fork: 代碼塊名
      執行語句1;
      執行語句2;
      ...
    join
    ```

- 範例，fork-join 範例

  <img src="doc/process/fork-join-example.png" width=50% height=auto>

- 範例，串行塊和並行塊可以嵌套
  ```verilog
  `timescale      1ns/1ns
  
  module test ;
  
      reg [3:0]   ai_sequen2, bi_sequen2 ;
      reg [3:0]   ai_paral2,  bi_paral2 ;

      initial begin // 串行塊
          ai_sequen2         = 4'd5 ;    //at 0ns

          fork // 並行塊
              #10 ai_paral2          = 4'd5 ;    //at 10ns
              #15 bi_paral2          = 4'd8 ;    //at 15ns
          join

          #20 bi_sequen2      = 4'd8 ;    //at 35ns
      end
  
  endmodule
  ```

## disable 語句: 離開指定迴圈或任務(Task)
- 使用場景
  - 離開指定的代碼塊，可以是內部或外部的代碼塊
  - 離開 task，注意，不能用於 function

- 語法:
  ```verilog
  disable 代碼塊名
  ```

- 範例，利用 disable 離開指定的迴圈
  ```verilog
  always @* begin

    begin: aa
      integer i;
      first_bit = 0;

      for (i=0; i<=63; i=i+1) begin: bb
        if (i < start_range)
          disable bb; // 離開 bb 迴圈，跳轉到aa代碼塊中繼續執行

        if (i > end_range)
          disable aa; // 離開 aa代碼塊

        if ( data[i] ) begin
          first_bit = i;
          disable aa; // 離開 aa代碼塊
        end

      end // for

    end // begin

    // 其他處理代碼
    ...

  end // always
  ```

- 範例，利用 disable 離開Task
  ```verilog
  task add_up_to_max (
    input [5:0] max,
    output [63:0] result
  );

    integer i;

    begin
      result = 1;

      if (max == 0)
        disable add_up_to_max; // 結束任務
      
      for (i=1; i<=63; i=i+1) begin
          result = result + result;
          if (i == max)
            disable add_up_to_max; // 結束任務
      end // for

    end // begin

  endtask // task
  ```

- 範例，停止嵌套模塊的迴圈
  ```verilog
  `timescale 1ns/1ns
  
  module test;
  
      initial begin: aa   // aa 代碼塊
          integer    i_d ;
          i_d = 0 ;
          while(i_d<=100) begin: bb   // bb 代碼塊
              # 10 ;
              if (i_d >= 50) begin      
                  disable cc.clk_gen ;  // 停止外部 cc 的 clk_gen 迴圈代碼塊
                  disable bb ;          // 停止當嵌的 bb 迴圈代碼塊
              end
              i_d = i_d + 10 ;
          end
      end
  
      reg clk ;
      initial begin: cc     // cc 代碼塊
          while (1) begin: clk_gen  // clk_gen 迴圈代碼塊
              clk=1 ;      #10 ;
              clk=0 ;      #10 ;
          end
      end
  
  endmodule
  ```