`timescale 1ns/1ps
`include "top.v"

module test ;
	reg clk, rstn ;
	reg en ;
	reg din_rvs ;
	wire flag_safe, flag_dgs ;

	// 實例化待測模組
	top uut
	(
		.clk (clk),
		.rstn (rstn),
		.en (en),
		.din_rvs (din_rvs),
		.flag(flag_dgs)
	);

	// 輸出波形
	initial begin
		$dumpfile("result.vcd");
		$dumpvars;
	end

	// 配置 clock
	initial begin
		rstn = 1'b0 ;
		clk = 1'b0 ;
		#5 rstn = 1'b1 ;
		forever begin
				#5 clk = ~clk ;
		end
	end

	// 配置 輸入訊號
	initial begin
		en = 1'b0 ;
		din_rvs = 1'b1 ;
		#19; en = 1'b1 ;		// 模擬輸入端的傳輸延遲，在 #19 後，din_rvs 和 en 同相
		#1; din_rvs   = 1'b0 ;
	end

	// 配置結束條件
	initial begin
			forever begin
					#100;
					if ($time >= 1000)  $finish ;
			end
	end

endmodule // test