// ==== 會產生 glitch 的範例 ====
// module top
// (
// 	input clk ,
// 	input rstn ,
// 	input en ,
// 	input din_rvs ,
// 	output reg flag
// );

// 	// 容易產生毛刺的邏輯
// 	wire condition = din_rvs & en;

// 	always @(posedge clk or negedge rstn) begin
// 		if (!rstn) begin
// 				flag <= 1'b0 ;
// 		end
// 		else begin
// 				flag <= condition ;
// 		end
// 	end

// endmodule

// ==== 改善 glitch 的範例 ====
module top
(
	input clk ,
	input rstn ,
	input en ,
	input din_rvs ,
	output reg flag
);

	reg din_rvs_r ;
	reg en_r ;

	always @(posedge clk or negedge rstn) begin
			if (!rstn) begin
					din_rvs_r <= 1'b0 ;
					en_r <= 1'b0 ;
			end
			else begin
					din_rvs_r <= din_rvs ;
					en_r <= en ;
			end
	end

	wire condition = din_rvs_r & en_r ;
	
	always @(posedge clk or negedge rstn) begin
			if (!rstn) begin
					flag <= 1'b0 ;
			end
			else begin

					flag <= condition ;
			end
	end

endmodule