// 10進制bcd計數器
module BCD(
  input clk,
  input rst,
  input Cin,
  output reg[3:0]sum,
  output Cout
    );
	
always@(posedge clk)begin
  if(rst)
    sum <= 0;
  else if(Cin)
    sum <= (sum == 9) ? 0 : sum+1;   
end

assign Cout = (sum == 9 & Cin);

endmodule

// 利用十進位BCD計數器組出從0計數到9999的BCD計數器
module BCD_counter(
  input clk,
  input rst,
  output [15:0]result
);

genvar i;
wire Ci[3:0];
wire Co[3:0];

assign Ci[0] = 1;
assign Ci[1] = Co[0];
assign Ci[2] = Co[1];
assign Ci[3] = Co[2];

assign result = {sum[3],sum[2],sum[1],sum[0]};

wire [3:0]sum[3:0];
generate
  for(i=0;i<4;i=i+1)begin
    BCD 
    B(
      .clk(clk),
      .rst(rst),
	  .Cin(Ci[i]),
	  .sum(sum[i]),
	  .Cout(Co[i])  
    );
  end
endgenerate  