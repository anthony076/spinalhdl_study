#include <windows.h>

HANDLE hComm;

void OpenComm()
{
  DCB dcb;

  hComm = CreateFile("COM1:", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
  if(hComm==INVALID_HANDLE_VALUE) exit(1);
  if(!SetupComm(hComm, 4096, 4096)) exit(1);
  
  if(!GetCommState(hComm, &dcb)) exit(1);
  dcb.BaudRate = 115200;
  dcb.ByteSize = 8;
  dcb.Parity = NOPARITY;
  dcb.StopBits = 0;
  if(!SetCommState(hComm, &dcb)) exit(1);
}

void CloseComm()
{
  CloseHandle(hComm);
}

DWORD WriteComm(char* buf, int len)
{
  DWORD nSend;
  if(!WriteFile(hComm, buf, len, &nSend, NULL)) exit(1);

  return nSend;
} 

void WriteCommByte(BYTE b)
{
  WriteComm(&b, 1);
}

DWORD ReadComm(char *buf, int len) 
{
  DWORD nRec;
  if(!ReadFile(hComm, buf, len, &nRec, NULL)) exit(1);

  return nRec;
}

void main()
{
  OpenComm();

  // initialize the LCD module
  WriteCommByte(0x38);   // "Function Set" in 8 bits mode
  WriteCommByte(0x0F);   // "Display ON" with cursors ON
  WriteCommByte(0x01);   // "Clear Display", can take up to 1.64ms, so the delay
  Sleep(2);

  // display "hello"
  WriteCommByte('h'+0x80);
  WriteCommByte('e'+0x80);
  WriteCommByte('l'+0x80);
  WriteCommByte('l'+0x80);
  WriteCommByte('o'+0x80);

  CloseComm();
}
