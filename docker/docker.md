## Dockerfile-bootcamp
- spinalhdl-bootcamp
  - This docker image is for the environment of [Spinal-bootcamp](https://github.com/jijingg/Spinal-bootcamp).
  - The repo of Spinal-bootcamp has been included in docker image.
  - The dockerfile could be found in [here](https://gitlab.com/anthony076/spinalhdl_study)

  - docker-image on [DockerHub](https://hub.docker.com/r/chinghua0731/spinalhdl-bootcamp)

- environment
  - python v3.8.10
  - jupyter v6.4.9
  - almond v0.10.0

- usage
  
  `docker run -p 8888:8888 -it chinghua0731/spinalhdl-bootcamp`


## Dockerfile-icestorm-toolchain
- This Repo
  - open source Verilog-to-Bitstream flow for iCE40 FPGAs.

  - [iceStorm official](https://clifford.at/icestorm)

  - [dockerfile](https://gitlab.com/anthony076/spinalhdl_study/-/tree/main/docker)

  - docker-image on [DockerHub](https://hub.docker.com/repository/docker/chinghua0731/icestorm-toolchain
  )

- Environment
  - For each  tool listed below, you can find pacman installer (*.zst) at `install`
  
  - ArchLinux
  
  - [icestorm on AUR](https://aur.archlinux.org/packages/icestorm-git)
  
    `docker pull chinghua0731/icestorm-toolchain:icestorm`
    
  - [arachne-pnr on AUR](https://aur.archlinux.org/packages/arachne-pnr-git)

    `docker pull chinghua0731/icestorm-toolchain:arachnepnr`

  - [nextpnr on AUR](https://aur.archlinux.org/packages/nextpnr-git)

    `docker pull chinghua0731/icestorm-toolchain:nextpnr`

  - [yosys on AUR](https://aur.archlinux.org/packages/yosys-git)

    `docker pull chinghua0731/icestorm-toolchain:yosys`

- icestorm-toolchain options
  - icestorm + arachnepnr + yosys

    `docker pull chinghua0731/icestorm-toolchain:icestorm-arachnepnr-yosys`

  - icestorm + nextpnr + yosys

    `docker pull chinghua0731/icestorm-toolchain:icestorm-nextpnr-yosys`

## Dockerfile-risc-gcc
- riscv-gcc
  - The dockerfile could be found in [Here](https://gitlab.com/anthony076/spinalhdl_study)
  - docker-image on [DockerHub](https://hub.docker.com/r/chinghua0731/riscv-gcc)
  - The pacman installer (*.zst) of riscv-gcc is located at `install`

- environment
  - Archlinux
  - [riscv-gnu-toolchain on AUR](https://aur.archlinux.org/packages/riscv-gnu-toolchain-bin)
  
- usage
  - docker pull chinghua0731/riscv-gcc
  - docker run -it chinghua0731/riscv-gcc bash

## Dockerfile-spinalhdl-dev
- This Repo
  - The environment for spinalhdl-project development.
  - The dockerfile could be found in [Here](https://gitlab.com/anthony076/spinalhdl_study)
  - docker-image on [DockerHub](https://hub.docker.com/r/chinghua0731/spinalhdl-dev)

- environment
  - Archlinux:latest
  - scala
  - sbt build system
  - yosys
  - iverilog (Icarus Verilog)
    > docker pull chinghua0731/spinalhdl-dev:icarus
    
  - gtkwave (for checking waveform)
  - x11 enable
  
- usage
  - docker pull chinghua0731/spinalhdl-dev
  - docker run -it chinghua0731/spinalhdl-dev bash