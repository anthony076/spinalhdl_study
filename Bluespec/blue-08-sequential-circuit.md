## 撰寫時序電路
- 在時序電路中，`Reg變數`和`Wire變數`是`可合成類型`，
  
  Reg變數會建立實際的暫存器硬體，Wire變數會建立硬體之間的連線，
  
  因此變數值都是透過`內建模塊的實例化`建立，需要透過函數調用的方式執行實例化

  - 例如，實例化 Reg變數，`Reg#(Int(4)) aa <- mkReg(0);`

    例如，實例化 Wire變數，`Wire#(int) <- mkDWire(0);`

- 幾種實例化Reg的`FF模塊`:
  - `方法1`，mkReg(初始值) : 需要提供初始值
    ```verilog
    Reg#(int) x <- mkReg(23);  //初值=23
    ```

  - `方法2`，mkRegU : 不需要提供初始值，所以的初始值為 Unknown，可能為0或1
    ```c
    Reg#(int) y <- mkRegU;     //初值未知，但類型確定是 int
    ```

  - `方法3`，mkDReg(默認值) : 只保留一個clock的寫入值，其他時間都讀出默認值
  
    參考，[mkDReg的使用](#模塊-mkdreg-的使用)

  - `方法4`，mkCReg(接口數，初始值): 並發暫存器 = mkReg() + mkDWire()

  - 注意，
    - mkReg / mkRegU / mkDReg 只使用`1個`Reg#(type td)的接口
    - mkCReg 使用`多個`Reg#(type td)的接口


- 幾種實例化Wire的`Wire模塊`:
  - `方法1`，mkWire: 沒有寫入值時，讀取語句就`不執行`
  - `方法2`，mkDWire(默認值): 沒有寫入值時，讀取語句就`返回默認值`，D = Default
  - `方法3`，mkBypassWire: 必定要有寫入值，否則編譯無法通過，Bypass = cant bypass compiler
  - `方法4`，mkRWire: 沒有寫入值時，讀取語句就`返回Maybe類型的無效值`，R = Restrict
  - `方法5`，mkPulseWire: 沒有寫入值時，讀取語句就`返回 False`，Pulse = return True/False only

  - Wire模塊使用的接口
    - 接口1，mkWire、mkDWire、mkBypassWire 三者都使用 `Wire#(type td)` 接口
      - 讀取函數: _read()
      - 寫入函數: _write()
  
    - 接口2，mkRWire 使用 `RWire#(type td)` 接口
      - 讀取函數: wget()，返回 Maybe類型值
      - 寫入函數: wset()

    - 接口3，mkPulseWire 使用 `PulseWire#(type td)` 接口
      - 讀取函數: _read()，返回 Bool類型值
      - 寫入函數: send()

  - 注意，在 時序電路中使用wire變量的目的在於`建立時序電路中的組合邏輯`，
    
    除了透過`wire模塊建立組合邏輯`外，也可以透過[定義變量+賦值](blue-07-combinational-circuit.md)的方式建立組合邏輯

- FF模塊/Wire模塊的比較

  |                           | FF模塊       | Wire模塊     | Comment            |
  | ------------------------- | ------------ | ------------ | ------------------ |
  | 保存數據                  | 可保存       | 無法保存     |                    |
  | 寫入週期 (clk n)          | 無法反映新值 | 立刻反映新值 | 值只能保留一個週期 |
  | 寫入後下一個週期(clk n+1) | 出現新值     | 恢復到預設值 | 值可以持續保留     |

  - 範例，FF模塊和Wire模塊的差異
    ```verilog
    module mkTb ();
      Reg#(int) cnt <- mkReg(0);
      rule up_counter;
          cnt <= cnt + 1;
          if(cnt > 3) $finish;
      endrule

      Wire#(int) w1 <- mkDWire(99);  // w1 默認值為 99
      Reg#(int)  r1 <- mkReg(99);    // r1 初始值為 99
      
      rule test1 (cnt%2 == 0);     // 只有 clk 在 2的倍數週期才執行寫入 (clk=0、2、4、...) 
          w1 <= cnt;
      endrule

      rule test2 (cnt%2 == 0);     // 只有 clk 在 2的倍數週期才執行寫入 (clk=0、2、4、...)
          r1 <= cnt;
      endrule

      rule show;
          $display("cnt=%2d   w1=%2d   r1=%2d", cnt, w1, r1);
          // cnt= 0   w1= 0   r1=99   // 寫入週期，w1 值立刻反應寫入值，r1值尚未反映
          // cnt= 1   w1=99   r1= 0   // 寫入週期+1，w1 無法保存值，恢復默認值99，r1反映出寫入值

          // cnt= 2   w1= 2   r1= 0   // 寫入週期，
          // cnt= 3   w1=99   r1= 2

          // cnt= 4   w1= 4   r1= 2   // 寫入週期，
      endrule
    endmodule
    ```

## FF/Wire模塊的實現 和 暫存器賦值的簡化
- FF模塊的實現
  - 暫存器模塊(mkReg、mkRegU、mkDReg)都繼承自`Reg#(type td)`接口類，
  
  - `type td`，代表調用暫存器模塊時，需要傳入值，且值的類型為通用類型td，
    
    因為是通用類型，代表類型還沒決定，因此使用 `td` 作為類型名的代稱，類似泛型類型<T>

  - Action-Method，`_write(新值)`，為暫存器值的 `setter` 函數，用於寫入暫存器中的值
    
    General-Method，`_read()`，為暫存器值的 `getter` 函數，用於讀取暫存器中的值
  
  - 父類，`Reg#(type td)`接口的代碼
    ```verilog
    interface Reg#(type td);         // 調用暫存器模塊時會傳入的值，值的類型為通用類型 td
      method Action _write (td x);   // setter
      method td _read;               // getter
    endinterface
    ```

  - 子類，以 Reg#(type td) 實現 mkReg() 模塊
    ```verilog
    module mkReg#(td v) (Reg#(td))  // (td v) 是模塊的輸入參數，(Reg#(td)) 是mkReg繼承的接口 Reg#(td)
      // provisos，對 td 進行類型的限制，td 必須是 Bits() 的類型，參考 Interface的進階使用
      provisos (Bits#(td, sz));     
      ... 內容省略 ...
    endmodule
    ```

- Wire模塊的實現，以 mkDWire模塊為例
  - 和 Reg#(type td)接口一樣，
    - _write() 是 setter 函數，用於將值寫入 wire變數中
    - _read() 是 getter 函數，用於讀出wire變數的值

  - 父類，`Wire#(type td)`接口的代碼
    ```verilog
    interface Wire#(type td);
      method Action _write (td x);  // setter
      method td _read;              // getter
    endinterface
    ```

- `<= 符號`是 getter / setter 函數的簡化
  
  - 在未簡化之前，寫入/讀取暫存器的值，都需要調用 Reg變#(type td) 的 _write() / _read() 進行

  - 利用 <= 簡化 _write() / _read() 的調用，會執行先讀取再寫入的操作
  
  - 例如
    ```verilog
    module top ();
      Reg#(int) x <- mkReg(0);
      
      rule up_counter;              
        // 簡化前
        x._write( x._read + 1 );   // 先讀出x的值，+1後再寫回x
        
        // 簡化後
        //x <= x + 1;
        
        $display ("x=%d", x._read );
      endrule
      
      rule done (x >= 26); // x >=26 時，執行此rule代碼塊
        $finish;
      endrule

    endmodule
    ```

## 控制讀寫順序的調度註解(Scheduling-Annotation)
- 為什麼需要調度註解
  
  以變更FF的狀態為例，當 clock 觸發 FF 動作時，新的FF值與舊的FF值有一定的聯繫關係時，
  例如，`x <= x + 1;`，為了確保新值x的正確性，必須`先讀取舊值x的內容後`，進行+1的操作後，`再賦值給新值x`
  
  由上述可知，在 clock 觸發的同一個周期內，`對同一個暫存器讀取和寫入(賦值)的操作具有一定的順序性`，
  因此，需要由調度註解來`控制讀取/寫入方法的執行順序`，以確保操作的正確性

  對於內建的模塊，bsv 有內建的調度規則，每個模塊依照特性會`有不同的調度規則`，
  另外，bsv 也提供`調度屬性(Scheduling-Attribute)`，可以為自定義模塊添加調度規則

- 調度符號和調度順序

  <img src="doc/sequential-circuit/Scheduling-Annotation-Table.png" width=1500 height=auto>

  - <font color=red>注意，(mA和mB寫在不同的rule代碼中) != (mA和mB在不同的clock週期中執行)</font>
    
    不同的 rule代碼塊，有可能`在同一個 clock 週期內都會被執行`，例如，同時在 clock 上緣被觸發，

    編譯器編譯時，對於`兩個同時執行的模塊`，編譯器依舊會區分`執行的先後`，利用不同的rule代碼塊區分執行的先後
    
  - `原則1`，reg模塊，在寫入新值後，在下一個clk週期才會反映新值，在下一個clk週期來臨前的讀值會讀到舊值
    - 為了確保`在寫入週期讀到舊值`，因此在執行寫入前，需要先讀取並保存舊值 => `讀取優先`
      > FF 的特性: 在寫入週期，寫入的操作未完成，在寫入週期先返回就值，寫入週期的下一個週期，FF 才會反映新值
    - 讀取優先並不會改變暫存器的狀態 => 不需要將寫入和讀取分開不同的 rule代碼塊

  - `原則2`，wire模塊，在寫入新值後，在同一個clk週期就會立刻反映新值，無法保存值
    - 為了確保`在寫入週期讀到新值`，因此在執行讀取前，需要先執行寫入 => `寫入優先`
    - 寫入優先會改變暫存器的狀態 => 需要將寫入和讀取分開不同的rule代碼塊，利用不同rule代碼塊，強制對讀寫順序進行區分
    - 幾種建立 wire模塊的方法 (mkWire、mkDWire、mkBypassWire、mkPulseWire) 擁有相同的調度規則，
      差別只在`調度方法的名稱不同`

- 違規處理和隱式執行條件

  SAR/SBR 的調度規則，決定了模塊讀寫函數違規時的行為
  
  以`methodA SAR methodB`的調度規則為例，此規則指明了 `methodA的rule代碼塊必須出現在methodB的rule代碼塊之後` 的規則，
  若違反了此規則，會有兩個處理方式
  
  - 違規處置1，讀取語句`不執行`，此時，調度規則又稱為`隱式的執行條件`
  - 違規處置2，讀取語句`會執行`，但不會返回寫入值
  
  > 注意，不是違反調度規則就會為讀取語句添加隱式執行條件，只有不執行的讀取語句才擁有隱式執行條件
  
  調度規則是`判斷模塊調用讀寫方法是否違規`的判斷依據，若符合規定，返回寫入值，

  反則，違規後的讀取函數的返回值，會依照模塊而有所差異
  - mkWire: 若違規時，讀取的語句`不會執行` (隱式條件)
  - mkDWire: 若違規時，讀取的語句`返回默認值`
  - mkBypassWire: 沒有隱式條件，違規就無法通過編譯 
  - mkRWire: 若違規時，讀取的語句`返回Maybe類型的無效值`
  - mkPulseWire: 若違規時，讀取的語句`返回False`

  以 mkWire 模塊為例，`_read(methodA) SAR _write(methodB)`，
  代表 _read的rule代碼塊必須出現在_write的rule代碼塊之後，否則符合違規條件，會造成讀取的語句不會執行

  ```verilog
  module mkTb ();
    Reg#(int) cnt <- mkReg(1);
    rule up_counter;
        cnt <= cnt + 1;
        if(cnt > 7) $finish;
    endrule

    Wire#(int) w1 <- mkWire;
    Wire#(int) w2 <- mkWire;
    
    rule test1 (cnt%2 == 0);     // rule-test1，只有在 clk = 0、2、4、6 的週期執行
        w1 <= cnt;    // 執行寫入
    endrule

    rule test2 (cnt%3 == 0);     // rule-test2，只有在 clk = 0、3、6、9 的週期執行
        w2 <= cnt;    // 執行寫入
    endrule

    rule show;     // rule-show，只有在 clk = 0、6、12 的週期執行
        
        // display 同時調用了 w1._read() 和 w2._read()，必須兩個條件都滿足，才會執行此語句
        //    clk = 0、2、4、8 ... w1 有執行寫入，因此可讀取w1，但 w2 沒有執行寫入，因此不會執行此語句
        //    clk = 0、3、9 ... w2 有執行寫入，因此可讀取w2，但 w1 沒有執行寫入，因此不會執行此語句
        $display("cnt=%1d   w1=%2d   w2=%2d", cnt, w1, w2);
    endrule
  endmodule
  ```

- 調度註解範例
  - [mkReg、mkRegU、mkDReg 的調度註解](#模塊-mkreg-的使用)
  - [mkWire、mkDWire 的調度註解](#模塊-mkwire-的使用)
  - [mkRWire 的調度註解](#模塊-mkrwire-的使用)
  - [mkPulseWire 的調度註解](#模塊-mkpulsewire-的使用)
  
## [模塊] mkReg 的使用
- `mkReg、mkRegU、mkDReg`	的調度註解

  | mkReg、mkRegU、mkDReg | mB = _read            | mB = _write                |
  | --------------------- | --------------------- | -------------------------- |
  | mA = _read            | CF (任意順序/同Rule)  | SB (_read優先/同Rule)      |
  | mA = _write           | SA (_read優先/同Rule) | SBR (_write必須在不同Rule) |

  - (mA = _read) `CF` (mB = _read) : _read 和 _read (多次讀取) 不會發生衝突，可以任意顛倒順序，可寫在同一個 Rule 中
  - (mA = _read) `SB` (mB = _write) : _read 在 _write 之前 (讀取優先)，可寫在同一個 Rule 中
  - (mA = _write) `SA` (mB = _read) : _write 在 _read 之後 (讀取優先)，可寫在同一個 Rule 中
  - (mA = _write) `SBR` (mB = _write) : _write 和 _write (多次寫入) 會發生衝突，不能寫在同一個 Rule 中
  
  - 結論: 
    - 從上表可知，mkReg、mkRegU、mkDReg 在使用讀寫函數時，讀取函數擁有更高的優先權
    - 若違反了以上的調度規則，編譯器會在編譯時拋出錯誤
      ```verilog
      // 錯誤，違反調度規則的範例
      rule test1;
        x <= cnt + 1;       // <= 會調用 x._write() 一次
        x <= cnt + 99;      // 再度調用 x._write()，同一個 clock 週期內，不能調用 _write() 兩次
      endrule
      
      // 正確，透過條件式讓兩個 _write 不要同時執行
      rule test1;
        if(cnt%2 == 0)
         x <= cnt + 1;
      else
         x <= cnt + 99;
      endrule
      
      // 正確，需要寫在不同的 rule 中
      //    注意，在 clock 觸發時，rule-aa 和 rule-bb 都會被執行，
      //    但編譯器仍然會區分順序，此時，寫在前面的 rule-aa 會先被執行，x 會被後執行的 rule-bb 覆蓋
      rule aa;
        x <= cnt + 1;
      endrule

      rule bb;
        x <= cnt + 99;
      endrule
      ```

- mkReg 使用範例
  ```verilog
  module mkTb ();
        Reg#(int) x <- mkReg(23);
        
        rule up_counter;
          x <= x + 1;
          //x._write( x._read + 1 );  // 等效語法

          $display ("x=%d", x.);
          //$display ("x=%d", x._read );  // 等效語法
        endrule
        
        rule done (x >= 26);
          $finish;
        endrule
    endmodule
  ```

## [模塊] mkDReg 的使用
- 使用 mkDReg 前，需要先導入庫，`import DReg::*;`

- [mkDReg 的調度註解](#模塊-mkreg-的使用)

- mkDReg 只有在寫入週期後的下一個週期，會讀出寫入值(只保留一周期的寫入結果)，之後會恢復默認值
  
  例如，
  - clock D，執行寫入
  - clock D+1，讀出寫入值
  - clock D+2，讀出默認值
  - clock D+3，讀出默認值

- 範例
  ```verilog
  module top ();
    Reg#(int) cnt <- mkReg(0);

    rule up_counter;  // 每次clk都會觸發
        cnt <= cnt + 1;
      if(cnt > 9)  $finish;
    endrule
    
    Reg#(int) g_reg <- mkReg(99);   // g_reg 初值  = 99
    Reg#(int) d_reg <- mkDReg(99);  // d_reg 默认值 = 99

    rule test (cnt%3 == 0);     // 只有在clk=0、3、6、9時執行寫入
        g_reg <= -cnt;
        d_reg <= -cnt;
    endrule

    rule show;
        $display("cnt=%2d    reg1=%2d    d_reg=%2d", cnt, g_reg, d_reg);
    endrule
  endmodule
  ```

  <img src="doc/sequential-circuit/mkDReg-waveform.png" width=800 height=auto>

## [模塊] mkCReg 的使用，並發暫存器

- mkCReg 可以具有多個 Reg#() 的接口，建立`一個`具有`多個接口`的暫存器，
  可以透過不同的接口修改暫存器中的值

- mkCReg 是 mkReg 和 mkDWire 的複合體

  像 mkReg 一樣，mkCReg `可以保存舊值`，  
  像 mkDWire 一樣，mkCReg 若執行了寫入的操作，會`立刻在同一個 clk 週期讀取到新值`，
  不需要像一般的暫存器，在下一個 clk 週期才會讀取到`新值`

  透過不同的接口，可以在同一個週期中，選擇要讀取舊值或是讀取新值
  - (index)小的接口，讀取舊值
  - (index)大的接口，讀取新值

- 允許每周期讀取多次，寫入多次
  ```verilog
  // 實例化1個具有3組接口的暫存器，透過 creg[0]、creg[1]、creg[2] 存取特定的接口
  Reg#(int) creg [3] <- mkCReg(3, 0);

  ```

- 語法: mkCReg(接口數，初始值)

- `mkCReg` 的調度註解

  | mkCReg             | mB = crg[i]._read | mB = crg[i]._write | mB = crg[j]._read | mB = crg[i]._write |
  | ------------------ | ----------------- | ------------------ | ----------------- | ------------------ |
  | mA = crg[i]._read  | CF                | SB                 | SBR               | SBR                |
  | mA = crg[i]._write | SA                | SBR                | SBR               | SBR                |

  - 對於`同一個接口`，讀取/寫入函數的調用，表現出和`mkReg`一樣的特性，`讀取優先`(SA、SB)
  - 對於`不同個接口`，index小的接口的寫入函數，和index大的接口的讀取函數，表現出和`mkDWire`一樣的特性，`寫入優先`(SBR)

- 範例，從`小接口`讀取`舊值`
  ```verilog
  module mkTb ();
    Reg#(int) cnt <- mkReg(23);       // cnt 從 23 開始

    rule up_counter;
        cnt <= cnt + 1;
        if(cnt > 32) $finish;
    endrule
    
    Reg#(int) creg [3] <- mkCReg(3, 0);

    rule r5 (cnt%5 == 0);     // 每5的周期執行一次，clk = 0、5、10、... 時觸發
        creg[0] <= creg[0] + 1;       
    endrule

    rule r3 (cnt%3 == 0);     // 每3的周期執行一次，clk = 0、3、6、... 時觸發
        creg[1] <= creg[1] + 1;       
    endrule

    rule r2 (cnt%2 == 0);     // 每2的周期執行一次，clk = 0、2、10、... 時觸發
        creg[2] <= creg[2] + 1;       
    endrule

    rule show;
        // 注意，因為是從小接口讀取值，因此得到的是舊值
        $display("cnt=%2d    creg0=%2d", cnt, creg[0]);   // 由 cnt 觸發
        
        // cnt=23    creg= 0 ，沒有任何規則被觸發
        // cnt=24    creg= 0 ，r3、r2     被觸發，+1 執行2次 0+2=2
        // cnt=25    creg= 2 ，r5         被觸發，+1 執行1次 2+1=3
        // cnt=26    creg= 3 ，r2         被觸發，+1 執行1次 3+1=4
        // cnt=27    creg= 4 ，r3         被觸發，+1 執行1次 4+1=5
        // cnt=28    creg= 5 ，r2         被觸發，+1 執行1次 5+1=6
        // cnt=29    creg= 6 ，沒有任何規則被觸發 保存值 6
        // cnt=30    creg= 6 ，r5、r3、r2 被觸發，+1 執行3次 6+3=9
        // cnt=31    creg= 9 ，沒有任何規則被觸發 保存值 9
        // cnt=32    creg= 9 ，r2         被觸發，+1 執行1次 9+1=10
        // cnt=33    creg= 10

    endrule
  endmodule
  ```

- 範例，從`大接口`讀取`新值`: 需要新增讀取接口，並透過該接口讀新值
  ```verilog

  module mkTb ();
    Reg#(int) cnt <- mkReg(23);

    rule up_counter;
        cnt <= cnt + 1;
        if(cnt > 32) $finish;
    endrule
    
    Reg#(int) creg [4] <- mkCReg(4, 0);  // 注意，此處是4個接口

    rule rule_test5 (cnt%5 == 0);
        creg[0] <= creg[0] + 1;
    endrule

    rule rule_test3 (cnt%3 == 0);
        creg[1] <= creg[1] + 1;
    endrule

    rule rule_test2 (cnt%2 == 0);
        creg[2] <= creg[2] + 1;
    endrule

    rule show;
        $display("cnt=%2d    creg[3]=%2d", cnt, creg[3]);  // 從新的接口讀新值
    endrule
  endmodule
  ```

---

## [模塊] mkWire 的使用
- mkWire 使用 [Wire#(type td)](#ffwire模塊的實現-和-暫存器賦值的簡化) 的接口模塊

- `mkWire、mkDWire` 的調度註解

  | mkWire、mkDWire | mB = _read              | mB = _write               |
  | --------------- | ----------------------- | ------------------------- |
  | mA = _read      | CF (任意順序/同Rule)    | SAR (_write優先/不同Rule) |
  | mA = _write     | SBR (_write優先/同Rule) | C (_write必須在不同Rule)  |

  - (mA = _read) `CF` (mB = _read) : _read 和 _read (多次讀取) 不會發生衝突，可以任意顛倒順序，可寫在同一個rule代碼塊中
  - (mA = _read) `SAR` (mB = _write) : _read的的rule代碼塊，必須出現在 _write的rule代碼塊之後 (寫入優先)，否則 _read 不會被執行
  - (mA = _write) `SBR` (mB = _read) : _write的的rule代碼塊，必須出現在 _read的rule代碼塊之前 (寫入優先)，否則 _write 不會被執行
  - (mA = _write) `SBR` (mB = _write) : _write 和 _write (多次寫入) 會發生衝突，不能寫在同一個rule代碼塊中

  - 從上表可知，
    - mkWire、mkDWire 在使用讀寫函數時，寫入函數擁有更高的優先權(寫入優先)
    - 讀取和寫入的操作不能同時寫在同一個rule代碼塊
    - 在同一個clk週期內，必須`先執行寫入，讀取才會被執行` (_read 執行的隱式條件)

- 使用方式
  
  mkWire `不需要提供初始值或默認值`，
  因為 mkWire 只有在執行寫入後，`讀取的語句才會執行`，因此，不會出現需要初始值或默認值的情況

- 範例
  ```verilog

  module mkTb ();
    Reg#(int) cnt <- mkReg(1);
    rule up_counter;
        cnt <= cnt + 1;
        if(cnt > 7) $finish;
    endrule

    Wire#(int) w1 <- mkWire;
    
    rule write (cnt%2 == 0); // clk = 0、2、4、6...
        w1 <= cnt;
    endrule

    rule show;
        // 只有在 clk = 0、2、4、6... 會執行寫入，
        // 因此，只有在 clk = 0、2、4、6... 才會執行讀取 w1 的語句
        $display("cnt=%1d w1=%2d", cnt, w1);
    endrule
  endmodule
  ```

## [模塊] mkDWire 的使用
- mkDWire 使用 [Wire#(type td)](#ffwire模塊的實現-和-暫存器賦值的簡化) 的接口模塊

- [mkDWire 的調度註解](#模塊-mkwire-的使用)

- 使用方法
  
  在同一個clk週期內，若有執行寫入，則讀取的返回值是寫入值，否則讀取的返回值是默認值

## [模塊] mkBypassWire 的使用
- mkDWire 使用 [Wire#(type td)](#ffwire模塊的實現-和-暫存器賦值的簡化) 的接口模塊

- 使用方式

  行為和 mkWire 相同，只要求`每個clk週期都必須執行寫入`，否則會報錯，
  因此，mkBypassWire 的讀取函數，_read()，永遠會被執行

## [模塊] mkRWire 的使用
- mkRWire 使用 `RWire#(type td)` 的接口模塊
  ```verilog
  interface RWire#(type td);    
    method Action wset(td x);  // 寫入時，調用 wset()
    method Maybe#(td) wget();  // 讀取時，調用 wget()，注意，返回的是 Maybe#(td) 類型
  endinterface: RWire
  ```

- 注意，mkRWire 的讀值方法，wget() 的`返回值`是 `Maybe#(td) 類型值`，
  - 利用 isValid() 判斷返回值是否有效
  - 利用 fromMaybe() 對類型值取值
  - 詳見，[Maybe 類型值的使用](blue-04-primitive-datatype.md#maybe類型)

- mkRWire 的調度註解
  
  | mkRWire   | mB = wget             | mB = wset               |
  | --------- | --------------------- | ----------------------- |
  | mA = wget | CF (任意順序/同Rule)  | SAR (wset優先/不同Rule) |
  | mA = wset | SBR (wset優先/同Rule) | C (wset必須在不同Rule)  |

  - (mA = wget) `CF` (mB = wget) : wget 和 wget (多次讀取) 不會發生衝突，可以任意顛倒順序，可寫在同一個rule代碼塊中
  - (mA = wget) `SAR` (mB = wset) : wget的的rule代碼塊，必須出現在 wset的rule代碼塊`之後` (寫入優先)，否則返回無效值
  - (mA = wset) `SBR` (mB = wget) : wset的的rule代碼塊，必須出現在 wget的rule代碼塊`之前` (寫入優先)，否則返回無效值
  - (mA = wset) `SBR` (mB = wset) : wset 和 wset (多次寫入) 會發生衝突，不能寫在同一個rule代碼塊中

  - 從上表可知，
    - mkRWire 在使用讀寫函數時，寫入函數擁有更高的優先權(寫入優先)
    - 讀取和寫入的操作不能同時寫在同一個rule代碼塊

- 使用方式:
  
  在同一個clk週期內，必須`有執行寫入，則讀取到的返回值才是Maybe類型的有效值`，否則讀取到的返回值是Maybe類型的無效值
  
## [模塊] mkPulseWire 的使用
- mkPulseWire 使用 `PulseWire#(type td)` 的接口模塊
  ```verilog
  interface PulseWire;      
    method Action send();  // 寫入時，調用 send()，用於寫入 True 或 False
    method Bool _read();   // 讀取時，調用 _read()，注意，返回的是 Bool 類型
  endinterface
  ```

- 注意，mkPulseWire 的讀值方法，_read() 的`返回值`是 `Bool 類型值`，

- mkPulseWire 的調度註解
  
  | mkPulseWire | mB = _read            | mB = send               |
  | ----------- | --------------------- | ----------------------- |
  | mA = _read  | CF (任意順序/同Rule)  | SAR (send優先/不同Rule) |
  | mA = send   | SBR (send優先/同Rule) | C (send必須在不同Rule)  |

  - (mA = _read) `CF` (mB = _read) : _read 和 _read (多次讀取) 不會發生衝突，可以任意顛倒順序，可寫在同一個rule代碼塊中
  - (mA = _read) `SAR` (mB = send) : _read的的rule代碼塊，必須出現在 send的rule代碼塊`之後` (寫入優先)，否則返回False
  - (mA = send) `SBR` (mB = _read) : send的的rule代碼塊，必須出現在 _read的rule代碼塊`之前` (寫入優先)，否則返回False
  - (mA = send) `SBR` (mB = send) : send 和 send (多次寫入) 會發生衝突，不能寫在同一個rule代碼塊中

  - 從上表可知，
    - mkRWire 在使用讀寫函數時，寫入函數擁有更高的優先權(寫入優先)
    - 讀取和寫入的操作不能同時寫在同一個rule代碼塊

- 使用方式
  - mkPulseWire是不帶數據的RWire，只用來傳遞是否有效的信號
  - 在同一個clk週期內，必須`有執行寫入，則讀取到的返回值才是True`，否則讀取到的返回值是False

## Ref
- [module 基礎](blue-03-module-basic.md)
- [泛型的使用](blue-polymorphism.md)
- [Interface的進階使用](blue-interface-and-module-adv.md)
