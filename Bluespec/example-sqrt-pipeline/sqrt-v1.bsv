// ==== 等效的 python 代碼 ====
// 原理: 使用逐次逼近迭代法求平方根，
//    step1，利用移位得到一個猜測值t，猜測值t同時也是新的 x 值
//    step2，將猜測值t與x進行比較，並設置新的x和y值，透過遞歸函數重複執行上述操作
//    step3，重複遞歸函數16次後，得到開平方的結果(近似值)

// 初始值
// x = 114514                      # x 為輸入數據
// y = 0                           # y 為計算結果

// 遞歸函數
// for n in range(15, -1, -1):     # 循環 16 次
//     t = (y << 1 << n) + ( 1 << n << n)   # 
//     if x >= t:                  # 迭代体
//         x -= t                  # 迭代体
//         y += (1<<n)             # 迭代体
// print(y)                        # 打印开方结果 √114514

package Sqrt_v1;

import DReg::*;


module mkTb();

   // ==== implement ====

   // 遞歸函數，根據輸入的 x、y、n 建立猜測值t，並根據猜測值t的結果，設置新的x、y
   // 此函數會被重複調用16次
   function Tuple2#(UInt#(32), UInt#(32)) sqrtIteration( Tuple2#(UInt#(32), UInt#(32)) data, int n );
      match {.x, .y} = data;

      let t = (y<<1<<n) + (1<<n<<n);
      if(x >= t) begin
         x = x - t;
         y = y + (1<<n);
      end
      
      return tuple2(x, y);
   endfunction
   
   // 透過 DReg 傳遞流水線的中間結果，DReg 也稱為流水線段暫存器(Stage-Reg)
   Reg#( Tuple2#(UInt#(32), UInt#(32)) ) dregs [17];

   // 實例化 17個 DReg
   for(int n=16; n>=0; n=n-1)
      dregs[n] <- mkDReg( tuple2(0, 0) );

   // 建立 16個 rule，每個 rule 都會有一個DReg的模塊，
   //   每個DReg的模塊的輸入是 sqrtIteration 組合邏輯的輸出， 
   //   sqrtIteration 組合邏輯的輸入，是上一個DReg的模塊的輸出
   for(int n=15; n>=0; n=n-1)
      rule pipe_stages;
         dregs[n] <= sqrtIteration( dregs[n+1] , n );
      endrule
   
   // ==== test bench ====

   Reg#(UInt#(32)) cnt <- mkReg(1);

   rule sqrter_input;
      UInt#(32) x = cnt * 10000000;                             

      dregs[16] <= tuple2(x, 0);                                
      $display("input:%d      output:%d", x, tpl_2(dregs[0]));  // 從流水線最末端的FF中取出數據
      
      cnt <= cnt + 1;
      if(cnt > 40) $finish;
   endrule

endmodule


endpackage