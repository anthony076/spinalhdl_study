
## Ref 
- [bsv by examples @ 官方教程](http://csg.csail.mit.edu/6.S078/6_S078_2012_www/resources/bsv_by_example.pdf)

- [Bluespec SystemVerilog Reference Guide](https://web.ece.ucsb.edu/its/bluespec/doc/BSV/reference-guide.pdf)

- [中文 Bluespec SystemVerilog (BSV) 教程](https://github.com/WangXuan95/BSV_Tutorial_cn)

- [Bluespec BSV Tutorial](https://github.com/rsnikhil/Bluespec_BSV_Tutorial)

- [Intro Bluespec User Guide](https://github.com/kcamenzind/BluespecIntroGuide/blob/master/BluespecIntroGuide.md)

- [Bluespec SystemVerilog 和 Bluespec Haskell 的 repo](https://cgithub.com/bsvlang/main)

- [Bluespec Compiler (BSC)](https://github.com/B-Lang-org/bsc)

- 相關文章
  - [Bluespec 學習](https://www.zhihu.com/people/lzc-42-12-68/posts)

- 應用
  - [AES加速](https://github.com/bluespec/Accel_AES)

- 應用: 開源 riscv 設計
  - [Piccolo](https://github.com/bluespec/Piccolo)
  - [Flute](https://github.com/bluespec/Flute)
  - [Toooba](https://github.com/bluespec/Toooba)

- 比較，
  - [bsv vs verilog vs spinahdl](https://www.zhihu.com/question/26816009?utm_id=0)