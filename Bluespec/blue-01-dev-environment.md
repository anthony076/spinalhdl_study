## 利用 WSL 建立開發環境
- step1，安裝 BlueSpec-Compiler (bsc)

  - 1-1，從官方下載，Ubuntu 的版本，
    
  	https://github.com/B-Lang-org/bsc/releases

    注意，不要在 windows 下解壓縮，

  - 1-2，確保使用的 WSL 是 ubuntu-20.04
  	- `$ wsl -l`
  	- `$ wsl --set-version Ubuntu-20.04 2`

  - 1-3，啟動 WSL，並切換到壓縮檔的路徑 (Desktop)
    
    cmd > wsl > cd Desktop

  - 1-4，在 wsl 中解壓縮 
    
    `$ tar xvf bsc-2022.01-ubuntu-20.04.tar.gz`

  - 1-5，建立編譯器放置的資料夾，
    
    `$ sudo mkdir -p /opt/tools/bsc`

  - 1-6，複製到指定位置，
    
    `$ sudo mv bsc-2022.01-ubuntu-20.04 /opt/tools/bsc/bsc-2022.01-ubuntu-20.04`

  - 1-7，切換到 bsc 目錄 並建立軟連結
    - `$ cd /opt/tools/bsc`
    - `$ sudo ln -s bsc-2022.01-ubuntu-20.04 latest`

      此命令會建立 `/opt/tools/bsc/latest` 的軟連結，可透過 `$ ls -l` 查看

  - 1-8，將軟連結添加到系統路徑中
  	- 暫時添加，
      
        `$ export PATH=/opt/tools/bsc/latest/bin:$PATH`

  	- 永久添加
  		- `$ nano ~/.bashrc`

  		- 添加以下
            ```shell
            export PATH=/opt/tools/bsc/latest/bin:$PATH
            export LIBRARY_PATH=/opt/tools/bsc/latest/lib:$LIBRARY_PATH
            ```
      - 重新啟動 terminal

  - 1-9，測試
    
    `$ bsc -help`

- step2，在 wsl 中安裝 iverilog、tcl-del、gtkwave
  
  `$ apt-get install iverilog tcl-dev gtkwave`

- step3，推薦 vscode 插件
  - Bluespec System Verilog

- step4，使用 [第三方的script進行編譯(bsvbuild.sh)](https://gitee.com/wangxuan95/BSV_Tutorial_cn)

  編譯範例見，[BlueSpec專案開發模板](bsv-dev-template/)

## Virtualbox 建立開發環境 (透過 ssh 連接)
- step0，下載 ubuntu 20.04 x64 -server 版本的 iso，並在 virtualbox 上進行安裝
  
  注意，安裝時需要啟用 openssh

- step1，設置 ssh 連線


  - `1-1`，檢查 openssh 是否啟用，確保 ssh 在啟用狀態
    > $ sudo service ssh status

  - `1-2`，檢視 ubuntu 的 ip (ex: 10.0.2.15)
    > $ ip -4 addr

  - `1-3`，檢視 virtualbox-server 的 ip (ex: 192.168.56.1/24)
    > File > Tool > Network-manager > check IPv4 Prefix 
    
  - `1-4`，在 virtualbox 中添加連接埠轉發
    - 1-4-1，選擇 ubuntu-vm > 設定 > 網路 > 介面卡-1 > 進階 > port-forwarding

    - 1-4-2，添加以下內容
  		- host-ip = 192.168.56.1 (指定 virtualbox-server-ip)
  		- host-port = 22
  		- customer-ip = 10.0.2.15 (指定 ubuntu-vm-ip)
  		- customer-port = 22

    - 1-4-3，重新啟動 ubuntu-vm

  - step6，在 windows 中，利用 ssh 連線
    > 打開 powershell，輸入: ssh 使用者名稱@ubuntu-vm-ip

- step2，掛載共享資料夾
  - 2-1，在 virtualbox 中添加共享資料夾

    注意，`唯讀`和`自動掛載`的選項不要勾選 

  - 2-2，在 virtualbox 中插入 VBoxGuestAdditions.iso
  
  - 2-3，(必要) 在 ubuntu-vm 中安裝 VirtualBox-guest-additions
    - 注意，需要安裝 VirtualBox-guest-additions，共享資料夾才會有效 (才會有vboxsf，virtualbox-shared-folder)

    - 2-3-1，在 ubuntu-vm 中掛載 VBoxGuestAdditions.iso 
      > $ sudo mount -t iso9660 /dev/cdrom /media/cdrom

    - 2-3-2，安裝 VBoxGuestAdditions 的依賴
      > $ sudo apt-get update
      
      > $ sudo apt-get install -y build-essential linux-headers-`uname -r`

    - 2-3-3，執行 VBoxGuestAdditions.iso 中的安裝腳本
      > $ sudo /media/cdrom/./VBoxLinuxAdditions.run

    - 2-3-4，重新啟動 ubuntu-vm
      > $ sudo reboot now

  - 2-4，掛載共享資料夾
    - 注意，在 ubuntu-vm 中的資料夾名不要和 virtualbox 中設置的分享資料夾名相同，
      否則會發生找不到目錄的錯誤

    - 2-4-1，在 ubuntu-vm 中建立分享資料夾
      > $ mkdir ~/share

    - 2-4-2，(暫時性)掛載共享資料夾
      > $ sudo mount -t vboxsf test ~/share

    - 2-4-3，(永久性)掛載共享資料夾
      > $ sudo nano /etc/fstab

      添加以下
      ```shell
      test /home/ching/share vboxsf defaults 0 0
      ```

      > $ sudo nano /etc/modules

      添加以下
      ```shell
      vboxsf
      ```

    - 2-4-4，重新啟動 ubuntu-vm
      > $ sudo reboot now
