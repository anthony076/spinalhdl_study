// 功能：8*8 的矩陣轉置，以BRAM作為緩存

package top;

import BRAM::*;

// 定義接口
interface TransposeBuffer;
   method Action rewind;            // 重置用，若當前有一塊沒有完全寫完，撤銷已寫入數據，重新寫入
   method ActionValue#(int) get;    // 像模塊中讀取轉置前的數據，由左向右依序讀取
   method Action put(int val);      // 像模塊中寫入轉置後的數據，由上向下依序寫入
endinterface

// 定義8*8矩陣轉置器模塊
module mkTransposeBuffer88 (TransposeBuffer);
  // 實例化bram，建立雙口的BRAM
  //    BRAM2Port#( 位址類型, 數據類型 ) ram <- mkBRAM2Server(配置);
  //    Tuple3#(bit, UInt#(3), UInt#(3))，將地址區分為三個變數組成: block-n | col-or-row-index | col-or-row-index
  BRAM2Port#( Tuple3#(bit, UInt#(3), UInt#(3)) , int ) ram <- mkBRAM2Server(defaultValue);
  // defaultValue 的內容
  //   memorySize   : 0,      // 存储器容量 = 2^(位址線的寬度)，在此例中ADDR為 7bit = 1+3+3
  //   loadFormat   : None,   // 不進行文件初始化
  //   latency      : 1,      // 響應的延遲時間為1
  //   outFIFODepth : 3,      // BRAM模塊內的緩衝大小為3
  //   allowWriteResponseBypass : False

  // 注意，位址中的block-n位置只占用1bit，但 wblock 占用 2bit，wblock 的高位值用於判斷空或滿，不會被當成位址
  //    wblock 表示寫入的區塊和狀態，column-index 表示寫入當前block的column位置，row-index column-index 表示寫入當前block的row位置
  //    rblock 表示讀取的區塊和狀態，column-index 表示寫入當前block的column位置，row-index column-index 表示寫入當前block的row位置
  Reg#(Bit#(2))  wblock <- mkReg(0);    // 標記寫入block的位置和指示block狀態
  Reg#(UInt#(3)) wcol <- mkReg(0);      // 標記當前寫入的 column 位置
  Reg#(UInt#(3)) wrow <- mkReg(0);      // 標記當前寫入的 row 位置

  Reg#(Bit#(2))  rblock <- mkReg(0);    // 標記讀取block的位置和指示block狀態
  Reg#(UInt#(3)) rcol <- mkReg(0);      // 標記當前讀取的 column 位置
  Reg#(UInt#(3)) rrow <- mkReg(0);      // 標記當前讀取的 row 位置

  Wire#(Bool) empty <- mkWire;  // 標記緩衝是否為空，若為空，重設 column 和 row 為 0
  Wire#(Bool) full <- mkWire;  // 標記緩衝是否為滿，若為滿，不執行讀取操作

  // ==== 實時判斷緩衝器的狀態 ====
  rule empty_full;
    // 低位相同，且高位相同時為 empty
    empty <= wblock == rblock;
    
    // 低位相同，且高位不同時為 full
    full  <= wblock == {~rblock[1], rblock[0]};
  endrule
  
  // ==== step2，讀取bram並放入bram的緩衝區 ====
  // 只有(緩衝非空)時讀取
  rule read_ram ( !empty );

    // 對 bram 寫入讀操作的請求
    ram.portB.request.put(

      BRAMRequest {
        write: False,             // 進行讀操作
        responseOnWrite: False,   // 進行寫操作時不需要回應，只對寫操作有效
        address: tuple3(rblock[0], rcol, rrow), datain: 0 }   // 讀取位址由 rblock|rcol|rrow| 組成
    );

    // 沿著 column 讀取數據 (由左至右)
    rcol <= rcol + 1;

    if(rcol == 7) begin
      // 讀取位置到達 column 尾部，移動到下一個 row
      rrow <= rrow + 1;

      // 讀取位置到達 row 尾部，移動到下一個 block
      if(rrow == 7)
        rblock <= rblock + 1;

    end
  endrule

  // ==== 在 empty 時，將 wblock 的指標重置為0 ====
  // empty 有兩種狀況，
  //    狀況1: 初始化時 wblock/rblock @ 00
  //    狀況2: 寫入的區塊都已經被讀取 wblock/rblock @ 10
  // 在狀況2 時需要將 wblock 的指標重置為0，wblock 才能繼續指示寫入位置和緩衝器的狀態

  // mkPulseWire 用於傳遞 True/False 的訊號，
  // Wire 的數據無法保存，寫入 True 後，下一個週期會自動恢復為 False
  PulseWire rewind_call <- mkPulseWire;

  method Action rewind if( empty );
    rewind_call.send; // 將 rewind_call 變數設置為 True
    wcol <= 0;
    wrow <= 0;
  endmethod
  
  // ==== step1，寫入bram ====
  // 讀取外部輸入，並寫入bram 中
  // 只有(緩衝未滿)且(ewind_call未被觸發時調用)寫入
  method Action put(int val) if( !full && !rewind_call );

    // 對 bram 寫入寫操作的請求
    ram.portA.request.put(
      BRAMRequest {
        write: True,            // 進行寫操作
        responseOnWrite: False, // 不需要寫響應
        address: tuple3(wblock[0], wcol, wrow), datain: val }   // 寫入位址由 wblock|wcol|wrow| 組成
    );

    // 沿著 row 寫入數據 (由上至下)
    wrow <= wrow + 1;

    if(wrow == 7) begin
      // 寫入位置到達 row 尾部，移動到下一個 column
      wcol <= wcol + 1;

      // 寫入位置到達 column 尾部，移動到下一個 block
      if(wcol == 7)
        wblock <= wblock + 1;
    end
  endmethod

  // ==== 從 bram 的緩衝區獲取操作結果 ====
  method get = ram.portB.response.get;


endmodule

// mkTransposeBuffer88 的 testbench
// 行为：向矩阵转置器中输入 0,1,2,3,...,255 。然后打印输出
module top ();
  Reg#(int) cnt <- mkReg(0);
  rule up_counter;
    cnt <= cnt + 1;
  endrule

  Reg#(int) indata <- mkReg(0);   // 寫入值
  Reg#(int) cnt_ref <- mkReg(0);  // 預期的讀取值

  let transposebuffer <- mkTransposeBuffer88;

  (* preempts = "transposebuffer_rewind, transposebuffer_put" *)
  rule transposebuffer_rewind (cnt == 0);
    transposebuffer.rewind;
  endrule

  // 寫入值
  rule transposebuffer_put; // (cnt%2 == 0);   // 矩阵转置器 输入，可添加隐式条件来实现不积极输入
    transposebuffer.put(indata);  // 在transposebuffer內部由上至下寫入
    indata <= indata + 1;
  endrule

  // 驗證寫入值
  rule transposebuffer_get; // (cnt%3 == 0);   // 矩阵转置器 输出并验证，可添加隐式条件来实现不积极输出
    cnt_ref <= cnt_ref + 1;

    /*
      // 寫入矩陣 (寫入值 = col row)

                    col=000     col=001
      row=000     0(000_000)  8(001_000)
      row=001     1(000_001)  9(001_001)
      row=010     2(000_010)  A(001_010)
      row=011     3(000_011)  B(001_011)
      row=100     4(000_100)  C(001_100)
      row=101     5(000_101)  D(001_101)
      row=110     6(000_110)  E(001_110)
      row=111     7(000_111)  F(001_111)
                    col_row

      // 讀取矩陣 (讀取值 = row col)

                    col=000     col=001     col=010     col=011     col=100     col=101     col=110     col=111
      row=000     0(000_000)  1(000_001)  2(000_010)  3(000_011)  4(000_100)  5(000_101)  6(000_110)  7(000_111)
      row=001     8(001_000)  9(001_001)  A(001_010)  B(001_011)  C(001_100)  D(001_101)  E(001_110)  F(001_111)
                                            row_col

      寫入值 == 寫入位置 == (col)(row)
      - 例如，依照寫入矩陣，寫入值=9 ，寫在 col=001 row=001 的位置，轉換為位址為 0b001_001 = 9h
      - 讀取位置和寫入位置是同步的 addr-write = addr=read = 9H

      讀取值 != 讀取位置
      - 讀取的位置，需要將 col 和 row 的互換，才能獲取正確的預期值
      - 例如，寫入值A，位於 AH 的位址
      - 若不將 col 和 row 互換，讀取 Ah(0b001_010) 位址得到值=XXX，並不會得到值A
      - 需將 col 和 row 互換 Ah(0b001_010) -> 0b010_001，才會得到值A
    */

    // 取得預期值
    // data_ref = cnt_ref 高26位 | cnt_ref 低三位 | cnt_ref 中三位
    //    寫入值==寫入位置==(col)(row)，
    //    轉置後，需要將 (col)(row) 互換，(col)(row) -> (row)(col) 才會得到正確的讀取值

    //    cnt_ref 低三位 = row值
    //    cnt_ref 中三位 = col值
    int data_ref = unpack( { pack(cnt_ref)[31:6], pack(cnt_ref)[2:0], pack(cnt_ref)[5:3] } );
    
    // 取得實際值
    int data_out <- transposebuffer.get;  // 在transposebuffer內部由左至右讀取

    // 因为 ram2 的延迟=2，所以调用 request.put 进行读操作的两个周期后
    $display("cnt=%3d    output_data:%3d    reference_data:%3d", cnt, data_out, data_ref);

    if(data_out != data_ref) $display("wrong!");

    if(data_ref >= 255) $finish;
  endrule

endmodule


endpackage