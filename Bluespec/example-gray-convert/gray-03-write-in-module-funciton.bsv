package Gray_Code_transform_v3;

module top ();

   // ==== 二進制碼 -> 格雷碼 ==== 
   function Bit#(6) bin2gray(Bit#(6) bin);
      return (bin >> 1 ) ^ bin;
   endfunction

   // ==== 格雷碼 -> 二進制碼 ====
   function Bit#(6) gray2bin(Bit#(6) gray);
      // 區域變數
      Bit#(6) bin = 0;
      bin[5] = gray[5];
      bin[4] = gray[4] ^ bin[5];
      bin[3] = gray[3] ^ bin[4];
      bin[2] = gray[2] ^ bin[3];
      bin[1] = gray[1] ^ bin[2];
      bin[0] = gray[0] ^ bin[1];
      return bin;
   endfunction

   // 全局變數
   Reg#(Bit#(6)) cnt <- mkReg(0);

   // 每次 clk 觸發時都會執行
   //    功能1，建立計數值，用於判斷仿真結束的時機
   //    功能2，cnt 作為二進制輸入的來源
   rule up_counter;
      cnt <= cnt + 1;
      if(cnt >= 8) $finish;
   endrule

   // 每次 clk 觸發時都會執行
   rule convert;
      Bit#(6) cnt_gray = bin2gray(cnt);
      Bit#(6) cnt_bin = gray2bin(cnt_gray);

      $display("cnt=%b   gray=%b   bin=%b", cnt, cnt_gray, cnt_bin);
   endrule

endmodule

endpackage