package Gray_Code_transform_v1;

module top ();

  Reg#(Bit#(6)) cnt <- mkReg(0);
  
  // 每次 clk 觸發時都會執行
  //    功能1，建立計數值，用於判斷仿真結束的時機
  //    功能2，cnt 作為二進制輸入的來源
  rule up_counter;
      cnt <= cnt + 1;
      if(cnt >= 8) $finish;
  endrule

  // 每次 clk 觸發時都會執行
  rule convert;
      // 二進制碼 -> 格雷碼
      Bit#(6) gray = (cnt >> 1) ^ cnt;

      // 格雷碼 -> 二進制碼
      Bit#(6) bin = 0;
      bin[5] = gray[5];
      bin[4] = gray[4] ^ bin[5];
      bin[3] = gray[3] ^ bin[4];
      bin[2] = gray[2] ^ bin[3];
      bin[1] = gray[1] ^ bin[2];
      bin[0] = gray[0] ^ bin[1];

      // 或使用 for-loop 簡化
      // bin[5] = gray[5];
      // for(int i=4; i>=0; i=i-1)
      //    bin[i] = gray[i] ^ bin[i+1];

      $display("cnt=%b   gray=%b   bin=%b", cnt, gray, bin );
      // cnt=000000   cnt_gray=000000   cnt_bin=000000
      // cnt=000001   cnt_gray=000001   cnt_bin=000001
      // cnt=000010   cnt_gray=000011   cnt_bin=000010
      // cnt=000011   cnt_gray=000010   cnt_bin=000011
      // cnt=000100   cnt_gray=000110   cnt_bin=000100
      // cnt=000101   cnt_gray=000111   cnt_bin=000101
      // cnt=000110   cnt_gray=000101   cnt_bin=000110
      // cnt=000111   cnt_gray=000100   cnt_bin=000111
  endrule

endmodule

endpackage