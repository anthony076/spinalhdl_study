## Bluespec 介紹
- blue-system-verilog 更像一個產生verilog的字符串替換系統，
  bluespec 才是用TRS實現，和chisel對應的全新語言

- 隱式時鐘/復位：只需給出寄存器的複位狀態，和每個時鐘週期的行為，時鐘和復位信號會自動生成。
- 方法抽象：把接口動作抽象為方法調用。
- 自動生成握手信號：BSV 會根據方法能否能生效，來自動生成握手信號。
- 自動生成狀態機：提供了根據順序模型自動生成狀態機的庫，使我們能專注於行為描述，不需要手動維護狀態判斷和跳轉。

