## Module 基礎

- 本文檔用於建立模塊的基本概念和使用，每個組成的完整使用方式見各分節
  - [Interface 和 module 的進階使用](blue-interface-and-module-adv.md)
  - [rule 的進階使用](blue-09-rule-adv.md)

- interface 和 module 的關係

  interface 是只有外殼的模塊，module 是只有內在的模塊
  
  當 module 繼承 interface 後，就將只有外殼的模塊和只有內在的模塊合併成一個完整的模塊，
  因此，interface實例 = module實例，
  
  interface實際上不進行接口的初始化，指定義接口的名稱，因此需要 module 來進行模塊的實例化，
  module 就相當於是模塊的構造函數，調用 module 就相當於是為指定接口建立接口的實例

  默認其況下，module(interface的構造函數)會返回接口實例，
  因此會限制 module 將接口的實現(method 的實現)寫在 module 的`尾部`，

  若 module 使用已經定義好的接口，也可以透過 `return` 直接返回接口實例
  (見 [接口實現的簡化](blue-10-interface-and-module-adv.md#簡化-接口函數實作的簡化方法2透過-return-語句直接返回接口實例))

- method 和 rule 的關係
  - 兩者都會存取接口值 
  - interface-method 用於建立接口 (透過 method 名)，和`數據輸入/輸出`時的邏輯
  - module-rule 用於定義接口在 clk 觸發後的行為，定義`模塊觸發後`的內部邏輯，

## 建立 module
- module 的基本組成
  - `組成1`，利用 interface 定義端口
    - interface 是`預定義模塊`，但`只有定義端口和內建的基本訊號`，並未實例化端口變數的值，也未定義模塊的內容

    - interface 需要透過 module 的`繼承`和對 module 對端口的實作，才能實例化端口的值，和實現模塊內容

      透過多態，可以利用子類 module 的構造函數，來建立父類 interface 的實例

    - `1-1`，利用 method `定義`端口
      - 語法: `method 端口值的類型 端口名`;
      - 介面類的實例化，會產生`實際的模塊`，介面中定義的端口，會產生`實際的訊號`
      - 透過 interface 的 method ，使得同樣的端口可以給不同的模組重複使用的目的，
        透過函數的形式，`讓上層模塊可以調用子模塊`
      
    - `1-2`，在繼承的 module 中 `實例化`端口
      ```verilog
      interface Foo;
        // 定義端口，未實例化
        method Bool aa; 
      endinterface

      module Bar(Foo);
        ... 內容省略 ...
        
        // 對端口實例化，注意，必須放在 module 尾部
        method Bool aa = True;
      endmodule
      ```

    - `1-3`，`存取`端口值的兩種方法
      - 方法1，透過 `實例.method名` 來存取端口值
      - 方法2，透過 `實例.method名()` 來存取端口值

      - 端口函數，是端口值的 getter/setter 函數，
        getter/setter函數可以`不使用函數形式`調用，可以利用`屬性值的方式`直接調用，
        
        例如，可以利用 Foo.aa 取得端口值，來取代 Foo.aa() 取得端口值
  
  - `組成2`，定義 module
    - 語法1: `module 模組名 (interface名);`
    - 語法2: `module 模組名 ();`

      若是非合成的模組，例如，仿真用模組、異常用模組，在不需要端口的情況下，不需要提供端口名

    - interface名 是特殊類，用於建立模塊的基礎架構，用於定義模塊的端口

  - `組成3`，建立 module 的內容
    - 3-1，定義[內部變數](blue-05-variable-and-assignment.md)
    - 3-2，透過 rule 建立內部變數之間的連接關係
    - 3-3，在 module 尾部(必要)，對介面類的`端口進行實作`，用於建立`端口與內部訊號`之間的連接

  - 範例
    ```verilog
    // ==== step1，建立 interface，用於建立端口介面類 ====
    interface DecCounter;
      method UInt#(4) count;  // 定義 count 端口，相當於建立 output 的變數，未實例化
      method Bool overflow;   // 定義 count 端口，相當於建立 output 的變數，未實例化
      ... 內容省略 ...
    endinterface

    // ==== step2，建立 module ====
    // - 利用繼承介面類來建立模塊
    module Mymodule (DecCounter);

      // ==== step3，建立內部變數 ====
      // 建立 Reg 變數，並透過官方函數賦值
      Reg#(UInt#(4)) cnt <- mkReg(0);

      // 建立 wire 變數
      Bool oflow = cnt >= 9;

      // ==== step4，利用 rule 代碼塊，建立訊號之間的連接 ====
      // rule 代碼塊，類似 verilog 的 always 區塊
      rule run_counter;
          cnt <= oflow ? 0 : cnt + 1;
      endrule

      // ==== step5，建立模塊端口與內部訊號的連接 ====
      // 注意，端口介面只有定義名稱，必須在繼承的 module 進行實作，且必須放在 module 底部
      method UInt#(4) count = cnt;     // 將 count端口 與內部的 cnt-reg 進行連接 (透過FF進行連接)
      method Bool overflow = oflow;    // 將 overflow端口 與內部的 oflow-wire 進行連接 (直接連接)

    endmodule
    ```

- 調用子模塊
  - 語法1: `interface名 自定義模塊實例名 <- module名()`
  - 語法2: `interface名 自定義模塊實例名 <- module名`

  - 注意，`module名`，必須是繼承 interface 的 module，module 和 interface 必須是繼承關係才能使用

  - 注意，使用 `<-` 進行 module 的實例化，而不是使用 `=`

    參考，[兩種賦值的差異](blue-05-variable-and-assignment.md)

  - 兩種調用 module 構造函數的寫法
    
    和調用 interface 的 method 類似，雖然是存取屬性變數，但實際上是調用 getter/setter 函數
      - method 的調用`會產生實際值`，編譯器並自動建立同名的變數，來保存產生的實際值
      - module 的調用會`產生實際的硬體(module實例)`，編譯器會自動建立同名的變數，來保存建立的實例

    因此，調用 module 的構造函數，可省略輸入引數()
    - 透過 `<- module名()`，會調用 module 的構造函數，建立 module 的實例，並將自定義的module實例名指向該實例
    - 透過 `<- module`，會調用 module 的構造函數，建立 module 的實例，，並將自定義的module實例名指向該實例`的變數`

## rule 的使用
- 語法
  ```verilog
  rule 規則名 (觸發條件);
    ... 內容省略 ...
  endrule
  ```
- [rule 的進階使用](#module-基礎)
  
## function 的使用
- 語法
  ```verilog
  function 返回值類型 函數名(類型 輸入變數1, 類型 輸入變數2, ...);
    ... 內容省略
    return 返回值
  endfunction
  ```

- [function 的種類](blue-10-interface-and-module-adv.md#function-的使用)
- [function 的進階使用](blue-polymorphism-in-fn.md): 利用 function 實現多態

## [範例] 建立 module 的完整
```verilog
package DecCounter;

  // ==== 建立用於創建端口的 interface ====
  interface DecCounter;               
    method UInt#(4) count;  // 透過 method 定義端口，count-port，用於建立 input/output 的變數
    method Bool overflow;   // 透過 method 定義端口，overflow-port，用於建立 input/output 的變數
  endinterface
  
  // ==== 建立子模塊 ====
  (* synthesize *)                    // 單獨合成 mkDecCounter模塊 (建立mkDecCounter模塊.v)，而不是將 mkDecCounter 內的邏輯內嵌到調用模塊中
  module mkDecCounter (DecCounter);   // 語法: module 模組名 (端口介面名);
    
    // 建立內部變數
    Reg#(UInt#(4)) cnt <- mkReg(0);   // 建立內部的 cnt變數(reg類型)，並透過 mkReg() 進行實例化


    Bool oflow = cnt >= 9;           // 建立內部的 oflow變數(wire類型) 變數，用於判斷 cnt 是否溢出，是組合邏輯

    // 建立模塊內部的邏輯
    rule run_counter;                // 過程代碼塊，類似 verilog 的 always 區塊
        cnt <= oflow ? 0 : cnt + 1;
    endrule

    // 實現端口和內部訊號的連接(端口)
    method UInt#(4) count = cnt;     // 重新實現 count-port (count-port 初始化)，透過賦值，將 count-port 與內部的 cnt-reg 進行連接
    method Bool overflow = oflow;    // 重新實現 overflow-port (count-port 初始化)，透過賦值，將 overflow-port 與內部的 oflow-wire 進行連接
  endmodule

  // ==== 建立主模塊，並調用子模塊(mkDecCounter) ====
  module top();                                 
      
    // 實例化子模塊
    DecCounter counter <- mkDecCounter;       // 建立使用 DecCounter 接口的模塊，透過 <- 賦值的方式，對模塊實例化

    rule test;
      $display("count=%d",  counter.count);     // 透過模塊實例，counter，來調用子模塊的成員 (調用子模塊的內部變數)
                                                //    調用方法1，模塊實例.成員名
                                                //    調用方法2，模塊實例.成員名()

      $display("overflow=%d",  counter.overflow);

      if( counter.overflow )
          $finish;

    endrule
  
  endmodule

endpackage
```

## 進階使用
- [interface 的進階使用](#module-基礎)

- [利用 = 簡化接口函數的實現](blue-10-interface-and-module-adv.md#簡化-接口函數實作的簡化方法1透過等號賦值簡化接口函數實作)
- [利用 return 簡化接口函數的實現](blue-10-interface-and-module-adv.md#簡化-接口函數實作的簡化方法2透過-return-語句直接返回接口實例)

- [利用接口合併多個接口(接口嵌套)](blue-10-interface-and-module-adv.md#合併-合併接口1利用接口嵌套合併多個接口)
- [利用複合結構繼承多個接口](blue-10-interface-and-module-adv.md#合併-合併接口2利用-tuple-合併多個接口)