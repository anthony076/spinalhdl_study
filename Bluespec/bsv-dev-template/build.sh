
# 格式: bsvbuild.sh <參數> <要編譯的module名> <要編譯的bsv檔名>
# 注意，若使用預設的 top.bsv，編譯命令可以不提供 bsv 檔名
# 注意，若使用預設的 top-module，編譯命令可以不提供 module名
# 例如，sh bsvbuild-my.sh -v

# 編譯參數
#   -v: compile *.bsv to *.v
#   範例: sh bsvbuild.sh -v mytop main.bsv
sh bsvbuild-my.sh -v
