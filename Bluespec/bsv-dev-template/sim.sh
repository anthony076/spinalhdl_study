# 格式: bsvbuild.sh <參數> <要編譯的module名> <要編譯的bsv檔名>
# 注意，若使用預設的 top.bsv，編譯命令可以不提供 bsv 檔名
# 注意，若使用預設的 top-module，編譯命令可以不提供 module名
# 例如，sh bsvbuild-my.sh -v

# 注意，使用 bluespec 仿真時，可以不提供 -v 的參數，bsvbuild 會先編譯後仿真
# 注意，-s 參數、-w 參數，是提供仿真輸出源的參數，代表要進行仿真，必須搭配 -v 或 -b 一起使用
# 注意，-b 參數，不能獨立存在，必須提供仿真輸出源，例如，-bs 或 -bw 或 -bsw
#     ，-v 參數，可以獨立存在，代表只編譯不仿真，例如，-v
#     ，-v 參數，可以和仿真輸出源一起使用，例如，-vs 或 -vw 或 -vsw

#   (optional) 使用 bsv 仿真
#     -b 參數: use bsv to simulate，
#     -s 參數: shell output when simulate
#     -w 參數: output vcd file
#   範例: sh bsvbuild-my.sh -bsw mytop main.bsv


#   (optional) 使用 verilog 仿真
#     -v 參數: use bsv to simulate，
#     -s 參數: shell output when simulate
#     -w 參數: output vcd file
#   範例: sh bsvbuild-my.sh -vsw mytop main.bsv && gtk top_bw.vcd


#   (optional) 使用 verilog 仿真，並將 shell 的輸出打印到檔案
#     -v 參數: use bsv to simulate，
#     -s 參數: shell output when simulate
#     注意，使用 -s 和提供 log.txt 才能將 shell 的輸出寫入檔案，且 shell 不會顯示輸出內容 
#   範例: sh bsvbuild-my.sh -vs mytop main.bsv log.txt

sh bsvbuild-my.sh -vsw
gtkwave result.vcd