## Interface 和 module 的進階使用

- 定義 interface 和端口方法
  ```verilog
  interface 接口模塊名;
    method int 方法名1;                        // 定義值方法
    method Action 方法名2 (int x);             // 定義動作方法
    method ActionValue#(int) 方法名3 (int x);  // 定義動作方法
  endinterface
  ```

  根據訊號的類型，interface 的`端口方法`可區分為以下幾種類型
  - 端口方法1，定義`值方法(value-method)`: 一定會返回值(返回模塊內部狀態)的方法
    > method int 方法名;

  - 端口方法2，定義`動作方法(action-method)`: 該方法含有修改狀態的動作
    > method Action 方法名(輸入參數類型 輸入參數變數);

  - 端口方法3，定義`動作值方法(action-value-method)`: 該方法含有修改狀態的動作，並且會返回更新後的值
    > method ActionValue#(返回值類型) method3 (輸入參數類型 輸入參數變數);

  - 三種端口方法的比較和對應的硬體意義
    - 寫入值 = 變更模塊內部的狀態
    - 讀取值 = 讀取模塊內部的狀態

    | 比較項目     | 值方法(value-method)   | 動作方法(action-method)                         | 動作值方法(action-value-method)                 |
    | ------------ | ---------------------- | ----------------------------------------------- | ----------------------------------------------- |
    | 功能         | 讀取值                 | 寫入值                                          | 寫入+讀取值                                     |
    | 對應verilog  | output                 | input                                           |                                                 |
    | 對應程式語言 | getter                 | setter                                          |                                                 |
    | 識別         | 未加任何關鍵字         | method 後方有 `Action` 關鍵字                   | method 後方有 `ActionValue#(返回值類型)` 關鍵字 |
    | 賦值簡寫     | = 代表取得右側產生的值 | <= 代表將右側值寫入(左側變數或函數)，不取得結果 | <- 代表執行右側函數，並取得函數結果             |
    | 使用場景     | wire                   | reg                                             | 有輸入才有輸出\n例如模塊初始化或其他特殊場景    |
    | 有輸入值     | X                      | V (選用)                                        | V (選用)                                        |
    | 變更狀態     | X                      | V                                               | V                                               |
    | 有返回值     | V                      | X                                               | V                                               |

- 定義和實例化 module
  - module名`習慣以 mk 開頭`，為 make 的簡寫，通常代表建立實體模塊
  - module 中需要`實現接口模塊中的方法`，且需要`放在 module 的尾部`
  - module 的實例化，必須使用 <- 的賦值方法，
    
    和動作值方法類似，module 的構造函數會`接收輸入參數`，並根據輸入參數`產生新的模塊實例`後，返回給指定的變數

  - `方法1`，沒有定義參數的 module
    ```verilog
    module 模組名 (接口名);     
      /* 
      rules
        ... 建立電路邏輯 ...
      endrule
      */

      /* 
      method ...
        ... interface-method 的實現 ...
      endmethod 
      */
    endmodule
    ```

    實例化方法，`端口名 自定義模塊實例名 <- 模塊名;`

  - `方法2`，有定義參數的 module
    - 傳入的常數用於一些`必須靜態確定的硬件配置`，例如，指定 module 內部變數的位寬，
    - 在生成Verilog 代碼時，參數也會轉化成 Verilog 模塊的 parameter 常數，而不是input/output 信號。

    ```verilog
    module 模組名 #(parameter 輸入參數類型 輸入參數類型) (接口名);     
      /* 
      rules
        ... 建立電路邏輯 ...
      endrule
      */

      /* 
      method ...
        ... interface-method 的實現 ...
      endmethod 
      */
    endmodule
    ```

    實例化方法，`端口名 自定義模塊實例名 <- 模塊名(輸入參數);`

- 實作接口函數時，可為接口函數`添加隱式執行條件`
  - 隱式執行條件在實作接口函數時才添加，在 interface 宣告端口函數時不添加

  - 三種接口函數都可以添加隱式條件，若隱式執行條件不滿足，則調用值方法的 rule代碼塊就不會被激活
  
    參考，[調度規則和讀取語句的隱式執行方法](blue-08-sequential-circuit.md#控制讀寫順序的調度註解scheduling-annotation)

    - 為值方法添加隱式條件，`method int 方法名 if (隱式條件);`
    - 為動作方法添加隱式條件，`method Action 方法名(輸入參數類型 輸入參數變數) if (隱式條件);`
    - 為動作值方法添加隱式條件，`method ActionValue#(返回值類型) method3 (輸入參數類型 輸入參數變數) if (隱式條件);`

  - 範例，為值方法添加隱式條件
    
    若端口函數不添加隱式條件，[完整代碼](example-bitcoder/BitCoder_v1.bsv)
    ```verilog
    // 接口函數的定義
    method Bit#(16) get = dout;
    
    // 讀取接口值，具有隱式條件的 rule
    // 只在 dut.get_valid 有效時才執行 dut.get
    rule get_result (dut.get_valid);             
        $display("cnt=%4d   %b", cnt, dut.get);
    endrule
    ```

    等效寫法，為端口函數添加隱式條件，[完整代碼](example-bitcoder/BitCoder_v2.bsv)
    ```verilog
    // 接口函數的定義
    method Bool get if (dout_valid) = dout;
    
    // 讀取接口值，具有隱式條件的 rule
    // 隱式條件添加在接口函數中，因此 rule 的執行條件可省略
    rule get_result;
        $display("cnt=%4d   %b", cnt, dut.get);
    endrule  
    ```

  - 範例，[為動作方法添加隱式條件](#範例比特編碼器v3反壓範例為動作方法添加隱式條件)



## [端口方法] interface的值方法(value-method)
- 寄存器的內建模塊，mkReg 的 _read()，就是屬於值方法，
  不接收輸入參數，不會變更暫存器的內部狀態，只會返回暫存器的值

- 調用值方法並賦值給某個變數，可以透過 `= 賦值` 進行簡化，例如，
  ```verilog
  Reg#(int) aa = mkReg(1);
  let bb = aa;  // 等效於 let bb = aa._read();
  ```

- 讀值(`調用值方法`)的幾種簡化方式，
  
  interface 的值方法，相當於程式語言中的 getter，
  因此，在獲取暫存器的值時，可以寫成`屬性值調用`的形式，可以`不寫成函數調用`的形式，
  或直接使用暫存器變數的實例名，都可以取代值方法的調用

  > 暫存器 == 最小模塊實例，
  讀取暫存器值 == 讀取最小模塊的內部值 == 調用interface的值方法

  ```verilog
  Reg#(int) x <- mkReg(23);
  
  // 以下三種讀值方式是等效的
  $display ("x=%d", x._read() );  // 方法1，顯式以函數的形式調用值方法
  $display ("x=%d", x._read );    // 方法2，以屬性值的方式調用值方法
  $display ("x=%d", x);           // 方法3，以實例變數名，隱式調用值方法
  ```

- `值方法實作`的簡化
  - 值方法類似於 getter 函數，bluespec 會為返回值建立與函數同名的變數，用來保存返回值

  - `方法1`，使用傳統方法，使用 return 返回值
    ```verilog
    Reg#(int) r <- mkReg(0);
    method int get();
      return r + 1;   // 使用 return
    endmethod
    ```
    
  - `方法2`，使用 = 取代 return
    ```verilog
    Reg#(int) r <- mkReg(0);
    method int get = r + 1;   // 使用 = 取代 return 
    ```

- 值方法不會對模塊內部的狀態進行改變，因此，module 在實現 interface的值方法時，
  `不能調用`當前模塊的` action-method` 或 `action-value-method`，以避免改變內部狀態

- 範例，[值方法使用範例](#範例比特編碼器v1interface-完整範例)

## [端口方法] interface的動作方法(action-method)
- 寄存器的內建模塊，mkReg 的 _write()，就是屬於動作方法，
  可接收輸入參數(可以沒有輸入參數)，會變更暫存器的內部狀態，但不會返回暫存器的值

- 調用動作方法，可以透過 `<= 賦值` 進行簡化，例如，
  ```verilog
  Reg#(int) aa = mkReg(1);
  let aa <= aa + 1;  // 等效於 aa._write(aa._read() + 1);
  ```

- 範例，[動作方法使用範例](#範例比特編碼器v1interface-完整範例)

## [端口方法] interface的動作值方法(action-value-method)
- 可接收輸入參數(可以省略輸入參數)，會變更模塊的內部狀態，但不會返回模塊內部的值

- 動作值方法 = 動作方法 + 值方法
  
  使用動作值方法，代表將寫入動作和讀取值進行綁定，兩者必須同時進行，無法切割，
  雖然動作值方法可以簡化代碼，但需要考慮寫入和讀取是否需要單獨執行的場景，
  若有，就不適合使用動作值方法進行簡化

- 動作值方法的使用
  
  必須使用`<- 賦值`，代表執行右側函數，並取得函數結果，例如，

  ```verilog
  interface IFoo;
    method ActionValue#(Bit#(8)) set(int x); // 定義動作值方法
  endinterface

  module Foo(IFoo);
    Reg#(Bool) aa  <- mkReg(False); // 寫入參數，並取回結果
    Reg#(Bit#(8)) bb <- mkReg(0);   // 寫入參數，並取回結果
    
    // 實作動作值方法
    method ActionValue#(Bit#(8)) set(int x); 
        aa <= False;  // 改變狀態: 寫入參數，不取回結果
        return x + 1; // 返回其他內部狀態
    endmethod
  endmodule

  module Top;
    IFoo foo <- Foo();
    
    // 調用動作值方法，並取得結果
    let result <- foo.set(10);
  endmodule
  ```

- 範例，[動作值方法使用範例](#範例比特編碼器v4另一種反壓的實現為動作值方法添加隱式條件)

## [簡化] 接口函數實作的簡化方法1，透過等號賦值(=)，簡化接口函數實作
- 使用場景
  
  當父模塊和子模塊的接口函數屬於同一個方法類型，且`具有相同的定義和實作`時，
  可以透過等號賦值(=)，將子模塊的實現和隱式條件傳遞給父模塊，
  透過此方法，不需要重複定義新的接口函數，直接使用現有的接口函數

  在實務上，methodA 不是拷貝 methodB 的定義，
  而是兩者會形成一個`閉包的函數調用結構`，methodA 的內部會調用 methodB

- 語法

  - 未使用簡化語法前
    ```verilog
    // 注意，methodA 和 methodB 的的定義必須相同 : 都是動作函數，輸入參數定義都相同
    method Action methodA(int x);
      // 在 methodA 中調用 methodB的接口方法
      moduleB_instance.methodB(x);
    endmethod
    ```

  - 使用簡化語法，等效結果
    ```verilog
    // 注意，不需要再指定 method 的類型是值方法/動作方法/動作值方法
    method A = method B // methodA 的定義與 methodB 相同
    ```

    實務上，A 會引用 B，以實現調用A時，實際上是調用B的效果

- 範例，父模塊直接使用內建模外的接口實現
  ```verilog
  // 父模塊的接口定義
  interface IncreaseReg;
    method Action write(int x);   // mkIncreaseReg.write() 和 Reg._write() 一樣都是動作方法，且具有相同的輸入參數定義
    method int read;              // mkIncreaseReg.read() 和 Rreg._read() 一樣都是值方法，且具有相同的輸入參數定義
  endinterface

  // 父模塊定義
  module mkIncreaseReg (IncreaseReg);
    ... 內容省略 ...
    Reg#(int) reg_data <- mkReg(0); // 子模塊實例

    ... 內容省略 ...
    method write = reg_data._write;     // 父模塊.write() 會調用 子模塊實例._write()
    method read  = reg_data._read;      // 父模塊.read() 會調用 子模塊實例._read()

  endmodule
  ```

## [簡化] 接口函數實作的簡化方法2，透過 return 語句，直接返回接口實例
- 使用場景
  
  和[接口函數實作的簡化](#接口函數實作的簡化)一樣，
  若接口已經在其他的 interface 中定義過，就不需要重複定義新的 interface，也不需要`再定義相同類型的接口`

  和`接口函數實作的簡化方法1`的限制一樣，
  必須父模塊和子模塊的接口函數是同類型方法，且輸入參數的定義一致才能使用

  實務上，對模塊進行實例化時，例如，`IFoo foo <- mkFoo;`，
  會執行 mkFoo() 的內容，並調用尾部 method 函數來取得接口實例，

  簡化時，利用 return 取代 method函數的調用，直接透過 return 取得接口實例

- 和`接口函數實作的簡化方法1`的差異
  - 方法1，透過等號賦值(=)，需要定義 method，但是簡化 method 的內容，`適用單一接口`
    > 此方法會為父模塊的接口函數，建立與子模塊接口函數的引用
  
  - 方法2，透過 return 直接返回子模塊實例，不再需要定義接口函數，`適合多個街口`

- 語法
  
  例如，父模塊使用的接口和子模塊的接口定義相同
  ```verilog
  // 子模塊使用的接口
  interface ISon;
    method Action write (int x);
    method read;
  endinterface

  // 定義子模塊
  module mkSon(ISon);
    ... 省略 ...

    // 定義 write 接口
    method Action write (int x);
      ... 內容省略 ...
    endmethod

    // 定義 read 接口
    method read;
      ... 內容省略 ...
    endmethod
  endmodule
  ```

  若使用`接口函數實作的簡化方法1的等號賦值簡化`，仍然`需要定義 method`，但是簡化 method 的內容
  ```verilog
  // 定義父模塊
  module Parent(ISon);
    ...內容省略...
    ISon son <- mkSon;

    ...內容省略...
    method write = son.write;   // 為父模塊的接口建立引用，直接使用子模塊實例的接口定義
    method read = son.read;     // 為父模塊的接口建立引用，直接使用子模塊實例的接口定義
  ```

  若使用`接口函數實作的簡化方法2的直接返回接口實例`，`不需要定義 method`，直接透過 return 返回接口實例
  ```verilog
  // 定義父模塊
  module Parent(ISon);
    ...內容省略...
    ISon son <- mkSon;

    ...內容省略...
    return son;     // return 必須放在結尾，和 method 必須放在結尾一樣，利用 return 返回子模塊實例的所有內容
                    // module 結尾用於實現接口函數，或 利用 return 返回接口實例
  endmodule
  ```

## [合併] 合併接口1，利用接口嵌套合併多個接口
- 語法1，實現子接口時，將接口實例賦值給子接口名
  ```verilog
  // 定義父模塊使用的接口，為接口1和接口2的合併
  interface 自定義接口名;
    interface 子接口類型名1 接口實例名1;
    interface 子接口類型名1 接口實例名2;
  endinterface

  // 定義父模塊
  module 模塊名(自定義接口名);
    接口類型名1 子模塊實例1 <- 子模塊名1;
    接口類型名2 子模塊實例2 <- 子模塊名2;

    ...內容省略...
    interface 接口實例名1 = 子模塊實例1;
    interface 接口實例名2 = 子模塊實例2;
  endmodule

  // 調用
  模塊接口 模塊實例 <- 模塊名;
  模塊實例.接口實例名1.方法
  模塊實例.接口實例名2.方法
  ```

- 語法2，實現子接口時，只使用接口定義，但改寫接口內容
  ```verilog
  // 定義嵌套interface
  interface 自定義接口名;
    interface 子接口類型名1 接口實例名1;
    interface 子接口類型名1 接口實例名2;
  endinterface

  // 定義模塊
  module 模塊名;
    ... 內容省略 ...

    // 重新改寫子接口的內容，但 method 的定義一致
    // 類似實作method，嵌套interface時，改成實作子接口)
    interface 接口實例名 = interface 子接口類型名;    // 子接口類型名，需要與嵌套interface中的定義一致
      method 接口定義;                               // method 的接口定義，需要與子接口類型名中的一致
        ... 自定義接口函數內容 ...
      endmethod

      method _read = reg_data._read;  // 使用接口函數實作的簡化方法1
    endinterface
  endmodule
  ```

- 範例
  [完整代碼](example-embed-interface/increaseRegCfg-v1.bsv)

  ```verilog
  // 定義父模塊的接口
  interface IncreaseRegCfg;
    interface Reg#(int) data;    // 使用 Reg#(int) 接口作為子接口，且子接口名為 data
    interface Reg#(int) step;    // 使用 Reg#(int) 接口作為子接口，且子接口名為 step
    interface Reg#(int) mine;
  endinterface

  module mkIncreaseRegCfg (IncreaseRegCfg);

    Reg#(int) reg_data <- mkReg(0);
    Reg#(int) reg_step <- mkReg(1);
    Reg#(int) reg_mine <- mkReg(2); 

    ... 內容省略 ...

    // 語法1，
    interface data = reg_data; // 將子接口名綁定到接口實例
    interface step = reg_step; // 將子接口名綁定到接口實例

    // 語法2，只使用接口定義，但改寫接口內容
    interface data = interface Reg#(int);
      method Action _write(int x); 
        ... 重新定義接口函數內容 ...
      endmethod

      method _read = reg_data._read;  // 使用接口函數實作的簡化方法1
    endinterface

  endmodule
  ```

## [合併] 合併接口2，利用 Tuple 合併多個接口
- 使用場景

  使用此方式不需要定義新的 interface 來合併多個接口，
  取得代之，利用[複合結構](blue-05-composite-datatype.md)將多個接口組合成一個更大的接口模塊

  注意，繼承兩個接口，也需要返回兩個接口

- 複合結構的選擇，可使用 
  - 接口Array: 適合多個接口
  - 接口Struct: 使用前需要定義結構體，使用起來不會比接口嵌套方便
  - 接口Tuple: 適合合併兩個接口，不需要額外定義 Tuple，推薦使用

- 範例，利用 Tuple 繼承多個接口
  ```verilog
  module mkFoo ( Tuple2#(Reg#(int), Reg#(int)) ); // 繼承兩個接口
    Reg#(int) reg_data <- mkReg(0);
    Reg#(int) reg_step <- mkReg(1); 

    ... 代碼省略 ...

    return tuple2(reg_data, reg_step);    // 繼承兩個接口，也需要返回兩個接口
  endmodule

  // 實例化模塊時，透過 match 獲取多個接口的實例
  // match {.inc_reg_data, .inc_reg_step} <- mkFoo;
  ```

## function 的使用
- 使用場景
  
  常用於簡化重複性的組合邏輯

- function 和 method 的差異
  - method 是接口函數，接口專用的處理函數，是`公開函數`且可被外部調用
  - function 是使用者自定義的數據處理函數，是模塊`內部的私有函數`，無法被外部調用

- function 有兩種
  - 值函數(value-function): 類似接口的值方法(value-method)
  - 動作函數(value-function): 類似接口的動作方法(action-method)
  - 沒有動作值函數
  
- function 的使用限制
  
  不能調用會觸發狀態改變的動作方法或動作函數
   
  |                      | function (值函數) | function Action (動作函數) |
  | -------------------- | ----------------- | -------------------------- |
  | 調用子模塊的動作方法 | 禁止              | 允許                       |
  | 調用其他動作函數     | 禁止              | 允許                       |
  | 返回值               | 有返回值          | 沒有返回值                 |
  | 類比 verilog         | function          | task                       |

- 語法1，值函數
  ```verilog
  function 返回值類型 函數名(類型 輸入變數1, 類型 輸入變數2, ...);
    ... 內容省略  ...
    return 返回值
  endfunction
  ```

- 語法2，動作函數(Action-Function)
  
  注意，動作函數常和 `action ... endaction` 一起搭配使用，
  用來返回多個語句

  ```verilog
  function Action 函數名(類型 輸入變數1, 類型 輸入變數2, ...);
    return action
      ... 多個語句 ... 
    endaction;
  endfunction
  ```

- 範例，值函數範例
  
  [比特編碼器v1](example-bitcoder/BitCoder_v1.bsv)

- 範例，動作函數範例
  ```verilog
  module mkTb ();
    Reg#(int) x <- mkReg (0);
    Reg#(int) y <- mkReg (0);

    // 定義動作函數
    function Action incr_both (int dx, int dy);
      // 利用 action ... endaction 返回一整個代碼塊
      return action
        x <= x + dx;
        y <= y + dy;
      endaction;
    endfunction

    rule r1 (x <= y);
      // 調用動作方法
      incr_both(5, 1);

      $display ("r1: x=%d  y=%d", x, y);
      if (x > 10) $finish;
    endrule

    rule r2 (x > y);
      // 調用動作方法
      incr_both(1, 4);
      
      $display ("r2: x=%d  y=%d", x, y);
    endrule
  endmodule
  ```

## 範例，值方法和動作方法範例
  ```verilog
  interface IfcTest;                 // 定義接口
    method Action reset;             // 定義無輸入參數的動作方法
    method Action set(int value);    // 定義有輸入參數的動作方法
    method int get;                  // 定義值方法
  endinterface

  module mkTest(IfcTest);             // 模塊 mkTest 使用 IfcTest 的接口
    Reg#(int) rg [2] <- mkCReg(2, 0);

    // ==== 實現端口函數 ==== 
    method Action reset;              // 實現動作方法，reset
      rg[1] <= 0;                     // 透過 <= 簡化 rg[1]._write() 的調用，等效於 rg[1]._write(0);
    endmethod

    method Action set(int value);    // 實現動作方法，set
      rg[0] <= value + 2;            // 透過 <= 簡化 rg[0]._write() 的調用，等效於 rg[0]._write(value + 2);
    endmethod

    method int get = rg[0] + 1;     // 實現值方法，get
                                    // 透過 = 簡化 rg[0]._read() 的調用，等效於 get = rg[0]._read();
  endmodule

  module mkTb();
    IfcTest test_module <- mkTest;  // 透過 mkTest() 實例化 test_module

    rule r1;
      int x = test_module.get;      // 讀取 test_module 模塊的值，調用 get()
      test_module.set(x);           // 寫入 test_module 模塊的值，調用 set()
    endrule

    rule r2;
      test_module.reset;            // 寫入 test_module 模塊的值，調用 reset()
    endrule
    
  endmodule
  ```

## 範例，比特編碼器v1，interface完整範例
- 功能

  比特編碼器為一個無損編碼器，產生的輸出碼可以無歧異的還原為輸入碼，用來壓縮一個數據流

- 要求:
  - 輸入碼:輸入 8bit 的數據碼，每8bit進行一次壓縮，輸入碼可以為空

  - 長度碼:
    - 長度碼固定為 3bit 寬
    - 為輸入碼左側第一個1的index值，
    - 例如，輸入碼為 0010_1100，第1個1位於bit5，則長度碼=5=101 

  - 數據碼:
    - 輸入碼的`最高位為長度碼`，剩下的為數據碼
    - 根據長度碼指示的長度，從低位開始，從輸入碼取同等長度的數據
    - 例如，輸入碼為 0010_1100，最高位1在bit5，因此長度碼為5(101)，數據碼為 01100 

  - 生成碼:
    - `生成碼 = 數據碼 + 長度碼`，生成碼為數據碼和長度碼的拼接
    - 例如，輸入碼為 0010_1100，長度碼為5(101)，數據碼為 01100，
      拚街後的生成碼為 01100 + 101 = 01100_101

  - 遺留碼: 
    - `遺留碼 = 生成碼 + 上一次的遺留碼`
    - 例如，此次的生成碼為 9'b010111110，上次的遺留碼為 4'b0110，
      則此次的遺留碼為  9'b010111110 + 4'b0110 = 13'b010111110_0110
  
  - 輸出碼或更新遺留碼
    - 若遺留碼長度 < 16，輸出碼為空，不輸出
      > 例如，當前的遺留碼為 13'b0101111100110，長度不足16bit，輸出碼為空，遺留碼仍然是 13'b0101111100110

    - 若遺留碼長度 >= 16，低16bit作為輸出碼，剩下為遺留碼，放到下一次輸出

      > 例如，當前生成碼為 7'b1100100，前一次的遺留碼為 13'b0101111100110，
      則此次的遺留碼為 7'b1100100 + 13'b0101111100110 = 20'b11001000101111100110 > 16bit，
      則輸出碼為低16bit的 16'b1000101111100110，遺留碼更新為剩下未輸出的高4位 4'b1100

- 編碼範例

  | 輸入碼(input) | 長度碼(len) | 數據碼(data) | 生成碼(gen)=數據碼+長度碼 | 遺留碼(remain)=生成碼+前次遺留碼 | 輸出碼(output)         |
  | ------------- | ----------- | ------------ | ------------------------- | -------------------------------- | ---------------------- |
  | 固定8bit      | 固定3bit    | 可變長度     | 可變長度                  | 可變持度                         | 固定16位               |
  | 'b11111111    | 7(111)      | 7'b1111111   | 10'b1111111111            | 10'b1111111111                   | 遺留碼未滿16位，不輸出 |
  | 'b10110011    | 7(111)      | 7'b0110011   | 10'b0110011111            | 4'b0110                          | 'b0111111111111111     |
  | 'b01010111    | 7(110)      | 6'b010111    | 9'b010111110              | 13'b0101111100110                | 遺留碼未滿16位，不輸出 |
  | 'b00011100    | 4(100)      | 4'b1100      | 7'b1100100                | 4'b1100                          | 'b1000101111100110     |
  | 'b00001101    | 3(011)      | 3'b101       | 6'b101011                 | 10'b1010111100                   | 遺留碼未滿16位，不輸出 |
  | 'b00000011    | 1(001)      | 1'b1         | 4'b1001                   | 14'b10011010111100               | 遺留碼未滿16位，不輸出 |
  | 'b00000010    | 1(001)      | 1'b0         | 4'b0001                   | 2'b00                            | 'b0110011010111100     |
  | 'b00000001    | 0(000)      | 1'b1         | 4'b1000                   | 6'b100000                        | 遺留碼未滿16位，不輸出 |
  | 'b00000000    | 0(000)      | 1'b0         | 4'b0000                   | 10'b0000100000                   | 遺留碼未滿16位，不輸出 |

- verilog 代碼
  - [完整代碼](example-bitcoder/BitCoder_v1.bsv)

  - 電路流程
  
    <img src="doc/interface/bitcoder-flowchart.png" width=500 height=auto>

  - 代碼
    
    step1，產生生成碼和生成碼長度的函數
    ```verilog
    // 利用 function 建立生成碼的組合邏輯
    function Tuple2#(Bit#(10), UInt#(4)) getCode(Bit#(8) din);
      
      // 取得長度碼
      UInt#(4) len = 0;
      for(UInt#(4) i=0; i<8; i=i+1)
          if(din[i] == 1)
            len = i;
      
      UInt#(4) trim_len = len>0 ? len : 1;

      // 取得數據碼
      Bit#(7) trim = truncate(din) & ~('1<<trim_len);

      // 取得生成碼
      Bit#(10) code = {trim, pack(len)[2:0]};

      // 返回生成碼和生成碼的長度
      return tuple2( code, trim_len+3 );

    endfunction
    ```

    step2，定義接口模塊
    ```verilog
    interface BitCoder;
      method Action   put(Bit#(8) din);    // put 接口的類型為 input
      method Bit#(16) get;                 // get 接口的類型為 output，用於取得輸出碼
      method Bool     get_valid;           // get_valid 接口的類型為 output，用於指示編碼已完成(用於輸出碼可用)
    endinterface
    ```

    step3，定義模塊，並繼承BitCoder接口
    ```verilog
    module mkBitCoder (BitCoder);
      // ==== step1，建立模塊內部使用到的變數 ====
      // 定義流水線第1級使用的數據，用於保存生成碼組合邏輯的輸出值
      // 定義 in_code_and_len 變數，並將 in_code_and_len 初始化為0
      Reg#(Tuple2#(Bit#(10), UInt#(4))) in_code_and_len <- mkDReg( tuple2(0,0) );

      // 定義流水線第2級使用的數據，用於產生輸出碼(dout)和輸出碼指示(dout_valid)
      // 遺留碼(drem)、遺留碼長度(drem_len)是產生 dout 和 dout_valid 的中間變數
      Reg#(Bit#(31)) drem       <- mkReg(0);        // drem: 儲存遺留碼
      Reg#(UInt#(5)) drem_len   <- mkReg(0);        // drem_len: 遺留碼的長度
      Reg#(Bool)     dout_valid <- mkDReg(False);   // dout_valid: 指示 dout 是否有效
      Reg#(Bit#(16)) dout       <- mkReg(0);

      // ==== step3，建立 mkBitCoder 模塊的內部邏輯 ====
      rule get_drem_and_dout;
        match {.code, .code_len} = in_code_and_len;  // 取得前一級輸出的生成碼和生成碼長度

        Bit#(31) data = (extend(code) << drem_len) | drem;   // 建立未被裁減的遺留碼(data) = 生成碼(code) + 前一次的遺留碼(drem)
        UInt#(5) len = extend(code_len) + drem_len;          // 取德遺留碼的長度

        if(len >= 16) begin                   //   若遺留碼長度 > 16，則設置輸出(dout)和輸出指示位(dout_valid)
          dout_valid <= True;                //        設置輸出指示位(dout_valid)為 True
          dout <= truncate(data);            //        高位截斷，設置輸出碼(dout) = 遺留碼的低16位
          data = data >> 16;                 //        高位移到低位，更新遺留碼(data) = 遺留碼的高位，
          len = len - 16;                    //        更新遺留碼的長度
        end

        drem <= data;                         // 保存遺留碼，供下次觸發使用
        drem_len <= len;                      // 保存遺留碼長度，供下次觸發使用
      endrule

      // ==== step2，實作put接口，建立 put接口與內部訊號的連接 ====
      // 建立輸入數據(din) -> put接口 -> 產生生成碼的組合邏輯 -> 流水線第1級之間的邏輯關係
      method Action put(Bit#(8) din);          
        in_code_and_len <= getCode(din);      // put接口，接受din的輸出，經過 getcode 的組合邏輯後，輸出 in_code_and_len
      endmethod

      // ==== step4，實作get_valid接口，建立 get_valid接口與內部訊號的連接 ====
      // 配置 get_valid接口，建立內部變數dout_valid和get_valid接口之間的連接
      method Bool get_valid = dout_valid;      

      // ==== step5，實作get接口，建立 get接口與內部訊號的連接 ====
      // 配置 get接口，建立內部變數dout和get接口之間的連接
      method Bit#(16) get = dout;                 
    endmodule
    ```

    step4，測試代碼
    ```verilog
    module top();
      
      // 實例化測試模組(實例化mkBitCoder模組)
      let dut <- mkBitCoder;

      // 建立計數器
      Reg#(int) cnt <- mkReg(0);
      rule up_counter;
        cnt <= cnt + 1;
      endrule

      // 建立 din 訊號
      Reg#(Bit#(10)) din <- mkReg(0);

      // ==== 將輸入訊號傳遞給 dut ==== 
      rule dut_put;
        din <= din + 1;
        
        // 設置結束條件
        if(din < 'h200)
          dut.put( truncate(din) );   // 將 din 訊號輸入給 dut
        else if(din == '1)
          $finish;
      endrule

      // ==== 讀取dut的輸出 ====
      rule get_result (dut.get_valid);             // 只在 dut.get_valid 有效時才輸出
          $display("cnt=%4d   %b", cnt, dut.get);
      endrule
    endmodule
    ```

    輸出結果
    ```shell
    cnt=   5   1001000110000000
    cnt=   9   0100100101000010
    cnt=  11   0010110000111101
    cnt=  14   0011011011010011
    cnt=  17   1111001110101110
    cnt=  19   0110000001001110
    ... 以下省略 ...
    ```

## 範例，比特編碼器v3，反壓範例(為動作方法添加隱式條件)
- 反壓基本概念

  對於比特編碼器此類的電路，輸出為固定大小`(開口固定)`，且前次的數據會累積到下一次輸入`(數據會累績)`，
  代表在有固定大小的模塊下，`新輸入 + 前次數據 > 模塊容量` 時，就會造成`數據的溢出`

  為避免發生數據溢出，`反壓`作為避免數據溢出的手段之一，用於`主動反向減少輸入，以緩解輸出的壓力`

  反壓會`對輸入進行限制`，當模塊中的數據超過某個設計量之後，會主動停止輸入數據的傳入，
  以避免過多的數據對模塊造成壓力

  以比特編碼器為例，對流水線第2級的模塊而言，生成碼固定輸入10bit + 模塊累積前次的結果(不定長度) + 輸出限制為 8bit，使得`輸入 > 輸出`的情況下，就容易造成數據堆積超過容量而造成溢位

  <img src="doc/interface/back-pressure-example.png" width=800 height=auto>

  為避免超過 drem 超過 31bit 的限制，需要計算遺留碼最大累積長度(max_drem_len)
  ```shell
  # 遺留碼 = 生成碼 + 上期遺留碼
  遺留碼最大累積長度(max_drem_len) + 本期生成碼輸入長度 +  上期遺留碼剩餘長度 <= 31
  ```
  - 其中，
    - 遺留碼最大累積長度(max_drem_len)

      利用此值決定遺留碼累積長度的最大值為何，

      若超過此值，表示模塊即將超過模塊最大容量，即將產生數據溢位

    - 本期生成碼輸入長度: 生成碼固定輸入的長度為 10 bit
    - 上期遺留碼剩餘長度:
    
      在生成碼固定為 10 bit 的狀況下 > 輸出碼固定為 8bit 的情況下，
      ```shell
      上期遺留碼長度 = 輸入的生成碼長度 - 輸出碼輸出的長度 = 10-8 = 2bit，
      ```
      在有輸出碼輸出的情況下，每次都會新產生 2bit 的遺留碼到下一次

  - 由上述可以得到
    ```shell
    遺留碼最大累積長度(max_drem_len) + 本期生成碼輸入長度 +  上期遺留碼剩餘長度 <= 31
    max_drem_len + 10 + 2 <= 31
    max_drem_len <= 19
    ```
    代表當遺留碼累積的數量 <= 19 時，不會發生溢位，不需要限制輸入 

- 代碼: 加入反壓的限制 
  
  [完整代碼](example-bitcoder/BitCoder_v3.bsv)

  將輸出接口的寬度改小，塑造產生溢位的狀況
  ```verilog
  interface BitCoder;
    method Action  put(Bit#(8) din);
    
    //method Bit#(16) get;
    method Bit#(8) get;    // 將輸出寬度縮小為8bit，出口不夠大，容易造成溢位

    method Bool     get_valid;
  endinterface
  ```

  輸出接口改小後，當遺留碼長度超過8bit，就要對輸出碼和遺留碼進行處理
  ```verilog
   rule get_drem_and_dout;
      match {.code, .code_len} = in_code_and_len;

      Bit#(31) data = (extend(code) << drem_len) | drem;
      UInt#(5) len = extend(code_len) + drem_len;

      //if(len >= 16) begin
      if(len >= 8) begin      // 輸出寬度改為 8 bit，每 8bit 輸出一次並更新遺留碼
         dout_valid <= True;
         dout <= truncate(data);
         
         //data = data >> 16;
         data = data >> 8;
         
         //len = len - 16;
         len = len - 8;
      end

      drem <= data;
      drem_len <= len;
   endrule
  ```

  利用對輸入接入函數(動作函數)添加限制，實現反壓的機制
  ```verilog
   //method Action put(Bit#(8) din);
   method Action put(Bit#(8) din) if(drem_len <= 19); // 為輸入添加限制，drem_len <= 19 沒有數據溢位風險
      in_code_and_len <= getCode(din);
   endmethod
  ```

## 範例，比特編碼器v4，另一種反壓的實現(為動作值方法添加隱式條件)

- 新的限制+新思路

  相較於比特編碼器v3的範例，是藉由縮小輸出的寬度，造成數據溢位的風險，
  當偵測到遺留碼超過一定長度時，代表即將發生數據溢位，需要主動對禁止輸入，從而避免數據溢位的發生，
  在實務上，透過為`put接口`添加隱式的執行條件，在執行put()方法前，若遺留碼超過指定長度就不執行。

  <font color="blue"> 另一種造成數據溢位的風險</font>

  會發生在`觸發輸出的時機，只會發生在某些特定的條件下` (不是遺留碼長度足夠就輸出)
  當`觸發輸出的條件未滿足前`，即使數據已累積到指定數量(以比特編碼器v3來說，達到8bit就一定輸出)，
  但因為輸出條件未觸發，造成數據逐漸累績，進而造成溢位的風險

  <font color="blue">另一個可以控制數據溢位的思路</font>

  - 比特編碼器v3是對流水線`第1級模塊輸入`進行控制
  - 比特編碼器v4是對流水線`第2級模塊的輸入和輸出`進行控制(輸出背壓)

  - <font color = "red">(對輸入的控制)</font>
    
    當流水線第2級模塊從第1級模塊讀取生成碼後，會將`生成碼標記為無效`，(或初始狀態下生成碼為無效)，
    當生成碼被標記無效後，會`使輸入接口(put接口)有效`，從put接口讀取輸入數據後，透過流水線第1級模塊重新產生新的生成碼，並將`生成碼標記為有效`

    在`未發生溢位且生成碼為有效`的狀況下，第2級模塊才會讀取生成碼，並將`生成碼標記為無效`，
    在數據為達到可輸出的數量前，就會重複上述流程，使得遺留值逐漸累積

  - <font color = "red">(對輸出的控制)</font>

    當流水線第2級模塊的遺留碼已經達到可輸出的數量後，將`輸出狀態標記為可輸出`，當輸出條件被觸發後才將輸出碼輸出，
    若輸出狀態標記為不可輸出，即使輸出條件被觸發也不會執行輸出的語句

    當觸發輸出的條件被激活時，會檢查狀態是否是可輸出狀態，
    若是，將輸出狀態重新設置為不可輸出的狀態後，才將狀態輸出，
    否則，不執行輸出的語句

  <font color="blue">要實現此思路，需要 din_valid 和 dout_valid 兩個變數，</font>
  - din_valid 用來標記第1級模塊的`生成碼是否有效`，生成碼有效且不會造成溢位的狀況下，才可以將生成碼輸入第2級模塊
  - dout_valid 用來標記第2級模塊的`遺留碼是否累積到足夠可輸出的長度`

- 代碼
  
  [完整範例](example-bitcoder/BitCoder_v4.bsv)

  將 get接口改為動作值方法:

  因為輸出碼輸出後，需要將內部的 dout_valid 的狀態改為 False，
  因此需要改為動作值方法

  ```verilog
  interface BitCoder;
    method Action put(Bit#(8) din);
    
    // step1，將 get() 方法改為動作值方法
    //method Bit#(8) get;
    method ActionValue#(Bit#(8)) get;   // get() 會改變狀態(dout_valid)，因此改用動作值方法

    // 不在需要get_valid接口，由外部觸發獲取輸出碼的時機，
    // 不再透過 get_valid 確認編碼器的狀態
    // method Bool     get_valid;
  endinterface
  ```

  修改 module 的內容
  ```verilog
  module mkBitCoder (BitCoder);

    ... 內容省略 ...

    // step2，新增 din_valid 變數，
    // 用來標記 第1級模塊輸出的 in_code_and_len 是否有效(是否產生新的未被讀取的生成碼)
    Reg#(Bool) din_valid  <- mkReg(False);

    // 用於標記第1級模塊的遺留碼是否已達可輸出的長度
    Reg#(Bool) dout_valid <- mkDReg(False);

    // ============

    // step7，進行調度衝突分析，通知編譯器兩者可同時激活不衝突
    //    put接口，會在 din_valid=True 時被激活
    //    Rule: get_drem_and_dout，會在 dout_valid=False 時被激活
    //    兩者有可能會同時激活，且都對 din_valid 進行寫入，
    //    但透過 if 語句，兩者不會同時執行，
    //    因此加上 conflict_free 調度屬姓，告知編譯器兩者會同時激活，但不會產生衝突
     (* conflict_free = "put, get_drem_and_dout" *)

    // step4，為輸出的規則建立執行條件
    //rule get_drem_and_dout;
    rule get_drem_and_dout (!dout_valid);   // 添加執行條件，遺留碼累積到一定長度才執行rule
      
      Bit#(31) data = drem;      // 取得舊的遺留碼，尚未更新
      UInt#(6) len = drem_len;   // 取得舊的遺留碼長度，尚未更新

      match {.code, .code_len} = in_code_and_len;

      // step5，為獲取生成碼建立執行條件
      // 只有遺留碼長度不會造成數據溢為，且生成碼未被讀取過才更新輸出碼和遺留碼長度
      if(extend(code_len) + drem_len < 32 && din_valid) begin
       
        // 讀取生成碼
        data = (extend(code) << drem_len) | data;    // 更新輸出碼
        len = extend(code_len) + len;                // 更新遺留碼長度
        
        // 生成碼被讀取後就不再有效，需要重新將 din_valid 設置為 Fasle ，
        // din_valid = False，表示生成碼已無效，需要產生新的生成碼
        din_valid <= False;  
      end

      if(len >= 8) begin
        // 遺留碼長度以達到可輸出長度，將 dout_valid 設置為 True
        dout_valid <= True;

        dout <= truncate(data);
        data = data >> 8;
        len = len - 8;
      end

      drem <= data;
      drem_len <= len;
    endrule

    // ============
    // step3，修改 put接口的執行條件: 生成碼被讀取失效後，就使 put接口重新生效
    //method Action put(Bit#(8) din) if(drem_len <= 19);
    method Action put(Bit#(8) din) if(!din_valid);    // 改由 din_valid 判斷是否需要重新產生生成碼，若需要時 put接口才有效

      // 若 put() 可被執行，表示即將獲取新生成碼
      // 獲取新生成碼前，先將 din_valid 設置為 True
      din_valid <= True;
      
      // 獲取新生成碼
      in_code_and_len <= getCode(din);

    endmethod

    // 不在需要此接口，由外部觸發獲取輸出碼的時機，
    // 不再透過 get_valid 確認編碼器的狀態
    // method Bool get_valid = dout_valid;

    // step6，修改獲取輸出碼的 get接口的內容
    //method Bit#(16) get if(dout_valid) = dout;
    method ActionValue#(Bit#(8)) get if(dout_valid);   // 將 get() 改為動作值方法，因為會改變內部狀態，同時添加隱式執行條件

        dout_valid <= False;        // 若 get() 可以執行，表示輸出碼即將輸出，
                                    // 在輸出碼輸出前，重新將輸出碼可輸出狀態設置為 False

        return dout;
    endmethod

  endmodule
  ```

