## 類型多態(Polymorphism)

- 注意，bluespec 的類型多態(Polymorphism)，指的是模塊或函數的輸入值，可以是多種類型
  
  bluespec 的類型多態，對應的是程式語言的泛型(Generic)，而不是物件導向的多態(Polymorphism)


// 利用 interface 實現泛型
interface Reg#(type td);         // 寄存器中存储的数据的类型名为 td ，可能是任何类型
   method Action _write (td x);  // 该方法用于把 td 类型的变量 x 写入寄存器
   method td _read;              // 该方法用于读出寄存器的值，得到 td 类型的返回值
endinterface

// 利用 module 實現泛型
module mkReg#(td v) (Reg#(td))  // 第一个括号里是模块参数，是一个类型为 td 的变量 v ，这里是作为寄存器初始值。
                                // 第二个括号里，表示 mkReg 具有 Reg#(td) 类型的接口
   provisos (Bits#(td, sz));    // 要求 td 派生自 Bits 类型类，即寄存器的值必须有特定的位宽（保证寄存器可综合）

// 利用 function 實現泛型

