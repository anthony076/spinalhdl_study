## 撰寫組合邏輯電路
- 撰寫組合邏輯電路的幾種方法
  - 寫法1，寫在 rule 中
  - 寫法2，寫在 module 中
  - 寫法3，寫在 module 內的 function 中，僅 module 內可用
  - 寫法4，寫在 package 內的 function 中，可供外部調用

- 若要在`rule代碼塊`中建立`組合邏輯`，也可以使用[透過Wire模塊建立wire變數](blue-08-sequential-circuit.md#實例化wire的相關模塊)

- 範例，以二進制和格雷碼的轉換為例
  - [理論] 格雷碼 -> 二進制:
    - 若格雷碼為 G[3:0]，二進制碼為 B[3:0]
    - rule1，最高位保持不變
    - rule2，其餘位，$B_{n}$ = $G_n \oplus B_{n+1}$，`當前G位值`和`前一位高位的B位值`進行`互斥或`運算
    
    - 例如: 以`格雷碼 G = 0111` 為例
      - B[3] = G[3] = 0
      - B[2] = ( G[2] xor B[3] ) = ( 1 xor 0) = 1
      - B[1] = ( G[1] xor B[2] ) = ( 1 xor 1) = 0
      - B[0] = ( G[0] xor B[1] ) = ( 1 xor 0) = 1
      - 得到 二進制碼 B[3:0] = 0101

  - [理論] 二進制 -> 格雷碼: 
    - 若格雷碼為 G[3:0]，二進制碼為 B[3:0]

    - rule，$G = (B >> 1) \oplus B$，將`二進制值右移1位`後與`原始二進制值`進行`互斥或`運算

    -例如: 以`二進制碼B = 0101` 為例
      
      得到 G = (B>>1) xor B = ( 0010 xor 0101 ) = 0111

  - [實作] 以 verilog 實現格雷碼 -> 二進制
    <img src="doc/combinational-circuit/gray-transform-verilog.png" width=800 height=auto>

  - [實作] 以 bluespec 實現格雷碼 -> 二進制
    
    - `寫法1`，寫在 rule 中
      ```verilog
      module top ();

        Reg#(Bit#(6)) cnt <- mkReg(0);
        
        // ==== 設置二進制值 ====
        //    每次 clk 觸發時都會執行
        //    功能1，建立計數值，用於判斷仿真結束的時機
        //    功能2，cnt 作為二進制輸入的來源

        rule up_counter;
            cnt <= cnt + 1;
            if(cnt >= 8) $finish;
        endrule

        // ==== 設置轉換 ====
        //  每次 clk 觸發時都會執行

        rule convert;
          // 二進制碼 -> 格雷碼
          Bit#(6) gray = (cnt >> 1) ^ cnt;

          // 格雷碼 -> 二進制碼，方法1
          Bit#(6) bin = 0;
          bin[5] = cnt[5];
          bin[4] = gray[4] ^ bin[5];
          bin[3] = gray[3] ^ bin[4];
          bin[2] = gray[2] ^ bin[3];
          bin[1] = gray[1] ^ bin[2];
          bin[0] = gray[0] ^ bin[1];

          // 或使用 for-loop 簡化，方法2
          //bin[5] = cnt[5];
          //for(int i=4; i>=0; i=i-1)
          //  bin[i] = gray[i] ^ bin[i+1];

          // ==== 查看結果 ====
          $display("cnt=%b   gray=%b   bin=%b", cnt, gray, bin );
          // cnt=000000   cnt_gray=000000   cnt_bin=000000
          // cnt=000001   cnt_gray=000001   cnt_bin=000001
          // cnt=000010   cnt_gray=000011   cnt_bin=000010
          // cnt=000011   cnt_gray=000010   cnt_bin=000011
          // cnt=000100   cnt_gray=000110   cnt_bin=000100
          // cnt=000101   cnt_gray=000111   cnt_bin=000101
          // cnt=000110   cnt_gray=000101   cnt_bin=000110
          // cnt=000111   cnt_gray=000100   cnt_bin=000111

        endrule

      endmodule
      ```

    - `寫法2`，寫在 module 中
      
      寫在 module 中的變數，在所有 rule 中都能存取

      ```verilog
      module top ();

        // ==== 二進制碼 -> 格雷碼 ==== 
        Reg#(Bit#(6)) cnt <- mkReg(0);
        Bit#(6) gray = (cnt >> 1) ^ cnt;

        // ==== 格雷碼 -> 二進制碼 ====
        Bit#(6) bin = 0;
        bin[5] = cnt[5];
        bin[4] = gray[4] ^ bin[5];
        bin[3] = gray[3] ^ bin[4];
        bin[2] = gray[2] ^ bin[3];
        bin[1] = gray[1] ^ bin[2];
        bin[0] = gray[0] ^ bin[1];

        // 每次 clk 觸發時都會執行
        //    功能1，建立計數值，用於判斷仿真結束的時機
        //    功能2，cnt 作為二進制輸入的來源
        rule up_counter;
            cnt <= cnt + 1;
            if(cnt >= 8) $finish;
        endrule

        // 每次 clk 觸發時都會執行
        rule convert;
            $display("cnt=%b   gray=%b   bin=%b", cnt, gray, bin );
        endrule

      endmodule
      ```

    - `寫法3`，寫在 module 內的 function 中
      - 寫在 module 中的變數為全局變數
      - 寫在 function 中的變數為區域變數
      ```verilog
      package Gray_Code_transform_v3;

      module top ();

        // ==== 二進制碼 -> 格雷碼 ==== 
        function Bit#(6) bin2gray(Bit#(6) bin);
            return (bin >> 1 ) ^ bin;
        endfunction

        // ==== 格雷碼 -> 二進制碼 ====
        function Bit#(6) gray2bin(Bit#(6) gray);
            // 區域變數
            Bit#(6) bin = 0;
            bin[5] = gray[5];
            bin[4] = gray[4] ^ bin[5];
            bin[3] = gray[3] ^ bin[4];
            bin[2] = gray[2] ^ bin[3];
            bin[1] = gray[1] ^ bin[2];
            bin[0] = gray[0] ^ bin[1];
            return bin;
        endfunction

        // 全局變數
        Reg#(Bit#(6)) cnt <- mkReg(0);

        // 每次 clk 觸發時都會執行
        //    功能1，建立計數值，用於判斷仿真結束的時機
        //    功能2，cnt 作為二進制輸入的來源
        rule up_counter;
            cnt <= cnt + 1;
            if(cnt >= 8) $finish;
        endrule

        // 每次 clk 觸發時都會執行
        rule convert;
            Bit#(6) cnt_gray = bin2gray(cnt);
            Bit#(6) cnt_bin = gray2bin(cnt_gray);

            $display("cnt=%b   gray=%b   bin=%b", cnt, cnt_gray, cnt_bin);
        endrule

      endmodule

      endpackage
      ```
      

    - `寫法4`，module 外的 function 中
      - 單純將 module 內的 function 移到 module 外，function 屬於 package 的成員，
      - package 的成員`可以被其他的 package`導入並使用，適合用於 IP 核
      - 外界可透過 `Gray_Code_transform_v4::* 進行調用` 

      ```verilog
      package Gray_Code_transform_v4;

      // ==== 二進制碼 -> 格雷碼 ==== 
      function Bit#(6) bin2gray(Bit#(6) bin);
        return (bin >> 1 ) ^ bin;
      endfunction

      // ==== 格雷碼 -> 二進制碼 ====
      function Bit#(6) gray2bin(Bit#(6) gray);
        // 區域變數
        Bit#(6) bin = 0;
        bin[5] = gray[5];
        bin[4] = gray[4] ^ bin[5];
        bin[3] = gray[3] ^ bin[4];
        bin[2] = gray[2] ^ bin[3];
        bin[1] = gray[1] ^ bin[2];
        bin[0] = gray[0] ^ bin[1];
        return bin;
      endfunction

      module top ();

        // 全局變數
        Reg#(Bit#(6)) cnt <- mkReg(0);

        // 每次 clk 觸發時都會執行
        //    功能1，建立計數值，用於判斷仿真結束的時機
        //    功能2，cnt 作為二進制輸入的來源
        rule up_counter;
            cnt <= cnt + 1;
            if(cnt >= 8) $finish;
        endrule

        // 每次 clk 觸發時都會執行
        rule convert;
            Bit#(6) cnt_gray = bin2gray(cnt);
            Bit#(6) cnt_bin = gray2bin(cnt_gray);

            $display("cnt=%b   gray=%b   bin=%b", cnt, cnt_gray, cnt_bin);
        endrule

      endmodule

      endpackage
      ```