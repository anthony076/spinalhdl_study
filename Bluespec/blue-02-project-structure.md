## 專案結構概述

- 每個 .bsv 內只能有一個與文件名相同的 package 名

- 預設狀況下，`package名`與`入口模組名`具有相同的名稱，但不是必須

   若不一致時，編譯時會產生 Warning，且編譯命令需要提供入口模組的名稱才能正確的編譯

- 入口模組不一定要是可合成的，入口模組可做為測試用模組

   因此，若入口模組不可合成時，bsc 編譯器不會報錯

## 單包(package) + 單模塊(module) 專案
```verilog
package Hello;       // package名(Hello): 每個 .bsv 內只能有一個與文件名相同的 package 名

  module Top ();       // module名(Top)，
    rule hello;       // 建立規則(rule)，規則名為 hello，類似 verilog 的過程代碼塊
        $display("Hello World!");              
        $finish;                   
    endrule
  endmodule

endpackage
```

## 單包(package) + 多模塊(module) 專案
```verilog
// 定義包名
package DecCounter;

  // 建立 interface，用於創建端口的
  interface DecCounter;               
    method UInt#(4) count;           // 建立 count 端口，並指定端口的寬度和值類型
    method Bool overflow;            // 建立 overflow 端口，並指定端口的值類型
  endinterface

  // 建立子模塊，並指定使用的接口
  module mkDecCounter (DecCounter);   

    // ==== 建立內部變數 ====
    ... 內容省略 ...
    
    // ==== 建立模塊內部的邏輯 ====
    rule run_counter;
      ... 內容省略 ...
    endrule

    // ==== 實現端口和內部訊號的連接 (注意，必須放在模塊定義的尾部) ====
    method UInt#(4) count = ...
    method Bool overflow = ...

  endmodule

  // 建立主模塊，會調用 mkDecCounter-module
  module top();

    DecCounter counter <- mkDecCounter();      //實例化子模塊 

    rule test;
        ... 內容省略 ...
        counter.count     // 存取模塊實例的的成員
        ... 內容省略 ...
    endrule
  
  endmodule

endpackage
```

## 多包調用
- 每一個 bsv 檔案對應一個獨立的 package，透過 import 語法，調用不同 package 中的 module
- 語法: `import package名::*`
- 範例
  
  專案中有 三個 package，透過 `import SPIController::*` 導入 SPIController-package 中的所有模塊
  
  <img src="doc/project-structure/import-module-from-ouside-package.png" width=800 height=auto>