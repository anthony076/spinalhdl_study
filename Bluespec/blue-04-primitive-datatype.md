## 類型系統概述

- bluespec 是`強類型`系統，所有的值都必須有類型

- 注意，以下可建立`多位元值的類型`或是`帶有#符號的類型`，都是利用`泛型(template)`實現的類型(模板類)

  以`Reg#()類型`為例，Reg#()類型是以 interface 實現的泛型類

    ```bsv
    interface Reg#(type T);
      method Action _write (T x);
      method T _read;
    endinterface
    ```
    
  模板類不是實際的類型，模板類會將輸入參數帶入後，再產生實際的類，
  因此，透過模板類產生的類型都是獨立的類型，即使都是使用同一個模板類產生出來的類型，
  都是不同的類型

  例如，`Bit#(1)` 和 `Bit#(4)` 建立的值是`不同的類型`

---

- 用於`自定義模塊`的特殊類
  - interface，`抽象類`，`建立module的端口`和`基礎的 module 架構`
  - module，繼承 interface 的特殊類，用於建立模塊實體或仿真

- 用於建立`硬體連接`和`暫存器`的硬體類
  - Reg#(值類型)
  - Wire#(值類型)

- 用於標記`訊號值類型`的類
  - `種類1`，可用於硬體電路(可綜合數據類型)
    - Bit#(n)類，繼承自 Bits類
      
      注意，Bits類不能拿來做為值類型，不要使用 `Bits aa = 'b1;` 的語句，
      統一使用`Bit#(n)`來宣告值類型

    - Int#(n)類
    - UInt#(n)類
    - Bool類
  
  - `種類2`，不可用於硬體電路，可用於仿真或其他用途(不可綜合數據類型)
    - Integer類
    - String類

  - 由 Bits類延伸的子類
    - Eq 類: 判斷值是否相等
    - Ord 類: 比較值的大小
    - Arith 類: 可以進行算術運算
    - Literal 類: 用於建立整數字串 
    - RealLiteral 類: 用於建立浮點數字串
    - Bounded 類: 具有限制範圍的值
    - Bitwise 類: 進行按位的邏輯運算
    - BitReduction 類: 逐位的合併運算
    - BitExtend 類:進行位的擴展操作

  - 注意，只有宣告為`Bits類型`和`Bits類型延伸的子類型`的值，可用於硬體(可綜合數據類型)
    
    實際的硬體電路是以 0 和 1 來儲存，
    因此，只有繼承 Bits 類的類型，或由 Bits類延伸的子類，所建立的值才能用於 FF、FIFO、Reg

    例如，Bool類、Integer類、String類，
    因為不是繼承自 Bits 的類型，建立的值無法用於 FF、FIFO、Reg

  - 注意，可建立`多位元值的類型`，是利用泛型(template)實現的類型(模板類)

    例如，Bit#(n)、Int#(n)、UInt#(n)

    模板類不是實際的類型，模板類會將輸入參數帶入後，再產生實際的類，
    因此，透過模板類產生的類型都是獨立的類型，即使都是使用同一個模板類產生出來的類型，
    都是不同的類型

    例如，`Bit#(1)` 和 `Bit#(4)` 建立的值是`不同的類型`

- 自定義類型
  - 透過 `typedef + struct` 語法，可以將結構(struct)轉變為自定義的類型，
    或為 enum、struct、tagged-union 取別名

  - 語法
    ``` verilog 
    
    // 建立自定義類
    typedef struct {
      類型  類成員1;
      類型  類成員2;
      ...
    } 自定義類名 deriving(父類1, 父類1, ...)

    // 實例化自定義類 (透過 {} 對類成員賦值)
    自定義類名 實例名 = 自定義類名{類成員1: 值1, 類成員2: 值2, ...}
    ```

  - 範例
    ```verilog
    // 建立自定義類
    typedef struct {
      UInt#(48) aa;     // 成員變量1
      UInt#(48) bb;     // 成員變量2
      UInt#(16) cc;     // 成員變量3 
    } Foo deriving(Bits, Eq);  // 派生自的类型类是 Bits 和 Eq

    // 實例化
    Foo myfoo = Foo{aa: 'h123, bb: 'h456, cc: 'h789}
    ```
---
## Bit#(n) 類型，可合成數據類型
- Bit#(n): 將值宣告為單位元/多位元的 bit 類型
  - 單位元的bit: `Bit#(1)` 或 `bit`，bit 是 Bit#(1) 的簡寫
  - 多位元的bit: `Bit#(n)`

- BSV 會嚴格進行位寬檢查，只支持`顯式的高位截斷`和`顯式的擴展`，位數相同才能進行操作

- BSV 不會將省略位寬的常數，都當作 32bit
  ```verilog
  // 在 verilog 中
  wire [6:0] aa = 'b111    // 建立 32bit 值，導致出現位寬不一致，產生隱式的高位截斷

  // 在 bsv 中
  Bit#(7) aa = 123 // aa = 1111011
  ```

  因此，對於省略位寬的變數，宣告的位數和值的實際位數必須一致(或大於)，否則就報錯
  - 若值的實際位數 < 宣告位數: bsv 會自動補齊多餘的部分
  - 若值的位數 > 宣告位數: bsv 會拋出錯誤

- 所有位都為1(0) 的語法糖
  ```verilog
  Bit#(45) t1 = '1;      // t1 的所有位置都為 1
  Bit#(89) t2 = '0;      // t2 的所有位置都為 0
  ```

- 利用 index 對位置進行選擇
  ```verilog
  Bit#(8)  t1 = 'hFF;
  Bit#(16) t2 = 'h10;
  Bit#(16) t3 = 'h3456;

  Bit#(5)  t4 = t3[12:8]; //  5'h14
  bit      t5 = t3[7]; // 1'b0
  Bit#(13) t6 = { t3[2:0], t3[1:0], t1 }; // 13'h1AFF
  ```

- 範例集
  ```verilog
  
  // ==== 截斷與擴展 ====
  Bit#(7)  t1 = 'h12;          // 正確
  //Bit#(12) t2 = t1;          // 錯誤，t1位數 > t2位數，t1 未寫明擴展方式
  Bit#(12) t3 = extend(t1);    // 正確，t1 進行0擴展後，再傳遞給 t3
  //Bit#(4)  t4 = t1;          // 錯誤，t1位數 < t2位數，t1 未寫明截斷方式
  Bit#(4)  t5 = truncate(t1);  // 正確，t1 進行高位截斷後，再傳遞給 t5

  Bit#(7)  t1 = 7'b0010111;   
  Bit#(7)  t2 = 'b0010111;    // 值不用指定位寬，編譯器會自動匹配
  Bit#(7)  t3 = 'b10111;      // 高位補零
  //Bit#(7)  t4 = 5'b10111;   // 錯誤，位寬不匹配

  Bit#(7)  t6 = 7'd123;       
  Bit#(7)  t7 = 'd123;        
  Bit#(7)  t8 = 123;          // 不指明進制，預設使用十進制

  // ==== 算術運算 ====
  Bit#(16) t8 = t3 - t2;          // 16'h3446
  Bit#(16) t9 = t3 * t2;          // 16'h4560

  //Bit#(16) t10 = t2 * t1;       // 錯誤，t1 和 t2 的位寬不同
  Bit#(16) t11= t2 * extend(t1);  // 正確，t1 先擴展後再進行運算

  Bit#(16) t12= t2 | t3;          // 按位或，得到 16'h3456
  Bit#(16) t13= t2 | extend(t1);  // 按位或，得到 16'h00FF

  // ==== 逐位邏輯運算 ====
  bit      t14= &t2;    // 對 t2 的所有位，進行 AND 運算
  Bool     t15= unpack(&t2);  // 對 t2 的所有位，進行 AND 運算後，再轉換為 Bool 值
  ```

## UInt#(n) 類型，無符號數類型，可合成數據類型
- UInt#(n)類型，代表n位`無符號數`，範圍從 0 ~ $2^{n-1}$

- UInt#(n)類型值可進行和Bit#(n)值一樣的操作

  例如，常數賦值、零擴展(zeroExtend)、高位截斷(truncate)、逐位合併運算、
  按位邏輯運算、算術運算

- UInt#(n)類型值的限制，
  - `無法`透過 index 進行`位置選擇`，

  - 無法進行`位拼接`

  - 運算時，不能將 UInt#(n)類型值 和 Bit#(n)類型值進行混用，
    必須透過[類型轉換函數](#類型轉換函數)轉換為同類型值

  - 範例
    ```verilog
    UInt#(4) t1 = 13;
    //UInt#(2) t2 = t1[2:1];    // 錯誤
    //UInt#(2) t2 = {t1, t1};   // 錯誤
    
    UInt#(16) t1 = 12;
    //bit     t2 = &t1; // 錯誤，t1 和 t2 的寬度不同
    UInt#(1)  t3 = &t1; // 正確的逐位合併
    ```

## Int#(n) 類型，有符號數類型，可合成數據類型
- Int#(n)類型，代表n位`有符號數`，範圍從 $-2^{n-1}$ ~ $2^{n-1}$

- `int` 是 Int#(32) 的簡寫，和 C 語言一致

- Int#(n)類型值可進行和Bit#(n)值一樣的操作

  例如，常數賦值、零擴展(zeroExtend)、高位截斷(truncate)、逐位合併運算、
  按位邏輯運算、算術運算

- Int#(n)類型值的限制，和 [UInt](#uintn-類型無符號數類型可合成數據類型) 有一樣的限制

- 注意，`有符號數`(Int#(n))`轉換為無符號數`(UInt#(n))後，`十進制值會不同`

  例如，無符號數 u1 = 0xFA = 0b1111_1010 = 0d250

  經過 `Int#(8) i1 = unpack(pack(u1));` 的轉換後

  有符號數 i1 = 0b1111_1010 ，其中，`最高位 bit7=1`，所以 i1 會被編譯器視為是`負數值`，

  因此 i1 轉換為十進值時，會`捨棄最高位的符號位`，並`將剩餘的位數(bit6-bit0)取補數+1`

  i1 = 0b1_111_1010 -> (十進制轉換) -> 0b000_0101 + 1 -> 0b000_0110 = -6

  原本無符號數為250，轉換為有符號數為-6，
  無符號數的最高位會被視為是符號位，造成十進值不同(僅二進制值相同)，

  因此，無符號數轉換為有符號數後，進行的比較運算、擴展等操作時，需要注意無符號數和有符號數值之間的差異

  ```verilog
  module mkTb();
    rule extend_example;
        UInt#(8)  u1 = 'hFA;              // 0d250
        Int#(8)   i1 = unpack(pack(u1));  // i1 = 11111010 = 0d-5
        $display("%d", u1 > 2);           // 250 > 2，True 
        $display("%d", i1 > 2);           // -5 > 2，False
      
        UInt#(16) u2 = extend(u1);        // u1 高位進行零擴展，得到 0x00fa
        Int#(16)  i2 = extend(i1);        // i1 高位進行1擴展，得到 0xfffa
        $display("u2=%x", u2);
        $display("i2=%x", i2);

        $finish;
    endrule
  endmodule
  ```
  





## Bool 類型，可合成數據類型
- Bool類型值只有兩種，False / True

- Bool類型和Bit#(n)類型不能混用，Bool是字面量，必須透過 pack/unpack 轉換
  ```verilog
  Bool b1 = True;
  bit  b2 = pack(b1);    // b2 = 1'b1;
  Bool b3 = unpack(b2);  // b3 = True
  ```

- 所有`比較運算的結果`或`條件表達式的結果`，都必須是 Bool類型

## Integer類型，不可合成數據類型
- Integer類，繼承自用於算術運算的Arith類

- Integer類型值是不可合成的數據類型，不能用於硬體變數(Reg、Mem、FIFO)
  ```verilog
  Reg#(int) <- mkReg(0);        // 寄存器里存放 int 类型，正确
  //Reg#(Integer) <- mkReg(0);  // 寄存器里存放 Integer 类型，错误！！
  ```

- 常用於仿真，可作為循環語句的變數
  ```verilog
  int arr[16];
  
  for (int i=0; i<16; i=i+1)
    arr[i] = i;

  for (Integer i=0; i<16; i=i+1)
    arr[i] = fromInteger(i);
  ```

- Integer類是數學上的整數，是沒有邊界的，對 Integer類型值進行算術運算永遠不會溢出
  不像 Int#(n) 和 UInt#(n)，根據 n 值可表達的數值有上下限

## String類型，不可合成數據類型
- String類型值是不可合成的數據類型，不能用於硬體變數(Reg、Mem、FIFO)

- 特殊字符
  - 回車符，`\r`
  - 換行符，`\n`
  - tab，`\t`
  - 轉譯跳脫，`\`

- 可使用 + 串接 String類型值
  ```verilog
  rule test;
    String s = "aa";
    String t = " bb";
    String r = s + t;
    $display(r);  // aa bb
  endrule
  ```

## Maybe類型
- 為任意值類型添加 有效或無效的訊息，在有效的情況下，才能存取值

- 語法1:`Maybe#(值類型) 變數名 = tagged Invalid;`，為指定類型添加`無效`的訊息
- 語法2:`Maybe#(值類型) 變數名 = tagged Valid 保存值;`，為指定類型添加`有效`的訊息，有效才能存取值

- Maybe相關函數
  - `isValid(Maybe類型值)`: 讀取輸入的`Maybe類型值`的`有效欄位值`，是否是 Valid
    - 若有效，返回 True
    - 若無效，返回 False

  - `fromMay(無效時的返回值r, Maybe類型值x)`，判斷輸入的Maybe類型值是否有效，若無效，返回指定的值，返回 Maybe類型保存的值
    - 若 Maybe類型值x 有效，返回 x 保存的值
    - 若 Maybe類型值x 無效，返回 r

  - 範例
    ```verilog
    Maybe#(Int#(9)) value1 = tagged Invalid;    // value1 為無效的 Maybe類型值
    Maybe#(Int#(9)) value2 = tagged Valid 42;   // value2 為2效的 Maybe類型值，保存值為 42

    let v1 = isValid(value1);           // v1 = False
    let d1 = fromMaybe(-99, value1);    // d1 = -99
    let v2 = isValid(value2);           // v2 = True
    let d2 = fromMaybe(-99, value2);    // d2 = 42
    ```

## 類型轉換函數
- Bits.pack(): 將 Bits類型的值，轉換為Bit#(n)類型的值 (單位元 -> 多位元)
- Bits.unpack(): 將 Bits#(n)類型的值，轉換為Bits類型(或 Bits 延伸的子類型)的值 (多位元 -> 單位元)
- BitExtend.truncate(): 高位截斷，例如，Int#(8) -> Int#(4)
- BitExtend.zeroExtend(): 高位補零擴展，例如，Int#(4) -> Int#(8)，高位補0
- BitExtend.signExtend(): 高位符號擴展，例如，Int#(16)) -> Int#(32)
- BitExtend.extend(): 高位擴展，編譯器根據類型和最高位值，自動選擇採用 zeroExtend() 或 signExtend()
  > 若為有符號數(UInt#(n))，且最高位為1，採用 signExtend()
