## 複合類型
- 複合類型的差異

  (只能透過`index`存取)
  - Array: 只能存放`同類型`數據，使用index存取元素

  - Vector: 
    - 動態版的 Array，可動態刪除或插入元素
    - 進階版的 Array，可提供比 array 更多功能，例如，replicate、map、genVector、reverse、rotate
  
  (透過`符號`存取值 / 需要透過 typedef 定義變數名)
  - Enum: 限定值內容，獨立內存空間，
  - Struct: 不限定值內容，獨立內存空間
  - Union: 不限定值內容，共享內存空間

  (匿名數據)
  - Tuple: `只有值沒有符號`，無法使用index存取元素，只能透過特定函數存取值

## [Array] index存取 / 只能存放同類型數據

- 建立 array
  - 語法: `元素類型 arr名[元素個數n] = {值1, 值2, 值n, ...};`
  
- 賦值方法
  - 方法1，透過 index
  - 方法2，透過 for-loop
  - 範例
    ```verilog
    UInt#(8) arr[4] = { 0, 0, 0, 0 };

    // 賦值1，透過 index
    arr[0] = 11;
    arr[1] = 22;
    arr[2] = 33;
    arr[3] = 44;

    // 賦值2，透過 for-loop
    for ( int i=0; i<10; i=i+1)
      arr[i] = i * 3;
    ```

## [Vector] index存取 / 進階版的 Array
- 使用 vector 需要導入庫，`import Vector::*;`

- 建立 vector
  
  語法: `Vector#(元素個數n, 元素類型) 變數名;`

  除了透過 index 賦值外，可以透過 Vector庫提供的多個方式產生 vector 類型的值，
  例如，replicate()、map()、reverse()、rotate()

- 範例
  ```verilog
  Vector#(4, Int#(16)) v1;

  // 賦值1，透過 index
  v1[0] = 11;
  v1[1] = 22;
  v1[2] = 33;
  v1[3] = 44;

  // 賦值方式，透過 unpack() 進行賦值
  Vector#(4, Int#(16)) v2 = unpack('h1234); // v2 的元素分別為 1,2,3,4;

  // 賦值方式，replicate(值): 建立元素都是某個值得 vector
  Vector#(10, Bit#(4)) v3 = replicate(7); // v1 的每個元素都是 0b111 

  // 賦值方式，透過 genVector()，根據左值的 vector 個數n，產生 0 ~ n 的 vector (類型為Integer)
  Vector#(3, Integer) v4 = genVector(); // Vector{ 0, 1, 2 }

  // 賦值方式，利用 map( 輸出函數名, 輸入函數名)
  // 注意，map 用於 vector 時，
  //    - 輸入函數的返回值，必須是 vector的類型值，運行時，會將返回值的元素依序帶入輸入函數中
  //    - 輸出函數的返回值，必須和 vector的元素類型一致
  // 以下為例，
  //    - step1，genVector 產生 0,1,2,3 的 vector，並依序帶入 fromInteger 的函數中
  //    - step2，fromInteger 接受 genVector 傳入的 Integer，並轉換為 Int#(4) 的類型
  // fromInteger 的使用: 
  //    Int#(4) value = fromInteger(4); 將輸入的 Integer 4，轉換為 Int#(4)
  Vector#(3, Int#(4)) v5 = map( fromInteger, genVector ); 

  // 透過自定義的 map 函數產生 vector 類型值
  function Integer square(Integer x) = x * x;                            
  Vector#(64, int) v6 = map( fromInteger, map( square, genVector ) );
  
  // 賦值方式，
  Vector#(4, int) v7 = map( fromInteger, genVector ); // v7： 0,1,2,3
  Vector#(4, int) v8 = reverse( v7 ); // v8： 3,2,1,0
  Vector#(4, int) v9 = rotate( v7 ); // v9： 1,2,3,0

  // 建立多維 vector
  Vector#(3, Vector#(4, int)) vec34;
  vec34[0][0] = 3;
  vec34[1][2] = -9;
  vec34[2][3] = -8;

  // 寄存器類型的 vector，需要透過 replicateM() 批量實例化 reg (M for multiple)
  Vector#(8, Reg#(int)) reg_vec <- replicateM( mkReg(1) ); 
  ```

  - 範例，存放 vector類型值的 reg 變數，實現部分寫入 vector
    
    因為 reg 變數只能一次性寫入，若要實現部分寫入 vector，
    需要先將值讀出，修改後再寫回 reg

    ```verilog
    rule test;   // vec_in_reg 的類型是 Reg#(Vector#(8, int))
      Vector#(8, int) vec = vec_in_reg;    // 將 reg 中的 vec 讀取出來
      vec[4] = vec[5] + 1;                 // 修改
      vec_in_reg <= vec;                   // 將修改後的 vector 寫回 reg
    endrule
    ```

## [Enum] 符號存取 / typedef 定義變數名 / 限定值內容(單類型) / 獨立內存
- 注意，標籤和字串的不同

  標籤不是字串，是一段特殊的符號，無法透過 diplay 打印出來

- 建立 enum 變數 
  
  需要透過 `typedef` 定義 enum 變數值的名稱

  語法: `typdef enum { 標籤1, 標籤2, ... } 類型名 deriving(父類1, 父類2, ...);`
  ```c
  typdef enum { Green, Yellow, Red} Light deriving(Eq, Bits);   // 注意，只能繼承 Bits 類型 或 Bits 的子類型
  ```

- 可以只定義符號，不定義值 (可以不設置值)

- 若需要設置值，需要`顯式繼承 Bits類`，
  
  繼承 Bits類後，若不提供預設值，編譯器會`自動為每個符號進行編碼`
  ```c
  typedef enum {Green, Yellow, Red} Light deriving(Eq, Bool); // Green='b00, Yellow='b01, Red='b10
  ```

  自動編碼會根據前一個符號的`編碼值會自動+1`
  ```c
  typedef enum {Green, Yellow=5, Red} Light deriving(Eq, Bool); // Green=0, Yellow=5, Red=6
  ```

  或手動為每個符號賦予 `Bits類型的初始值`
  ```c
  typedef enum {Green='b11, Yellow='b10, Red='b01} Light deriving(Eq, Bool);
  ```

  enum 只能繼承 Bits類，或 Bits類型的子類
  ```c
  typedef enum {Green, Yellow, Red} Light deriving(Eq, Bool); // 錯誤，不能繼承 Bool
  ```
    
- 存取 Enum 類型值: Enum 類型值只能被賦予預先定義的符號
  ```c
  typedef enum {Green, Yellow, Red} Light deriving(Eq, Bool); // Green='b00, Yellow='b01, Red='b10

  Light va = Green      // 正確
  $display("%b", va);   // 00

  Light va = Foo  // 錯誤，Foo 不是 Light Enum 預先定義的符號
  ```

- 範例，
  ```verilog
  package Test;

    typedef enum {Green=125, Yellow=20, Red=85} Light deriving(Eq, Bits);   // Green=125, Yellow=20, Red=85

    module mkTb();
      rule test;
          Light va = Green;
          $display("Green = %b", va);
          va = Yellow;
          $display("Yellow = %b", va);
          va = Red;
          $display("Red = %b", va);

          $finish;
      endrule
    endmodule

  endpackage  
  ```

## [Struct] 符號存取 / typedef 定義變數名 / 不限定值內容(多類型) / 獨立內存
- 建立 struct 變數 
  
  需要透過 `typedef` 定義 struct 變數值的名稱

  語法: 
  ```c
  typdef struct { 
    類型 變數名1, 
    類型 變數名2,
    ...
  } 類型名 deriving(父類1, 父類2);  // 注意，只能繼承 Bits 類型 或 Bits 的子類型
  ```

- 存取 struct 變數
  ```verilog
  package top;

    typedef struct {
        Bit#(2) x;
        Bit#(2) y;
    } Point deriving(Bits, Eq);

    module top ();
      
      rule hello;
          Point p = Point{
            x: 'b01,
            y: 'b10
          };

          p.x = 'b11;

          $display("p.x=%b", p.x);
          $display("p.y=%b", p.y);

          $finish;
      endrule

    endmodule

  endpackage
  ```

## [Union-tagged] 符號存取 / typedef 定義變數名 / 不限定值內容(多類型) / 共享內存
- 類似 struct 類似的複合資料結構，但所有數據成員都`共享同一個內存`，因此，只有`最後一個賦值才是有效`的

- `step1`，建立 union-tagged 變數
  ```verilog
   typedef union tagged {
      // 定義標籤，和標籤值的類型
      // 注意，標籤限制第一個字必須大寫   
      標籤值類型 標籤名1;
      標籤值類型 標籤名2;

   } union類型名 deriving(父類1, 父類2, ...);  // 注意，只能繼承 Bits 類型 或 Bits 的子類型
  ```

- `step2`，賦值
  - 注意，標籤不是字串
    
    標籤不是字串，指定標籤名的時候需要加上 tagged，方便識別

  - 方法1，直接賦值
    ```verilog
    union類型名 變數名 = tagged 標籤名 值;
    ```

  - 方法2，先建立標籤值變數，在賦值給 union變數
    ```verilog
    // 建立標籤值
    union類型名 標籤值變數名 = tagged 標籤名 值;
    
    // 將標籤值賦值給 union變數
    union類型名 union變數名 = 標籤值變數名;
    ```

- `step3`，對 union變數進行取值
  - 注意
    
    因為`只有最後賦值的標籤有效`，需要`先判斷變數內的標籤`為何，才能進行取值

  - 方法1，透過 if match
    ```verilog
    // 如果 tag 是 AA，就建立 aa 變數
    if (p matches tagged AA .aa)
      $display( "aa = %d", aa );
    ... 內容省略
    ```

  - 方法2，透過 case
    ```verilog
      case (p) matches
         // 如果 tag 是 AA，就建立 aa 變數
         tagged AA .aa : $display( "aa = %d", aa );
         tagged BB .bb : $display( "bb = %d", bb );
      endcase
    ```

- 範例
  ```verilog

  package top;

    typedef union tagged {
        // 定義標籤明和標籤值的類型
        // 注意，標籤名限制第一個字大寫      
        UInt#(8) AA;
        UInt#(8) BB;
    } Pixel deriving(Bits, Eq);

  module top ();
    
    rule hello;        
        // 建立 union-tag 變數
        Pixel p;

        // ==== 賦值方法1，直接賦值 ====
        // 建立標籤名AA的值 10
        p = tagged AA 10;

        // ==== 賦值方法2，先建立標籤值再賦值 ====
        // 建立標籤值
        //Pixel v1 = tagged AA 10;
        //Pixel v2 = tagged BB 20;

        // 將標籤值賦予 union-tag 變數
        //p = v1;  // 無效
        //p = v2;  // 只有最後的賦值有效

        // ==== 取值方法1，透過 if match ====
    
        // if (p matches tagged AA .aa)
        //    $display( "aa = %d", aa );
        // else if (p matches tagged BB .bb)
        //    $display( "bb = %d", bb );
        // else
        //    $display( "no match");

        // ==== 取值方法2，透過 case ====

        case (p) matches
          // 如果 tag 是 AA，就建立 aa 變數
          tagged AA .aa : $display( "aa = %d", aa );
          tagged BB .bb : $display( "bb = %d", bb );
        endcase

        $finish;
        
    endrule
  endmodule

  endpackage
  ```

## [Tuple] 將各種類型的值放在一起的複合類型，使用內建函數存取元素 
- 建立 tuple
  
  語法: `Tuple<元素個數n>#(值1類型, 值2類型, 值n類型, ...) 變數名 = tuple<元素個數n>(值1, 值2, ... 值n);`

  ```verilog
  // 2個元素的tuple
  Tuple2#(Bool, Int#(4)) t1 = tuple2(True, 10);

  // 4個元素的tuple
  Tuple2#(Bool, Int#(4), Bit#(2), int) t1 = tuple2(True, 10, 'b11, 1);
  ```

- 存取 tuple
  - `方法1`，透過內建的 tpl 函數

    透過 tpl_<元素個數>() 的函數存取第 n 個元素

    ```verilog
    Tuple2#(Bool, Int#(4)) t1 = tuple2(True, 10);

    Bool e1 = tpl_1(t1)   // 存取 t1 tuple 中的第一個元素
    ```
  
  - `方法2`，透過 match {.變數1, .變數2}語句
    ```verilog
    Tuple2#(Bool, Int#(4)) t1 = tuple2(True, 10);
    match {.aa, .bb} = t1   // aa = True, bb = 10
    ```
  
