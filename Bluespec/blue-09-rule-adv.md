## Rule 的進階使用

- 注意，服用本篇前，須確保有[調度屬性的基礎](blue-08-sequential-circuit.md)

- `rule代碼塊`和`verilog-always代碼塊`的區別
  
  兩者在部分功能是相似的，都可以用來合成組合/時序邏輯電路，

  差別在於 rule代碼塊 是`根據調度規則來決定執行的順序`，verilog 並沒有決定 always代碼塊的邏輯順序，
  verilog 的所有always代碼塊都是併發的，verilog根據書寫順序`合併成一個大的 always代碼塊`，

- `rule代碼塊`和 `method代碼塊`的區別
  - 相同: 兩者都有機會對暫存器進行讀or寫
  - 相異: 

    兩者調用的時機不同，method 會在`數據輸入模塊或輸出模塊`時被調用，
    rule 會在每次`clk觸發`時被調用
  
- rule代碼塊的`語法`
  - 語法1，添加觸發條件
    
    等同於 verilog 的 `always @ (觸發條件)`

    ```verilog
    // 只有觸發條件返回 True 時，此 rule 才會被執行
    rule 規則名 (觸發條件); 
      ... 內容省略 ...
    endrule
    ```

  - 語法2，每次都觸發，設置在 clk 的週期會自動執行

    等同於 verilog 的 `always @ (posedge clk)`

    ```verilog
    rule 規則名; 
      ... 內容省略 ...
    endrule
    ```

- rule代碼塊的`特性`
  - `瞬時性`: 
    
    對於同一個 clk 週期一起執行的多個 rule代碼塊，這些rule代碼塊的語句，
    都會`在一個 clk 週期內完成`，而不考慮暫存器的建立時間(Tsetup)和保持時間(Thold)，

    瞬時性用於靜態時序分析，忽略現實世界的延遲
  
  - `原子性`:

    指 rule 代碼塊若是被激活，rule 內的所有語句都會執行，否則，rule 內的所有語句都不執行，
    rule 代碼塊看作是一個原子，具有不可分割的特性，因此語句無法獨立執行

- rule代碼塊的`激活`
  - 注意，`激活`是針對rule代碼塊，`執行`是針對語句

  - 當 rule代碼塊`被激活`時，rule內的`所有語句都執行`，
    
    當 rule代碼塊`未被激活`時，rule內的`所有語句都不會執行`，

  - rule代碼塊的`激活條件`
    - 條件1，rule代碼塊的`顯式觸發條件`必須為 True
    - 條件2，rule代碼塊內的語句，含有[隱式執行條件](#控制讀寫順序的調度註解scheduling-annotation)必須滿足
      
      例如，會調用 mkWire() 建立的Wire變數，在 rule代碼塊中調用該變數的讀取函數(_read())

## rule代碼塊的執行順序

- 範例1，單一rule內的執行順序，是由調度規則決定
  - 以`合成時序電路`的角度      
    - 將 y-FF 的輸出，經過 +1 的邏輯電路後，連接到 x-FF 的輸入
    - 將 x-FF 的輸出，經過 +1 的邏輯電路後，連接到 y-FF 的輸入
    - x、y 兩個暫存器是並排的，都會在同一個週期觸發的

  - 以`調度規則`的角度
    
    FF 會先讀在寫，因此順序為
    (正確順序) 讀 y -> 讀 x -> 寫 x -> 寫 y，實現xy交換

  - 不要以`軟體思維`判斷時序電路的執行順序 
    
    (錯誤順序) 讀 y -> 寫 x -> 讀 x -> 寫 y，無法實現xy交換

  ```verilog
  // 若 x y 都是暫存器模塊

  rule swap;
    x <= y + 1;   // 讀y, 寫x
    y <= x + 1;   // 讀x, 寫y
  ```

- 範例2，多個 rule 的執行順序，
  - 多個規則之間`在邏輯`上是順序執行的，不是依照書寫順序
    同樣的代碼，調用的模塊是Reg或Wire變數，會產生`不同的執行順序`
  
  - 以下列代碼為例
    ```verilog
    Reg#(int) x <- mkReg(1);
    
    // 若 y 是 Reg 时，rule的逻辑执行顺序是： r3 → r2 → r1
    // 若 y 是 Wire 时 rule的逻辑执行顺序是： r2 → r3 → r1
    
    Reg#(int) y <- mkReg(2);
    //Wire#(int) y <- mkDWire(2);

    rule r1;               // 读 x，写 x
        $display("r1");
        x <= x + 1;
        if(x >= 2) $finish;
    endrule

    rule r2;               // 读 x，写 y
        $display("r2");
        y <= x;
    endrule

    rule r3;               // 读 x，读 y
        $display("r3   x=%1d  y=%1d", x, y);
    endrule
    ```

  - 分析過程

    <img src="doc/sequential-circuit/rule-execute-order.png" width=800 height=auto>

- 分析原則
  - step1，隨意挑選任意兩個 rule 代碼塊，找出`同時具有讀寫操作的變數`
  - step2，根據該變數的調度規則，判斷兩個代碼塊的順序關係
  - step3，根據所有分析後的順序關係，推導出`符合所有分析結果`的邏輯關係

## rule代碼塊的衝突與調度屬性

- 發生衝突的原因

  rule代碼塊會根據調度規則，自動判斷rule的執行順序，但 rule 中的操作不符合邏輯時，就會遇到衝突

- 衝突分為以下兩種，
  - 資源衝突: 使用者`未遵守 Conflict 調度規則`而造成的衝突
  - 排序衝突: 編譯器在分析所有rule代碼塊後，執行順序`違反邏輯規則`
    - 例如，無法得到滿足所有條件的邏輯順序
    - 例如，發現有衝突的邏輯順序，
    
    ```verilog
      // 排序衝突範例: 有衝突的邏輯順序

      Reg#(int) x <- mkReg(1);
      Reg#(int) y <- mkReg(2);
      
      // ========
      // 由 x 正反器來看: 讀x(1)寫x(2)，讀取優先，因此 (1) --> (2)
      // 由 y 正反器來看: 讀y(2)寫y(1)，讀取優先，因此 (2) --> (1)
      // 兩個邏輯關係有衝突
      rule x2y;               // 讀x，寫y  ... (1)
          y <= x;
      endrule
      
      // ========
      rule y2x;               // 讀y，寫x  ... (2)
          x <= y;
      endrule

      // ========
      rule show;              // 讀x，讀y  ... (3)
          $display("x=%1d  y=%1d", x, y);
      endrule
    ```

  - 衝突的解決方式: 只有緊急的(第一個)rule代碼塊會被執行

    - 方法1，(預設狀況) 編譯器依照書寫順序，為起衝突的rule代碼塊添加緊急程度的屬性，
      且`只有緊急的rule代碼塊會被執行`，不緊急的rule代碼塊不會被執行

      以上述範例為例，rule-x2y 和 rule-y2x 起衝突，編譯器會為第一個 rule-x2y 添加緊急的屬性，
      因此，只有rule-x2y代碼塊會被執行，`rule-y2x代碼塊就不會被執行`

    - 方法2，使用者手動添加調度屬性(調度指令)，手動提供編譯起處理衝突的方法

- 注意，調度指令同時適用於 rule 和 method
  - method 會在`數據輸入模塊或輸出模塊`時被調用，有機會會對暫存器進行讀or寫
  - rule 會在每次`clk觸發`時被調用，有機會會對暫存器進行讀or寫
  - method 和 rule 在時序電路中有機會在同一個clk週期同時被執行，
    因此同樣都需要遵守調度規則
  - 範例，見 [比特編碼器v4](blue-10-interface-and-module-adv.md#範例比特編碼器v4另一種反壓的實現為動作值方法添加隱式條件)

## 提供衝突解決方法的調度屬性(調度指令)
- 為什麼需要調度屬性
  
  若不添加調度屬姓，當 rule 發生衝突時，編譯器會自行決定緊急的rule，
  並造成不緊急的rule不會被激活，進而使得rule中的語句不被執行，
  
  透過調度屬姓，可以控制衝突發生時的行為

- 調度屬姓的位置
  - 只能寫在 module 定義的上方，會 module 提供調度衝突的解決方式
  - 只能寫在 rule 定義的上方，會 rule 提供調度衝突的解決方式

- 幾種調度屬姓

  `(告知編譯器，發生衝突時該如何處理)`
  - 屬性1，descending_urgency 調度屬性 (降序緊急性): 手動指定 rule 的緊急程度
  
  `(告知編譯器，衝突不會發生)`
  - 屬性2，mutually_exclusive 調度屬性: 指明`rule不會同時激活`，不會發生衝突
  - 屬性3，conflict_free 調度屬姓: 指明`rule會同時激活`，但語句不會同時執行，不會發生衝突
  
  `(告知編譯器，強制當作衝突處理)`
  - 屬性4，preempts 調度屬姓 (搶佔的/衝突的): 強制為兩個 rule 添加衝突的屬性(即使實際上不衝突)，同時指
  定緊急程度

  `(用於斷言)`
  - 屬性5，fire_when_enabled 調度屬性: 檢查`當前rule`是否真的被激活
  - 屬性6，no_implicit_conditions 調度屬性: 檢查`當前rule`是否會被其他隱式條件阻塞

## [屬性] descending_urgency 調度屬性
- 使用場景
  
  告訴編譯器，發生衝突時該怎麼處理，無法避免衝突發生，
  告知衝突發生時，優先執行哪一個 rule

- 語法: `(* descending_urgency = "r1, r2, r3" *)`，緊急程度 r1 > r2 > r3

- 注意，此屬性只用來表示緊急程度，並不改變執行的順序
  
  衝突發生時，用來告知編譯器，哪個 rule 更緊急，只會作用在衝突的規則上，
  
  descending_urgency 排出緊急順序後，衝突仍然會發生，
  透過此調度屬姓手動決定哪一個 rule 是最緊急的規則，不緊急的 rule 仍然不會執行，

  而不是手動排出順序避免發生衝突，從而使所有的 rule 都執行

- 範例
  ```verilog
  module mkTb ();
    Reg#(int) cnt <- mkReg(0);
    rule up_counter;
        cnt <= cnt + 1;
        if(cnt > 5) $finish;
    endrule

    Reg#(int) x <- mkReg(1);
    Reg#(int) y <- mkReg(2);

    // y2x 比 x2y 緊急，衝突時優先執行 y2x 的規則
    (* descending_urgency = "y2x, x2y" *)  
    rule x2y;   // 會發生衝突
        y <= x + 1;
    endrule

    rule y2x;   // 會發生衝突
        x <= y + 1;
    endrule
    
    rule show;  // 不會發生衝突
        $display("cnt=%1d  x=%1d  y=%1d", cnt, x, y);
    endrule
  endmodule
  ```

- 其他範例見以下
  
## [屬性] mutually_exclusive 調度屬性
- 使用場景
  
  在某些情況下，使用者可以根據`顯式條件/隱式條件`分析出rule之間是否會發生衝突，但編譯器無法發現，
  
  此時，可透過 mutually_exclusive / conflict_free 調度屬性告知是否會發生衝突
  - mutually_exclusive 屬性: rule 激活的條件互斥，`不會在同時間激活`，因此不會發生衝突
  - conflict_free 屬性: rule `可以同時激活`，但導致衝突的語句不會同時發生

- 範例，編譯器`無法區分是否會發生衝突(錯誤判定為衝突)`的場景
  
  對編譯器來說，兩個rule的激活條件都使用 cnt 變數，但編譯器並`不會針對變數的單一位置`去比對是否衝突，
  而是考慮所有取值，例如，cnt = 'b0110 時，兩個 rule 都會同時被激活，因此判定兩個 rule 是衝突的

  但就使用者來說，cnt 的透過移位進行，1 每次前進一位，並`不會出現同時為1的狀況 ('b0110 不會出現)`，
  此時就需要人為加上 mutually_exclusive 的調度屬性，告知編譯器不會發生衝突

  ```verilog
  Reg#(Bit#(32)) cnt <- mkReg(1);  // cnt 初始值 = 'b0001
  Reg#(int) x <- mkReg(1);

  rule shift_counter;
    cnt <= cnt << 1;    // cnt = 'b0001 -> 'b0010 -> 'b0100 
    if(cnt > 10) $finish;
  endrule

  rule test1 (cnt[1] == 1);
    x <= x + 1;
  endrule

  rule test2 (cnt[2] == 1);
    x <= x - 1;
  endrule
  ```

  解決方式: 添加 mutually_exclusive 調度屬性，
  告知編譯器 rule 不會同時激活

  ```verilog
  // 告知編譯器 rule-test1 和 rule-test2 不會同時激活
  (* mutually_exclusive = "test1, test2" *)

  rule test1 (cnt[1] == 1);
    x <= x + 1;
  endrule

  rule test2 (cnt[2] == 1);
    x <= x - 1;
  endrule
  ```

- 注意，若用戶分析錯誤，
  
  如果用戶分析錯誤，以為2個rule互斥，但實際上並不互斥，
  那麼衝突語句的`執行結果可能無法預料`，或在仿真時會拋出錯誤，
  這種情況是使用者應該極力避免的，
  
  無法確認是否互斥時，應該用的是 descending_urgency 來指定規則的緊急程度，`讓結果變得可預知`，而不是用 mutually_exclusive

## [屬性] conflict_free 調度屬姓
- 使用場景
  
  在某些情況下，使用者可以根據`顯式條件/隱式條件`分析出rule之間是否會發生衝突，但編譯器無法發現，
  
  此時，可透過 mutually_exclusive / conflict_free 調度屬性告知是否會發生衝突
  - mutually_exclusive 屬性: rule 激活的條件互斥，`不會在同時間激活`，因此不會發生衝突
  - conflict_free 屬性: rule `可以同時激活`，但導致衝突的語句不會同時發生

- 範例
  - rule-test1 和 rule-test1 會同一個 clk 週期時被激活，使得以下兩個語句發生衝突
    - rule-test1 的 `x <= x + 1;` 語句 
    - rule-test2 的 `x <= x - 1;` 語句
  
  - 因為 rule-test1 的 `if(cnt < 3)`，使得 `x <= x + 1;` 語句 只會在 `cnt < 3` 的週期才會執行
  - 因為 rule-test2 的 `if(cnt > 3)`，使得 `x <= x - 1;` 語句 只會在 `cnt > 3` 的週期才會執行
  - 因此 rule-test1 和 rule-test1 可以同時執行，且語句不會發生衝突

  ```verilog

  Reg#(int) cnt <- mkReg(0);

  rule up_counter;
    cnt <= cnt + 1;
    if(cnt > 5) $finish;
  endrule

  Reg#(int) x <- mkReg(1);
  Reg#(int) y <- mkReg(0);
  Reg#(int) z <- mkReg(0);

  // 狀況1，不加任何調度属性，
  //    - rule-test1 和 rule-test2 每個周期都會執行
  //    - 兩個 rule 都有 讀x 寫x 的操作而造成衝突
  //    - 編譯器自動為 rule-test1 添加緊急屬性，使得 rule-test2 永遠不會被執行
  
  // 狀況2，使用 mutually_exclusive: (* mutually_exclusive = "test1, test2" *)
  //    - 告知 rule 不會同時執行，但實際上會同時執行
  //    - 編譯器不會產生 Warrning，但仿真時會拋出錯誤: 告知編譯器不會同時執行，但實際上會

  // 狀況3，使用 conflict_free: (* conflict_free = "test1, test2" *)
  //    - 告知 rule 會同時執行，但語句不會發生出衝突
  //    - 編譯器不會產生 Warrning，仿真結果符合預期

  (* conflict_free = "test1, test2" *)

  rule test1;
    y <= y + 1;       // 讀y，寫y，不會產生衝突的語句
    if(cnt < 3)
        x <= x + 1;   // 讀x，寫x，產生衝突的語句
  endrule

  rule test2;
    z <= z + 2;       // 讀z，寫z，不會產生衝突的語句
    if(cnt > 3)
        x <= x - 1;   // 讀x，寫x，產生衝突的語句
  endrule

  rule show;
    $display("x=%1d  y=%1d  z=%1d", x, y, z);
  endrule
  ```

## [屬性] preempts 調度屬姓
- 使用場景

  強制給兩個rule添加衝突的屬性，使得兩個rule必定不會同時執行，
  preempts 可以用來指定在隱式條件不滿足時的操作

- 注意，preempts `只能作用於兩個規則`，( ) 是多個 preempts 調度屬性的簡寫，
  不會出現 (* preempts = "規則1, 規則2, 規則3" *) 的語法

- 語法
  - 語法1，`(* preempts = "規則1, 規則2" *)`，只能作用2個規則

    規則1和規則2不能同時激活，且規則1的緊急性 > 規則2

  - 語法2，`(* preempts = "(規則1, 規則2), 規則3" *)`，多個 preempts 的簡化語法
      - `規則1`或`規則2`或`規則1規則2同時激活`時，規則3都不能激活
      - 等效於
        ```verilog
        (* preempts = "r1, r3" *) 
        (* preempts = "r2, r3" *)
        ```

- 注意，descending_urgency 和 preempts 的不同，
  - `比較1`，作用的時機不同
    
    descending_urgency 是作用於發生衝突時，用來指定衝突時的緊急程度，
    若不發生衝突時，descending_urgency 是沒有作用的

    preempts 無論 rule 是否會發生衝突，都強制視為是衝突

  - `比較2`，preempts 只能作用兩個 rule 

  - `比較3`，descending_urgency 的緊急程度具有傳遞性

    例如，`(* descending_urgency = "r1, r2, r3" *)`
    緊急性 r1 > r2
    緊急性 r2 > r3
    因此，緊急性 r1 > r3

    例如，`(* preempts = "r1, r2" *)` 且 `(* preempts = "r2, r3" *)`
    緊急性 r1 > r2
    緊急性 r2 > r3
    錯誤，緊急性 r1 > r3，對 preempts 而言，r1 和 r3 沒有衝突，可以同時激活

- 範例
  ```verilog
  module mkTb ();
    Reg#(Bit#(32)) cnt <- mkReg(0);

    rule up_counter;
        cnt <= cnt + 1;
        if(cnt > 8) $finish;
    endrule

    Reg#(int) x <- mkReg(0);
    Reg#(int) y <- mkReg(0);
    Reg#(int) z <- mkReg(0);

    // divide3, divide2, other 並不衝突，但強制加上衝突
    // 當 divide3激活 或 divide2激活 或 兩者都激活時，other 都不能激活
    (* preempts = "(divide3, divide2), other" *)
    // 等效於
    // (* preempts = "divide3, other" *)
    // (* preempts = "divide2, other" *)

    rule divide3 (cnt%3 == 0);
        x <= x + 1;
    endrule

    rule divide2 (cnt%2 == 0);
        y <= y + 1;
    endrule

    rule other;
        z <= z + 1;
    endrule

    rule show;
        $display("cnt=%1d  x=%1d  y=%1d  z=%1d", cnt, x, y, z);
    endrule
  endmodule
  ```

## [屬性] fire_when_enabled 調度屬姓
- 使用場景

  用於檢查，檢查 rule 在`顯式激活條件`和`隱式激活條件`同時成立的狀況下(應該被激活)，是否真的被激活，
  換句話說，檢查應該激活的rule，`是否會被其他調度屬性抑制`，而導致應該激活的rule卻無法激活

  若檢查的結果為 False，代表該規則的顯式條件和隱式條件都成立，但因為和其他規則衝突，
  導致當前規則會被其他調度屬性抑制而無法激活

- 語法
  
  注意，fire_when_enabled 只能作用於一個 rule
  ```verilog
  (* fire_when_enabled *)
  rule test1;
  ```

- 範例，應該激活的rule卻無法激活的狀況
  - 編譯器會檢查 test1 若顯式和隱式條件都成立，是否會真的被激活
  - 因 test2 和 test1 發生衝突，且 test1 具有較低的緊急性，因此 test1 會被抑制
  - 由上述兩點可知，fire_when_enabled 的檢查結果為 False，在編譯時會報錯

  ```verilog
   (* descending_urgency = "test2, test1" *)
   (* fire_when_enabled *)      // 檢查 test1 在顯示條件和隱式條件成立下，是否會被激活
   rule test1 (cnt%2 == 0);     // clk = 0, 2, 4, 6 ... 衝突週期 6, 12, 18, ...
      x <= x + 1;   // 讀x，寫x
   endrule

   rule test2 (cnt%3 == 0);     // clk = 0, 3, 6, 9 ... 衝突週期 6, 12, 18, ...
      x <= x + 2;   // 讀x，寫x
   endrule
  ```

  修正方法1，
  - 將 test2 的顯式條件改為 `rule test2 (cnt%2 == 1);`，clk = 1, 3, 5, 7 時激活
  - 編譯器分析得知 test1 和 test2 互斥，因此 descending_urgency 失效，test1 會被激活，不會被抑制

  修正方法2，
  - 調整緊急順序，`(* descending_urgency = "test1, test2" *)`，
  - 雖然有衝突，但 test1 不會被抑制

## [屬性] no_implicit_conditions 調度屬姓
- 使用場景

  檢查`當前rule`是否會被其他隱式條件阻塞，
  - 若檢查結果為 True，不會被其他隱式條件阻塞
  - 若檢查結果為 False，有其他隱式條件會阻塞當前rule的執行

  用於編寫代碼時，由編譯器檢查是否其他的隱式條件，
  同時可以提式開發者，該規則不應該有隱式條件

## 解除衝突的範例
- 注意，if 語句不會參與 rule 激活條件的判斷，但是會影響 rule 之間是否產生衝突

- 範例，利用`descending_urgency 調度屬姓 + 顯式的rule條件`避免衝突
  - 顯式的rule條件，用於減少發生衝突的週期，但衝突無法避免
  - 當衝突發生時，利用 descending_urgency 讓編譯器優先執行某個指定的規則
  - 代碼

    會發生衝突的規則
    ```verilog
      rule x2y;
          y <= x + 1;   // 讀x，寫y
      endrule

      rule y2x;
          x <= y + 1;   // 讀y，寫x
      endrule
    ```

    避免衝突
    ```verilog
    (* descending_urgency = "y2x, x2y" *)
    rule x2y;           // cnt < 3 時，兩個 rule 發生衝突，
                        // 但由於調度屬性，因為會執行 rule-y2x 的規則
        y <= x + 1;     // 讀x，寫y

    endrule

    rule y2x (cnt<3);   // 只會在特定時段被激活
        x <= y + 1;     // 讀y，寫x
    endrule
    ```

- 範例，利用`descending_urgency 調度屬姓 + 語句的隱式條件`避免衝突
  - 參考，[調度規則的隱式條件](blue-08-sequential-circuit.md#控制讀寫順序的調度註解scheduling-annotation)
  
  - 會發生衝突的代碼
    ```verilog
      rule x2y;
          y <= x + 1;   // 讀x，寫y
      endrule

      rule y2x;
          x <= y + 1;   // 讀y，寫x
      endrule
    ```

  - 避免衝突的代碼
    ```verilog
    module mkTb ();
      Reg#(int) cnt <- mkReg(0);
      Wire#(int) w1 <- mkWire;   // w1 用於建立隱式條件

      rule up_counter;
          cnt <= cnt + 1;
          if(cnt < 2) w1 <= cnt + 1;  // 只有 cnt < 2 時會執行 w1 的寫入
          if(cnt > 5) $finish;
      endrule

      Reg#(int) x <- mkReg(1);
      Reg#(int) y <- mkReg(2);

      (* descending_urgency = "y2x, x2y" *)
      rule x2y;     
          y <= x + 1;       // 讀x，寫y
      endrule

      rule y2x;             // 因為隱式條件w1，只有在 cnt < 2 時，才會激活 rule-y2x
          x <= y + w1;      // 讀y，讀w1，寫 x ，此行有隱式條件
      endrule

      rule show;
          $display("cnt=%1d  x=%1d  y=%1d", cnt, x, y);
      endrule
    endmodule
    ```