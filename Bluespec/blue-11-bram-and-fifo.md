## BRAM 概述

- BRAM = 片內儲存器，可暫存 幾百Byte ~ 幾十MByte 的數據
  
- BRAM(Block-memory) 和 DRAM(Distributed-memory) 的區別
  - `BRAM` 
    - 是整塊雙口RAM資源，是 FPGA 定製的RAM資源
    - 需要有 clk 才會輸出
    - 較大的儲存應用，使用 BRAM

      或要產生大的FIFO或timing要求較高時，優先使用BRAM

      FPGA中的資源位置是固定的，例如BRAM就是一列一列分佈的，
      有可能會造成使用者邏輯和BRAM之間的route延時比較長

  - `DRAM`
    - 是利用多個 LUT(Look-up-table) 拼湊而成 (LUT當作RAM使用)

      LUT 本身就是由一塊小的 RAM 區域，透過預先在 RAM 中寫入特定值，
      透過地址線輸入特定數值，會對應RAM特定區域的值

      將多個 LUT 組合起來就可以當作DRAM使用，但LUT作為RAM使用後就無法再作為LUT使用，
      會浪費FPGA的邏輯資源

    - 不需要 clk 觸發，給存取地址後會立刻輸出對應的資料，
      若需要 clk 觸發，可加上 reg

    - 適合小應用

- BRAM模塊分為兩種
  - 種類1，`BRAMCore庫`提供的BRAM模塊，提供最原始的 BRAM，不提供以下高級功能
    - 不提供讀取操作的緩衝
    - 不提供寫操作響應的緩衝
    - 讀操作/寫操作沒有任何隱式條件
    - 生成的代碼會自動與各種型號FPGA的晶片匹配，保證底層使用的是 BRAM
  
  - 種類2，`BRAM庫`提供的BRAM模塊，提供更高級功能，使用起來更方便

## [BRAMCore庫] 原始的 BRAM 模塊
- 導入 BRAM 庫，`import BRAMCore::*;`

- BRAM庫提供的BRAM模塊，有以下兩種
  - 單一接口的 BRAM，`mkBRAMCore1模塊`，
    > module mkBRAMCore1#(Integer memSize, Bool hasOutputRegister) (BRAM_PORT#(addr, data))
  
  - 雙接口的 BRAM，`mkBRAMCore2模塊`，
    >module mkBRAMCore2#(Integer memSize, Bool hasOutputRegister) (BRAM_DUAL_PORT#(addr, data))
  
  - 其中
    - memSize參數: 用於指定 BRAM 的記憶體大小
    - hasOutputRegister參數: 決定讀出的數據是否額外經過一個暫存器
      - hasOutputRegister = False，讀取延遲為1 (從調用讀操作，到讀出數據需要經過1個週期)
      - hasOutputRegister = True，讀取延遲為2 (從調用讀操作，到讀出數據需要經過2個週期)

- `BRAM_PORT類型變量`，用於單口模塊(mkBRAMCore1)的接口
  ```verilog
  // BRAM_PORT 單接口的定義
  interface BRAM_PORT#(type addr, type data);
    // 每周期只允許操作一次，操作可以是讀取操作或寫入操作
    method Action put(Bool write, addr address, data datain);
    // 獲取讀取的數據
    method data read();
  endinterface
  ```

- `BRAM2Port類型變量`，用於雙口模塊(mkBRAMCore2)的接口
  ```verilog
  // BRAM_DUAL_PORT 雙接口的定義，為 BRAM_PORT 單接口 的合併
  interface BRAM_DUAL_PORT#(type addr, type data);
    interface BRAM_PORT#(addr, data) a;
    interface BRAM_PORT#(addr, data) b;
  endinterface
  ```
  
- 範例，mkBRAMCore1 / mkBRAMCore2 使用範例
  ```verilog
  // ==== 實例化 BRAM 模塊 ====
  // 實例化單接口的 native-bram-module
  //    記憶體大小為 4096，讀操作的輸出經過暫存器
  //    接口位址類型為 Bit，寬度為 12bit
  //    接口數據類型為 UInt，寬度為 40bit
  BRAM_PORT#(Bit#(12), UInt#(40)) ram1 <- mkBRAMCore1(4096, True);

  // 實例化雙接口的 native-bram-module
  //    記憶體大小為 10000，讀操作的輸出不經過暫存器
  //    接口位址類型為 Bit，寬度為 15bit
  //    接口數據類型為 UInt，寬度為 32bit
  BRAM_DUAL_PORT#(Bit#(15), int) ram2 <- mkBRAMCore2(10000, False);

  // ==== 透過單接口讀寫數據 ====
  ram1.put(True, 1234, 1000);   // 調用寫操作，寫入位址=1234，寫入數據=1000
  ram1.put(False, 4000, 0);     // 調用讀操作

  // 注意，ram1 的輸出有經過暫存器，
  // 調用 read 方法後，需要2個週期後，再調用 ram1.read，才能確保拿到正確的數據
  UInt#(40) rdata = ram1.read;  // 取值

  // ==== 透過雙接口讀寫數據 ====
  ram2.a.put(True , 1234, 114514);  // portA 寫入數據
  ram2.b.put(False, 4000, 0);       // portB 讀取數據

  // 注意，ram2 的輸出沒有經過暫存器，
  // 調用 read 方法後，需要1個週期後，再調用 ram2.b.read，才能確保拿到正確的數據
  int rdata = ram2.b.read;
  ```

- 範例，仿真時利用檔案對 BRAM 的內容進行初始化
  - 使用 `mkBRAMCore1Load模塊` 或 `mkBRAMCore2Load模塊`，

    使用方式和 mkBRAMCore1 / mkBRAMCore2 相同，
    差別在常數輸入參數多了 file 和 binary 兩個參數，用於指定初始化檔案的路徑，和 binary/hex 格式的選擇 

  ```verilog
  // 帶有初始文件設置的單口bram模塊，single-port-native-bram-with-Load-Parameter
  module mkBRAMCore1Load#(Integer memSize, Bool hasOutputRegister, String file, Bool binary ) (BRAM_PORT#(addr, data))

  // 帶有初始文件設置的雙口bram模塊，dual-port-native-bram-with-Load-Parameter
  module mkBRAMCore2Load#(Integer memSize, Bool hasOutputRegister, String file, Bool binary ) (BRAM_PORT#(addr, data))
  ```

## [BRAM庫] 進階的 BRAM 模塊

- 導入 BRAM 庫，`import BRAM::*;`

- BRAM庫提供的BRAM模塊，有以下兩種
  - 單一接口的 BRAM，`mkBRAM1Server模塊`，
    > module mkBRAM1Server #( BRAM_Configure cfg ) ( BRAM1Port #(taddr, tdata) )
  
  - 雙接口的 BRAM，`mkBRAM2Server模塊`，
    > module mkBRAM2Server #( BRAM_Configure cfg ) ( BRAM2Port #(taddr, tdata) )
  
  - `BRAM1Port類型變量`，用於單口模塊(mkBRAM1Server)的接口
    ```verilog
    // BRAM1Port 接口的定義
    interface BRAM1Port#(type taddr, type tdata);   // taddr 地址類型， tdata 是bram數據的類型
      interface BRAMServer#(taddr, tdata) portA;    // BRAMServer接口，用於發起讀寫操作/獲取讀操作的數據/獲取寫操作的反饋
      method Action portAClear;                     // 用於清除讀操作和寫操作反饋的緩衝
    endinterface
    ```

  - `BRAM2Port類型變量`，用於雙口模塊(mkBRAM2Server)的接口
    ```verilog
    // BRAM2Port 接口的定義
    interface BRAM2Port#(type taddr, type tdata); // taddr 地址類型， tdata 是bram數據的類型
      interface BRAMServer#(taddr, tdata) portA;  // BRAMServer接口A，用於發起讀寫操作/獲取讀操作的數據/獲取寫操作的反饋
      interface BRAMServer#(taddr, tdata) portB;  // BRAMServer接口B，用於發起讀寫操作/獲取讀操作的數據/獲取寫操作的反饋
      method Action portAClear;                   // 用於清除 portA 的緩衝
      method Action portBClear;                   // 用於清除 portB 的緩衝
    endinterface: BRAM2Port
    ```

  - `BRAM_Configure類型變量`，用於 mkBRAM1Server/mkBRAM2Server 模塊的配置
    ```verilog
    // BRAM_Configure 類的定義
    typedef struct {
      Integer    memorySize;               // 指定RAM的容量，memory=0，容量=位址線的寬度
      LoadFormat loadFormat;               // 初始數據，取 None | tagged Hex "文件名" | tagged Binary "文件名" 
      Integer    latency;                  // 讀操作回應/寫操作反饋的延遲，只能是 1 | 2
      Integer    outFIFODepth;             // 讀操作回應/寫操作反饋的緩衝大小，推薦值為 latency+2
      Bool       allowWriteResponseBypass; // 是否允許略過之前的讀數據操作，直接進行寫操作的反饋，常用 False
    } BRAM_Configure;
    ```
  
  - `BRAMRequest類型變量`，用讀寫操作時，將數據、位址、其他相關資訊包裝為請求後，再透過接口發送
    ```verilog
    typedef struct {
      Bool write;              // True=寫操作 / False=讀操作
      Bool responseOnWrite;    // 寫操作時，使否需要響應通知寫操作完成
      addr address;            // 讀取/寫入的地址
      data datain;             // 寫入數據
    } BRAMRequest#(type addr, type data) deriving(Bits, Eq);
    ```

- BRAM庫的隱式執行條件

  BRAM庫提供的bram模塊的輸出是有帶緩衝的(有內建queue)，
  queue實務上是由`帶有特定讀寫規則的暫存器`所組成，因此，BRAM庫提供的bram模塊的讀寫操作是具有隱式執行條件

  - 讀取數據 (response.get) 的隱式執行條件

    當緩衝的內容`不為空`時，讀取操作的語句(response.get)就可執行，

  - 寫入數據 (request.put) 的隱式執行條件

    當緩衝的內容`不為滿`時，寫入操作的語句(request.put)就可執行，

  - 注意，隱式條件只針對同一個接口，對於雙接口的portA和portB有獨立的緩衝，互不影響

- 範例
  ```verilog
  // 透過 BRAM_Configure 建立 bram 的配置
  //    
  BRAM_Configure config = BRAM_Configure {
    memorySize   : 0,      // ram 的容量 = 2^(位址線的寬度)
    loadFormat   : None,   // 不使用檔案對RAM的內容進行初始化
    latency      : 1,      // 讀操作的響應和寫操作的響應延遲為 1個 cycle
    outFIFODepth : 3,      // 讀操作響應和寫操作響應的緩存大小設置為3 
    allowWriteResponseBypass : False 
  };

  // 建立具有緩衝/單一接口/位址寬度為10bit-Bit/數據類型為Int，數據長度為8bit 的 ram
  BRAM1Port#(Bit#(10) , Int#(8)) ram1 <- mkBRAM1Server(config);

  // 建立具有緩衝/雙接口/位址寬度為12bit-Uint/數據類型為int，數據長度為32bit 的 ram
  BRAM2Port#(UInt#(12), int) ram2 <- mkBRAM2Server(
    BRAM_Configure {                            
        memorySize   : 2000,                       // ram 的容量為 2000 bits
        loadFormat   : tagged Hex "data.txt",      // 使用 data.txt hex 的文件，對 ram 的內容初始化
        latency      : 2,                          // 讀操作和寫操作響應的延遲 = 2
        outFIFODepth : 4,                          // 讀操作和寫操作響應的緩存大小為 4
        allowWriteResponseBypass : False  }        
    }
  );

  // 建立讀請求
  Bool      iswrite = False;
  UInt#(12) addr    = 100;
  int       wdata   = 1234;

  ram2.portB.request.put(
    BRAMRequest{
        write: iswrite,
        responseOnWrite: False,
        address: addr,
        datain: wdata
    }
  );

  // 從 portB 讀取返回的數據
  int rdata <- ram2.portB.response.get;
  ```

- 範例，以 bram 實現矩陣轉置

  - [完整範例](example-bram-transpose-buffer/transpose-buffer.bsv)

  - `原理`，矩陣轉置，將矩陣的行列互換，例如，將 2*4 的矩陣經過轉置後，轉換為 4*2 的矩陣，
    
    2*4 的矩陣，可以看成是row有2行的數據，column的數據各有4欄，

    4*2 的矩陣，可以看成是row有4行的數據，column的數據各有2欄，
    
    要實現轉置的功能，由上至下(column的方向)依序將輸入矩陣的數據寫入，
    並由左至右(row的方向)將數據輸出

    例如，
    ```verilog
    // 寫入矩陣 (寫入值 = col row)

                  col=000     col=001
    row=000     0(000_000)  8(001_000)
    row=001     1(000_001)  9(001_001)
    row=010     2(000_010)  A(001_010)
    row=011     3(000_011)  B(001_011)
    row=100     4(000_100)  C(001_100)
    row=101     5(000_101)  D(001_101)
    row=110     6(000_110)  E(001_110)
    row=111     7(000_111)  F(001_111)
                  col_row

    // 讀取矩陣 (讀取值 = row col)

                  col=000     col=001     col=010     col=011     col=100     col=101     col=110     col=111
    row=000     0(000_000)  1(000_001)  2(000_010)  3(000_011)  4(000_100)  5(000_101)  6(000_110)  7(000_111)
    row=001     8(001_000)  9(001_001)  A(001_010)  B(001_011)  C(001_100)  D(001_101)  E(001_110)  F(001_111)
                                          row_col

    寫入值 == 寫入位置 == (col)(row)
      例如，依照上述的寫入矩陣，寫入值=A(0b001_010)，可拆分為
      - 高三位的001，代表 col 值
      - 低三位的010，代表 row 值
      - 寫入值 A(0b001_010) == 寫入位置 AH

    轉置後，讀取值 != 讀取位置
    - 讀取的位置，需要將 col 和 row 的互換，才能獲取正確的預期值
    - 例如，寫入值A，位於 AH 的位址
    - 若不將 col 和 row 互換，讀取 Ah(0b001_010) 位址得到值=XXX，並不會得到值A
    - 需將 col 和 row 互換 0b001_010 -> 0b010_001，才會得到值A
    ```
    
  - `需求`
    - `需求1`，以兩個緩衝區(兩個bram-block) + 流水線 的設計進行轉置

      若只使用一個RAM，讀取和寫入無法同時進行，需要把數據讀出後，再對數據重新擺放，
      讀取需要 n 個週期，寫入也同樣需要 n 個週期，因此總共需要 2n 個週期

      若有兩個RAM，`讀和寫的操作可以在同一個週期操作`，使得花費時間僅需要 n 個週期，
      且透過流水線的設計，可以在同一個周期內執行讀寫的操作，將 row-n 的數據讀出，再寫入 column-n 的位置

      雙口 BRAM 來構成雙緩衝，在邏輯上會分為2個 block，每個 block 為8行8列
      
      雙緩衝的讀取和寫入會`交替在2個 block 上執行`，寫入 block-0的同時，只能讀取 block-1，讀寫不會同時發生在同一個 block

    - `需求2`，利用 wblock/rblock 標記存取位置，和標記緩衝內的資料狀態(empty或full)
      - 規則1: 當值寫入block後，wblock 值 +1，當值從block讀出後，rblock 值 +1  

      - 規則2: wblock/rblock 的高位和低位，分別代表 (高位: 讀寫狀態) (低位: 當前存取block位置)

        <font color="red">注意，不是單純的利用高位的0或1來表示緩衝的empty或full，
        
        需要和低位值的狀態一起參與考慮，才能決定緩衝的狀態</font>

        <img src="doc/bram-and-fifo/bram-transpose-full-empty.png" width=100% height=auto>

        由上表可知，
        - 兩個block都`滿`的狀況會發生在時序3，wblock/rblock的低位值相同，但高位值不同
        - 兩個block都`空`的狀況會發生在時序1和時序5，wblock/rblock的高位值和低位值都相同

  - 代碼

    實例化雙口bram，將地址區分為三個變數組成: block-n | col-or-row-index | col-or-row-index
    
    ```verilog
    // BRAM2Port#( 位址類型, 數據類型 ) ram <- mkBRAM2Server(配置);
    // 位址類型 = Tuple3#(bit, UInt#(3), UInt#(3))，  block-n(1bit) | col-or-row-index | col-or-row-index
    BRAM2Port#( Tuple3#(bit, UInt#(3), UInt#(3)) , int ) ram <- mkBRAM2Server(defaultValue);
    // defaultValue 的內容
    //   memorySize   : 0,      // 存储器容量 = 2^(位址線的寬度)，在此例中ADDR為 7bit = 1+3+3
    //   loadFormat   : None,   // 不進行文件初始化
    //   latency      : 1,      // 響應的延遲時間為1
    //   outFIFODepth : 3,      // BRAM模塊內的緩衝大小為3
    //   allowWriteResponseBypass : False

    // 定義 write-block 的地址變數
    // 注意，位址中的block-n位置只占用1bit，但 wblock 占用 2bit，wblock 的高位值用於判斷空或滿，不會被當成位址
    Reg#(Bit#(2))  wblock <- mkReg(0);    // 標記寫入block的位置和指示block狀態
    Reg#(UInt#(3)) wcol <- mkReg(0);      // 標記當前寫入的 column 位置
    Reg#(UInt#(3)) wrow <- mkReg(0);      // 標記當前寫入的 row 位置

    // 定義 read-block 的地址變數
    Reg#(Bit#(2))  rblock <- mkReg(0);    // 標記讀取block的位置和指示block狀態
    Reg#(UInt#(3)) rcol <- mkReg(0);      // 標記當前讀取的 column 位置
    Reg#(UInt#(3)) rrow <- mkReg(0);      // 標記當前讀取的 row 位置
    ```

    

    
## FIFO