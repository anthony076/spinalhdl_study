
package top;

import DReg::*;

// ==== 取得生成碼和生成碼長度 ====
// 利用 function 建立生成碼的組合邏輯
function Tuple2#(Bit#(10), UInt#(4)) getCode(Bit#(8) din);
   
   // 取得長度碼
   UInt#(4) len = 0;
   for(UInt#(4) i=0; i<8; i=i+1)
      if(din[i] == 1)
         len = i;
   
   UInt#(4) trim_len = len>0 ? len : 1;

   // 取得數據碼
   Bit#(7) trim = truncate(din) & ~('1<<trim_len);

   // 取得生成碼
   Bit#(10) code = {trim, pack(len)[2:0]};

   // 返回生成碼和生成碼的長度
   return tuple2( code, trim_len+3 );

endfunction

// ==== 定義接口 ====
interface BitCoder;
   method Action   put(Bit#(8) din);    // put 接口的類型為 input
   method Bit#(16) get;                 // get 接口的類型為 output，用於取得輸出碼
   method Bool     get_valid;           // get_valid 接口的類型為 output，用於指示編碼已完成(用於輸出碼可用)
endinterface

// ==== 定義模組 ====
(* synthesize *)                        // 將 mkBitCoder 編譯為單獨的模塊(mkBitCoder.v)
module mkBitCoder (BitCoder);
   /*
      電路流程:
      輸入數據din -> put接口 -> getcode()，產生生成碼的組合邏輯 -> 
      流水線第1級，保存前一級輸入的生成碼和長度 -> 流水線第2級，產生輸出碼(dout)和輸出碼指示(dout_valid)
   */

   // ==== step1，建立模塊內部使用到的變數 ====
   // 定義流水線第1級使用的數據，用於保存生成碼組合邏輯的輸出值
   // 定義 in_code_and_len 變數，並將 in_code_and_len 初始化為0
   Reg#(Tuple2#(Bit#(10), UInt#(4))) in_code_and_len <- mkDReg( tuple2(0,0) );

   // 定義流水線第2級使用的數據，用於產生輸出碼(dout)和輸出碼指示(dout_valid)
   // 遺留碼(drem)、遺留碼長度(drem_len)是產生 dout 和 dout_valid 的中間變數
   Reg#(Bit#(31)) drem       <- mkReg(0);        // drem: 儲存遺留碼
   Reg#(UInt#(5)) drem_len   <- mkReg(0);        // drem_len: 遺留碼的長度
   Reg#(Bool)     dout_valid <- mkDReg(False);   // dout_valid: 指示 dout 是否有效
   Reg#(Bit#(16)) dout       <- mkReg(0);

   // ==== step3，建立 mkBitCoder 模塊的內部邏輯 ====
   rule get_drem_and_dout;
      match {.code, .code_len} = in_code_and_len;  // 取得前一級輸出的生成碼和生成碼長度

      Bit#(31) data = (extend(code) << drem_len) | drem;   // 建立未被裁減的遺留碼(data) = 生成碼(code) + 前一次的遺留碼(drem)
      UInt#(5) len = extend(code_len) + drem_len;          // 取德遺留碼的長度

      if(len >= 16) begin                   //   若遺留碼長度 > 16，則設置輸出(dout)和輸出指示位(dout_valid)
         dout_valid <= True;                //        設置輸出指示位(dout_valid)為 True
         dout <= truncate(data);            //        高位截斷，設置輸出碼(dout) = 遺留碼的低16位
         data = data >> 16;                 //        高位移到低位，更新遺留碼(data) = 遺留碼的高位，
         len = len - 16;                    //        更新遺留碼的長度
      end

      drem <= data;                         // 保存遺留碼，供下次觸發使用
      drem_len <= len;                      // 保存遺留碼長度，供下次觸發使用
   endrule

   // ==== step2，實作put接口，建立 put接口與內部訊號的連接 ====
   // 建立輸入數據(din) -> put接口 -> 產生生成碼的組合邏輯 -> 流水線第1級之間的邏輯關係
   method Action put(Bit#(8) din);          
      in_code_and_len <= getCode(din);      // put接口，接受din的輸出，經過 getcode 的組合邏輯後，輸出 in_code_and_len
   endmethod

   // ==== step4，實作get_valid接口，建立 get_valid接口與內部訊號的連接 ====
   // 配置 get_valid接口，建立內部變數dout_valid和get_valid接口之間的連接
   method Bool get_valid = dout_valid;      

   // ==== step5，實作get接口，建立 get接口與內部訊號的連接 ====
   // 配置 get接口，建立內部變數dout和get接口之間的連接
   method Bit#(16) get = dout;   
endmodule

// ==== 調用 mkBitCoder 模組，用於測試模組 ====
module top();
   
   // 實例化測試模組(實例化mkBitCoder模組)
   let dut <- mkBitCoder;

   // 建立計數器
   Reg#(int) cnt <- mkReg(0);
   rule up_counter;
      cnt <= cnt + 1;
   endrule

   // 建立 din 訊號
   Reg#(Bit#(10)) din <- mkReg(0);

   // ==== 將輸入訊號傳遞給 dut ==== 
   rule dut_put;
      din <= din + 1;
      
      // 設置結束條件
      if(din < 'h200)
         dut.put( truncate(din) );   // 將 din 訊號輸入給 dut
      else if(din == '1)
         $finish;

   endrule

   // ==== 讀取dut的輸出 ====
   rule get_result (dut.get_valid);             // 只在 dut.get_valid 有效時才輸出
      $display("cnt=%4d   %b", cnt, dut.get);
   endrule
endmodule
endpackage
