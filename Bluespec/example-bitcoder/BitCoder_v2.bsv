
package top;

import DReg::*;

function Tuple2#(Bit#(10), UInt#(4)) getCode(Bit#(8) din);
   
   UInt#(4) len = 0;
   for(UInt#(4) i=0; i<8; i=i+1)
      if(din[i] == 1)
         len = i;
   
   UInt#(4) trim_len = len>0 ? len : 1;
   Bit#(7) trim = truncate(din) & ~('1<<trim_len);
   Bit#(10) code = {trim, pack(len)[2:0]};

   return tuple2( code, trim_len+3 );

endfunction

// ============
interface BitCoder;
   method Action   put(Bit#(8) din);    
   method Bit#(16) get;                 
   method Bool     get_valid;           
endinterface

// ============
(* synthesize *)
module mkBitCoder (BitCoder);
   Reg#(Tuple2#(Bit#(10), UInt#(4))) in_code_and_len <- mkDReg( tuple2(0,0) );

   Reg#(Bit#(31)) drem       <- mkReg(0);
   Reg#(UInt#(5)) drem_len   <- mkReg(0);
   Reg#(Bool)     dout_valid <- mkDReg(False);   
   Reg#(Bit#(16)) dout       <- mkReg(0);

   // ============
   rule get_drem_and_dout;
      match {.code, .code_len} = in_code_and_len;

      Bit#(31) data = (extend(code) << drem_len) | drem;
      UInt#(5) len = extend(code_len) + drem_len;

      if(len >= 16) begin       
         dout_valid <= True;    
         dout <= truncate(data);
         data = data >> 16;     
         len = len - 16;        
      end

      drem <= data;             
      drem_len <= len;          
   endrule

   // ============
   method Action put(Bit#(8) din);
      in_code_and_len <= getCode(din);
   endmethod

   method Bool get_valid = dout_valid;      

   //method Bit#(16) get = dout;                // 未添加隱式條件
   method Bit#(16) get if(dout_valid) = dout;   // 為值方法添加隱式條件

endmodule

// ============
module top();
   
   let dut <- mkBitCoder;

   Reg#(int) cnt <- mkReg(0);
   rule up_counter;
      cnt <= cnt + 1;
   endrule

   Reg#(Bit#(10)) din <- mkReg(0);


   rule dut_put;
      din <= din + 1;
      
      if(din < 'h200)
         dut.put( truncate(din) );
      else if(din == '1)
         $finish;

   endrule

   // rule get_result (dut.get_valid);    // 值方法沒有添加隱式執行條件時，需要手動添加 rule 的執行條件
   rule get_result;                       // 值方法已添加隱式執行條件時，就不需要添加 rule 的執行條件
      $display("cnt=%4d   %b", cnt, dut.get);
   endrule
endmodule
endpackage
