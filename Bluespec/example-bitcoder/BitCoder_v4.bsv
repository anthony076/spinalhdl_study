
package top;

import DReg::*;

function Tuple2#(Bit#(10), UInt#(4)) getCode(Bit#(8) din);
   
  UInt#(4) len = 0;
  for(UInt#(4) i=0; i<8; i=i+1)
    if(din[i] == 1)
        len = i;
  
  UInt#(4) trim_len = len>0 ? len : 1;
  Bit#(7) trim = truncate(din) & ~('1<<trim_len);
  Bit#(10) code = {trim, pack(len)[2:0]};

  return tuple2( code, trim_len+3 );

endfunction

// ============
interface BitCoder;
  method Action put(Bit#(8) din);
  
  // step1，將 get() 方法改為動作值方法
  //method Bit#(8) get;
  method ActionValue#(Bit#(8)) get;   // get() 會改變狀態(dout_valid)，因此改用動作值方法
  
  // 不在需要此接口，由外部觸發獲取輸出碼的時機，
  // 不再透過 get_valid 確認編碼器的狀態
  // method Bool     get_valid;
endinterface

// ============
(* synthesize *)
module mkBitCoder (BitCoder);

  Reg#(Tuple2#(Bit#(10), UInt#(4))) in_code_and_len <- mkDReg( tuple2(0,0) );

  Reg#(Bit#(31)) drem <- mkReg(0);
  Reg#(UInt#(6)) drem_len <- mkReg(0);
  
  //Reg#(Bit#(16)) dout <- mkReg(0);
  Reg#(Bit#(8)) dout <- mkReg(0);

  // step2，新增 din_valid 變數，
  // 用來標記 第1級模塊輸出的 in_code_and_len 是否有效(是否產生新的未被讀取的生成碼)
  Reg#(Bool) din_valid  <- mkReg(False);
  
  // 用於標記第1級模塊的遺留碼是否已達可輸出的長度
  Reg#(Bool) dout_valid <- mkDReg(False);

  // ============

  // step7，進行調度衝突分析，通知編譯器兩者可同時激活不衝突
  //    put接口，會在 din_valid=True 時被激活
  //    Rule: get_drem_and_dout，會在 dout_valid=False 時被激活
  //    兩者有可能會同時激活，且都對 din_valid 進行寫入，
  //    但透過 if 語句，兩者不會同時執行，
  //    因此加上 conflict_free 調度屬姓，告知編譯器兩者會同時激活，但不會產生衝突
  (* conflict_free = "put, get_drem_and_dout" *)
    
  // step4，為輸出的規則建立執行條件
  //rule get_drem_and_dout;
  rule get_drem_and_dout (!dout_valid);   // 添加執行條件，遺留碼累積到一定長度才執行rule
    
    Bit#(31) data = drem;      // 取得舊的遺留碼，尚未更新
    UInt#(6) len = drem_len;   // 取得舊的遺留碼長度，尚未更新

    match {.code, .code_len} = in_code_and_len;

    // step5，為獲取生成碼建立執行條件
    // 只有遺留碼長度不會造成數據溢為，且生成碼未被讀取過才更新輸出碼和遺留碼長度
    if(extend(code_len) + drem_len < 32 && din_valid) begin
      
      // 讀取生成碼
      data = (extend(code) << drem_len) | data;    // 更新輸出碼
      len = extend(code_len) + len;                // 更新遺留碼長度
      
      // 生成碼已經讀取，重新將 din_valid 設置為 Fasle ，
      // din_valid = False，表示生成碼已無效，需要產生新的生成碼
      din_valid <= False;  
    end

    if(len >= 8) begin
      // 遺留碼長度以達到可輸出長度，將 dout_valid 設置為 True
      dout_valid <= True;
      dout <= truncate(data);
      data = data >> 8;
      len = len - 8;
    end

    drem <= data;
    drem_len <= len;
  endrule

  // ============

  // step3，修改 put接口的執行條件: 生成碼被讀取失效後，就使 put接口重新生效
  //method Action put(Bit#(8) din) if(drem_len <= 19);
  method Action put(Bit#(8) din) if(!din_valid);    // 改由 din_valid 判斷是否需要重新產生生成碼
    
    // 若 put() 可被執行，表示即將獲取新生成碼
    // 獲取新生成碼前，先將 din_valid 設置為 True
    din_valid <= True;
    
    // 獲取新生成碼
    in_code_and_len <= getCode(din);

  endmethod

  // 不在需要此接口，由外部觸發獲取輸出碼的時機，
  // 不再透過 get_valid 確認編碼器的狀態
  // method Bool get_valid = dout_valid;

  // step6，修改獲取輸出碼的 get接口的內容
  //method Bit#(16) get if(dout_valid) = dout;
   method ActionValue#(Bit#(8)) get if(dout_valid);   // 將 get() 改為動作值方法，因為會改變內部狀態，同時添加隱式執行條件
      dout_valid <= False;                            // 若 get() 可以執行，表示輸出碼即將輸出，
                                                      // 在輸出碼輸出前，重新將輸出碼可輸出狀態設置為 False

      return dout;
   endmethod

endmodule

// ============

module top();
   
    let dut <- mkBitCoder;

    Reg#(int) cnt <- mkReg(0);

    rule up_counter;
      cnt <= cnt + 1;
    endrule

    Reg#(Bit#(10)) din <- mkReg(0);

    rule dut_put;
      din <= din + 1;
      
      if(din < 'h200)
          dut.put( truncate(din) );
      else if(din == '1)
          $finish;
    endrule

    rule dut_get (cnt%3 == 0);  // 只在三個倍數周期才取用

      let dout <- dut.get;
      $display("cnt=%4d   %b", cnt, dout);

    endrule
endmodule

endpackage
