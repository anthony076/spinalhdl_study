
## icesuguar-nano-board(iCE40LP1k) 相關資訊

- SPEC
  
  <img src="icesuguar/icesugar-nano-spec.png" width=320 height=auto>

- Pinout

  <img src="icesuguar/icesugar-nano-pinout.png" width=400 height=auto>

## Examples
- https://github.com/wuxx/icesugar-nano
- https://github.com/damdoy/ice40_ultraplus_examples
- https://github.com/icebreaker-fpga/icebreaker-verilog-examples
- [ice40_ultraplus_examples](https://github.com/damdoy/ice40_ultraplus_examples)
- [iCEBreaker examples](https://github.com/icebreaker-fpga/icebreaker-verilog-examples)
- [A RISC-V CPU on an FPGA - iCE Sugar Nano](https://www.youtube.com/watch?v=JkWhsz30BaY)
- [EMTORV32 / FEMTOSOC: a minimalistic RISC-V CPU @ BrunoLevy](https://github.com/BrunoLevy/learn-fpga/blob/master/FemtoRV/README.md)
- [riscv-cpu on icesuguar-nano @ abderraouf-adjal](https://github.com/abderraouf-adjal/learn-fpga/blob/master/FemtoRV/BOARDS/icesugar.mk)
