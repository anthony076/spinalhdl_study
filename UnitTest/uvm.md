## uvm 環境架設
- uvm 下載 + 參考使手冊
  - [下載頁面](https://www.accellera.org/downloads/standards/uvm)
  - [uvm 1.2 reference](https://www.accellera.org/images/downloads/standards/uvm/UVM_Class_Reference_Manual_1.2.pdf)
  - [uvm 1.2 庫下載](https://www.accellera.org/images/downloads/standards/uvm/uvm-1.2.tar.gz)
  - [uvm 1.2 使用者手冊](http://www.accellera.org/images//downloads/standards/uvm/uvm_users_guide_1.2.pdf)

- 編譯器
  - 注意，uvm 是基於 systemverilog 的測試框架，需要 systemverilog 的編譯器，

    可以編譯 systemverilog 的編譯器都可以編譯 uvm，大部分商用IDE都有支援

  - Sysnopsys VCS / Synopsys Verdi
  - Mentor Graphics ModelSim / Mentor Graphics Questasim
  - Cadence NC-Verilog
  - IcarusVerilog(iverilog)

- 編譯器的選擇

## 基礎
- 概念
  - driver 用於產生測試訊號，並將測試訊號送給 reference-model 和 DUT
  - monitor 取出 DUT 的結果
  - scoreboard 把結果和 reference-model 進行比較

# Ref
- 環境架設
  - [使用 UVM + VCS + vscode 開發](https://blog.csdn.net/qq_38113006/article/details/120924689)
  - [從零開始搭建 UVM + VCS 驗證平台](https://ppfocus.com/0/tea7a4cf9.html)
  - [UVM + ModelSim](https://blog.csdn.net/qq_41034231/article/details/107961983)
  - [UVM + Questasim](https://blog.csdn.net/qq_40893012/article/details/112258741)
  - [UVM + Verdi](https://blog.csdn.net/qq_38113006/article/details/120921003)

- debug 技巧
  - [VCS+VERDI+reverse](https://zhuanlan.zhihu.com/p/401940992)