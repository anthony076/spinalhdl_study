- [書籍資料](學習資料\FPGA\UVM)

- 推薦書籍
  - [芯片驗證漫遊指南](https://item.jd.com/10005697.html?cu=true&utm_source=kong&utm_medium=tuiguang&utm_campaign=t_1001542270_1002137813_0_1972251263&utm_term=bcb25d548c204afb91694b9938f85d0e)
  
  - systemverilog驗證 測試平台編寫指南 (SystemVerilog的語法書)

- 教學網站
	- 推薦，https://www.chipverify.com/uvm/uvm-tutorial 
	- 需要公司帳號，https://verificationacademy.com/courses/uvm-basics
  - UVM Tutorial for Candy Lovers
    - UVM糖果爱好者教程，https://blog.csdn.net/zhajio/category_9272893.html
    - 官方網站，http://cluelogic.com/
    - git，https://github.com/cluelogic/uvm-tutorial-for-candy-lovers

- 影片教學
  - [UVM實戰 - SPI模塊驗證 @ bili](https://www.bilibili.com/video/BV1ev41167fr)

  - [IC驗證- 手把手教你搭建UVM芯片驗證環境(含代碼)](https://www.bilibili.com/video/BV1yq4y177f6)

- 相關文章
  - https://blog.csdn.net/zhajio/category_9272893.html
  - https://verificationguide.com/
  - [知呼 - 德尔塔-摸鱼范式](https://www.zhihu.com/people/icparadigm/posts)
  
- 其他
  - 學習路徑推薦，Systemverilog for verification > UVM Primer > UVM实战 > mentor学院的《uvm-cookbook》
  - [OVM vs UVM](https://www.zhihu.com/question/33188128)
	- https://verificationguide.com/uvm/uvm-tutorial/
	- http://www.testbench.in/index.html
	- https://www.doulos.com/knowhow/systemverilog/uvm/easier-uvm/
  - [驗證資料匯聚](https://www.jianshu.com/p/eb9a50e619c7)
  - [UVM-Examples](https://github.com/mayurkubavat/UVM-Examples)
	
- 工具
  - [線上 UVM](https://www.edaplayground.com/)

  - [繪製時序圖，WaveDrom](https://zhiguoxin.blog.csdn.net/article/details/122616679?spm=1001.2101.3001.6650.9&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-9-122616679-blog-80536615.pc_relevant_layerdownloadsortv1&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-9-122616679-blog-80536615.pc_relevant_layerdownloadsortv1&utm_relevant_index=12)

	- [TimeGen](https://blog.csdn.net/wandou0511/article/details/126084718?spm=1001.2101.3001.6650.11&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-11-126084718-blog-80536615.pc_relevant_layerdownloadsortv1&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-11-126084718-blog-80536615.pc_relevant_layerdownloadsortv1&utm_relevant_index=14)
