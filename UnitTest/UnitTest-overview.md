## 測試框架的選擇
- [uvm]
  - 適用於 VHDL、Verilog、SystemVerilog
  - 優，開源免費
  - 優，業界使用多，工作保證
  - 優，組件多而全，對各種測試方式都有對應的解決方案
  - 優，大量的IP庫，對於複雜的組件，有現成的庫可直接進行測試
  - 優，可進行隨機測試
  - 缺，框架拆分的組件多，需要了解UVM框架的架構後，才會容易上手

- [uvm-python](https://github.com/tpoikela/uvm-python)
  - 只有部分移植，僅適用於 SystemVerilog 
  - 模擬器只支援 iverilog 和 verilator 
  - 持續開發中

- [svunit](https://github.com/svunit/svunit): 
  - 適用於 Verilog、SystemVerilog
  - 支援模擬器: Questa、VCS、Aldec Riviera-PRO

- [cocotb](https://github.com/cocotb/cocotb): 
  - 適用於 VHDL、SystemVerilog
  - 開源的python庫，和 UVM 設計理念相同
  - 支援模擬器: icarusVerilog、Aldec Riviera-PRO、Aldec Active-HDL、VCS、Questa/ModelSim、

- [vunit](https://vunit.github.io/): 
  - 適用於 VHDL、SystemVerilog、Verilog、
  - 開源的python庫
  - 支援模擬器: Aldec Riviera-PRO、Aldec Active-HDL、GHDL、ModelSim

- [OSVVM](https://osvvm.org/)
  - VHDL 的驗證框架
  - 支援模擬器: Aldec、Mentor、GHDL

## ref
- [work vunit with uvm](https://insights.sigasi.com/tech/vunit-uvm/)