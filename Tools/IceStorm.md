## 建立 SpinalHDL + iceStrom-toolchain 開發環境
- 建立 SpinalHDL + iceStrom-toolchain 的開發環境有以下幾種選擇
  - 開發環境1，docker + oss-on-windows
  - 開發環境2，在 qemu 上安裝 Linux，並在 Linux 中安裝 iceStorm 的套件
  - 開發環境3，使用 WSL2
  - 開發環境4，在 Linux 下安裝 iceStorm 的套件

- 開發流程
  - 參考，[hello-spinalhdl](../examples/hello-spinalhdl/Readme.md)

    官方範例，[SpinalTemplateSbt](https://github.com/SpinalHDL/SpinalTemplateSbt)
    
  - step1，使用 `docker` 進入開發環境  
  - step2，編寫 `spinalhdl`，
  - step3，透過 `sbt run` 產出 verilog / vhdl / systemlog 的代碼
  - step4，使用 `yosys` 產生邏輯圖，或產生合成硬體電路
  - step5，使用 `spinalhdl+icarus` 進行模擬測試+輸出波形圖
  - step6，使用 `gtkwave` 檢視波形圖

- `開發環境1`，docker + oss-on-windows，可用
  - 缺點，
    
    oss-on-windows 是一個工具集合包，除了 iceStorm 的工具集合外，另外還包含許多的工具，
    使用 oss-on-windows 會安裝其他無用的執行檔，占用硬碟空間

    docker 支援透過 --device 串接 usb， 但有以下限制
    - 支援 linux-host + linux-container
    - 支援 windows-host + windows-container
    - 不支援不同平台的 usb 串接

  - 優點
    
    docker 具有使用方便、簡化安裝環境的特性，透過簡單的幾個指令就可建立好開發環境，

    只有前端產生硬體描述語言時才使用 docker，將代碼燒錄到實際的硬體時不使用 docker，
    而是直接在 windows-host 上直接使用 oss 的軟件包，避免 docker 有條件存取 USB 的限制，
    造成無法燒錄FW的問題
    
    oss 的軟件包已經包含大部分需要的工具，使用此方式，
    即使在 windows 的 vscode 中，同樣能擁有語法高亮、Jump-to 的好處

  - `前端`，使用 docker 編寫 SpinaHDL ，`產生硬體描述語言 -> 仿真`

    參考，[Dockerfile-spinalhdl-dev](../docker/Dockerfile-spinalhdl-dev)

  - `後端`，使用 OSS (iceStorm-toolchain-on-windows) ，`合成 -> 佈局佈線 -> 燒錄`
    
    參考，[hello-icesugar-nano](../examples/hello-icesugar-nano)

- `開發環境2`，使用 qemu，不建議用
  - [在qemu上搭建archlinux](archlinux-on-qemu.md)

  - 若需要桌面系統，在 windows 下使用 linux-Desktop 的速度慢，且加速用的 Intel-HAXM 有使用限制
  
- `開發環境3`，使用 WSL2 (ubuntu-20.004) + usbip，尚不可用
  - step0，[安裝和配置 usbip](usbip.md)
  - step1，`usbipd wsl list`，得到 icesugar 位於 bus-id = 1-4 的位置
  - step2，`usbipd wsl attach --busid 1-4 --distribution Ubuntu-20.04`，將usb裝置附加到 wsl 中
  - step3，在 wsl 中確實看到 usb 裝置，
    ```shell
    Bus 001 Device 007: ID 1d50:602b OpenMoko, Inc. FPGALink
    ```
  - step4，在 wsl 中，透過 `./icesprog -r` 進行測試，但無法開啟 usb
    - 推測可能是未啟用 vhci-hcd，造成可列舉但不可使用

- `開發環境4`，在 Linux 下安裝 iceStorm 的套件
  > 僅推薦在純 Linux 系統下使用，不推薦透過虛擬機使用，使用虛擬機會有存取 usb 的問題


## 使用 iceStorm-toolchain 進行燒綠，以 icesuguar-nano 為例
- [icesuguar-nano 板的基礎訊息](../hardware/icesuguar-nano-board(iCE40LP1k).md)

- 燒錄流程
  - 完整代碼，[examples/hello-icesugar-nano](../examples/hello-icesugar-nano)
  
  - step1，啟動 `oss` 環境
  - step2，透過 `yosys` 產生 blif、json 檔
  - step3，透過 `nextpnr-ice40` 產生 asc 檔
  - step4，透過 `icepack` 產生 bin 檔
  - step5，透過 `iCELink` 或 `icesprog` 燒錄 bin 檔

    - USB 連接到PC後，會出現兩個USB裝置，`iCELink(D:)` 和 `DAPLink-CMSIS-DAP`

    - 燒錄方法1，透過 iCELink，直接複製到 硬碟
  
      注意，要透過 icelink 燒綠，必須 iCELink(D:) 裝置是有效的

    - 燒錄方法2，透過 icesprog
      - 注意，要透過 icesprog 燒綠，必須 DAPLink-CMSIS-DAP 裝置是有效的

- 使用 icesprog 對硬體進行設定 / 對spi進行操作 / 讀寫GPIO / 燒錄
  - icesprog 的使用

    <img src="doc/icestorm/icesprog-usage.png" width=600 height=auto>

  - 透過 icesprog 設定 CLK 頻率

    <img src="doc/icestorm/icesprog-set-clk.png" width=350 height=auto>

## Ref
- [Windows上使用iverilog+gtkwave仿真](https://www.cnblogs.com/lazypigwhy/p/10523712.html)
- [GTKWave使用範例](http://programmermagazine.github.io/201311/htm/message2.html)
