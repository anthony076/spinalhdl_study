## Lattice iCEcube2 的使用
- 測試硬體，以 icesugar-nano
- 測試代碼，以 icesugar-nano\src\basic\blink 為例
- 完整代碼，[led-blink-by-icecube2](../examples/led-blink-icecube2/)

- 優缺點，
  - 優，使用簡單，適合驗證或測試硬體描述語言用
  - 缺，沒有語法高亮，對寫代碼不友好，不適合直接使用 iCEcube2 編寫硬體描述語言

- `step1`，下載並安裝 [iCEcube2](https://www.latticesemi.com/iCEcube2#_4351BE10BA504435B5226390CF5D7D4C)

- `step2`，取得 [License](https://www.latticesemi.com/Support/Licensing/DiamondAndiCEcube2SoftwareLicensing/iceCube2)
  
  - 2-1，註冊帳號，
  
    <font color=red>  注意，個人用戶在公司一欄填入 Individual ，可加速審查，不要填入其他值 </font>
    
    <img src="doc/icecube2/speedup-sign-up.png" width=80% height=auto>

  - 2-2，進入申請申請頁面，並填入 MAC Address，審核後，License 檔會透過信件寄發

    <img src="doc/icecube2/get-icecube2-license.png" width=80% height=auto>

  - 2-3 第一次進入 iCEcube2 需要指定 License 檔案的位置

    <img src="doc/icecube2/specific-license-file.png" width=80% height=auto>

- `step3`，建立專案
  - 3-1，點擊 File > New Project 的圖示 (或 Ctrl+N)

  - 3-2，設置專案屬性
    
    <img src="doc/icecube2/set-project-property.png" width=40% height=auto>

- `step4`，添加硬體描述語言的檔案 (*.v 或 *.sv 或 *.vhdl)

    <img src="doc/icecube2/add-design-file.png" width=80% height=auto>

- `step5`，添加接腳映射 (*.pcf)

  <img src="doc/icecube2/add-pcf-file.png" width=80% height=auto>

  或透過 Pin-Constraints-Editor 進行編輯

  <img src="doc/icecube2/use-pin-constraints-editor.png" width=80% height=auto>

- `step6`，執行並輸出 bitmap 檔案(*.bin)
  - 方法1，執行 `Tool > Run All`
  - 方法2，依序點擊下列選項
    - Synthesis 的 `Run Synplify Pro Synthesis`
    - Place & Route Flow 的 `Run P&R`
  
  - 若沒有任何錯誤，
    
    左側在 `OutputFiles > Bitmap > *.bin` 會列出檔名
  
    實際的 bin 檔案，會放在`專案名/專案_Implmnt\sbt\outputs\bitmap`的目錄中

  <img src="doc/icecube2/execute-and-output.png" width=30% height=auto>

- option，執行 simulation
  - 參考，[First Project With Lattice FPGA - Part 4: Simulation](https://www.youtube.com/watch?v=b0OaaIPoKE8)

- `step7`，將 bitmap(*.bin) 燒錄到裝置中
  - 以 icesugar-nano-device 為例，可以將 *.bin 檔案拖曳到 iCELink 的磁碟中，iCELink 會自動燒錄

  - 注意，不同的開發版有不同的燒綠方式，其他開發版可能需要調用其他工具進行燒錄

- 常見問題
  - Invalid host. ，主機認證錯誤

    <img src="doc/icecube2/invalid-host-error.png" width=50% height=auto>

    使用 iCEcube2 需要提供 License，且 License 是與 MAC 綁定在一起的，
    若主機使用的網路不同，例如，註冊時提供無線網路的網卡，但開啟 iCEcube2 時連接的是有線網卡，
    
    主機連接不同的網路，造成與當初註冊的 MAC 不同時，就會出現此錯誤

