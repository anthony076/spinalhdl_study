@echo off

del -F -S -Q *.vcd *.vvp

iverilog -o test.vvp tb.v
vvp test.vvp
gtkwave result.vcd
