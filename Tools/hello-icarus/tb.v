module test;

  parameter period = 5'd10;
  reg clk;

  initial begin
    // 設置將波形匯出
    $dumpfile("result.vcd");
    // 匯出接腳設置
    $dumpvars;   // 無參數設計中的所有訊號都會被記錄
    
    clk = 0;
    repeat(6) #(period/2) clk = ~clk;
  end

endmodule