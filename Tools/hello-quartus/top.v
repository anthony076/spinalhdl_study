module top(output o, input x, input y);

  wire _or, _and;

  assign _or = x | y;
  assign _and = y & _or;
  assign o = _and;

endmodule