
## 編譯+仿真工具 IcarusVerilog 的使用
- Why IcarusVerilog
  - 輕量、免費、開源、跨平台的Verilog編譯器
  - 用於快速檢查 Verilog 文件的語法是否有錯誤，然後進行一些基本的時序仿真

- 下載與安裝
  - [IcarusVerilog](http://iverilog.icarus.com/)
  - [IcarusVerilog-for-windows](https://bleyer.org/icarus/)
  
  - 注意，若安裝 IcarusVerilog-for-windows，就`不需要再手動安裝gtkwave`，
    IcarusVerilog-for-windows 已經內建 gtkwave
  
  - [gtkwave](http://gtkwave.sourceforge.net/)
  - [gtkwave for windows](http://www.dspia.com/gtkwave.html)
  
- 使用流程
  - step1，在源碼的 initial 塊中，添加將仿真結果輸出成 VCD 檔的語句
  - step2，透過 iverilog.exe 將輸入的源碼(*.v) 編譯成可執行的 VVP 檔
  - step3，透過 VVP.exe 執行 step1 輸出的VVP檔，並輸出 VCD 檔
  - step4，以 gtkwave 執行 VCD 檔

## 範例，使用 iverilog + gtkwave 進行仿真語句測試和檢視波形
- [完整代碼](hello-icarus/)

- tb.v ，主要設計檔 或 testbench 檔
  ```verilog
  module top;

    parameter period = 5'd10;
    reg clk;

    initial begin
      // 設置將波形匯出
      $dumpfile("result.vcd");
      // 匯出接腳設置
      $dumpvars;   // 無參數設計中的所有訊號都會被記錄
      
      clk = 0;
      repeat(6) #(period/2) clk = ~clk;
    end

  endmodule
  ```

- 編譯命令
  ```batch
  iverilog -o test.vvp tb.v
  vvp test.vvp
  gtkwave result.vcd
  ```

## 範例，簡易範例
```batch
@REM step1，對源碼編譯，並產生 vvp 檔
iverilog.exe -o 123.vvp addtest.v add.v
::           ^^^^^^^^^^                   輸出 vvp 檔
::                      ^^^^^^^^^         testbench 檔，用於仿真測試
::                                ^^^^^   源碼，用於讀取定義

@REM step2，讀取 vvp 檔，並根據 testbench 的內容，產生 vcd 檔
:: -m 執行 module 的內容
vvp.exe -m addtest.vvp

@REM step3，透過 gtkwave 顯示 vcd 檔案的內容
gtkwave result.vcd
```

## Ref
- [iverilog 命令參數](https://aijishu.com/a/1060000000124967)
- [Icarus Verilog的工作原理](https://ppfocus.com/0/eda868b89.html)