## USBIP
- 將 windows-host 的 usb 裝置，透過 server/client 的架構分享給 Linux-vm
- 擁有 usb 端的PC為 server 端，存取 usb端的PC為 client 端

## 安裝 USBIP
- server
  - windows，https://github.com/dorssel/usbipd-win/releases

- WSL-ubuntu 20.04，[參考](#測試環境windows-host--wsl2)

## 測試環境，windows-host + WSL2 (ubuntu-20.004)
- 限制
  - 僅支援 Windows 11（內部版本 22000 或更高版本）
  - 僅支援 x86 / x64 的機器
  - 僅支援 wsl2
  - 僅支援 Linux 內核 5.10.60.1 或更高版本
    > 透過 unama -a 查看

- 注意，測試時，`將 docker 關閉`，docker 會啟動 WSL2 導致更新 wsl2 指令失敗

- 配置流程
  - step0，安裝 usbipd，連結見 [安裝usbip](#安裝-usbip)
  - step1，安裝 ubuntu-20.04 後，執行 `apt update`
  - step2，`wsl --update` 和 `wsl --shutdown`
  - step3，重新啟動 ubuntu
  - step4，`sudo apt install linux-tools-5.4.0-77-generic hwdata`
  - step5，`sudo update-alternatives --install /usr/local/bin/usbip usbip /usr/lib/linux-tools/5.4.0-77-generic/usbip 20`
  - step6，在 windows 上列出可用的 usb，`usbipd wsl list`
  - step7，在 windows 上，將指定的 usb 綁定給指定的 vm
    - 查詢可用的 distro，`wsl --list`
    - 綁定給指定的 distro，`usbipd wsl attach --busid 1-6 --distribution Ubuntu-20.04`
  - step8，回到 wsl2，透過 `lsusb` 查看是否附加成功

- 注意，上述步驟未執行 `sudo modprobe vhci-hcd` 的命令，
  - 此命令用於手動載入內核的 USB Virtual Host Controller Interface (vhci) 模組
    的 Host-Controller-Driver (hcd)

  - 此命令在 wsl2 中會出現
    ```
    modprobe: FATAL: Module vhci-hcd not found in directory /lib/modules/5.10.102.1-microsoft-standard-WSL2
    ```
    基礎的 WSL2 kernel 不允續載入內核模組，
	
	- [需要自己手動編譯內核](https://unix.stackexchange.com/questions/594470/wsl-2-does-not-have-lib-modules)
	
	- [usbip Error in Windows 10 (WSL 2)](https://github.com/cezanne/usbip-win/issues/149)

## 測試環境，windows-host + docker(archlinux)
- 若 docker 使用 WSL2 為後臺，可以使用此方式??
  
- 使用條件
  https://github-wiki-see.page/m/ARMmbed/connectedhomeip/wiki/Accessing-host-devices-from-inside-a-Docker-container

- pacman -S usbip usbutils
- pacman -S base (for lsusb)

## 測試環境，windows-host 
## Ref
- [在wsl中存取usb](https://docs.microsoft.com/en-us/windows/wsl/connect-usb)
- [usbipd-win](https://github.com/dorssel/usbipd-win/wiki/WSL-support)
- [USB/IP with WSL](https://askubuntu.com/questions/1384456/usbipd-not-found-for-kernel-when-using-usb-ip-with-wsl)
- [取代update-alternatives的另一種方式](https://www.xda-developers.com/wsl-connect-usb-devices-windows-11/)
- http://usbip.sourceforge.net/
- https://devblogs.microsoft.com/commandline/connecting-usb-devices-to-wsl/
