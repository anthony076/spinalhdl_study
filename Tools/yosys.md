## yosys 介紹
- yosys，用於 RTL 綜合工具的框架
- yosys-abc，用於合成和驗證同步硬件設計中出現的二進制時序邏輯電路
- windows下，推薦 [oss安裝](https://github.com/YosysHQ/oss-cad-suite-build)

## yosys 的使用
- 三種操作模式
  - 方法1，透過命令列參數

  - 方法2，透過互動模式
    - 支援命令自動補全，按下 tab 可看到可用的命令提示
     
  - 方法3，透過 yosys-script
    - step1，建立 *.ys 檔
    - step2，透過 yosys 檔名.ys 執行

## 常用命令
- 讀取檔案 
  - 讀取 verilog 檔，`read_verilog 檔名.v`，不可指定版本
    
  - 透過 `read` 讀取 verilog 檔，`read -vlog2k 檔名.v `
    > 可指定systemverilog 的版本，-vlog95|-vlog2k

  - 透過 `read` 讀取 systemverilog 檔，`read -sv 檔名.sv`，
    > 可指定systemverilog 的版本，-sv2005|-sv2009|-sv2012|-sv
    
  - 透過 `read` 讀取 vhdl 檔，`read -sv 檔名.vhd`，
  > 可指定systemverilog 的版本，-vhdl87|-vhdl93|-vhdl2k|-vhdl2008|-vhdl

- 層級檢查，`hierarchy -check`

- 執行優化，`opt`

- 映射
  - 將高階的 process 元件，映射為FF和MUX元件，並優化，`proc; opt;`
  - 將高階的 memory 元件，映射為FF和MUX元件，並優化，`memory; opt;`
  - 將高階的 finite-state-machine 元件，映射為FF和MUX元件，並優化，`fsm; opt;`
  - 將 netlist 映射為 gate-level 的 netlist，並優化，`techmap; opt;`

- 映射並添加到指定庫，
  - 利用`預設`的方法，將內部的FF映射到指定的library中，`dfflibmap -liberty 目標庫名`
  - 利用 `Berkeley ABC` 的方法，將內部的FF映射到指定的library中，`abc -liberty 目標庫名`

- 執行合成，
  - 手動指定合成的模組，`synth -top 模組名`
  - ice40-chip 的合成，`synth_ice40`

- 匯出功能方塊內容
  - `dump 模組名`，匯出到 console
  - `dump -o 檔案名 模組名`，匯出到指定檔案
  - `dump -m -o 檔案名 模組名`，匯出到指定檔案，並包含模組名

- 產生圖檔，
  - 推薦安裝 graphviz，
    - windows，https://graphviz.org/download/
    - vscode，https://marketplace.visualstudio.com/items?itemName=joaompinto.vscode-graphviz

  - `show`，預設寫入 show.dot，dot 可使用 graphviz 開啟
  
  - `show -format svg`，寫入 show.svg，
    
    注意，需要 graphviz 的 dot 命令有效才能正確轉換

- 寫入檔案
  - 將 netlist 寫入 json，`write_json 檔名.json`
  - 將 verilog優化的結果寫入檔案，`write_verilog 檔名.v`，注意，要手動執行優化再寫入
  - 寫入供 arachne-pnr/next-pnr 使用的 blif 檔，`write_blif 檔名.blif`
  
## 範例，透過互動模式執行 yosys
- 以 hello-yosys 為例，完整源碼位於 examples\hello-yosys
  
- hello.v
  ```verilog
  module top (
    input BTN1,
    input BTN2,
    output LED1,
  );

    assign LED1 = BTN1 & BTN2;

  endmodule
  ```

- 指令，`read_verilog hello.v`
- 指令，`show`，經過簡易優化處理
  
  <img src="doc/hello-1-read-verilog.png" width=500 height=auto>

- 指令，`opt`
- 指令，`show`，經過簡易優化處理後
  
  <img src="doc/hello-2-opt.png" width=500 height=auto>

- 指令，`synth_ice40`
- 指令，`show`，合成後，將上述邏輯以一個 lookup-table 取代
  
  <img src="doc/hello-3-synth-ice40.png" width=500 height=auto>

- 檢視真值表
  - 注意，必須先進入模組，才能指定要檢視的功能方塊，透過 `cd ..` 回到上一層的設計

  - `cd top`，進入 top 模組

    提示會變成，`yosys [top] >`，表示當前位置位於 top 的模組中

  - `dump LED1_SB_LUT4_O`，匯出 `LED1_SB_LUT4_O` 功能方塊的內容，包含真值表，得到

    ```verilog
    attribute \module_not_derived 1
    attribute \src "C:\\OSS-CA~1\\bin\\../share/yosys/ice40/cells_map.v:17.34-18.52"
    cell \SB_LUT4 \LED1_SB_LUT4_O
      // 得到真值表的書出結果為，16位的結果為 1111000000000000
      parameter \LUT_INIT 16'1111000000000000

      // 4個輸入，I0、I1、I2、I3，
      // 注意，I0、I1 已經寫死為 0，I0=I1=0，
      connect \I0 1'0
      connect \I1 1'0
      connect \I2 \BTN1
      connect \I3 \BTN2

      // 1個輸出結果
      connect \O \LED1
    end
    ```
  - 將16位的1111000000000000，轉換為真值表
    - 16位表示輸入 0-15 的結果
    - 因為 I1、I0 已寫死為 0，因此，I0 或 I1 出現 1 的狀況不會使用到，可忽略不計

      | value | I3  | I2  | I1  | I0  | O (LED) | 備註                      |
      | ----- | --- | --- | --- | --- | ------- | ------------------------- |
      | 0     | 0   | 0   | 0   | 0   | 0       | 使用，I1=0、I0=0          |
      | 1     | 0   | 0   | 0   | 1   | 0       | 未使用，I1、I0 已強制為 0 |
      | 2     | 0   | 0   | 1   | 0   | 0       | 未使用，I1、I0 已強制為 0 |
      | 3     | 0   | 0   | 1   | 1   | 0       | 未使用，I1、I0 已強制為 0 |
      | 4     | 0   | 1   | 0   | 0   | 0       | 使用，I1=0、I0=0          |
      | 5     | 0   | 1   | 0   | 1   | 0       | 未使用，I1、I0 已強制為 0 |
      | 6     | 0   | 1   | 1   | 0   | 0       | 未使用，I1、I0 已強制為 0 |
      | 7     | 0   | 1   | 1   | 1   | 0       | 未使用，I1、I0 已強制為 0 |
      | 8     | 1   | 0   | 0   | 0   | 0       | 使用，I1=0、I0=0          |
      | 9     | 1   | 0   | 0   | 1   | 0       | 未使用，I1、I0 已強制為 0 |
      | A     | 1   | 0   | 1   | 0   | 0       | 未使用，I1、I0 已強制為 0 |
      | B     | 1   | 0   | 1   | 1   | 0       | 未使用，I1、I0 已強制為 0 |
      | C     | 1   | 1   | 0   | 0   | 0       | 使用，I1=0、I0=0          |
      | D     | 1   | 1   | 0   | 1   | 1       | 未使用，I1、I0 已強制為 0 |
      | E     | 1   | 1   | 1   | 0   | 1       | 未使用，I1、I0 已強制為 0 |
      | F     | 1   | 1   | 1   | 1   | 1       | 未使用，I1、I0 已強制為 0 |

## 範例，yosys-script 使用範例
- step1，編寫 ys 檔，以 gendot.ys 為例
  ```bat
  // getPNG.ys
  read_read_verilog hello.v
  opt
  synth_ice40
  show
  ```
- step2，執行 `yosys gendot.ys` 
  

## 範例，透過 透過命令列參數 執行 yosys
- -p，執行命令
  
- 以 view.bat 為例
    
  ```shell
  :: view.bat
  yosys ^
    -p "read_read_verilog hello.v" ^
    -p "opt" ^
    -p "synth_ice40" ^
    -p "show"
  ```

## Ref
- [Open Source FPGA tool flow part 1: yosys](https://www.youtube.com/watch?v=A5AHglpfdtQ)
- [Command Reference](http://bygone.clairexen.net/yosys/documentation.html#command-reference)
- [Verilog開源的綜合工具-Yosys](https://www.kancloud.cn/dlover/fpga/1797858)