## ModelSim 的使用，測試手寫的 testbech
- step1，建立 Library，`File > New > Library`
- step2，添加設計檔，`選中新建的 Library > File > New > Source > Verilog`
- step3，添加代碼，並另存為 *.v 檔
  ```verilog
  module test;
    parameter clock = 10;
    reg clk_i;

    initial begin
      clk_i=0;
      repeat(6) #(clock/2) clk_i=~clk_i;
    end
  endmodule
  ```
- step4，編譯 *.v 檔，
  - Compile > Compile > 選擇要編譯的檔案
  - 編譯成功後，會在 當前 Library 底下出現當前的 module
- step5，選中 module > 右鍵 > simulate > 跳出 objects 的視窗
- step6，在 objects 的視窗，選中要模擬接腳 > 右鍵 > Add to Wave
- step7，在 console 中輸入 run，或點擊 simulate 的按鈕，或點擊 F9，以察看結果

## ModelSim 的使用，以 quartusII 產生的 simulation folder 為例
- 以 quartusII 產生的 simulation folder 為例

  <img src="doc/modelsim/quartus-generated-simulation-folder.png" width=70% height=auto>

- 使用流程
  - step1，建立 Library

    <img src="doc/modelsim/flow-1-create-library.png" width=40% height=auto>

  - step2，為 Library 指定目錄

    <img src="doc/modelsim/flow-2-point-to-simulation-folder.png" width=80% height=auto>

  - step3，選擇 Library > *.v 檔

    <img src="doc/modelsim/flow-3-select-hdl-file.png" width=80% height=auto>

  - step4，選擇要檢視接腳

    <img src="doc/modelsim/flow-4-select-view-pins.png" width=80% height=auto>

  - step5，輸入 Transcript 指令
    - 對輸入接腳產生波形
      - force -deposit /x 2#0, 2#1 20, 2#0 60, 2#1 80
      - force -deposit /y 2#0, 2#1 40, 2#0 80, 2#1 100
  
    - 輸入 run

    <img src="doc/modelsim/flow-5-select-input-cmd-and-run.png" width=80% height=auto>
  
## Transcript 指令的使用
- 範例，對輸入接腳產生波形
  ```shell
  Sim > force -deposit /x 2#0, 2#1 20, 2#0 60, 2#1 80
        ^^^^^  強制產生
              ^^^^^^^^ 產生波形
                       ^^ 要作用的接腳
                         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 波形描述
  ```

- 波形描述格式: `進制#值 結束時間`
  - 例如，`2#0`，從時間0s開始，顯示 0b0
  - 例如，`2#1 20`，到 20ps 之前，顯示 0b1
  - 例如，`2#0 60`，到 60ps 之前，顯示 0b0
  - 例如，`2#1 80`，到 80ps 之前，顯示 0b1
