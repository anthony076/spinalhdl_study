
## QuartusII 的使用

- [建立新專案](#功能-建立新專案)
- [使用 ModelSim 模擬波形](#功能-使用-modelsim-模擬波形)
- [使用內建的 University Program VWF 模擬波形](#功能-使用內建的-university-program-vwf-模擬波形)
- [繪製原理圖 (Symbol-File)](#功能-繪製原理圖-symbol-file)
- [使用 signaltap 對 device 進行除錯](#功能-使用-signaltap-對-device-進行除錯)
- [Qsys 系統訂製的使用](#功能-qsys-系統訂製的使用)

## [功能] 建立新專案
- [完整範例，hello-quartus](hello-quartus/)

- step1，透過 New Project Wizard 建立新專案，並指定 entry 名
  
  注意，entry 是入口檔案名，同時也是模組名

  <img src="doc/quartus/project-1-specify-design-entry.png" width=80% height=auto>

- step2，(選用) 手動添加檔案
  
  <img src="doc/quartus/project-2-manual-add-files.png" width=80% height=auto>

- step3，選擇晶片

  <img src="doc/quartus/project-3-select-device.png" width=80% height=auto>

- step4，(選用) 手動設置 EDA tools

  <img src="doc/quartus/project-4-set-tools.png" width=80% height=auto>

- step5，建立主要設計檔 (main-design-entry)

  <img src="doc/quartus/project-5-create-entry-file.png" width=80% height=auto>

- step6，編譯並執行

  <img src="doc/quartus/project-6-compile-and-run.png" width=80% height=auto>

- step7，以 RTL-View 檢視邏輯圖

  <img src="doc/quartus/project-7-rtl-view.png" width=80% height=auto>

- step8，(選用) [使用 ModelSim 模擬波形](#功能-使用-modelsim-模擬波形)

- step8，(選用) [使用內建的 University Program VWF 模擬波形](#功能-使用內建的-university-program-vwf-模擬波形)
  
## [功能] 使用 ModelSim 模擬波形
- step1，執行 modelsim

  注意，若沒有指定 testbench，就需要再 modelsim 中，以 Transcript 手動指定輸入接腳的波形，
  
  詳見，[modelsim的使用](modelsim.md)

  注意，若無法開啟 modelsim ，需要在 Tools > Options > General > EDA Tool Options 中，
  確認 modelsim 的路徑是否正確，且 <font> Use NativeLink with a Synplify/Synplify Pro node-locked license </font> 必須勾選

  <img src="doc/quartus/project-8-run-modelsim.png" width=80% height=auto>

- step2，選擇 Library: rtl_work > top.v

  <img src="doc/modelsim/flow-3-select-hdl-file.png" width=80% height=auto>

- step3，添加要檢視的接腳 > 選擇 Add Wave

  <img src="doc/modelsim/flow-4-select-view-pins.png" width=80% height=auto>

- step4，輸入指令並執行

  <img src="doc/modelsim/flow-5-select-input-cmd-and-run.png" width=80% height=auto>

## [功能] 使用內建的 University Program VWF 模擬波形
- step1，建立 University-Program-VWF
  
  <img src="doc/quartus/vwfsim-1-create-vwf.png" width=80% height=auto>

- step2，添加接腳
  
  <img src="doc/quartus/vwfsim-2-add-pin.png" width=80% height=auto>

- step3，為輸入接腳編輯波形
  
  <img src="doc/quartus/vwfsim-3-modify-input-waveform.png" width=80% height=auto>

- step4，保存為 vmf 檔
  
  <img src="doc/quartus/vwfsim-4-save-to-vwf.png" width=80% height=auto>

- step5，執行 simulation
  
  <img src="doc/quartus/vwfsim-5-execute-simulation.png" width=80% height=auto>

## [功能] 繪製原理圖 (Symbol-File)
- 使用場景
  
  在編寫代碼前，繪製 Schematic 提供參考，不需要執行就可以知道當前專案的目標
  
  注意，無法從 Schematic 產生 hdl 語言，但是<font color=red>標準版的 Quartus 可以將 *.v 轉換為 Schematic (*.bdf) </font>

- step1，建立 BlockDiagram/Schematic File

  <img src="doc/quartus/sch-1-create-new-sch.png" width=40% height=auto>

- step2，選擇元件

  <img src="doc/quartus/sch-2-add-symbol.png" width=100% height=auto>

- step3，選擇輸出輸入接腳

  <img src="doc/quartus/sch-3-add-pin.png" width=80% height=auto>

- step4，儲存成 bdf 檔

  <img src="doc/quartus/sch-4-save-to-bdf.png" width=80% height=auto>

## [功能] 使用 Signaltap 對 device 進行除錯
- [徹底掌握Quartus——Signaltap篇](https://blog.csdn.net/k331922164/article/details/47623501)

## [功能] Qsys 系統訂製的使用
- [qsys: 基於 GUI 的片上系統訂製框架，有大量CPU、IP核、外設可供使用](https://www.moore8.com/download/lecture_extra/1197?file=%E8%BD%AF%E6%A0%B8%E6%BC%94%E7%BB%83_01_%E4%BB%80%E4%B9%88%E6%98%AFqsys%EF%BC%9F.pdf)

- [基於Qsys的第一個 Nios II系統設計](https://www.796t.com/content/1548351910.html)

- [一個簡易的Qsys系統的開發流程](https://blog.csdn.net/SHYHOOD/article/details/115671998?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-2-115671998-blog-43986553.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-2-115671998-blog-43986553.pc_relevant_default&utm_relevant_index=3)

- [採用Qsys組件編輯器建立定制組件](https://www.youtube.com/watch?v=yu-advthIdY)

- [透過 Qsys 訂製系統 + hello-world 驗證功能](https://www.youtube.com/watch?v=vrnsSrbiX_Q)
  
## [範例] 使用 Schematic 進行編譯和模擬 (免hdl源碼)
https://youtu.be/azWXmfxa-bI?t=773

## [範例] 在 vwf 中模擬 10 進制的加法器
https://youtu.be/azWXmfxa-bI?t=773

輸入端A、輸入端B、輸出Count、輸出S，都是 4bit 的端口

<img src="doc/quartus/sim-adder-1.png" width=80% height=auto>

設置為 10進制

<img src="doc/quartus/sim-adder-2.png" width=50% height=auto>

模擬結果

<img src="doc/quartus/sim-adder-3.png" width=50% height=auto>



## 常用技巧
- 關閉編譯後，自動跳至 compilation-report
  > 取消 Tools > Options > General > Processing > Automatically open the Report window before starting a processing task 的選項

- (需要 License) 開啟編譯後自動開啟 RTL-Viewer
  > 勾選 Tools > Options > Compilation Process Settings > Run Netlist Viewers preprocess during compilation 的選項

## quartus + vscode
- 在 vscode 調用 quartus 編譯器 + RTL viewer
  - 完整範例，見 [hello-quartus](hello-quartus/)

  - build.exe
    ```batch
    @echo off
    set quartus_map="C:\<安裝路徑>\bin\quartus_map.exe"
    set qnui="C:\<安裝路徑>\bin\qnui.exe"

    @REM 編譯+合成
    ::格式: quartus_map --read_settings_files=on --write_settings_files=off <專案名> -c <入口模塊>
    ::    --read_settings_files 參數:  是否從 qsf 檔案讀取專案屬性
    ::    --write_settings_files 參數:  是否將專案屬性寫回 qsf 檔案，預設為開啟
    :: -c 要編譯的主模塊
    %quartus_map% --read_settings_files=on --write_settings_files=off hello-quartus -c top

    @REM 開啟 RTL-Viewer
    ::格式: quartus_map --read_settings_files=on --write_settings_files=off <專案名> -c <入口模塊>
    ::%qnui% hello-quartus
    ```
  - clean.exe
    ```batch
    @echo off

    del /F /S /Q *.qws *.bak *.rpt *.ddb *.summary
    rmdir /S /Q db incremental_db output_files simulation
    ```

- [推薦 vscode 插件](00_overview.md)
  
## Ref
- [Quartus II輸入原理圖及仿真步驟](https://www.cnblogs.com/mikewolf2002/p/10237681.html)
- [Quartus II & ModelSim](https://hackmd.io/@ETC/CNOC/https%3A%2F%2Fhackmd.io%2F%40ETC%2FrJUgIUX3b)
- [[Verilog入門教學] 本篇#2 電路驗證工具—Quartus II、FPGA](https://www.youtube.com/watch?v=bngDzUAs3KM)
- [quartus II輸入原理圖及仿真步驟](https://www.cnblogs.com/mikewolf2002/p/10237681.html)
  
- quartus + vscode 
  - [quartus2 執行檔的使用 @ 官方文檔](https://www.intel.com/programmable/technical-pdfs/654662.pdf)
  - [quartus2 執行檔使用範例](https://github.com/t-crest/patmos/blob/master/hardware/Makefile)
