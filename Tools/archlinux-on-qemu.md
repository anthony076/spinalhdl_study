## 安裝 qemu
- qemu for Windows，https://qemu.weilnetz.de/w64/

## 在 qemu 中安裝 archlinux
- 下載 archlinux iso

  https://archlinux.org/download/
  
- 建立硬碟檔
  
  `qemu-img.exe create disk.hd 10g`

  - 若要使用動態硬碟，加上 `-f qcow2` 的參數，檔名改成 `*.qcow2` 

- 簡化 qemu-system-x86_64w.exe 的命令
  ```batch
  @REM qemu.bat

  @echo off
  "C:\Program Files\qemu\qemu-system-x86_64w.exe" %*
  ```

- 啟動cdrom安裝，

  `qemu -m 8G -rtc base=localtime -hda disk.hd -cdrom archlinux-2022.04.05-x86_64.iso -boot d`

  - -m，指定記憶體大小
  - -rtc，設定時區
  - -hda，指定硬碟檔
  - -cdrom，指定iso檔
  - -boot d，從cdrom啟動，從 cdromd 讀取開機磁區

- 分割硬碟
  - 將硬碟分割為 
    - root-partition:8G
    - swap-partition:2G

  - 命令，`fdisk /dev/sda`
    - 建立 root-partition
      - 命令，`o`，建立新硬碟標籤 (for disk 2)
      - 命令，`n`，建立新分區
      - 命令，`p`，設置分區類型為primary
      - 命令，`1`，設置分區編號為1
      - 命令，`2048`，設置起始磁頭位置
      - 命令，`+8G`，設置分區大小為8G

    - 建立第二個磁碟分區
      - 命令，`n`，建立新分區
      - 命令，`p`，設置分區類型為primary
      - 命令，`2`，設置分區編號為1
    
    - 將第二個磁區變更類行為 swap
      - 命令，`t`，變更分區類型
      - 命令，`2`，選擇要變更的分區代號
      - 命令，`82`，將分區類型變更為 swap (82)

    - 命令，`w`，寫入變更
  
  - 命令，`mkfs.ext4 /dev/sda1`，將 sda1 格式化為 ext4 格式
  - 命令，`mount /dev/sda1 /mnt`，掛載 sda1
  - 命令，`mkswap /dev/sda2`，格式化 swap 磁區
  - 命令，`swapon /dev/sda2`，掛載 sda2

- 設置時區
  - 命令，`timedatectl set-ntp true`
  - 命令，`timedatectl status`

- 下載系統軟體到掛載的硬碟中
  - 命令，`ip addr`，確認網卡是存在的
  - 命令，`nano /etc/pacman.d/mirrorlist`，修改 mirrorlist 添加台灣的鏡像站
    
    查詢 [mirrorlist](https://archlinux.org/mirrorlist/)

  - 命令，`pacman -Syy`，更新軟體源
  - 命令，`pacstrap /mnt base linux linux-firmware`，透過 pactrap 安裝系統到掛載磁區 (安裝給 root 帳號)
  - 命令，`genfstab -U /mnt > /mnt/etc/fstab`，建立系統文件表

- 進入 chroot 環境並配置系統
  - 命令，`arch-chroot /mnt`
  - 命令，`pacman -Syy`，更新軟體源

- 設置日期和時間
  - 命令，`timedatectl list-timezones | grep Asia`，列出可用時區
  - 命令，`timedatectl set-timezone Asia/Taipei`，設置時區
  - 命令，`timedatectl set-ntp true`
  - 命令，`timedatectl status`
  - 命令，`tzselect`，切換時區
    - 命令，`4`，選擇 Asia
    - 命令，`44`，選擇 Taiwan
  - 命令，`ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime`，將系統時間設置為 Taipei (建立連結)
  - 命令，`date`，查看時間
  
- 設置文字編碼
  - 命令，`nano /etc/locale.gen`，
  
    添加 `en_US.UTF-8 UTF-8`，和 `zh_TW.UTF-8 UTF-8`

  - 命令，`echo LANG=en_US.UTF-8 > /etc/locale.conf`，將 utf-8 的編碼寫入 locale.conf，寫入預設編碼
  - 命令，`locale-gen`，產生文字編碼
  - 命令，`locale`，查看文字編碼
  
- 配置 hostname和 hosts
  - 命令，`echo arch-ching >> /etc/hostname`
  
  - 注意，hosts 可以不設置，由後面的 networkmanager 自動配置
  - 命令，`touch /etc/hosts`，建立 /etc/hosts 檔
  - 命令，`echo "127.0.0.1 localhost" >> /etc/hosts`
  - 命令，`echo "::1 localhost" >> /etc/hosts`
  - 命令，`echo "127.0.0.1 arch-ching" >> /etc/hosts`

- 變更 root 密碼
  
  命令，`passwd root`

- 安裝預設軟件
  - 命令，`pacman -S ntfs-3g iw wpa_supplicant wireless_tools net-tools dialog`，安裝必要套件
  - 命令，`pacman -S networkmanager`，安裝網路管理員，會自動配置網路
  - 命令，`systemctl enable NetworkManager`，開機自動啟動網路管理員


- 安裝 bootlooder
  - 命令，`pacman -S grub`，安裝引導套件(grub)
  - 命令，`grub-install /dev/sda`，安裝 bios 引導
  - 命令，`nano /etc/default/grub`，修改 grub 配置

    將 GRUB_TIMEOUT 修改為 0
    
  - 命令，`grub-mkconfig -o /boot/grub/grub.cfg`，生成引導套件的配置文件

- 關閉系統
  - 命令，`exit`，跳出 chroot 環境
  - 命令，`umount /mnt`，卸載主磁區，選用
  - 命令，`swapoff /dev/sda2`，卸載swap磁區，選用
  - 命令，`halt -p`，關閉系統

- 從硬碟檔執行 archlinux，

  `qemu -m 8G -rtc base=localtime -hda disk.hd -smp sockets=2,cores=1,threads=1 -vga std `

  注意，啟動環境要依照實際的硬體狀況設置，設置的環境越好，對啟動VM的負擔越大，

  - m 參數，指定記憶體大小
  - rtc 參數，設定時間
  - hda 參數，配置硬碟檔的位置
  - vga 參數，配置標準VGA螢幕，用於在桌面系統調整解析度用
  - smp 參數，設置 設置CPU/核心/線程數量
    - 語法，`-smp n,cores=值,threads=值,sockets=值`
    
    - n參數，選用，host模擬cpu(vcpu)的線程數量，host的每一個thread都視為是一個vcpu
    - sockets參數，選用，CPU插槽的數目，實際的CPU個數
    - cores參數，選用，每個CPU插槽擁有的核心數目
    - threads參數，選用，每個核心擁有的線程數目
    - maxcpus參數，選用，設置可熱插拔的CPU數量，用來限制 n 的值，n的值不能超過 maxcpus
    
    - 總核心數量 = cores * sockets
    - 總線程數量 = 總核心數量 * threads

## qemu 的缺點
- Qemu 在Shell模式下速度快，在桌面模式的速度慢，

    在 Windows 下並沒有獲得更高的指令權限權限或沒有管理員權限(User Mode 的 Emulator)，部分能加速虛擬化的指令無法執行，
	
		加速用的 CPU 的虛擬化擴展只能透過 kernel-driver 來驅動，在 window 上的 qemu 並沒有使用 kernel-driver  的權限，
    因此，和 virtualbox 相比，Qemu 在 windows 下虛擬出的 Linux 系統只能達到 virtualbox 的 1/10

- 加速方法
  - 在 Linux 上，透過 KVM 加速使用者模式
  - 在 Windows 上，透過 Intel-HAXM 加速使用者模式，但有使用限制

    必須能夠使用 VT-X 的功能，才能使用 Intel HAXM 來加速 windows 上的 qemu 虛擬機，
    https://www.qemu.org/2017/11/22/haxm-usage-windows/
    
  - Linux的KVM 和 Windows的HAXM 都提供了Kernel層的支援，使得 QEMU 在使用者層級模擬有很大的改善

# 其他啟動範例
- 範例
  ```
  qemu-system-x86_64 -accel whpx -hda 123.qcow2 -m 512 -net nic,model=virtio -net user -cdrom 456.iso -vga std -boot strict=on
  ```

- 範例
  ```
  qemu-system-x86_64.exe -accel whpx -drive file=123.img,index=0,media=disk,format=raw -cdrom 456.iso -m 4G -L Bios -usbdevice mouse -usbdevice keyboard -boot menu=on -rtc base=localtime,clock=host -parallel none -serial none -name ubuntu-20 -no-acpi -no-hpet -no-reboot -soundhw all -L "C:\Program Files\qemu"
  ```

- 範例
  ```
  qemu-system-x86_64 -enable-kvm -name ubuntutest  -m 2048 -hda ubuntutest1.qcow2 -vnc :19 -net nic,model=virtio -net tap,ifname=tap0,script=no,downscript=no -monitor stdio
  ```

- -accel參數，有使用KVM或HAXM時，此參數才有效
- -vga std參數，使桌面系統可以調整解析度
  

## 在 qemu 中設置 usb
  - https://unix.stackexchange.com/questions/452934/can-i-pass-through-a-usb-port-via-qemu-command-line
  - https://www.youtube.com/watch?v=OIV8n_KWXWQ
  - https://graspingtech.com/ubuntu-desktop-18.04-virtual-machine-macos-qemu/

## Ref
- [How to install Arch Linux on QEMU (for noob)](https://www.youtube.com/watch?v=OIV8n_KWXWQ)
- [Qtemu 的使用](https://howto88.com/zh-hant/%E5%A6%82%E4%BD%95%E5%9C%A8-windows-10-%E4%B8%8A%E4%BD%BF%E7%94%A8-qemu-gui-%E8%A8%AD%E7%BD%AE%E8%99%9B%E6%93%AC%E6%A9%9F)