
## IceStorm-toolchain 開源工具總覽
- [from](https://github.com/SebastianBoe/turborav)
  
  <img src="doc/overview/toolchain-overview.png" width=500 height=auto>

- [yosys](yosys.md)，合成工具，將設計的邏輯電路，合成實際的硬體電路

- arachne-pnr/next-pnr，佈局/佈線工具

- [iceStorm-toolchain](IceStorm.md)，燒錄和分析相關
  - icepack，產生 bin 檔
  - iceprog，燒錄 bin 檔
  - icesprog，燒錄 bin 檔，專門用於 iceSugar 系列
  - icetime，timing analysis
  - 其他套件提供的功能，[詳見](https://clifford.at/icestorm#)

- 模擬
  - ModelSim，Verilog 的編譯+仿真
  - Veritak Verilog 的編譯+仿真
  - Verilator，Verilog/SystemVerilog 的編譯+仿真
    - 以 verilog 編寫硬體電路，但 `testbench 是以 cpp 編寫`
    - 優缺，編譯速度慢，執行速度快

  - GHDL，VHDL 的編譯+仿真

  - [IcarusVerilog](IcarusVerilog.md)，
    - Verilog 的編譯器
    - 沒有 RTL view，需要搭配 gtkwave 使用

- 檢視 VCD 波形
  - gtkwave，檢視仿真結果(*.vcd)

- risc-gcc，將高階語言的代碼編譯為機械碼，當FPGA實作RISCV CPU 時才需要
  
## 安裝 IceStorm-toolchain
- on Windows，推薦使用 Oss-Cad-Suite 工具包
  
  https://github.com/YosysHQ/oss-cad-suite-build

- on ArchLinux
  - 選擇1，從 AUR 安裝 icestorm-git、arachne-pnr-git、yosys-git

  - 選擇2，手動編譯套件 (已放在 docker-hub)
    - [總覽](../docker/docker.md)
    - spinalhdl-bootcamp
    - icestorm
    - arachnepnr / nextpnr
    - yosys
    - riscv-toolchain
    - scala (for spinalhdl)

- 其他平台，詳見[官網](https://clifford.at/icestorm)


  
## 建立開發環境
- [建立 SpinalHDL + iceStrom-toolchain 開發環境 和 使用範例](IceStorm.md)
- [在vscode調用官方軟體+開源軟體](../HDL/dev-template-quartus/readme.md)

## vscode 推薦套件
- [Verilog-HDL/SystemVerilog/Bluespec SystemVerilog support for VS Code](https://marketplace.visualstudio.com/items?itemName=mshr-h.VerilogHDL)
  - 功能: 語法高亮、語法錯誤檢查(linter)、自動跳轉(ctags)
  
  - 注意，以下執行檔需要`添加到環境變數`中，並確保在 cmd 中`可以執行`，重啟vscode，再到插件頁面進行設置相關參數
   
  - 外部調用插件，自動跳轉，Universal Ctags 的 ctags 執行檔
    - 注意，ctags 支援多文件跳轉，但插件僅支援當前打開的 *.v 檔，[不支援多文件的跳轉](https://github.com/mshr-h/vscode-verilog-hdl-support/issues/66)
    - 注意，路徑不能用中文
    - 下載 [ctags 執行檔](https://github.com/universal-ctags/ctags-win32)
  
  - 外部調用插件，語法偵錯，vivado IDE 的 xvlog 執行檔
    - 下載 [xvlog 執行檔](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/)
  
  - 外部調用插件，語法偵錯，IcarusVerilog 的 iverilog 執行檔
    - 下載 [iverilog 執行檔](IcarusVerilog.md)
    - iverilog 支援多文件，但插件僅支援單文件編譯，抑制 iverilog [實例化的錯誤](https://zhuanlan.zhihu.com/p/338497672)
  
  - 外部調用插件，語法偵錯，ModelSim 的 modelsim 執行檔
    - 下載 [modelsim 執行檔](modelsim.md)

- [Verilog Snippet](https://marketplace.visualstudio.com/items?itemName=czh.czh-verilog-snippet)
  - 功能: 比上述插件功能更加齊全的 auto-complete

- [Verilog_Testbench](https://marketplace.visualstudio.com/items?itemName=Truecrab.verilog-testbench-instance)
  - 功能: testbench模板

- [WaveTrace](https://marketplace.visualstudio.com/items?itemName=wavetrace.wavetrace)
  - 功能: 檢視模擬波形
  - 8個訊號以內免費，超過需要收費

- [verilog-formatter](https://marketplace.visualstudio.com/items?itemName=IsaacT.verilog-formatter)
  - 功能: 代碼格式化
  - 需要搭配 [istyle](https://github.com/thomasrussellmurphy/istyle-verilog-formatter) 使用

- [TerosHDL](https://marketplace.visualstudio.com/items?itemName=teros-technology.teroshdl)
  - 功能: 多功能集合

## Ref

- [Verilator，高品質＆開源的 Verilog 模擬器介紹＆教學](https://ys-hayashi.me/2020/12/verilator/)
  